#!/bin/bash
reset

#Simple Neighborhoods
#testes=( __M __S __KS __KERS __TM __RM __LM3 __RS __MWD )

#Combined Neighborhoods
#testes=( __LM3_RM __LM3_TM __RM_TM __LM3_RM_TM __LM3_KERS __LM3_MWD __LM3_RSM )

#Other Combined Neighborhoods
#testes=( __MS __MS_KERS __MS_MWD __MS_RSM )

#All Neighborhoods
#testes=( __M __S __KS __KERS __TM __RM __LM3 __RS __MWD __LM3_RM __LM3_TM __RM_TM __LM3_RM_TM __LM3_KERS __LM3_MWD __LM3_RSM __MS __MS_KERS __MS_MWD __MS_RSM )
testes=( __LM1 __LM2 __LM3 __LM4)

#Algoritmos de Construção
testes=( __C1_LM3, __C2_LM3, __C3_LM3)

#Informações colhidas nos testes:
#Seed
#fo da construção
#tempo da construção
#fo da busca local
#tempo da busca local
#número de iteracoes da busca local
#número de movimentos realizados
#número de movimentos tentados
#número de movimentos tentados de cada tipo
#número total de iterações GRASP
#tempo total


cd src
mkdir -p bin
make
cd ../src

mkdir -p ../results
mkdir -p ../tests
mkdir -p ../tests/summaryperinstance

seeds=( 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 )
instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )

title="$instance"
for teste in ${testes[@]} ; do
	echo "testing $teste..."
	#Cria o diretório onde vão ficar os resultados
	mkdir -p ../tests/$teste

	#Executa as instâncias para todas as seeds
	for instance in ${instances[@]} ; do
			#echo "Instancia $instance"
			instancia=../instances/$instance.ctt

			for seed in ${seeds[@]} ; do
				#echo "Seed $seed"
				resultado=../results/$teste.$instance.$seed.res
				saida=../tests/$teste/$instance-$seed.dat

				#Testing Contruction Algoritms
				if [ $teste = __C1_LM3 ]; then #Wallace
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=10 const=0 timeout=220 grafico=1 > $saida
				elif [ $teste = __C2_LM3 ]; then #GulosaLuHao
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=10 const=1 timeout=220 grafico=1 > $saida
				elif [ $teste = __C3_LM3 ]; then #GulosaComAleatoriedade
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=10 const=2 timeout=220 grafico=1 > $saida

				#Simple Neighborhood tests
				elif [ $teste = __M ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=1 const=1 grafico=1 > $saida
				elif [ $teste = __S ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=2 const=1 grafico=1 > $saida
				elif [ $teste = __KS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=3 const=1 grafico=1 > $saida
				elif [ $teste = __KERS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=15 const=1 grafico=1 > $saida
				elif [ $teste = __TM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=4 const=1 grafico=1 > $saida
				elif [ $teste = __RM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=5 const=1 grafico=1 > $saida
				elif [ $teste = __LM1 ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=6 const=1 grafico=1 > $saida
				elif [ $teste = __LM2 ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=17 const=1 grafico=1 > $saida
				elif [ $teste = __LM3 ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=10 const=1 grafico=1 > $saida
				elif [ $teste = __LM4 ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=11 const=1 grafico=1 > $saida
				elif [ $teste = __RS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=7 const=1 grafico=1 > $saida
				elif [ $teste = __MWD ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=8 const=1 grafico=1 > $saida

				#Combined Neighborhood Tests
				elif [ $teste = __MS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=0 const=1 grafico=1 > $saida
				elif [ $teste = __MS_KERS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=50 const=1 grafico=1 > $saida
				elif [ $teste = __MS_MWD ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=51 const=1 grafico=1 > $saida
				elif [ $teste = __MS_RSM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=52 const=1 grafico=1 > $saida
				elif [ $teste = __LM3_KERS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=53 const=1 grafico=1 > $saida
				elif [ $teste = __LM3_MWD ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=54 const=1 grafico=1 > $saida
				elif [ $teste = __LM3_RSM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=55 const=1 grafico=1 > $saida
				elif [ $teste = __LM3_RM_TM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=59 const=1 grafico=1 > $saida
				elif [ $teste = __LM3_RM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=60 const=1 grafico=1 > $saida
				elif [ $teste = __LM3_TM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=61 const=1 grafico=1 > $saida
				elif [ $teste = __RM_TM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=62 const=1 grafico=1 > $saida

				else
				  echo "Invalid test ($teste)"
				fi

			done
			cat ../tests/$teste/$instance-* > ../tests/summaryperinstance/$teste-$instance.dat
	done
done

echo "done"
