#ifndef uCBCTTH
#define uCBCTTH

#include <stdio.h>
#include "/opt/ibm/ILOG/CPLEX_Studio128/cplex/include/ilcplex/cplex.h"

// Valores limites baseados nas inst�ncias comp
#define MAX_DIS 131  // 131
#define MAX_TUR 150  // 150
#define MAX_PRO 135  // 135
#define MAX_SAL 20   // 20
#define MAX_DIA 6    // 6
#define MAX_PER 9    // 9
#define MAX_RES 1368 // 1368
#define MAX_SPS 150  // subproblemas

//================================ ESTRUTURAS ================================\\

//------------------------------------------------------------------------------
typedef struct tDisciplina
{
 char nome_[50]; // nome da disciplina
 int professor_; // id do professor
 int numPer_;    // n�mero de per�odos de oferta
 int diaMin_;    // dias m�nimos para distribui��o
 int numAlu_;    // n�mero de alunos
}Disciplina;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tTurma
{
 char nome_[50];       // nome da turma
 int numDis_;          // n�mero de disciplinas
 int vetDis_[MAX_DIS]; // vetor com as disciplinas (ids)
}Turma;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tProfessor
{
 char nome_[50]; // nome da turma
}Professor;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tSala
{
 char nome_[50];  // nome da sala
 int capacidade_; // capacidade
}Sala;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tSolucao
{
 // FO (restri��es SOFT)
 int capSal_; // n�mero de alunos que violam a capacidade da sala
 int janHor_; // n�mero de janelas no hor�rio das turmas
 int diaMin_; // n�mero "dias m�nimos" n�o respeitados
 int salDif_; // n�mero de salas diferentes usadas para as disciplinas
 int funObj_; // valor da fun��o objetivo

 // restri��es HARD
 int vioNumAul_; // viola��es na restri��o 1 - n�mero de aulas
 int vioAulSim_; // viola��es na restri��o 2 - aulas simultaneas
 int vioDisSim_; // viola��es na restri��o 3 - disciplinas simultaneas
 int vioProSim_; // viola��es na restri��o 4 - professores simultaneos
 int vioTurSim_; // viola��es na restri��o 5 - turmas simultaneas

 // vetor de solu��o (vari�veis x)
 double vetSol_[MAX_PER*MAX_DIA*MAX_SAL*MAX_DIS];
 
 // matriz de solu��o (per�odo x dia x sala)
 int matSolSal_[MAX_PER][MAX_DIA][MAX_SAL];

 // matriz de solu��o (per�odo x dia x turma)
 int matSolTur_[MAX_PER][MAX_DIA][MAX_TUR];
}Solucao;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tCpx
{
 CPXLPptr lp_;   // problema no CPLEX
 int numVar_;    // n�mero de vari�veis do problema
 int numRes_;    // n�mero de restri��es do problema
 double limSup_; // valor do limitante superior (solu��o do problema)
 double limInf_; // valor do limitante inferior (melhor n�)
 double gap_;    // valor do gap
 double tempo_;  // tempo de execu��o
}Cpx;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tVarReal
{
 int id_;      // id da vari�vel (sequencial de acordo com o modelo original)
 int sp_;      // id do subproblema no qual a vari�vel ir� ficar
 int dis_;     // id da disciplina
 int per_;     // id do per�odo
 int sal_;     // id do sala
 double coef_; // coeficiente (original) da vari�vel na FO
}VarReal;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tVarCopia
{
 int idRea_; // id da vari�vel "real" (sequencial de acordo com o modelo original)
 int idCop_; // id da vari�vel "c�pia" (sequencial, iniciando ap�s as vari�veis reais do modelo original)
 int spCop_; // subproblema da vari�vel "c�pia"
 int spRea_; // subproblema da vari�vel "real"
}VarCopia;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tMetRel
{
 int numSps_;     // n�mero de subproblemas
 int fatDes_;     // fator de desbalanceamento - usado pela METIS na parti��o do "grafo"
 int numCtsM_;    // n�mero de arestas cortadas (valor retornado pela METIS ou pelo CPLEX)
 int numCts_;     // n�mero de arestas cortadas (valor calculado - apenas para confer�ncia)
 int pesCts_;     // peso das arestas cortadas (valor calculado - apenas para confer�ncia)
 double limSup_;  // valor do limitante superior (solu��o real - vi�vel)
 double limInf1_; // valor do limitante inferior calculado com as solu��es (limitantes superiores) dos subproblemas (solu��o relaxada)
 double limInf2_; // valor do limitante inferior calculado com os limitantes inferiores dos subproblemas (solu��o relaxada)
 // se os subproblemas forem resolvidos completamente no tempo limite, limInf1_ = limInf2_
 double gap_;     // valor do gap
 double temPrt_;  // tempo de particionamento do problema
 double temRes_;  // tempo para definir as restri��es relaxadas e vari�veis "c�pias"
 double temSub_;  // tempo para montar o(s) subproblema(s)
 double temExe_;  // tempo de execu��o da relaxa��o (resolver os subproblemas) ou do algoritmo de subgradientes
 double temTot_;  // tempo total de execu��o
 int vetSpDis_[MAX_DIS]; // vetor com o id do subproblema em que cada disciplina ficou
 int vetSpTur_[MAX_TUR]; // vetor com o id do subproblema em que cada turma ficou
 int numVar_;       // n�mero de vari�veis "reais" do problema
 int numCop_;       // n�mero de vari�veis "c�pias" geradas
 int numResProRel_; // n�mero de restri��es de professor relaxadas 
 int numResSalRel_; // n�mero de restri��es de sala relaxadas 
 int *vetCntVar_;      // vetor com a quantidade de vari�veis "reais" de cada subproblema
 int *vetCntCop_;      // vetor com a quantidade de vari�veis "c�pias" de cada subproblema
 VarReal *vetVarRea_;  // vetor com as vari�veis "reais" 
 VarCopia *vetVarCop_; // vetor com as vari�veis "c�pias"
 int matIdVar_[MAX_DIS][MAX_PER*MAX_DIA][MAX_SAL]; // id (posi��o sequencial) de cada vari�vel real (n�o inclui as c�pias)
 int *vetIniSp_;     // vetor com a posi��o da primeira vari�vel de cada subproblema
 int *vetFinSp_;     // vetor com a posi��o da �ltima vari�vel de cada subproblema
 int *vetIdVar_;     // vetor com o id de cada vari�vel (real e c�pia), separado por subproblema e limitado pelos dois vetores acima
 double *vetCoeVar_; // vetor com os "novos" (calculados) coeficientes das vari�veis 
 double *vetML_;     // vetor com os multiplicadores de Lagrange
 double *vetRes_;    // vetor auxiliar para armazenar o vetor de solu��o obtido do CPLEX
 double *vetSolRel_; // vetor com a solu��o relaxada do problema
 double *vetSol_;    // vetor com a solu��o real (vi�vel) do problema   
 int *vetIndFix_;    // vetor auxiliar com os �ndices "fixos" (0 a numVar + numCop)
 unsigned int *vetCntVarRes_;  // vetor com a quantidade de vari�veis em cada restri��o relaxada 
 unsigned int **matVarResRel_; // matriz com o id (sequencial) de cada vari�vel real presente em cada restri��o relaxada 
}MetRel;
//---------------------------------------------------------------------------

//==============================================================================


//============================= VARI�VEIS GLOBAIS ==============================
// ------------ Dados de entrada
char nomInst__[150]; // nome da inst�ncia
int numDis__;        // n�mero de disciplinas 
int numTur__;        // n�mero de turmas
int numPro__;        // n�mero de professores
int numSal__;        // n�mero de salas
int numDia__;        // n�mero de dias
int numPerDia__;     // n�mero de per�odos por dia
int numPerTot__;     // n�mero de per�odos total
int numRes__;        // n�mero de restri��es
bool matResDisPer__[MAX_DIS][MAX_DIA*MAX_PER]; // matriz que indica as restri��es de oferta (disciplina proibidas nos per�odos)
Disciplina vetDisciplinas__[MAX_DIS];
Turma vetTurmas__[MAX_TUR];
Professor vetProfessores__[MAX_PRO];
Sala vetSalas__[MAX_SAL];
// ------------ CPLEX
CPXENVptr env__;              // ambiente do CPLEX
static Cpx vetCpx__[MAX_SPS]; // vetor de problemas
static MetRel mr__;           // m�todos de relaxa��o
// ------------ Auxiliares
bool matDisTur__[MAX_DIS][MAX_TUR]; // matriz que indica se uma turma possui uma disciplina
int numSalDif__;                    // n�mero de "tamanhos" diferentes de sala  
int vetTamSal__[MAX_SAL];           // vetor com os tamanhos diferentes das salas
int vetQtdVar__[8];  // vetor com a quantidade de vari�veis nos modelos
                     // aulas - aulas isoladas (AI) - dias m�nimo (DM) - salas (S)
                     // aux DM - aux AI - aux S - total
int vetQtdRes__[11]; // vetor com a quantidade de restri��es nos modelos
                     // n. aulas - mesma sala - mesmo per�odo - mesmo professor -
                     // mesma turma - rest. oferta - dias m�nimo -
                     // aulas isoladas - salas por disc. - cap. salas - total
//==============================================================================

//===================================== M�TODOS ================================
void configuraParametros(int piNParam, char **piParams);
// ------------ M�todos de relaxa��o
void exeSG(Solucao &s,const int &sps,const int &fat,const int &pes,char *id);
void defResRelCop();      
void mntSubProbDL(); 
void escResMR(FILE *f);
void exeMetRel(const int &sps,const int &fat,const int &pes);
void exeTesPar(const int &sps,const int &fat,const int &pes);
void parGraTur(const int &sps,const int &fat,const int &pes);
void parGraDis(const int &sps,const int &fat,const int &pes);
void parGraTurCpx(const int &sps,const int &fat,const int &pes);
void parGraDisCpx(const int &sps,const int &fat,const int &pes);
void monSpsTurLacLub12(const int &adp);
void monSpsDisLacLub12(const int &adp);
void monSpsTurNovo();
void monSpsDisNovo();
// ------------ CPLEX
void exeCntVarRes(char *id);
void exeCpx(char *id,Solucao &s,const int &rl);
void exeF2(char *id,Solucao &s);
void escResCpx(FILE *f,const int &rl);
void cpxToSol(Solucao &s);
void monModBurEal10(char *arq,const int &rl);
void monModLacLub12_F1(char *arq,const int &rl,const int &adp);
void monModLacLub12_F2(char *arq,bool ***matDisPerSal);
void monModNovo(char *arq,const int &rl);
// ------------ Solu��o
void escSolucao(Solucao &s,FILE *f);
// ------------ Auxiliares
void lerInstancia(char *arq);
void testarEntrada();
void ordVetCapSal();
//==============================================================================

#endif
