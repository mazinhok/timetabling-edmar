#!/bin/bash

reset
make

#numSP=( 10 9 8 7 6 5 4 3 2 )
#instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )

#	for nsp in ${numSP[@]} ; do
#	   echo "Inicio $nsp"
#           for instance in ${instances[@]} ; do
#	       ./multiStart $instance numSubProb=$nsp tGrafo=1  >> $instance.csv
#	   done
#           rm sp*.lp
#	   echo "... fim $nsp"
#	done

./uCBCTT comp01 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp011.txt
./uCBCTT comp02 model=2 metodo=5 sps=3 timeout=0 saida=resumo-fd1.csv  >> comp021.txt
./uCBCTT comp03 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp031.txt
./uCBCTT comp04 model=2 metodo=5 sps=5 timeout=0 saida=resumo-fd1.csv  >> comp041.txt
./uCBCTT comp05 model=2 metodo=5 sps=3 timeout=0 saida=resumo-fd1.csv  >> comp051.txt
./uCBCTT comp06 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp061.txt
./uCBCTT comp07 model=2 metodo=5 sps=4 timeout=0 saida=resumo-fd1.csv  >> comp071.txt
./uCBCTT comp08 model=2 metodo=5 sps=5 timeout=0 saida=resumo-fd1.csv  >> comp081.txt
./uCBCTT comp09 model=2 metodo=5 sps=3 timeout=0 saida=resumo-fd1.csv  >> comp091.txt
./uCBCTT comp10 model=2 metodo=5 sps=4 timeout=0 saida=resumo-fd1.csv  >> comp101.txt
./uCBCTT comp11 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp111.txt
./uCBCTT comp12 model=2 metodo=5 sps=5 timeout=0 saida=resumo-fd1.csv  >> comp121.txt
./uCBCTT comp13 model=2 metodo=5 sps=7 timeout=0 saida=resumo-fd1.csv  >> comp131.txt
./uCBCTT comp14 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp141.txt
./uCBCTT comp15 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp151.txt
./uCBCTT comp16 model=2 metodo=5 sps=6 timeout=0 saida=resumo-fd1.csv  >> comp161.txt
./uCBCTT comp17 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp171.txt
./uCBCTT comp18 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp181.txt
./uCBCTT comp19 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp191.txt
./uCBCTT comp20 model=2 metodo=5 sps=3 timeout=0 saida=resumo-fd1.csv  >> comp201.txt
./uCBCTT comp21 model=2 metodo=5 sps=2 timeout=0 saida=resumo-fd1.csv  >> comp211.txt
rm sp*.lp
./uCBCTT comp01 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp01-1.txt
./uCBCTT comp02 model=2 metodo=5 sps=3 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp02-1.txt
./uCBCTT comp03 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp03-1.txt
./uCBCTT comp04 model=2 metodo=5 sps=5 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp04-1.txt
./uCBCTT comp05 model=2 metodo=5 sps=3 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp05-1.txt
./uCBCTT comp06 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp06-1.txt
./uCBCTT comp07 model=2 metodo=5 sps=4 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp07-1.txt
./uCBCTT comp08 model=2 metodo=5 sps=5 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp08-1.txt
./uCBCTT comp09 model=2 metodo=5 sps=3 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp09-1.txt
./uCBCTT comp10 model=2 metodo=5 sps=4 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp10-1.txt
./uCBCTT comp11 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp11-1.txt
./uCBCTT comp12 model=2 metodo=5 sps=5 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp12-1.txt
./uCBCTT comp13 model=2 metodo=5 sps=7 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp13-1.txt
./uCBCTT comp14 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp14-1.txt
./uCBCTT comp15 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp15-1.txt
./uCBCTT comp16 model=2 metodo=5 sps=6 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp16-1.txt
./uCBCTT comp17 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp17-1.txt
./uCBCTT comp18 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp18-1.txt
./uCBCTT comp19 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp19-1.txt
./uCBCTT comp20 model=2 metodo=5 sps=3 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp20-1.txt
./uCBCTT comp21 model=2 metodo=5 sps=2 fd=-1 timeout=0 saida=resumo-fd-1.csv  >> comp21-1.txt
rm sp*.lp
./uCBCTT comp01 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp01-2.txt
./uCBCTT comp02 model=2 metodo=5 sps=3 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp02-2.txt              
./uCBCTT comp03 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp03-2.txt              
./uCBCTT comp04 model=2 metodo=5 sps=5 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp04-2.txt              
./uCBCTT comp05 model=2 metodo=5 sps=3 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp05-2.txt              
./uCBCTT comp06 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp06-2.txt              
./uCBCTT comp07 model=2 metodo=5 sps=4 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp07-2.txt              
./uCBCTT comp08 model=2 metodo=5 sps=5 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp08-2.txt              
./uCBCTT comp09 model=2 metodo=5 sps=3 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp09-2.txt              
./uCBCTT comp10 model=2 metodo=5 sps=4 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp10-2.txt              
./uCBCTT comp11 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp11-2.txt              
./uCBCTT comp12 model=2 metodo=5 sps=5 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp12-2.txt              
./uCBCTT comp13 model=2 metodo=5 sps=7 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp13-2.txt              
./uCBCTT comp14 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp14-2.txt              
./uCBCTT comp15 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp15-2.txt              
./uCBCTT comp16 model=2 metodo=5 sps=6 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp16-2.txt              
./uCBCTT comp17 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp17-2.txt
./uCBCTT comp18 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp18-2.txt
./uCBCTT comp19 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp19-2.txt
./uCBCTT comp20 model=2 metodo=5 sps=3 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp20-2.txt
./uCBCTT comp21 model=2 metodo=5 sps=2 fd=-2 timeout=0 saida=resumo-fd-2.csv  >> comp21-2.txt
rm sp*.lp