#include "uCBCTT.h"

#include <string.h>
#include <time.h>
#include <math.h>
#include "/labotim-local/Documentos/timetabling-edmar/metis-5.0.1-X64/include/metis.h"

#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))

// Usar para exibir informa��es na tela
#define DBG

//============================== DADOS DE ENTRADA ==============================
char INST[50] = "toy";    // inst�ncia   
//-------------------------------------------------------------
int MODELO = 8;           // -1 --> Lach e Lubbecke (2012) - duas fases        -2 --> Lach e Lubbecke (2012) alterado - duas fases
                          // 0 --> Burke et al. (2010)
                          // 1 --> Lach e Lubbecke (2012)          2 --> Lach e Lubbecke (2012) - alterado
                          // 3 --> Novo 1       4 --> Novo 2       5 --> Novo 3
                          // 6 --> Novo 4       7 --> Novo 5       8 --> Novo 6
//-------------------------------------------------------------
int METODO = 6;           // -1 --> verificar as dimens�es do modelo
                          // 0 --> Rel. Linear - Cplex
                          // 1 --> Cplex
                          // 2 --> Teste de particionamento por TURMA
                          // 3 --> Teste de particionamento por DISCIPLINA
                          // 4 --> Relaxa��o de particionamento por TURMA
                          // 5 --> Relaxa��o de particionamento por DISCIPLINA
                          // 6 --> DecLag por TURMA
//-------------------------------------------------------------
int OPT_CPX = 0;          // usar (1) ou n�o (0) a "otimiza��o" do Cplex
int SPS = 2;              // n�mero de subproblemas
int FD = 1;               // fator de desbalanceamento no particionamento do grafo (-1 = CPLEX; -2 = CPLEX com fator 1.2; Outros valores = METIS)
int PES = 1;              // usar (1) ou n�o (0) o peso das arestas no particionamento do grafo
char SAIDA[50] = "N";     // arquivo "geral" com as informa��es b�sicas das solu��es obtidas; usar "N" para ignorar 
double TEM_LIM = 60;      // tempo limite de execu��o (em segundos); usar 0 para tempo ilimitado
int PESOS[4] = {1,2,5,1}; // pesos da FO - definidos na literatura
//==============================================================================


//================================== PRINCIPAL ================================= 

//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
 int sts;
 FILE *arq;
 char aux[50];
 Solucao sol;
 strcpy(INST,argv[1]);
 strcpy(aux,"../../instances/");
 strcat(aux,argv[1]);
 strcat(aux,".ctt");
 
 configuraParametros(argc, argv);
 lerInstancia(aux); 
 //testarEntrada();
 env__ = CPXopenCPLEX(&sts);
 #ifdef DBG
   CPXsetintparam(env__,CPX_PARAM_SCRIND,CPX_ON);
 #else
   CPXsetintparam(env__,CPX_PARAM_SCRIND,CPX_OFF);
 #endif
 CPXsetdblparam(env__,CPX_PARAM_EPGAP,0.000000000000001);
 if(TEM_LIM != 0)
   CPXsetdblparam(env__,CPX_PARAM_TILIM,TEM_LIM);
 strcpy(aux,INST);
 if(MODELO == 0)
   strcat(aux,"-B");
 else if((MODELO == 1) || (MODELO == -1))
   strcat(aux,"-L");
 else if((MODELO == 2) || (MODELO == -2))
   strcat(aux,"-L2");
 else if(MODELO == 3)
   strcat(aux,"-N1");
 else if(MODELO == 4)
   strcat(aux,"-N2");
 else if(MODELO == 5)
   strcat(aux,"-N3");
 else if(MODELO == 6)
   strcat(aux,"-N4");
 else if(MODELO == 7)
   strcat(aux,"-N5");
 else
   strcat(aux,"-N6");
 if(PESOS[3] == 0)
   strcat(aux,"-p0");
 if(OPT_CPX == 1)
   strcat(aux,"-opt");
 if(METODO == -1)
   exeCntVarRes(aux);
 else if(METODO == 0)
   exeCpx(aux,sol,1);
 else if(METODO == 1)
   exeCpx(aux,sol,0);
 else if((METODO == 2) || (METODO == 3))
   exeTesPar(SPS,FD,PES);
 else if((METODO == 4) || (METODO == 5))
   exeMetRel(SPS,FD,PES);
 else if(METODO == 6)
   exeSG(sol,SPS,FD,PES,aux);
   
 if((strcmp(SAIDA,"N") != 0) && (METODO != -1) && (METODO != 2) && (METODO != 3))
  {
   strcpy(aux,"Saidas/");
   strcat(aux,SAIDA);
   arq = fopen(aux,"at");
   if(METODO == 0)
     fprintf(arq,"%s\t%d\t%d\t%d\t%d\t%.2f\t%.2f\n",INST,MODELO,PESOS[3],vetCpx__[0].numVar_,vetCpx__[0].numRes_,
             vetCpx__[0].limInf_,vetCpx__[0].tempo_);
   else if(METODO == 1) 
    {
     fprintf(arq,"%s\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%.2f\t%.2f",INST,MODELO,PESOS[3],OPT_CPX,vetCpx__[0].numVar_,vetCpx__[0].numRes_,
             vetCpx__[0].limSup_,vetCpx__[0].limInf_,vetCpx__[0].gap_,vetCpx__[0].tempo_);
     if(MODELO < 0)
      {
       fprintf(arq,"\t%d\t%d\t%.2f\t%.2f\t%.2f\t%.2f",vetCpx__[1].numVar_,vetCpx__[1].numRes_,vetCpx__[1].limSup_,
               vetCpx__[1].limInf_,vetCpx__[1].gap_,vetCpx__[1].tempo_);
       fprintf(arq,"\t%d\t%d\t%.2f\t%.2f\t%.2f\t%.2f\n",vetCpx__[0].numVar_+vetCpx__[1].numVar_,vetCpx__[0].numRes_+vetCpx__[1].numRes_,
               vetCpx__[0].limSup_+vetCpx__[1].limSup_,vetCpx__[0].limInf_,
               ((((vetCpx__[0].limSup_+vetCpx__[1].limSup_)-vetCpx__[0].limInf_)/(vetCpx__[0].limSup_+vetCpx__[1].limSup_))*100),
               vetCpx__[0].tempo_+vetCpx__[1].tempo_);
      }
     else
       fprintf(arq,"\n");
    }
   else if(METODO == 4)
     fprintf(arq,"%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n",INST,MODELO,PESOS[3],OPT_CPX,SPS,FD,PES,
             numTur__,mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,mr__.limInf1_,mr__.limInf2_,mr__.temPrt_,mr__.temSub_,mr__.temExe_,mr__.temTot_);
   else if(METODO == 5)
     fprintf(arq,"%s;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;\n",INST,MODELO,PESOS[3],OPT_CPX,SPS,FD,PES,
             numDis__,mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,mr__.limInf1_,mr__.limInf2_,mr__.temPrt_,mr__.temSub_,mr__.temExe_,mr__.temTot_);
 
//   else if(METODO == 6)
//     fprintf(arq,"%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n",INST,MODELO,PESOS[3],OPT_CPX,SPS,FD,PES,
//             numTur__,mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,mr__.numVar_,mr__.numCop_,mr__.numResNao_,mr__.numResRel_,mr__.numResExt_,
//             mr__.limSup_,mr__.limInf1_,mr__.limInf2_,mr__.temPrt_,mr__.temSub_,mr__.temExe_,mr__.temTot_);
   fclose(arq);
  }
 else if(METODO > 3)
   printf("\nLB1: %.2f\nLB2: %.2f\nTempo: %.2f\n",mr__.limInf1_,mr__.limInf2_,mr__.temTot_);
 CPXcloseCPLEX(&env__);
 if(argc == 1){
   printf("\n\n>>> Pressione ENTER para encerrar: ");
 }
 return 0;
}
//------------------------------------------------------------------------------

//==============================================================================


//=========================== M�TODOS DE RELAXA��O =============================

//------------------------------------------------------------------------------
void exeSG(Solucao &s,const int &sps,const int &fat,const int &pes,char *id)
{
 FILE *f;
 char arq[100];
 time_t hI,hF;
 int step,iter;
 double *vetGrad;
 double alfa,teta,norm,primal,dual,tempo;
 CPXsetintparam(env__,CPX_PARAM_SCRIND,CPX_OFF);
 // -----------------------------------
 // particionar o grafo, definir as restri��es relaxadas e as c�pias (se for o caso) e montar os subproblemas
 parGraTur(sps,fat,pes);
 defResRelCop();
 mntSubProbDL();           
 // -----------------------------------
 // "ler" os subproblemas
 for(int i = 0; i < mr__.numSps_; i++)
  {
   sprintf(arq,"sp%d.lp",i+1);
   vetCpx__[i].lp_ = CPXcreateprob(env__,&iter,"");
   CPXreadcopyprob(env__,vetCpx__[i].lp_,arq,NULL);
  }
 // -----------------------------------
 // preparar as vari�veis auxiliares
 tempo = TEM_LIM - mr__.temPrt_ - mr__.temRes_ - mr__.temSub_; 
 hI = time(NULL);
 vetGrad = new double[mr__.numCop_+mr__.numResProRel_+mr__.numResSalRel_];
 mr__.vetRes_ = new double[mr__.numVar_+mr__.numCop_];
 mr__.vetSolRel_ = new double[mr__.numVar_+mr__.numCop_];
 mr__.vetSol_ = new double[mr__.numVar_+mr__.numCop_];
 // -----------------------------------
 // execucao do algoritmo de sub-gradientes
 #ifdef DBG 
   printf("\n\nIter\tUpBnd\tLoBnd\tPrimal\tDual\tGap\tTempo\n\n");
 #endif
 iter = 0;
 alfa = 2;
 teta = 0.0;
 step = 0;
 mr__.limSup_ = INT_MAX;
 mr__.limInf1_ = -INT_MAX;
 do
  {
   iter++;
   step++;
   //--------------------------
   // resolver os subproblemas e pegar o valor da FO e a solu��o, e aplicar a heur�stica primal
   dual = 0.0;
   for(int i = 0; i < mr__.numSps_; i++)
    {
     CPXmipopt(env__,vetCpx__[i].lp_);
     CPXgetmipobjval(env__,vetCpx__[i].lp_,&vetCpx__[i].limSup_);
     dual += vetCpx__[i].limSup_;
     CPXgetmipx(env__,vetCpx__[i].lp_,mr__.vetRes_,0,mr__.vetCntVar_[i]+mr__.vetCntCop_[i]-1);
     for(int j = mr__.vetIniSp_[i]; j <= mr__.vetFinSp_[i]; j++)
      {
       if(mr__.vetRes_[j-mr__.vetIniSp_[i]] >= 0.5)
         mr__.vetSolRel_[mr__.vetIdVar_[j]] = 1;
       else
         mr__.vetSolRel_[mr__.vetIdVar_[j]] = 0;
       mr__.vetSol_[mr__.vetIdVar_[j]] = mr__.vetSolRel_[mr__.vetIdVar_[j]];
      }
    }
   for(int i = 0; i < mr__.numResProRel_+mr__.numResSalRel_; i++)
     dual -= mr__.vetML_[mr__.numCop_+i];
   dual -= numDis__;
   primal = 35;//heuPrimal();
//   if(primal < metRel__.limSup_)
//     vet2Mat(s);
   //--------------------------
   // atualizar os limitantes, o passo e o alfa
   if(dual > mr__.limInf1_)
     step = 0;
   mr__.limSup_ = MIN(mr__.limSup_,primal);
   mr__.limInf1_ = MAX(mr__.limInf1_,dual);
   if(step == 30)
    {
     alfa /= 2.0;
     step = 0;
    }   
   //--------------------------
   // calcular a norma a atualizar o vetor de gradientes
   norm = 0.0;
   for(int i = 0; i < mr__.numCop_; i++)
    {
     vetGrad[i] = (mr__.vetSolRel_[mr__.vetVarCop_[i].idCop_] - mr__.vetSolRel_[mr__.vetVarCop_[i].idRea_]);
     norm += vetGrad[i] * vetGrad[i];
    }
   for(int i = 0; i < mr__.numResProRel_+mr__.numResSalRel_; i++)
    {
     vetGrad[mr__.numCop_+i] = -1;
     for(int j = 0; j < mr__.vetCntVarRes_[i]; j++)
       vetGrad[mr__.numCop_+i] += mr__.vetSolRel_[mr__.matVarResRel_[i][j]];
     norm += vetGrad[mr__.numCop_+i] * vetGrad[mr__.numCop_+i];
    }
   if(norm > 0.00001)
    {
     //--------------------------
     // atualizar o teta
     teta = alfa * (mr__.limSup_ - mr__.limInf1_) / norm;
     //--------------------------
     // atualizar o vetor de multiplicadores de Lagrange
     for(int i = 0; i < mr__.numCop_; i++)
       mr__.vetML_[i] = mr__.vetML_[i] + teta * vetGrad[i];
     for(int i = 0; i < mr__.numResProRel_+mr__.numResSalRel_; i++)
       mr__.vetML_[mr__.numCop_+i] = MAX(0,mr__.vetML_[mr__.numCop_+i] + teta * vetGrad[mr__.numCop_+i]);
     //--------------------------
     // atualizar os subproblemas
     for(int i = 0; i < mr__.numVar_; i++)
       mr__.vetCoeVar_[i] = mr__.vetVarRea_[i].coef_;
     for(int i = 0; i < mr__.numCop_; i++)
       mr__.vetCoeVar_[mr__.numVar_+i] = 0.0;
     for(int i = 0; i < mr__.numCop_; i++)
      {
       mr__.vetCoeVar_[mr__.vetVarCop_[i].idCop_] += mr__.vetML_[i]; 
       mr__.vetCoeVar_[mr__.vetVarCop_[i].idRea_] -= mr__.vetML_[i]; 
      }
     for(int i = 0; i < mr__.numResProRel_+mr__.numResSalRel_; i++)
       for(int j = 0; j < mr__.vetCntVarRes_[i]; j++)
         mr__.vetCoeVar_[mr__.matVarResRel_[i][j]] += mr__.vetML_[mr__.numCop_+i];


/*
 printf("\n");
 for(int i = 0; i < mr__.numResProRel_ + mr__.numResSalRel_; i++)
  {
   printf("\nR %d: ",i);  
   for(int j = 0; j < mr__.vetCntVarRes_[i]; j++)
//     printf("%d - x_%d_%d_%d  ",mr__.matVarResRel_[i][j],mr__.vetVarRea_[mr__.matVarResRel_[i][j]].dis_,mr__.vetVarRea_[mr__.matVarResRel_[i][j]].per_,mr__.vetVarRea_[mr__.matVarResRel_[i][j]].sal_);
     printf("x_%d_%d_%d  ",mr__.vetVarRea_[mr__.matVarResRel_[i][j]].dis_,mr__.vetVarRea_[mr__.matVarResRel_[i][j]].per_,mr__.vetVarRea_[mr__.matVarResRel_[i][j]].sal_);
  }
 printf("\nRE:\n");
 for(int i = 0; i < mr__.numVar_; i++)
   if(mr__.vetSolRel_[i] > 0)
     printf("%.2f -> x_%d_%d_%d\n",mr__.vetSolRel_[i],mr__.vetVarRea_[i].dis_,mr__.vetVarRea_[i].per_,mr__.vetVarRea_[i].sal_);
 printf("\nCP:\n");
 for(int i = 0; i < mr__.numCop_; i++)
   if(mr__.vetSolRel_[mr__.numVar_+i] > 0)
     printf("%.2f -> x_%d_%d_%d\n",mr__.vetSolRel_[mr__.numVar_+i],mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].dis_,mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].per_,mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].sal_);
  
 printf("\nNORMA: %.2f - GRAD:\n",norm);
 for(int i = 0; i < mr__.numCop_; i++)
   if(vetGrad[i] != 0)
     printf("%d -> %.2f  \n",i,vetGrad[i]);
 printf("\nGRAD 2:\n");
 for(int i = 0; i < mr__.numResProRel_+mr__.numResSalRel_; i++)
   if(vetGrad[mr__.numCop_+i] == 0)
     printf("%d -> %.2f  \n",i,vetGrad[mr__.numCop_+i]);

 printf("\nTETA: %.2f - ML:\n",teta);
 for(int i = 0; i < mr__.numCop_; i++)
   if(mr__.vetML_[i] != 0)
     printf("%d -> %.2f\n",i,mr__.vetML_[i]);
 printf("\nML 2:\n");
 for(int i = 0; i < mr__.numResProRel_+mr__.numResSalRel_; i++)
   if(mr__.vetML_[mr__.numCop_+i] != 0)
     printf("%d -> %.2f\n",i,mr__.vetML_[mr__.numCop_+i]);

 printf("\nCoefR: \n");
 for(int i = 0; i < mr__.numVar_; i++)
   if(mr__.vetCoeVar_[i] != mr__.vetVarRea_[i].coef_)
     printf("%.2f  %.2f -> x_%d_%d_%d\n",mr__.vetVarRea_[i].coef_,mr__.vetCoeVar_[i],mr__.vetVarRea_[i].dis_,mr__.vetVarRea_[i].per_,mr__.vetVarRea_[i].sal_);
 printf("\nCoefC:\n");
 for(int i = 0; i < mr__.numCop_; i++)
   if(mr__.vetCoeVar_[mr__.vetVarCop_[i].idCop_] != 0)
     printf("%.2f -> x_%d_%d_%d\n",mr__.vetCoeVar_[mr__.vetVarCop_[i].idCop_],mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].dis_,mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].per_,mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].sal_);


 _getche();
*/



     for(int i = 0; i < mr__.numSps_; i++)
      {
       for(int j = mr__.vetIniSp_[i]; j <= mr__.vetFinSp_[i]; j++)
         mr__.vetRes_[j-mr__.vetIniSp_[i]] = mr__.vetCoeVar_[mr__.vetIdVar_[j]];
       CPXchgobj(env__,vetCpx__[i].lp_,mr__.vetCntVar_[i]+mr__.vetCntCop_[i],mr__.vetIndFix_,mr__.vetRes_);
      }		
    }
   mr__.gap_ = (fabs(mr__.limSup_-mr__.limInf1_)/mr__.limSup_)*100.0;
   hF = time(NULL);
   mr__.temExe_ = difftime(hF,hI);
   #ifdef DBG 
     printf("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n",iter,mr__.limSup_,mr__.limInf1_,primal,dual,mr__.gap_,mr__.temExe_);
   #endif
  }
 while((mr__.limSup_-mr__.limInf1_ >= 1) && (alfa >= 0.005) && (norm > 0.00001) && (mr__.temExe_ < tempo));
 #ifdef DBG  
   printf("\n\n>>> Norma: %.3f\t\tAlfa: %.3f\n",norm,alfa);
 #endif
 mr__.temTot_ = mr__.temPrt_ + mr__.temRes_ + mr__.temSub_ + mr__.temExe_;   
 // -----------------------------------
 // limpar a mem�ria e gerar arquivo de sa�da
 for(int i = 0; i < mr__.numSps_; i++)
   CPXfreeprob(env__,&vetCpx__[i].lp_);
 delete[] vetGrad;
 delete[] mr__.vetRes_;
 delete[] mr__.vetSolRel_;
 delete[] mr__.vetSol_;
 delete[] mr__.vetIniSp_;
 delete[] mr__.vetFinSp_;
 delete[] mr__.vetIdVar_;
 delete[] mr__.vetCoeVar_; 
 delete[] mr__.vetIndFix_;
 delete[] mr__.vetCntVarRes_;
 for(int i = 0; i < numPro__*numPerTot__+numSal__*numPerTot__; i++)
   delete[] mr__.matVarResRel_[i];
 delete[] mr__.matVarResRel_;
 delete[] mr__.vetCntVar_;
 delete[] mr__.vetCntCop_;
 delete[] mr__.vetVarRea_;
 delete[] mr__.vetVarCop_;
 delete[] mr__.vetML_;
 sprintf(arq,"Saidas\\%s-DL-%d-%d.res",id,mr__.numSps_,mr__.fatDes_);
 f = fopen(arq,"w");
 escResMR(f);
// escSolucao(s,f);
 fclose(f);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void defResRelCop()
{
 time_t h1,h2;
 int aux,flag;
 h1 = time(NULL);
 mr__.vetCntVar_ = new int[mr__.numSps_];
 mr__.vetCntCop_ = new int[mr__.numSps_];
 for(int i = 0; i < mr__.numSps_; i++)
  {
   mr__.vetCntVar_[i] = 0;
   mr__.vetCntCop_[i] = 0;
  }
 // ------------------ 
 // definir em qual subproblema cada disciplina ser� "contada" na FO
 memset(mr__.vetSpDis_,0,sizeof(mr__.vetSpDis_));
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u] && (vetTurmas__[u].numDis_ > aux))
      {
       aux = vetTurmas__[u].numDis_;
       mr__.vetSpDis_[c] = mr__.vetSpTur_[u];
      }
  } 
 //-------------------------
 // calcular o n�mero total, e por subproblema, de vari�veis reais
 mr__.numVar_ = 0;
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       for(int r = 0; r < numSal__; r++)
        {
         mr__.vetCntVar_[mr__.vetSpDis_[c]]++;
         mr__.numVar_++;
        } 
 //-------------------------
 // calcular o n�mero total, e por subproblema, de vari�veis c�pias
 mr__.numCop_ = 0;
 for(int c = 0; c < numDis__; c++)
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u])
       if(mr__.vetSpDis_[c] != mr__.vetSpTur_[u])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
              {
               mr__.vetCntCop_[mr__.vetSpTur_[u]]++;
               mr__.numCop_++;
              } 
 //-------------------------
 // definir as vari�veis reais
 memset(mr__.matIdVar_,-1,sizeof(mr__.matIdVar_));
 mr__.vetVarRea_ = new VarReal[mr__.numVar_];
 aux = 0;
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       for(int r = 0; r < numSal__; r++)
        {
         mr__.matIdVar_[c][p][r] = aux;
         mr__.vetVarRea_[aux].id_ = aux;
         mr__.vetVarRea_[aux].sp_ = mr__.vetSpDis_[c];
         mr__.vetVarRea_[aux].dis_ = c;
         mr__.vetVarRea_[aux].per_ = p;
         mr__.vetVarRea_[aux].sal_ = r;
         mr__.vetVarRea_[aux].coef_ = PESOS[0] * MAX(0,vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_);
         aux++;
        } 
 //-------------------------
 // definir as vari�veis c�pias
 mr__.vetVarCop_ = new VarCopia[mr__.numCop_];
 aux = 0;
 for(int c = 0; c < numDis__; c++)
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u])
       if(mr__.vetSpDis_[c] != mr__.vetSpTur_[u])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
              {
               mr__.vetVarCop_[aux].idCop_ = mr__.numVar_ + aux;
               mr__.vetVarCop_[aux].idRea_ = mr__.matIdVar_[c][p][r];
               mr__.vetVarCop_[aux].spCop_ = mr__.vetSpTur_[u];
               mr__.vetVarCop_[aux].spRea_ = mr__.vetSpDis_[c];
               aux++;
              } 
 //-------------------------
 // definir as restri��es relaxadas
 mr__.vetCntVarRes_ = new unsigned int[numPro__*numPerTot__+numSal__*numPerTot__];
 mr__.matVarResRel_ = new unsigned int*[numPro__*numPerTot__+numSal__*numPerTot__];
 for(int i = 0; i < numPro__*numPerTot__+numSal__*numPerTot__; i++)
   mr__.matVarResRel_[i] = new unsigned int[mr__.numVar_];
 mr__.numResProRel_ = 0;
 for(int t = 0; t < numPro__; t++)
  {
   for(int c = 0; c < numDis__; c++)
     if(vetDisciplinas__[c].professor_ == t)
      {
       aux = mr__.vetSpDis_[c];
       break;
      }
   flag = 0;
   for(int c = 0; c < numDis__; c++)
     if((vetDisciplinas__[c].professor_ == t) && (mr__.vetSpDis_[c] != aux))
      { 
       flag = 1;
       break;
      }
   if(flag)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       mr__.vetCntVarRes_[mr__.numResProRel_] = 0;
       for(int c = 0; c < numDis__; c++)
         if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
           for(int r = 0; r < numSal__; r++)
            {
             mr__.matVarResRel_[mr__.numResProRel_][mr__.vetCntVarRes_[mr__.numResProRel_]] = mr__.matIdVar_[c][p][r];
             mr__.vetCntVarRes_[mr__.numResProRel_]++;
            } 
       mr__.numResProRel_++;
      }
    }
  }    
 mr__.numResSalRel_ = 0;
 for(int r = 0; r < numSal__; r++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     mr__.vetCntVarRes_[mr__.numResProRel_+mr__.numResSalRel_] = 0;
     for(int c = 0; c < numDis__; c++)
       if(!matResDisPer__[c][p])
        {
         mr__.matVarResRel_[mr__.numResProRel_+mr__.numResSalRel_][mr__.vetCntVarRes_[mr__.numResProRel_+mr__.numResSalRel_]] = mr__.matIdVar_[c][p][r];
         mr__.vetCntVarRes_[mr__.numResProRel_+mr__.numResSalRel_]++;
        } 
     mr__.numResSalRel_++;
    } 
  }
 //-------------------------
 // alocar mem�ria e inicializar vari�veis "auxiliares"
 mr__.vetIdVar_ = new int[mr__.numVar_+mr__.numCop_];
 mr__.vetCoeVar_ = new double[mr__.numVar_+mr__.numCop_]; 
 mr__.vetIndFix_ = new int[mr__.numVar_+mr__.numCop_];
 for(int i = 0; i < mr__.numVar_+mr__.numCop_; i++)
   mr__.vetIndFix_[i] = i;
 mr__.vetIniSp_ = new int[mr__.numSps_];
 mr__.vetFinSp_ = new int[mr__.numSps_];
 //-------------------------
 // definir a posi��o inicial e final das vari�veis X de cada subproblema
 mr__.vetIniSp_[0] = 0;
 mr__.vetFinSp_[0] = mr__.vetCntVar_[0] + mr__.vetCntCop_[0] - 1;
 for(int i = 1; i < mr__.numSps_; i++)
  {
   mr__.vetIniSp_[i] = mr__.vetFinSp_[i-1] + 1;
   mr__.vetFinSp_[i] = mr__.vetIniSp_[i] + mr__.vetCntVar_[i] + mr__.vetCntCop_[i] - 1;
  }
 //-------------------------
 // organizar as vari�veis X (reais e c�pias) por subproblema (de forma sequencial)
 aux = 0;
 for(int i = 0; i < mr__.numSps_; i++)
  {
   for(int v = 0; v < mr__.numVar_; v++)
    {
     if(mr__.vetVarRea_[v].sp_ == i)
      {
       mr__.vetIdVar_[aux] = mr__.vetVarRea_[v].id_;
       aux++;
      }
    }
   for(int v = 0; v < mr__.numCop_; v++)
    {
     if(mr__.vetVarCop_[v].spCop_ == i)
      {
       mr__.vetIdVar_[aux] = mr__.vetVarCop_[v].idCop_;
       aux++;
      }
    }
  }

/*
 printf("SP TUR: ");
 for(int u = 0; u < numTur__; u++)
   printf("%d  ",mr__.vetSpTur_[u]);
 printf("\nSP DIS: ");
 for(int c = 0; c < numDis__; c++)
   printf("%d  ",mr__.vetSpDis_[c]);
 printf("\nNV: %d\tNC: %d\n",mr__.numVar_,mr__.numCop_);
 printf("\n");
 for(int s = 0; s < mr__.numSps_; s++)
   printf("SP: %d\tVAR: %d\tCOP: %d\n",s,mr__.vetCntVar_[s],mr__.vetCntCop_[s]);
 
 for(int i = 0; i < mr__.numVar_; i++)
   printf("V: %d - SP: %d - %.2f - x_%d_%d_%d\n",mr__.vetVarRea_[i].id_,mr__.vetVarRea_[i].sp_,mr__.vetVarRea_[i].coef_,mr__.vetVarRea_[i].dis_,mr__.vetVarRea_[i].per_,mr__.vetVarRea_[i].sal_);
 printf("\n");
 for(int i = 0; i < mr__.numCop_; i++)
  {
   printf("C: %d - R: %d - SPC: %d - SPR: %d\t",mr__.vetVarCop_[i].idCop_,mr__.vetVarCop_[i].idRea_,mr__.vetVarCop_[i].spCop_,mr__.vetVarCop_[i].spRea_);
   printf("x_%d_%d_%d\n",mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].dis_,mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].per_,mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].sal_);
  }
 printf("\nNRP: %d\tNRS: %d\n",mr__.numResProRel_,mr__.numResSalRel_);
 printf("\n");
 for(int s = 0; s < mr__.numSps_; s++)
   printf("SP: %d\tINI: %d\tFIN: %d\n",s,mr__.vetIniSp_[s],mr__.vetFinSp_[s]);

 printf("\nVetId:\n");
 for(int i = 0; i < mr__.numVar_+mr__.numCop_; i++)
   printf("%d\n",mr__.vetIdVar_[i]);

 _getche();
 exit(1);
*/
 //-------------------------
 // definir os multiplicadores de Lagrange
 aux = mr__.numCop_+mr__.numResProRel_+mr__.numResSalRel_;
 mr__.vetML_ = new double[aux];
 for(int i = 0; i < aux; i++)
   mr__.vetML_[i] = 0.0;
 h2 = time(NULL);
 mr__.temRes_ = difftime(h2,h1);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void mntSubProbDL()
{
 FILE *f;
 time_t h1,h2;
 char arq[50];
 int aux,flag;
 int vetAux[MAX_DIS];
 h1 = time(NULL);
 //-------------------------
 // montar os subproblemas
 for(int s = 0; s < mr__.numSps_; s++)
  {
   // ------------------ 
   // definir as disciplinas das turmas do subproblema sp
   memset(vetAux,0,sizeof(vetAux));
   for(int c = 0; c < numDis__; c++)
     for(int u = 0; u < numTur__; u++)
       if((mr__.vetSpTur_[u] == s) && matDisTur__[c][u])
        {
         vetAux[c] = 1;
         break;
        }
   sprintf(arq,"sp%d.lp",s+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   fprintf(f,"\n\\Variaveis X reais\n");
   for(int i = mr__.vetIniSp_[s]; i <= mr__.vetFinSp_[s]; i++)
     if(mr__.vetIdVar_[i] < mr__.numVar_)
       fprintf(f,"+ %.2f x_%d_%d_%d\n",mr__.vetVarRea_[mr__.vetIdVar_[i]].coef_,mr__.vetVarRea_[mr__.vetIdVar_[i]].dis_,mr__.vetVarRea_[mr__.vetIdVar_[i]].per_,mr__.vetVarRea_[mr__.vetIdVar_[i]].sal_);
   fprintf(f,"\n\\Variaveis X copias\n"); 
   for(int i = 0; i < mr__.numCop_; i++)
     if(mr__.vetVarCop_[i].spCop_ == s)
       fprintf(f,"+ 0.00 x_%d_%d_%d\n",mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].dis_,mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].per_,mr__.vetVarRea_[mr__.vetVarCop_[i].idRea_].sal_);
   fprintf(f,"\n\\Variaveis V\n"); 
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == s)
      {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ %d v_%d_%d ",PESOS[1],u,p); 
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\Variaveis W\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == s)
       fprintf(f,"+ %d w_%d ",PESOS[2],c); 
     else if(vetAux[c])
       fprintf(f,"+ %d w_%d ",0,c); 
   fprintf(f,"\n\n\\Variaveis Y\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == s)
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ %d y_%d_%d ",PESOS[3],c,r); 
       fprintf(f,"\n"); 
      }
     else if(vetAux[c])
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ %d y_%d_%d ",0,c,r); 
       fprintf(f,"\n"); 
      }
    // ------------------ restri��es
   fprintf(f,"\nST\n");
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
      {
       for(int p = 0; p < numPerTot__; p++)
         for(int r = 0; r < numSal__; r++)
           if(!matResDisPer__[c][p])
             fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
       fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
      }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
        {
         for(int q = 0; q < numPerDia__; q++)
           if(!matResDisPer__[c][(d*numPerDia__)+q])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ x_%d_%d_%d ",c,(d*numPerDia__)+q,r);
         fprintf(f,"- z_%d_%d >= 0\n",c,d);
        }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
      {
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"+ z_%d_%d ",c,d);
       fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
      }
   fprintf(f,"\n\\Nova 6 - Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == s)
      {
       for(int d = 0; d < numDia__; d++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          {
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__),r);
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+1])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+1,r);
          } 
         fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__));
         for(int q = 1; q < numPerDia__ - 1; q++)
          {
           for(int k = 0; k < vetTurmas__[u].numDis_; k++)
            {
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q-1])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q-1,r);
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q,r);
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q+1])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q+1,r);
            } 
           fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+q);
          }
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          {
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-2])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-2,r);
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-1])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-1,r);
          } 
         fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-1);
        }
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\Nova 7 - Aulas de uma turma no mesmo periodo\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == s)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],p,r);
         fprintf(f,"<= 1\n");
        }
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\Nova 5 - Numero de salas usadas por disciplina\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
      {
       for(int r = 0; r < numSal__; r++)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
         fprintf(f,"- %d y_%d_%d <= 0\n",vetDisciplinas__[c].numPer_,c,r); 
        } 
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ x_%d_%d_%d ",c,p,r);
       fprintf(f,"<= 1\n");
      }
     fprintf(f,"\n");
    }
   fprintf(f,"\n\\Nova 1 - Aulas simultaneas na mesma sala\n");
   for(int r = 0; r < numSal__; r++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
           if(!matResDisPer__[c][p])
             fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
         fprintf(f,"<= 1\n"); 
      } 
     fprintf(f,"\n"); 
    }
   // ------------------ limites
   fprintf(f,"\nBOUNDS\n");
   fprintf(f,"\n\\Variaveis X\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"0 <= x_%d_%d_%d <= 1\n",c,p,r); 
   fprintf(f,"\n\\Variaveis V\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == s)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis W\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
   fprintf(f,"\n\\Variaveis Y\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"0 <= y_%d_%d <= 1\n",c,r); 
   fprintf(f,"\n\\Variaveis Z\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
   // ------------------ vari�veis
   fprintf(f,"\nBINARIES\n");
   for(int c = 0; c < numDis__; c++) 
     if(vetAux[c])
      {
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"x_%d_%d_%d\n",c,p,r); 
       fprintf(f,"\n");
      }
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == s)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"y_%d_%d\n",c,r); 
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
   fprintf(f,"\n");
   fprintf(f,"\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nEND");
   fclose(f);
  }
 h2 = time(NULL);
 mr__.temSub_ = difftime(h2,h1);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void escResMR(FILE *f)
{


}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void exeMetRel(const int &sps,const int &fat,const int &pes)
{
 int sts;
 time_t h1,h2;
 char arq[50];
 if(METODO == 4)
   parGraTur(sps,fat,pes);
 else
   parGraDis(sps,fat,pes);
 if(MODELO < 1)
  {
   printf("\n>>> Particionamento nao implementado para esse modelo!");
   exit(1);
  }
 else
  {
   if(METODO == 4)
    {
     if(MODELO == 1)
       monSpsTurLacLub12(0);
     else if(MODELO == 2)
       monSpsTurLacLub12(1);
     else
       monSpsTurNovo();
    }
   else // METODO == 5
    {
     if(MODELO == 1)
       monSpsDisLacLub12(0);
     else if(MODELO == 2)
       monSpsDisLacLub12(1);
     else
       monSpsDisNovo();
    }
  }
 if(OPT_CPX == 1) 
   CPXsetintparam(env__,CPX_PARAM_MIPORDTYPE,1);
 for(int i = 0; i < mr__.numSps_; i++)
  {
   sprintf(arq,"sp%d.lp",i+1);
   vetCpx__[i].lp_ = CPXcreateprob(env__,&sts,"");
   CPXreadcopyprob(env__,vetCpx__[i].lp_,arq,NULL);
  }
 mr__.limInf1_ = 0.0;
 mr__.limInf2_ = 0.0;
 h1 = time(NULL);
 for(int i = 0; i < mr__.numSps_; i++)
  {
   CPXmipopt(env__,vetCpx__[i].lp_);
   CPXgetmipobjval(env__,vetCpx__[i].lp_,&vetCpx__[i].limSup_);
   CPXgetbestobjval(env__,vetCpx__[i].lp_,&vetCpx__[i].limInf_);
   mr__.limInf1_ += vetCpx__[i].limSup_;
   mr__.limInf2_ += vetCpx__[i].limInf_;
  }
 h2 = time(NULL);
 mr__.temExe_ = difftime(h2,h1);
 if((PESOS[3] != 0) && ((MODELO == 0) || (MODELO > 2)))
  {
   mr__.limInf1_ -= numDis__;
   mr__.limInf2_ -= numDis__;
  }
 for(int i = 0; i < mr__.numSps_; i++)
   CPXfreeprob(env__,&vetCpx__[i].lp_);
 mr__.temTot_ = mr__.temPrt_ + mr__.temSub_ + mr__.temExe_;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void exeTesPar(const int &sps,const int &fat,const int &pes)
{
 FILE *f;
 char arq[50];
 int cnt,maior,menor;
 strcpy(arq,"Saidas\\");
 strcat(arq,SAIDA);
 if(METODO == 2)
  {
   parGraTur(SPS,FD,PES);
   maior = 0;
   menor = numTur__;
   for(int k = 0; k < sps; k++)
    {
     cnt = 0;
     for(int i = 0; i < numTur__; i++)
       if(mr__.vetSpTur_[i] == k)
         cnt++;
     if(cnt > maior)
       maior = cnt;
     if(cnt < menor)
       menor = cnt;
    }    
   f = fopen(arq,"at");
   fprintf(f,"%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t",INST,MODELO,pes,numTur__,mr__.numSps_,mr__.fatDes_,
           mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,menor,maior,mr__.temPrt_);
   fclose(f);
   parGraTur(SPS,-1,PES);
   maior = 0;
   menor = numTur__;
   for(int k = 0; k < sps; k++)
    {
     cnt = 0;
     for(int i = 0; i < numTur__; i++)
       if(mr__.vetSpTur_[i] == k)
         cnt++;
     if(cnt > maior)
       maior = cnt;
     if(cnt < menor)
       menor = cnt;
    }    
   f = fopen(arq,"at");
   fprintf(f,"%d\t%d\t%d\t%d\t%d\t%.2f\t",mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,menor,maior,mr__.temPrt_);
   fclose(f);
   parGraTur(SPS,-2,PES);
   maior = 0;
   menor = numTur__;
   for(int k = 0; k < sps; k++)
    {
     cnt = 0;
     for(int i = 0; i < numTur__; i++)
       if(mr__.vetSpTur_[i] == k)
         cnt++;
     if(cnt > maior)
       maior = cnt;
     if(cnt < menor)
       menor = cnt;
    }    
   f = fopen(arq,"at");
   fprintf(f,"%d\t%d\t%d\t%d\t%d\t%.2f\n",mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,menor,maior,mr__.temPrt_);
   fclose(f);
  }
 else
  {
   parGraDis(SPS,FD,PES);
   maior = 0;
   menor = numDis__;
   for(int k = 0; k < sps; k++)
    {
     cnt = 0;
     for(int i = 0; i < numDis__; i++)
       if(mr__.vetSpDis_[i] == k)
         cnt++;
     if(cnt > maior)
       maior = cnt;
     if(cnt < menor)
       menor = cnt;
    }    
   f = fopen(arq,"at");
   fprintf(f,"%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t",INST,MODELO,pes,numDis__,mr__.numSps_,mr__.fatDes_,
           mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,menor,maior,mr__.temPrt_);
   fclose(f);
   parGraDis(SPS,-1,PES);
   maior = 0;
   menor = numDis__;
   for(int k = 0; k < sps; k++)
    {
     cnt = 0;
     for(int i = 0; i < numDis__; i++)
       if(mr__.vetSpDis_[i] == k)
         cnt++;
     if(cnt > maior)
       maior = cnt;
     if(cnt < menor)
       menor = cnt;
    }    
   f = fopen(arq,"at");
   fprintf(f,"%d\t%d\t%d\t%d\t%d\t%.2f\t",mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,menor,maior,mr__.temPrt_);
   fclose(f);
   parGraDis(SPS,-2,PES);
   maior = 0;
   menor = numDis__;
   for(int k = 0; k < sps; k++)
    {
     cnt = 0;
     for(int i = 0; i < numDis__; i++)
       if(mr__.vetSpDis_[i] == k)
         cnt++;
     if(cnt > maior)
       maior = cnt;
     if(cnt < menor)
       menor = cnt;
    }    
   f = fopen(arq,"at");
   fprintf(f,"%d\t%d\t%d\t%d\t%d\t%.2f\n",mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,menor,maior,mr__.temPrt_);
   fclose(f);
  }
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void parGraTur(const int &sps,const int &fat,const int &pes)
{
 time_t h1,h2;
 int aux,flag;
 int areCor,pesAre;
 int matAdj[MAX_TUR][MAX_TUR];
 int vetOrdSps[MAX_DIS];
 int vetQtdAdjSps[MAX_DIS];
 int vetQtdTurSps[MAX_DIS];
 h1 = time(NULL);
 mr__.numSps_ = MIN(sps,numTur__);
 mr__.fatDes_ = fat;
 memset(mr__.vetSpTur_,-1,sizeof(mr__.vetSpTur_));
 memset(matAdj,0,sizeof(matAdj));
 for(int i = 0; i < numTur__; i++)
   for(int j = 0; j < numTur__; j++)
     if(j != i)
       for(int k = 0; k < numDis__; k++)
         if(matDisTur__[k][i] && matDisTur__[k][j])
           matAdj[i][j]++;
 //-------------------------
 // todas as turmas em um �nico subproblema
 if(mr__.numSps_ <= 1)
   memset(mr__.vetSpTur_,0,sizeof(mr__.vetSpTur_));
 //-------------------------
 // cada turma em um subproblema
 else if(mr__.numSps_ == numTur__)
  {
   for(int i = 0; i < numTur__; i++)
     mr__.vetSpTur_[i] = i;
  }
 //-------------------------
 // particionamento usando a METIS ou o CPLEX
 else
  {
   if(fat < 0)
     parGraTurCpx(sps,fat,pes);
   else
    {
     int pos,are;
     idx_t *vetPrt;    // vetor de identifica��o das parti��es
     idx_t *vetNumAdj; // vetor com o n�mero de adjac�ncias de cada turma
     idx_t *vetTurAdj; // vetor com as turmas adjacentes de cada turma
     idx_t *vetPesAre; // vetor com o peso das arestas
     int wgtFlag = 0;  // flag para indicar a utilizacao ou nao de pesos (0: sem pesos)
     int numFlag = 0;  // flag para indicar o estilo dos contadores (comeca em 0: 0)
     idx_t *opts = new idx_t[METIS_NOPTIONS];
     METIS_SetDefaultOptions(opts);
     opts[METIS_OPTION_UFACTOR] = mr__.fatDes_;
     idx_t *nCon = new idx_t[1];
     nCon[0] = 1;
     vetPrt = new idx_t[numTur__];
     vetNumAdj = new idx_t[numTur__+1];
     vetTurAdj = new idx_t[numTur__*numTur__];
     vetPesAre = new idx_t[numTur__*numTur__];
     pos = 0;
     vetNumAdj[0] = 0;
     for(int i = 0; i < numTur__; i++)
      {
       are = 0;
       for(int j = 0; j < numTur__; j++)
        {
         if(j != i)
          {
           if(matAdj[i][j] > 0)
            {
             vetTurAdj[pos] = j;
             vetPesAre[pos] = matAdj[i][j];
             pos++;
             are++;
            }
          }
        }
       vetNumAdj[i+1] = vetNumAdj[i] + are;
      } 
     idx_t aux1 = numTur__;
     idx_t aux2 = mr__.numSps_;
     idx_t aux3; 
     if(pes)
       METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetTurAdj,NULL,NULL,vetPesAre,&aux2,NULL,NULL,opts,&aux3,vetPrt);
  //     METIS_PartGraphKway(&aux1,nCon,vetNumAdj,vetTurAdj,NULL,NULL,vetPesAre,&aux2,NULL,NULL,opts,&aux3,vetPrt);
     else
       METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetTurAdj,NULL,NULL,NULL,&aux2,NULL,NULL,opts,&aux3,vetPrt);
  //     METIS_PartGraphKway(&aux1,nCon,vetNumAdj,vetTurAdj,NULL,NULL,NULL,&aux2,NULL,NULL,opts,&aux3,vetPrt);
     mr__.numCtsM_ = aux3;
     for(int i = 0; i < numTur__; i++)
       mr__.vetSpTur_[i] = vetPrt[i];
     delete[] vetPrt;
     delete[] vetNumAdj;
     delete[] vetTurAdj;
     delete[] vetPesAre;
     delete[] opts;
     delete[] nCon;
    }
  }
 mr__.numCts_ = mr__.pesCts_ = 0;
 if(mr__.numSps_ > 1)
   for(int i = 0; i < (numTur__-1); i++)
     for(int j = i+1; j < numTur__; j++)
       if(mr__.vetSpTur_[j] != mr__.vetSpTur_[i])
        {
         mr__.numCts_ += MIN(1,matAdj[i][j]);
         mr__.pesCts_ += matAdj[i][j];
        } 
 //-------------------------
 // definir os subproblemas com mais arestas "externas"
 memset(vetQtdAdjSps,0,sizeof(vetQtdAdjSps));
 memset(vetQtdTurSps,0,sizeof(vetQtdTurSps));
 for(int i = 0; i < numTur__; i++)
  {
   vetQtdTurSps[mr__.vetSpTur_[i]]++;
   for(int j = 0; j < numTur__; j++)
     if((j != i) && (mr__.vetSpTur_[j] != mr__.vetSpTur_[i]))
       vetQtdAdjSps[mr__.vetSpTur_[i]] += matAdj[i][j];
  } 
 //-------------------------
 // ordenar os subproblemas (mais arestas "externas")
 for(int i = 0; i < mr__.numSps_; i++)
   vetOrdSps[i] = i;
 flag = 1;
 while(flag)
  {
   flag = 0;
   for(int i = 0; i < mr__.numSps_-1; i++)
     if((vetQtdAdjSps[vetOrdSps[i]] < vetQtdAdjSps[vetOrdSps[i+1]]) || 
        ((vetQtdAdjSps[vetOrdSps[i]] == vetQtdAdjSps[vetOrdSps[i+1]]) && (vetQtdTurSps[vetOrdSps[i]] < vetQtdTurSps[vetOrdSps[i+1]])))
      {
       aux = vetOrdSps[i];
       vetOrdSps[i] = vetOrdSps[i+1];
       vetOrdSps[i+1] = aux;
       flag = 1;
      }
  }
 //-------------------------
 // redefinir os subproblemas de cada turma
 for(int i = 0; i < numTur__; i++)
   if(mr__.vetSpTur_[i] == vetOrdSps[0])
     mr__.vetSpTur_[i] = -mr__.numSps_;
 for(int i = 1; i < mr__.numSps_; i++)
   for(int j = 0; j < numTur__; j++)
     if(mr__.vetSpTur_[j] == vetOrdSps[i])
       mr__.vetSpTur_[j] = -i;
 for(int i = 0; i < numTur__; i++)
   if(mr__.vetSpTur_[i] == -mr__.numSps_)
     mr__.vetSpTur_[i] = 0;
   else
     mr__.vetSpTur_[i] *= -1;
 h2 = time(NULL);
 mr__.temPrt_ = difftime(h2,h1);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void parGraDis(const int &sps,const int &fat,const int &pes)
{
 time_t h1,h2;
 int aux,flag;
 int areCor,pesAre;
 int matAdj[MAX_DIS][MAX_DIS];
 int vetOrdSps[MAX_DIS];
 int vetQtdAdjSps[MAX_DIS];
 int vetQtdDisSps[MAX_DIS];
 h1 = time(NULL);
 mr__.numSps_ = MIN(sps,numDis__);
 mr__.fatDes_ = fat;
 memset(mr__.vetSpDis_,-1,sizeof(mr__.vetSpDis_));
 memset(matAdj,0,sizeof(matAdj));
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numDis__; j++)
     if(j != i)
       for(int k = 0; k < numTur__; k++)
         if(matDisTur__[i][k] && matDisTur__[j][k])
           matAdj[i][j]++;
 //-------------------------
 // todas as disciplinas em um �nico subproblema
 if(mr__.numSps_ <= 1)
   memset(mr__.vetSpDis_,0,sizeof(mr__.vetSpDis_));
 //-------------------------
 // cada disciplina em um subproblema
 else if(mr__.numSps_ == numDis__)
  {
   for(int i = 0; i < numDis__; i++)
     mr__.vetSpDis_[i] = i;
  }
 //-------------------------
 // particionamento usando a METIS ou o CPLEX
 else
  {
   if(fat < 0)
     parGraDisCpx(sps,fat,pes);
   else
    {
     int pos,are;
     idx_t *vetPrt;    // vetor de identifica��o das parti��es
     idx_t *vetNumAdj; // vetor com o n�mero de adjac�ncias de cada disciplina
     idx_t *vetDisAdj; // vetor com as disciplinas adjacentes de cada disciplina
     idx_t *vetPesAre; // vetor com o peso das arestas
     int wgtFlag = 0;  // flag para indicar a utilizacao ou nao de pesos (0: sem pesos)
     int numFlag = 0;  // flag para indicar o estilo dos contadores (comeca em 0: 0)
     idx_t *opts = new idx_t[METIS_NOPTIONS];
     METIS_SetDefaultOptions(opts);
     opts[METIS_OPTION_UFACTOR] = mr__.fatDes_;
     idx_t *nCon = new idx_t[1];
     nCon[0] = 1;
     vetPrt = new idx_t[numDis__];
     vetNumAdj = new idx_t[numDis__+1];
     vetDisAdj = new idx_t[numDis__*numDis__];
     vetPesAre = new idx_t[numDis__*numDis__];
     pos = 0;
     vetNumAdj[0] = 0;
     for(int i = 0; i < numDis__; i++)
      {
       are = 0;
       for(int j = 0; j < numDis__; j++)
        {
         if(j != i)
          {
           if(matAdj[i][j] > 0)
            {
             vetDisAdj[pos] = j;
             vetPesAre[pos] = matAdj[i][j];
             pos++;
             are++;
            }
          }
        }
       vetNumAdj[i+1] = vetNumAdj[i] + are;
      } 
     idx_t aux1 = numDis__;
     idx_t aux2 = mr__.numSps_;
     idx_t aux3; 
     if(pes)
       METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetDisAdj,NULL,NULL,vetPesAre,&aux2,NULL,NULL,opts,&aux3,vetPrt);
  //     METIS_PartGraphKway(&aux1,nCon,vetNumAdj,vetDisAdj,NULL,NULL,vetPesAre,&aux2,NULL,NULL,opts,&aux3,vetPrt);
     else
       METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetDisAdj,NULL,NULL,NULL,&aux2,NULL,NULL,opts,&aux3,vetPrt);
  //     METIS_PartGraphKway(&aux1,nCon,vetNumAdj,vetDisAdj,NULL,NULL,NULL,&aux2,NULL,NULL,opts,&aux3,vetPrt);
     mr__.numCtsM_ = aux3;
     for(int i = 0; i < numDis__; i++)
       mr__.vetSpDis_[i] = vetPrt[i];
     delete[] vetPrt;
     delete[] vetNumAdj;
     delete[] vetDisAdj;
     delete[] vetPesAre;
     delete[] opts;
     delete[] nCon;
    }
  }
 mr__.numCts_ = mr__.pesCts_ = 0;
 if(mr__.numSps_ > 1)
   for(int i = 0; i < (numDis__-1); i++)
     for(int j = i+1; j < numDis__; j++)
       if(mr__.vetSpDis_[j] != mr__.vetSpDis_[i])
        {
         mr__.numCts_ += MIN(1,matAdj[i][j]);
         mr__.pesCts_ += matAdj[i][j];
        } 
 //-------------------------
 // definir os subproblemas com mais arestas "externas"
 memset(vetQtdAdjSps,0,sizeof(vetQtdAdjSps));
 memset(vetQtdDisSps,0,sizeof(vetQtdDisSps));
 for(int i = 0; i < numDis__; i++)
  {
   vetQtdDisSps[mr__.vetSpDis_[i]]++;
   for(int j = 0; j < numDis__; j++)
     if((j != i) && (mr__.vetSpDis_[j] != mr__.vetSpDis_[i]))
       vetQtdAdjSps[mr__.vetSpDis_[i]] += matAdj[i][j];
  } 
 //-------------------------
 // ordenar os subproblemas (mais arestas "externas")
 for(int i = 0; i < mr__.numSps_; i++)
   vetOrdSps[i] = i;
 flag = 1;
 while(flag)
  {
   flag = 0;
   for(int i = 0; i < mr__.numSps_-1; i++)
     if((vetQtdAdjSps[vetOrdSps[i]] < vetQtdAdjSps[vetOrdSps[i+1]]) || 
        ((vetQtdAdjSps[vetOrdSps[i]] == vetQtdAdjSps[vetOrdSps[i+1]]) && (vetQtdDisSps[vetOrdSps[i]] < vetQtdDisSps[vetOrdSps[i+1]])))
      {
       aux = vetOrdSps[i];
       vetOrdSps[i] = vetOrdSps[i+1];
       vetOrdSps[i+1] = aux;
       flag = 1;
      }
  }
 //-------------------------
 // redefinir os subproblemas de cada disciplina
 for(int i = 0; i < numDis__; i++)
   if(mr__.vetSpDis_[i] == vetOrdSps[0])
     mr__.vetSpDis_[i] = -mr__.numSps_;
 for(int i = 1; i < mr__.numSps_; i++)
   for(int j = 0; j < numDis__; j++)
     if(mr__.vetSpDis_[j] == vetOrdSps[i])
       mr__.vetSpDis_[j] = -i;
 for(int i = 0; i < numDis__; i++)
   if(mr__.vetSpDis_[i] == -mr__.numSps_)
     mr__.vetSpDis_[i] = 0;
   else
     mr__.vetSpDis_[i] *= -1;
 h2 = time(NULL);
 mr__.temPrt_ = difftime(h2,h1);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void parGraTurCpx(const int &sps,const int &fat,const int &pes)
{
 // -------------------
 // montar a matriz de adjac�ncias
 int matAdj[MAX_TUR][MAX_TUR];
 memset(matAdj,0,sizeof(matAdj));
 for(int i = 0; i < numTur__; i++)
   for(int j = 0; j < numTur__; j++)
     if(j != i)
       for(int k = 0; k < numDis__; k++)
         if(matDisTur__[k][i] && matDisTur__[k][j])
           matAdj[i][j]++;
 if(pes == 0)
   for(int i = 0; i < numTur__; i++)
     for(int j = 0; j < numTur__; j++)
       matAdj[i][j] = MIN(1,matAdj[i][j]);
 // -------------------
 // montar o modelo
 FILE *f = fopen("grafoT.lp","w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 for(int i = 0; i < numTur__; i++)
  {
   for(int k = 0; k < SPS; k++)
     fprintf(f,"+ 0 x_%d_%d ",i,k);        
   fprintf(f,"\n");        
  }
 fprintf(f,"\n");        
 for(int i = 0; i < numTur__; i++)
  {
   for(int k = 0; k < SPS; k++)
    {
     for(int j = i+1; j < numTur__; j++)
       if(matAdj[i][j] > 0)
         fprintf(f,"+ %d x_%d_%d - [%d x_%d_%d * x_%d_%d] / 2 ",matAdj[i][j],i,k,2*matAdj[i][j],i,k,j,k);        
    }
   fprintf(f,"\n");
  }
 // ------------------ restri��es
 fprintf(f,"\nST"); 
 fprintf(f,"\n\\Cada turma em 1 subproblema\n");
 for(int i = 0; i < numTur__; i++)
  {
   for(int k = 0; k < SPS; k++)
     fprintf(f,"+ x_%d_%d ",i,k);        
   fprintf(f,"= 1\n"); 
  }
 if(fat == -2)
  {
   fprintf(f,"\n\\Qtde de turmas em cada subproblema\n");
   for(int k = 0; k < SPS; k++)
    {
     for(int i = 0; i < numTur__; i++)
       fprintf(f,"+ x_%d_%d ",i,k);        
     if(floor(1.2*((double)numTur__/SPS))*SPS >= numTur__)
       fprintf(f,"<= %.2f\n",floor(1.2*((double)numTur__/SPS))); 
     else
       fprintf(f,"<= %.2f\n",floor(1.2*((double)numTur__/SPS))+1); 
    }
  }
 else
  {
   if(numTur__ % SPS == 0)
    {
     fprintf(f,"\n\\Qtde de turmas em cada subproblema\n");
     for(int k = 0; k < SPS; k++)
      {
       for(int i = 0; i < numTur__; i++)
         fprintf(f,"+ x_%d_%d ",i,k);        
       fprintf(f,"= %.2f\n",floor((double)numTur__/SPS)); 
      }
    }
   else
    {
     fprintf(f,"\n\\Qtde de turmas em cada subproblema 1\n");
     for(int k = 0; k < SPS; k++)
      {
       for(int i = 0; i < numTur__; i++)
         fprintf(f,"+ x_%d_%d ",i,k);        
       fprintf(f,">= %.2f\n",floor((double)numTur__/SPS)); 
      }
     fprintf(f,"\n\\Qtde de turmas em cada subproblema 2\n");
     for(int k = 0; k < SPS; k++)
      {
       for(int i = 0; i < numTur__; i++)
         fprintf(f,"+ x_%d_%d ",i,k);        
       fprintf(f,"<= %.2f\n",floor((double)numTur__/SPS)+1); 
      }
    }
  }
 fprintf(f,"\nBOUNDS\n");
 for(int i = 0; i < numTur__; i++)
   for(int k = 0; k < SPS; k++)
     fprintf(f,"0 <= x_%d_%d <= 1\n",i,k);        
 fprintf(f,"\nBINARIES\n");
 for(int i = 0; i < numTur__; i++)
   for(int k = 0; k < SPS; k++)
     fprintf(f,"x_%d_%d\n",i,k);        
 fprintf(f,"\nEND");
 fclose(f);
 // -------------------
 // resolver o modelo
 int sts;
 double aux;
 double *vetSol;
 vetCpx__[0].lp_ = CPXcreateprob(env__,&sts,"");
 CPXreadcopyprob(env__,vetCpx__[0].lp_,"grafoT.lp",NULL);
 vetSol = new double[CPXgetnumcols(env__,vetCpx__[0].lp_)];
 CPXmipopt(env__,vetCpx__[0].lp_);
 sts = CPXgetmipobjval(env__,vetCpx__[0].lp_,&aux);
 if(sts != 0)
  {
   printf("\n>>> Particionamento do grafo sem solucao!");
   exit(1);
  }
 mr__.numCtsM_ = aux;
 CPXgetmipx(env__,vetCpx__[0].lp_,vetSol,0,CPXgetnumcols(env__,vetCpx__[0].lp_)-1);
 CPXfreeprob(env__,&vetCpx__[0].lp_);
 // -------------------
 // preencher o vetor com os subproblemas de cada turma
 sts = 0;
 for(int i = 0; i < numTur__; i++)
   for(int k = 0; k < SPS; k++)
    {
     if(vetSol[sts] >= 0.5)
       mr__.vetSpTur_[i] = k;
     sts++;
    }
 delete vetSol;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void parGraDisCpx(const int &sps,const int &fat,const int &pes)
{
 // -------------------
 // montar a matriz de adjac�ncias
 int matAdj[MAX_DIS][MAX_DIS];
 memset(matAdj,0,sizeof(matAdj));
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numDis__; j++)
     if(j != i)
       for(int k = 0; k < numTur__; k++)
         if(matDisTur__[i][k] && matDisTur__[j][k])
           matAdj[i][j]++;
 if(pes == 0)
   for(int i = 0; i < numDis__; i++)
     for(int j = 0; j < numDis__; j++)
       matAdj[i][j] = MIN(1,matAdj[i][j]);
 // -------------------
 // montar o modelo
 FILE *f = fopen("grafoD.lp","w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 for(int i = 0; i < numDis__; i++)
  {
   for(int k = 0; k < SPS; k++)
     fprintf(f,"+ 0 x_%d_%d ",i,k);        
   fprintf(f,"\n");        
  }
 fprintf(f,"\n");        
 for(int i = 0; i < numDis__; i++)
  {
   for(int k = 0; k < SPS; k++)
    {
     for(int j = i+1; j < numDis__; j++)
       if(matAdj[i][j] > 0)
         fprintf(f,"+ %d x_%d_%d - [%d x_%d_%d * x_%d_%d] / 2 ",matAdj[i][j],i,k,2*matAdj[i][j],i,k,j,k);        
    }
   fprintf(f,"\n");
  }
 // ------------------ restri��es
 fprintf(f,"\nST"); 
 fprintf(f,"\n\\Cada disciplina em 1 subproblema\n");
 for(int i = 0; i < numDis__; i++)
  {
   for(int k = 0; k < SPS; k++)
     fprintf(f,"+ x_%d_%d ",i,k);        
   fprintf(f,"= 1\n"); 
  }
 if(fat == -2)
  {
   fprintf(f,"\n\\Qtde de disciplinas em cada subproblema\n");
   for(int k = 0; k < SPS; k++)
    {
     for(int i = 0; i < numDis__; i++)
       fprintf(f,"+ x_%d_%d ",i,k);    
     if(floor(1.2*((double)numDis__/SPS))*SPS >= numDis__)
       fprintf(f,"<= %.2f\n",floor(1.2*((double)numDis__/SPS))); 
     else
       fprintf(f,"<= %.2f\n",floor(1.2*((double)numDis__/SPS))+1); 
    }
  }
 else
  {
   if(numDis__ % SPS == 0)
    {
     fprintf(f,"\n\\Qtde de disciplinas em cada subproblema\n");
     for(int k = 0; k < SPS; k++)
      {
       for(int i = 0; i < numDis__; i++)
         fprintf(f,"+ x_%d_%d ",i,k);        
       fprintf(f,"= %.2f\n",floor((double)numDis__/SPS)); 
      }
    }
   else
    {
     fprintf(f,"\n\\Qtde de disciplinas em cada subproblema 1\n");
     for(int k = 0; k < SPS; k++)
      {
       for(int i = 0; i < numDis__; i++)
         fprintf(f,"+ x_%d_%d ",i,k);        
       fprintf(f,">= %.2f\n",floor((double)numDis__/SPS)); 
      }
     fprintf(f,"\n\\Qtde de disciplinas em cada subproblema 2\n");
     for(int k = 0; k < SPS; k++)
      {
       for(int i = 0; i < numDis__; i++)
         fprintf(f,"+ x_%d_%d ",i,k);        
       fprintf(f,"<= %.2f\n",floor((double)numDis__/SPS)+1); 
      }
    }
  }
 fprintf(f,"\nBOUNDS\n");
 for(int i = 0; i < numDis__; i++)
   for(int k = 0; k < SPS; k++)
     fprintf(f,"0 <= x_%d_%d <= 1\n",i,k);        
 fprintf(f,"\nBINARIES\n");
 for(int i = 0; i < numDis__; i++)
   for(int k = 0; k < SPS; k++)
     fprintf(f,"x_%d_%d\n",i,k);        
 fprintf(f,"\nEND");
 fclose(f);
 // -------------------
 // resolver o modelo
 int sts;
 double aux;
 double *vetSol;
 vetCpx__[0].lp_ = CPXcreateprob(env__,&sts,"");
 CPXreadcopyprob(env__,vetCpx__[0].lp_,"grafoD.lp",NULL);
 vetSol = new double[CPXgetnumcols(env__,vetCpx__[0].lp_)];
 CPXmipopt(env__,vetCpx__[0].lp_);
 sts = CPXgetmipobjval(env__,vetCpx__[0].lp_,&aux);
 if(sts != 0)
  {
   printf("\n>>> Particionamento do grafo sem solucao!");
   exit(1);
  }
 mr__.numCtsM_ = aux;
 CPXgetmipx(env__,vetCpx__[0].lp_,vetSol,0,CPXgetnumcols(env__,vetCpx__[0].lp_)-1);
 CPXfreeprob(env__,&vetCpx__[0].lp_);
 // -------------------
 // preencher o vetor com os subproblemas de cada disciplina
 sts = 0;
 for(int i = 0; i < numDis__; i++)
   for(int k = 0; k < SPS; k++)
    {
     if(vetSol[sts] >= 0.5)
       mr__.vetSpDis_[i] = k;
     sts++;
    }
 delete vetSol;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monSpsTurLacLub12(const int &adp)
{
 FILE *f; 
 int aux;
 time_t h1,h2;
 char arq[50];
 int vetAux[MAX_DIS];
 int vetTurDis[MAX_DIS];
 h1 = time(NULL);
 // ------------------ 
 // definir em qual turma cada disciplina ser� "contada" na FO
 memset(vetTurDis,0,sizeof(vetTurDis));
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u] && (vetTurmas__[u].numDis_ > aux))
      {
       aux = vetTurmas__[u].numDis_;
       vetTurDis[c] = u;
      }
  } 
 for(int sp = 0; sp < mr__.numSps_; sp++)
  {
   // ------------------ 
   // definir as disciplinas das turmas do subproblema sp
   memset(vetAux,0,sizeof(vetAux));
   for(int c = 0; c < numDis__; c++)
     for(int u = 0; u < numTur__; u++)
       if((mr__.vetSpTur_[u] == sp) && matDisTur__[c][u])
        {
         vetAux[c] = 1;
         break;
        }
   sprintf(arq,"sp%d.lp",sp+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
      {
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ 0 x_%d_%d ",c,p);
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int s = 0; s < (numSalDif__-1); s++)
      {
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
             if(adp)
               fprintf(f,"+ %d y_%d_%d_%d ",PESOS[0] * vetDisciplinas__[c].numAlu_ - vetTamSal__[s],s,c,p);
             else
               fprintf(f,"+ %d y_%d_%d_%d ",PESOS[0] * MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
      }
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\Dias minimos\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpTur_[vetTurDis[c]] == sp) 
       fprintf(f,"+ %d w_%d ",PESOS[2],c); 
     else
       fprintf(f,"+ %d w_%d ",0,c); 
   fprintf(f,"\n\n\\Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
      {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ %d v_%d_%d ",PESOS[1],u,p); 
       fprintf(f,"\n"); 
      }
   // ------------------ restri��es
   fprintf(f,"\nST\n");
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
      {
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d ",c,p); 
       fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
      }
   fprintf(f,"\n\\R2 - Numero de salas\n");
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d ",c,p); 
     fprintf(f,"<= %d\n",numSal__); 
    }
   fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     aux = 0;
     for(int i = 0; i < numSal__; i++)
       if(vetSalas__[i].capacidade_ > vetTamSal__[s])
         aux++;
     for(int p = 0; p < numPerTot__; p++) 
      {
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
             fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
       fprintf(f,"<= %d\n",aux); 
      } 
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
        {
         for(int q = 0; q < numPerDia__; q++)
           if(!matResDisPer__[c][(d*numPerDia__)+q])
             fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
         fprintf(f,"- z_%d_%d >= 0\n",c,d);
        }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
      {
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"+ z_%d_%d ",c,d);
       fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
      }
   fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
         fprintf(f,"- r_%d_%d = 0\n",u,p);
        }
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
      {
       for(int d = 0; d < numDia__; d++)
        {
         fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
         for(int q = 1; q < numPerDia__-1; q++)
           fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
         fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
        }
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
             fprintf(f,"+ x_%d_%d ",c,p);
       fprintf(f,"<= 1\n");
      }
     fprintf(f,"\n");
    }
   fprintf(f,"\nBOUNDS\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
   fprintf(f,"\n\\Variaveis y\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
   fprintf(f,"\n\\Variaveis w\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
   fprintf(f,"\n\\Variaveis v\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis z\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
   fprintf(f,"\n\\Variaveis r\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
   fprintf(f,"\nBINARIES\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"x_%d_%d\n",c,p);
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"y_%d_%d_%d\n",s,c,p);
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"r_%d_%d\n",u,p);
   fprintf(f,"\n\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nEND");
   fclose(f);
  }
 h2 = time(NULL);
 mr__.temSub_ = difftime(h2,h1);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monSpsDisLacLub12(const int &adp)
{
 int aux;
 FILE *f; 
 time_t h1,h2;
 char arq[50];
 h1 = time(NULL);
 bool vetAux[MAX_TUR];
 for(int u = 0; u < numTur__; u++)
  {
   vetAux[u] = 1;
   for(int c = 1; c < vetTurmas__[u].numDis_; c++)
     if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[c]] != mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]])
      {
       vetAux[u] = 0;
       break;
      } 
  }
 for(int sp = 0; sp < mr__.numSps_; sp++)
  {
   sprintf(arq,"sp%d.lp",sp+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
      {
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ 0 x_%d_%d ",c,p);
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int s = 0; s < (numSalDif__-1); s++)
      {
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
             if(adp)
               fprintf(f,"+ %d y_%d_%d_%d ",PESOS[0] * vetDisciplinas__[c].numAlu_ - vetTamSal__[s],s,c,p);
             else
               fprintf(f,"+ %d y_%d_%d_%d ",PESOS[0] * MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
      }
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\Dias minimos\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       fprintf(f,"+ %d w_%d ",PESOS[2],c); 
   fprintf(f,"\n\n\\Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
      {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ %d v_%d_%d ",PESOS[1],u,p); 
       fprintf(f,"\n"); 
      }
   // ------------------ restri��es
   fprintf(f,"\nST\n");
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
      {
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d ",c,p); 
       fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
      }
   fprintf(f,"\n\\R2 - Numero de salas\n");
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d ",c,p); 
     fprintf(f,"<= %d\n",numSal__); 
    }
   fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     aux = 0;
     for(int i = 0; i < numSal__; i++)
       if(vetSalas__[i].capacidade_ > vetTamSal__[s])
         aux++;
     for(int p = 0; p < numPerTot__; p++) 
      {
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
             fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
       fprintf(f,"<= %d\n",aux); 
      } 
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++)
        {
         for(int q = 0; q < numPerDia__; q++)
           if(!matResDisPer__[c][(d*numPerDia__)+q])
             fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
         fprintf(f,"- z_%d_%d >= 0\n",c,d);
        }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
      {
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"+ z_%d_%d ",c,d);
       fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
      }
   fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
         fprintf(f,"- r_%d_%d = 0\n",u,p);
        }
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
      {
       for(int d = 0; d < numDia__; d++)
        {
         fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
         for(int q = 1; q < numPerDia__-1; q++)
           fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
         fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
        }
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
             fprintf(f,"+ x_%d_%d ",c,p);
       fprintf(f,"<= 1\n");
      }
     fprintf(f,"\n");
    }
   fprintf(f,"\nBOUNDS\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
   fprintf(f,"\n\\Variaveis y\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
   fprintf(f,"\n\\Variaveis w\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
   fprintf(f,"\n\\Variaveis v\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis z\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
   fprintf(f,"\n\\Variaveis r\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
         
   fprintf(f,"\nBINARIES\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"x_%d_%d\n",c,p);
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"y_%d_%d_%d\n",s,c,p);
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"r_%d_%d\n",u,p);
         
   fprintf(f,"\n\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nEND");
   fclose(f);
  }
 h2 = time(NULL);
 mr__.temSub_ = difftime(h2,h1);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monSpsTurNovo()
{
 FILE *f; 
 int aux;
 time_t h1,h2;
 char arq[50];
 int vetAux[MAX_DIS];
 int vetTurDis[MAX_DIS];
 h1 = time(NULL);
 // ------------------ 
 // definir em qual turma cada disciplina ser� "contada" na FO
 memset(vetTurDis,0,sizeof(vetTurDis));
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u] && (vetTurmas__[u].numDis_ > aux))
      {
       aux = vetTurmas__[u].numDis_;
       vetTurDis[c] = u;
      }
  } 
 for(int sp = 0; sp < mr__.numSps_; sp++)
  {
   // ------------------ 
   // definir as disciplinas das turmas do subproblema sp
   memset(vetAux,0,sizeof(vetAux));
   for(int c = 0; c < numDis__; c++)
     for(int u = 0; u < numTur__; u++)
       if((mr__.vetSpTur_[u] == sp) && matDisTur__[c][u])
        {
         vetAux[c] = 1;
         break;
        }
   sprintf(arq,"sp%d.lp",sp+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   if(MODELO < 6)
    {
     fprintf(f,"\n\\Variaveis x\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
        {
         for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ 0 x_%d_%d ",c,p);
         fprintf(f,"\n"); 
        }
     fprintf(f,"\n\\Capacidade das salas\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
        {
         for(int p = 0; p < numPerTot__; p++)
          {
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ %d q_%d_%d_%d ",PESOS[0] * MAX(0,vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),c,p,r); 
           fprintf(f,"\n"); 
          }
         fprintf(f,"\n"); 
        }
    }
   else
    {
     fprintf(f,"\n\\Capacidade das salas\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
        {
         for(int p = 0; p < numPerTot__; p++)
          {  
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ %d x_%d_%d_%d ",PESOS[0] * MAX(0,vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),c,p,r); 
           fprintf(f,"\n"); 
          }
         fprintf(f,"\n");
        }
    }
   fprintf(f,"\n\n\\Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
      {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ %d v_%d_%d ",PESOS[1],u,p); 
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\Dias minimos\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpTur_[vetTurDis[c]] == sp)
       fprintf(f,"+ %d w_%d ",PESOS[2],c); 
   fprintf(f,"\n\n\\Salas diferentes\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpTur_[vetTurDis[c]] == sp)
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ %d y_%d_%d ",PESOS[3],c,r); 
       fprintf(f,"\n"); 
      }














   // ------------------ restri��es
   fprintf(f,"\nST\n");
   if(MODELO < 6)
    {
     fprintf(f,"\n\\R1 - Numero de aulas\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"+ x_%d_%d ",c,p); 
         fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
        }
     fprintf(f,"\n\\R5 - Dias minimos 1\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         for(int d = 0; d < numDia__; d++)
          {
           for(int q = 0; q < numPerDia__; q++)
             if(!matResDisPer__[c][(d*numPerDia__)+q])
               fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
           fprintf(f,"- z_%d_%d >= 0\n",c,d);
          }
     fprintf(f,"\n\\R6 - Dias minimos 2\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
        {
         for(int d = 0; d < numDia__; d++)
           fprintf(f,"+ z_%d_%d ",c,d);
         fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
        }
     if(MODELO != 5)
      {
       fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
       for(int u = 0; u < numTur__; u++)
         if(mr__.vetSpTur_[u] == sp)
          {
           for(int p = 0; p < numPerTot__; p++)
            {
             for(int k = 0; k < vetTurmas__[u].numDis_; k++)
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
                 fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
             fprintf(f,"- r_%d_%d = 0\n",u,p);
            }
           fprintf(f,"\n"); 
          }
       fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
       for(int u = 0; u < numTur__; u++)
         if(mr__.vetSpTur_[u] == sp)
          {
           for(int d = 0; d < numDia__; d++)
            {
             fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
             for(int q = 1; q < numPerDia__-1; q++)
               fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
             fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
            }
           fprintf(f,"\n"); 
          }
      }
     fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
     for(int t = 0; t < numPro__; t++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int c = 0; c < numDis__; c++)
           if(vetAux[c])
             if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
               fprintf(f,"+ x_%d_%d ",c,p);
         fprintf(f,"<= 1\n");
        }
       fprintf(f,"\n");
      }
     fprintf(f,"\n\\Nova 1 - Aulas simultaneas na mesma sala\n");
     for(int r = 0; r < numSal__; r++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int c = 0; c < numDis__; c++)
           if(vetAux[c])
             if(!matResDisPer__[c][p])
               fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
           fprintf(f,"<= 1\n"); 
        } 
       fprintf(f,"\n"); 
      }
     fprintf(f,"\n\\Nova 2 - Mesma aula em mais de uma sala\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
            {
             fprintf(f,"x_%d_%d ",c,p); 
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"- q_%d_%d_%d ",c,p,r); 
             fprintf(f,"= 0\n"); 
            }
         fprintf(f,"\n"); 
        }
     if(MODELO == 3)
      {
       fprintf(f,"\n\\Nova 3 - Numero de salas usadas por disciplina 1\n");
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])        
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
             fprintf(f,"- y_%d_%d >= 0\n",c,r); 
            } 
           fprintf(f,"\n"); 
          }
       fprintf(f,"\n\\Nova 4 - Numero de salas usadas por disciplina 2\n");
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])        
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"q_%d_%d_%d - y_%d_%d <= 0\n",c,p,r,c,r); 
            } 
           fprintf(f,"\n"); 
          }
      }
     else 
      {
       fprintf(f,"\n\\Nova 5 - Numero de salas usadas por disciplina\n");
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])        
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
             fprintf(f,"- %d y_%d_%d <= 0\n",vetDisciplinas__[c].numPer_,c,r); 
            } 
           fprintf(f,"\n"); 
          }
       if(MODELO == 5)
        {
         fprintf(f,"\n\\Nova 6 - Aulas isoladas\n");
         for(int u = 0; u < numTur__; u++)
           if(mr__.vetSpTur_[u] == sp)
            {
             for(int d = 0; d < numDia__; d++)
              {
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                {
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)])
                   fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__));
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+1])
                   fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+1);
                } 
               fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__));
               for(int q = 1; q < numPerDia__ - 1; q++)
                {
                 for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                  {
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q-1])
                     fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q-1);
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q])
                     fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q);
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q+1])
                     fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q+1);
                  } 
                 fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+q);
                }
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                {
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-2])
                   fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-2);
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-1])
                   fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-1);
                } 
               fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-1);
              }
             fprintf(f,"\n"); 
            }
         fprintf(f,"\n\\Nova 7 - Aulas de uma turma no mesmo periodo\n");
         for(int u = 0; u < numTur__; u++)
           if(mr__.vetSpTur_[u] == sp)
            {
             for(int p = 0; p < numPerTot__; p++)
              {
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
                   fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
               fprintf(f,"<= 1\n");
              }
             fprintf(f,"\n"); 
            }
        }
      }
    }
   else
    {
     fprintf(f,"\n\\R1 - Numero de aulas\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
        {
         for(int p = 0; p < numPerTot__; p++)
           for(int r = 0; r < numSal__; r++)
             if(!matResDisPer__[c][p])
               fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
         fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
        }
     fprintf(f,"\n\\R5 - Dias minimos 1\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         for(int d = 0; d < numDia__; d++)
          {
           for(int q = 0; q < numPerDia__; q++)
             if(!matResDisPer__[c][(d*numPerDia__)+q])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"+ x_%d_%d_%d ",c,(d*numPerDia__)+q,r);
           fprintf(f,"- z_%d_%d >= 0\n",c,d);
          }
     fprintf(f,"\n\\R6 - Dias minimos 2\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
        {
         for(int d = 0; d < numDia__; d++)
           fprintf(f,"+ z_%d_%d ",c,d);
         fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
        }
     if(MODELO != 8)
      {
       fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
       for(int u = 0; u < numTur__; u++)
         if(mr__.vetSpTur_[u] == sp)
          {
           for(int p = 0; p < numPerTot__; p++)
            {
             for(int k = 0; k < vetTurmas__[u].numDis_; k++)
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
                 for(int r = 0; r < numSal__; r++)
                   fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],p,r);
             fprintf(f,"- r_%d_%d = 0\n",u,p);
            }
           fprintf(f,"\n"); 
          }
       fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
       for(int u = 0; u < numTur__; u++)
         if(mr__.vetSpTur_[u] == sp)
          {
           for(int d = 0; d < numDia__; d++)
            {
             fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
             for(int q = 1; q < numPerDia__-1; q++)
               fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
             fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
            }
           fprintf(f,"\n"); 
          }
      }
     fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
     for(int t = 0; t < numPro__; t++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int c = 0; c < numDis__; c++)
           if(vetAux[c])
             if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"+ x_%d_%d_%d ",c,p,r);
           fprintf(f,"<= 1\n");
          }
         fprintf(f,"\n");
      }
     fprintf(f,"\n\\Nova 1 - Aulas simultaneas na mesma sala\n");
     for(int r = 0; r < numSal__; r++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int c = 0; c < numDis__; c++)
           if(vetAux[c])
             if(!matResDisPer__[c][p])
               fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
           fprintf(f,"<= 1\n"); 
          } 
         fprintf(f,"\n"); 
      }
     if(MODELO == 6)
      {
       fprintf(f,"\n\\Nova 3 - Numero de salas usadas por disciplina 1\n");
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
             fprintf(f,"- y_%d_%d >= 0\n",c,r); 
            } 
           fprintf(f,"\n"); 
          }
       fprintf(f,"\n\\Nova 4 - Numero de salas usadas por disciplina 2\n");
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"x_%d_%d_%d - y_%d_%d <= 0\n",c,p,r,c,r); 
            } 
           fprintf(f,"\n"); 
          }
      }
     else 
      {
       fprintf(f,"\n\\Nova 5 - Numero de salas usadas por disciplina\n");
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
             fprintf(f,"- %d y_%d_%d <= 0\n",vetDisciplinas__[c].numPer_,c,r); 
            } 
           fprintf(f,"\n"); 
          }
       if(MODELO == 8)
        {
         fprintf(f,"\n\\Nova 6 - Aulas isoladas\n");
         for(int u = 0; u < numTur__; u++)
           if(mr__.vetSpTur_[u] == sp)
            {
             for(int d = 0; d < numDia__; d++)
              {
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                {
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__),r);
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+1])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+1,r);
                } 
               fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__));
               for(int q = 1; q < numPerDia__ - 1; q++)
                {
                 for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                  {
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q-1])
                     for(int r = 0; r < numSal__; r++)
                       fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q-1,r);
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q])
                     for(int r = 0; r < numSal__; r++)
                       fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q,r);
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q+1])
                     for(int r = 0; r < numSal__; r++)
                       fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q+1,r);
                  } 
                 fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+q);
                }
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                {
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-2])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-2,r);
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-1])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-1,r);
                } 
               fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-1);
              }
             fprintf(f,"\n"); 
            }
         fprintf(f,"\n\\Nova 7 - Aulas de uma turma no mesmo periodo\n");
         for(int u = 0; u < numTur__; u++)
           if(mr__.vetSpTur_[u] == sp)
            {
             for(int p = 0; p < numPerTot__; p++)
              {
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],p,r);
               fprintf(f,"<= 1\n");
              }
             fprintf(f,"\n"); 
            }
        }
      }
    }
   // ------------------ limites
   fprintf(f,"\nBOUNDS\n");
   if(MODELO < 6)
    {
     fprintf(f,"\n\\Variaveis x\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
     fprintf(f,"\n\\Variaveis q\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"0 <= q_%d_%d_%d <= 1\n",c,p,r); 
    }
   else
    {
     fprintf(f,"\n\\Variaveis x\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"0 <= x_%d_%d_%d <= 1\n",c,p,r); 
    }
   fprintf(f,"\n\\Variaveis v\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis w\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_); // ? ou numDia__ ou CPX_INFBOUND (+ correto, por�m resultado pior) 
   fprintf(f,"\n\\Variaveis y\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"0 <= y_%d_%d <= 1\n",c,r); 
   fprintf(f,"\n\\Variaveis z\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
   if((MODELO != 5) && (MODELO != 8))
    {
     fprintf(f,"\n\\Variaveis r\n");
     for(int u = 0; u < numTur__; u++)
       if(mr__.vetSpTur_[u] == sp)
         for(int p = 0; p < numPerTot__; p++)
           fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
    }
   // ------------------ vari�veis
   fprintf(f,"\nBINARIES\n");
   if(MODELO < 6)
    {
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"x_%d_%d\n",c,p);
     fprintf(f,"\n");
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"q_%d_%d_%d\n",c,p,r); 
     fprintf(f,"\n");
    }
   else
    {
     for(int c = 0; c < numDis__; c++) 
       if(vetAux[c])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"x_%d_%d_%d\n",c,p,r); 
     fprintf(f,"\n");
    }
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"y_%d_%d\n",c,r); 
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
   fprintf(f,"\n");
   if((MODELO != 5) && (MODELO != 8))
    {
     for(int u = 0; u < numTur__; u++)
       if(mr__.vetSpTur_[u] == sp)
         for(int p = 0; p < numPerTot__; p++)
           fprintf(f,"r_%d_%d\n",u,p);
     fprintf(f,"\n");
    } 
   fprintf(f,"\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nEND");
   fclose(f);
  }
 h2 = time(NULL);
 mr__.temSub_ = difftime(h2,h1);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monSpsDisNovo()
{
 FILE *f; 
 time_t h1,h2;
 char arq[50];
 h1 = time(NULL);
 bool vetAux[MAX_TUR];
 for(int u = 0; u < numTur__; u++)
  {
   vetAux[u] = 1;
   for(int c = 1; c < vetTurmas__[u].numDis_; c++)
     if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[c]] != mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]])
      {
       vetAux[u] = 0;
       break;
      } 
  }
 for(int sp = 0; sp < mr__.numSps_; sp++)
  {
   sprintf(arq,"sp%d.lp",sp+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   if(MODELO < 6)
    {
     fprintf(f,"\n\\Variaveis x\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
        {
         for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ 0 x_%d_%d ",c,p);
         fprintf(f,"\n"); 
        }
     fprintf(f,"\n\\Capacidade das salas\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
        {
         for(int p = 0; p < numPerTot__; p++)
          {
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ %d q_%d_%d_%d ",PESOS[0] * MAX(0,vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),c,p,r); 
           fprintf(f,"\n"); 
          }
         fprintf(f,"\n"); 
        }
    }
   else
    {
     fprintf(f,"\n\\Capacidade das salas\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
        {
         for(int p = 0; p < numPerTot__; p++)
          {
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ %d x_%d_%d_%d ",PESOS[0] * MAX(0,vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),c,p,r); 
           fprintf(f,"\n"); 
          }
         fprintf(f,"\n");
        }
    }
   fprintf(f,"\n\n\\Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
      {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ %d v_%d_%d ",PESOS[1],u,p); 
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\Dias minimos\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       fprintf(f,"+ %d w_%d ",PESOS[2],c); 
   fprintf(f,"\n\n\\Salas diferentes\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ %d y_%d_%d ",PESOS[3],c,r); 
       fprintf(f,"\n"); 
      }
   // ------------------ restri��es
   fprintf(f,"\nST\n");
   if(MODELO < 6)
    {
     fprintf(f,"\n\\R1 - Numero de aulas\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"+ x_%d_%d ",c,p); 
         fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
        }
     fprintf(f,"\n\\R5 - Dias minimos 1\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         for(int d = 0; d < numDia__; d++)
          {
           for(int q = 0; q < numPerDia__; q++)
             if(!matResDisPer__[c][(d*numPerDia__)+q])
               fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
           fprintf(f,"- z_%d_%d >= 0\n",c,d);
          }
     fprintf(f,"\n\\R6 - Dias minimos 2\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
        {
         for(int d = 0; d < numDia__; d++)
           fprintf(f,"+ z_%d_%d ",c,d);
         fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
        }
     if(MODELO != 5)
      {
       fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
       for(int u = 0; u < numTur__; u++)
         if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
          {
           for(int p = 0; p < numPerTot__; p++)
            {
             for(int k = 0; k < vetTurmas__[u].numDis_; k++)
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
                 fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
             fprintf(f,"- r_%d_%d = 0\n",u,p);
            }
           fprintf(f,"\n"); 
          }
       fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
       for(int u = 0; u < numTur__; u++)
         if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
          {
           for(int d = 0; d < numDia__; d++)
            {
             fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
             for(int q = 1; q < numPerDia__-1; q++)
               fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
             fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
            }
           fprintf(f,"\n"); 
          }
      }
     fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
     for(int t = 0; t < numPro__; t++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int c = 0; c < numDis__; c++)
           if(mr__.vetSpDis_[c] == sp)
             if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
               fprintf(f,"+ x_%d_%d ",c,p);
         fprintf(f,"<= 1\n");
        }
       fprintf(f,"\n");
      }
     fprintf(f,"\n\\Nova 1 - Aulas simultaneas na mesma sala\n");
     for(int r = 0; r < numSal__; r++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int c = 0; c < numDis__; c++)
           if(mr__.vetSpDis_[c] == sp)
             if(!matResDisPer__[c][p])
               fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
           fprintf(f,"<= 1\n"); 
        } 
       fprintf(f,"\n"); 
      }
     fprintf(f,"\n\\Nova 2 - Mesma aula em mais de uma sala\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
            {
             fprintf(f,"x_%d_%d ",c,p); 
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"- q_%d_%d_%d ",c,p,r); 
             fprintf(f,"= 0\n"); 
            }
         fprintf(f,"\n"); 
        }
     if(MODELO == 3)
      {
       fprintf(f,"\n\\Nova 3 - Numero de salas usadas por disciplina 1\n");
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
             fprintf(f,"- y_%d_%d >= 0\n",c,r); 
            } 
           fprintf(f,"\n"); 
          }
       fprintf(f,"\n\\Nova 4 - Numero de salas usadas por disciplina 2\n");
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"q_%d_%d_%d - y_%d_%d <= 0\n",c,p,r,c,r); 
            } 
           fprintf(f,"\n"); 
          }
      }
     else 
      {
       fprintf(f,"\n\\Nova 5 - Numero de salas usadas por disciplina\n");
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
             fprintf(f,"- %d y_%d_%d <= 0\n",vetDisciplinas__[c].numPer_,c,r); 
            } 
           fprintf(f,"\n"); 
          }
       if(MODELO == 5)
        {
         fprintf(f,"\n\\Nova 6 - Aulas isoladas\n");
         for(int u = 0; u < numTur__; u++)
           if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
            {
             for(int d = 0; d < numDia__; d++)
              {
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                {
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)])
                   fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__));
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+1])
                   fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+1);
                } 
               fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__));
               for(int q = 1; q < numPerDia__ - 1; q++)
                {
                 for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                  {
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q-1])
                     fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q-1);
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q])
                     fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q);
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q+1])
                     fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q+1);
                  } 
                 fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+q);
                }
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                {
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-2])
                   fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-2);
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-1])
                   fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-1);
                } 
               fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-1);
              }
             fprintf(f,"\n"); 
            }
         fprintf(f,"\n\\Nova 7 - Aulas de uma turma no mesmo periodo\n");
         for(int u = 0; u < numTur__; u++)
           if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
            {
             for(int p = 0; p < numPerTot__; p++)
              {
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
                   fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
               fprintf(f,"<= 1\n");
              }
             fprintf(f,"\n"); 
            }
        }
      }
    }
   else
    {
     fprintf(f,"\n\\R1 - Numero de aulas\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
        {
         for(int p = 0; p < numPerTot__; p++)
           for(int r = 0; r < numSal__; r++)
             if(!matResDisPer__[c][p])
               fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
         fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
        }
     fprintf(f,"\n\\R5 - Dias minimos 1\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         for(int d = 0; d < numDia__; d++)
          {
           for(int q = 0; q < numPerDia__; q++)
             if(!matResDisPer__[c][(d*numPerDia__)+q])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"+ x_%d_%d_%d ",c,(d*numPerDia__)+q,r);
           fprintf(f,"- z_%d_%d >= 0\n",c,d);
          }
     fprintf(f,"\n\\R6 - Dias minimos 2\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
        {
         for(int d = 0; d < numDia__; d++)
           fprintf(f,"+ z_%d_%d ",c,d);
         fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
        }
     if(MODELO != 8)
      {
       fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
       for(int u = 0; u < numTur__; u++)
         if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
          {
           for(int p = 0; p < numPerTot__; p++)
            {
             for(int k = 0; k < vetTurmas__[u].numDis_; k++)
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
                 for(int r = 0; r < numSal__; r++)
                   fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],p,r);
             fprintf(f,"- r_%d_%d = 0\n",u,p);
            }
           fprintf(f,"\n"); 
          }
       fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
       for(int u = 0; u < numTur__; u++)
         if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
          {
           for(int d = 0; d < numDia__; d++)
            {
             fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
             for(int q = 1; q < numPerDia__-1; q++)
               fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
             fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
            }
           fprintf(f,"\n"); 
          }
      }
     fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
     for(int t = 0; t < numPro__; t++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int c = 0; c < numDis__; c++)
           if(mr__.vetSpDis_[c] == sp)
             if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"+ x_%d_%d_%d ",c,p,r);
           fprintf(f,"<= 1\n");
          }
         fprintf(f,"\n");
      }
     fprintf(f,"\n\\Nova 1 - Aulas simultaneas na mesma sala\n");
     for(int r = 0; r < numSal__; r++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int c = 0; c < numDis__; c++)
           if(mr__.vetSpDis_[c] == sp)
             if(!matResDisPer__[c][p])
               fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
           fprintf(f,"<= 1\n"); 
          } 
         fprintf(f,"\n"); 
      }
     if(MODELO == 6)
      {
       fprintf(f,"\n\\Nova 3 - Numero de salas usadas por disciplina 1\n");
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
             fprintf(f,"- y_%d_%d >= 0\n",c,r); 
            } 
           fprintf(f,"\n"); 
          }
       fprintf(f,"\n\\Nova 4 - Numero de salas usadas por disciplina 2\n");
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"x_%d_%d_%d - y_%d_%d <= 0\n",c,p,r,c,r); 
            } 
           fprintf(f,"\n"); 
          }
      }
     else 
      {
       fprintf(f,"\n\\Nova 5 - Numero de salas usadas por disciplina\n");
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
          {
           for(int r = 0; r < numSal__; r++)
            {
             for(int p = 0; p < numPerTot__; p++)
               if(!matResDisPer__[c][p])
                 fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
             fprintf(f,"- %d y_%d_%d <= 0\n",vetDisciplinas__[c].numPer_,c,r); 
            } 
           fprintf(f,"\n"); 
          }
       if(MODELO == 8)
        {
         fprintf(f,"\n\\Nova 6 - Aulas isoladas\n");
         for(int u = 0; u < numTur__; u++)
           if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
            {
             for(int d = 0; d < numDia__; d++)
              {
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                {
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__),r);
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+1])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+1,r);
                } 
               fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__));
               for(int q = 1; q < numPerDia__ - 1; q++)
                {
                 for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                  {
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q-1])
                     for(int r = 0; r < numSal__; r++)
                       fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q-1,r);
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q])
                     for(int r = 0; r < numSal__; r++)
                       fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q,r);
                   if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q+1])
                     for(int r = 0; r < numSal__; r++)
                       fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q+1,r);
                  } 
                 fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+q);
                }
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                {
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-2])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-2,r);
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-1])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-1,r);
                } 
               fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-1);
              }
             fprintf(f,"\n"); 
            }
         fprintf(f,"\n\\Nova 7 - Aulas de uma turma no mesmo periodo\n");
         for(int u = 0; u < numTur__; u++)
           if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
            {
             for(int p = 0; p < numPerTot__; p++)
              {
               for(int k = 0; k < vetTurmas__[u].numDis_; k++)
                 if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
                   for(int r = 0; r < numSal__; r++)
                     fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],p,r);
               fprintf(f,"<= 1\n");
              }
             fprintf(f,"\n"); 
            }
        }
      }
    }
   // ------------------ limites
   fprintf(f,"\nBOUNDS\n");
   if(MODELO < 6)
    {
     fprintf(f,"\n\\Variaveis x\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
     fprintf(f,"\n\\Variaveis q\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"0 <= q_%d_%d_%d <= 1\n",c,p,r); 
    }
   else
    {
     fprintf(f,"\n\\Variaveis x\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"0 <= x_%d_%d_%d <= 1\n",c,p,r); 
    }
   fprintf(f,"\n\\Variaveis v\n");
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis w\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_); // ? ou numDia__ ou CPX_INFBOUND (+ correto, por�m resultado pior) 
   fprintf(f,"\n\\Variaveis y\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"0 <= y_%d_%d <= 1\n",c,r); 
   fprintf(f,"\n\\Variaveis z\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
   if((MODELO != 5) && (MODELO != 8))
    {
     fprintf(f,"\n\\Variaveis r\n");
     for(int u = 0; u < numTur__; u++)
       if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
         for(int p = 0; p < numPerTot__; p++)
           fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
    }
   // ------------------ vari�veis
   fprintf(f,"\nBINARIES\n");
   if(MODELO < 6)
    {
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"x_%d_%d\n",c,p);
     fprintf(f,"\n");
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"q_%d_%d_%d\n",c,p,r); 
     fprintf(f,"\n");
    }
   else
    {
     for(int c = 0; c < numDis__; c++) 
       if(mr__.vetSpDis_[c] == sp)
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"x_%d_%d_%d\n",c,p,r); 
     fprintf(f,"\n");
    }
   for(int u = 0; u < numTur__; u++)
     if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"y_%d_%d\n",c,r); 
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
   fprintf(f,"\n");
   if((MODELO != 5) && (MODELO != 8))
    {
     for(int u = 0; u < numTur__; u++)
       if((vetAux[u]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]] == sp))
         for(int p = 0; p < numPerTot__; p++)
           fprintf(f,"r_%d_%d\n",u,p);
     fprintf(f,"\n");
    } 
   fprintf(f,"\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nEND");
   fclose(f);
  }
 h2 = time(NULL);
 mr__.temSub_ = difftime(h2,h1);
}
//------------------------------------------------------------------------------

//==============================================================================


//=================================== CPLEX ====================================

//------------------------------------------------------------------------------
void exeCntVarRes(char *id)
{
 FILE *f;
 int sts;
 char arq[50];
 strcpy(arq,"Saidas\\");
 strcat(arq,id);
 strcat(arq,".lp");
 if(MODELO == 0)
   monModBurEal10(arq,0);
 else if((MODELO == 1) || (MODELO == -1))
   monModLacLub12_F1(arq,0,0);
 else if((MODELO == 2) || (MODELO == -2))
   monModLacLub12_F1(arq,0,1);
 else
   monModNovo(arq,0);
 vetCpx__[0].lp_ = CPXcreateprob(env__,&sts,"");
 CPXreadcopyprob(env__,vetCpx__[0].lp_,arq,NULL);
 vetCpx__[0].numVar_ = CPXgetnumcols(env__,vetCpx__[0].lp_);
 vetCpx__[0].numRes_ = CPXgetnumrows(env__,vetCpx__[0].lp_);
 CPXfreeprob(env__,&vetCpx__[0].lp_);
 for(int i = 0; i < 7; i++)
   vetQtdVar__[7] += vetQtdVar__[i];
 for(int i = 0; i < 10; i++)
   vetQtdRes__[10] += vetQtdRes__[i];
 strcpy(arq,"Saidas\\");
 strcat(arq,SAIDA);
 f = fopen(arq,"at");
 fprintf(f,"%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",
         INST,MODELO,vetCpx__[0].numVar_,vetCpx__[0].numRes_,vetQtdVar__[0],vetQtdVar__[1],vetQtdVar__[2],
         vetQtdVar__[3],vetQtdVar__[4],vetQtdVar__[5],vetQtdVar__[6],vetQtdVar__[7],vetQtdRes__[0],
         vetQtdRes__[1],vetQtdRes__[2],vetQtdRes__[3],vetQtdRes__[4],vetQtdRes__[5],vetQtdRes__[6],
         vetQtdRes__[7],vetQtdRes__[8],vetQtdRes__[9],vetQtdRes__[10],vetQtdRes__[11]);
 fclose(f);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void exeCpx(char *id,Solucao &s,const int &rl)
{
 FILE *f;
 int sts;
 time_t h1,h2;
 char arq[50];
 strcpy(arq,"Saidas\\");
 strcat(arq,id);
 if(rl)
   strcat(arq,"-RLI");
 strcat(arq,".lp");
 if(MODELO == 0)
   monModBurEal10(arq,rl);
 else if((MODELO == 1) || (MODELO == -1))
   monModLacLub12_F1(arq,rl,0);
 else if((MODELO == 2) || (MODELO == -2))
   monModLacLub12_F1(arq,rl,1);
 else
   monModNovo(arq,rl);
 vetCpx__[0].lp_ = CPXcreateprob(env__,&sts,"");
 CPXreadcopyprob(env__,vetCpx__[0].lp_,arq,NULL);
 vetCpx__[0].numVar_ = CPXgetnumcols(env__,vetCpx__[0].lp_);
 vetCpx__[0].numRes_ = CPXgetnumrows(env__,vetCpx__[0].lp_);
 if(rl)
  {
   h1 = time(NULL);
   CPXprimopt(env__,vetCpx__[0].lp_);
   h2 = time(NULL);
   CPXgetobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limInf_);
   if((PESOS[3] != 0) && ((MODELO == 0) || (MODELO > 2)))
     vetCpx__[0].limInf_ -= numDis__;
   sprintf(arq,"Saidas\\%s-RLI.res",id);
  }
 else
  {
   if(OPT_CPX == 1) 
    {
     CPXsetintparam(env__,CPX_PARAM_MIPORDTYPE,1);
     // 0 - default 	- Do not generate a priority order
     // 1 -	CPX_MIPORDER_COST -	Use decreasing cost
     // 2 -	CPX_MIPORDER_BOUNDS -	Use increasing bound range
     // 3 -	CPX_MIPORDER_SCALEDCOST - Use increasing cost per coefficient count 
/*
     CPXsetintparam(env__,CPX_PARAM_MIPORDIND,CPX_ON);
     int cnt = 0;
     int *vetInd = new int[vetCpx__[0].numVar_];
     int *vetPri = new int[vetCpx__[0].numVar_];
     int *vetDir = new int[vetCpx__[0].numVar_];
     if(MODELO < 6)
      {
       // x
       for(int c = 0; c < numDis__; c++)
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
            {
             vetInd[cnt] = cnt;
             vetPri[cnt] = 1;
             vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
             cnt++;
            }
      // q
      for(int c = 0; c < numDis__; c++)
        for(int p = 0; p < numPerTot__; p++)
          if(!matResDisPer__[c][p])
            for(int r = 0; r < numSal__; r++)
             {
              vetInd[cnt] = cnt;
              vetPri[cnt] = 0;
              vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
              cnt++;
             }
      // v
      for(int u = 0; u < numTur__; u++)
        for(int p = 0; p < numPerTot__; p++)
         {
          vetInd[cnt] = cnt;
          vetPri[cnt] = 0;
          vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
          cnt++;
         }
      // w
      for(int c = 0; c < numDis__; c++)
       {
        vetInd[cnt] = cnt;
        vetPri[cnt] = 0;
        vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
        cnt++;
       }     
     // y
     for(int c = 0; c < numDis__; c++)
       for(int r = 0; r < numSal__; r++)
        {
         vetInd[cnt] = cnt;
         vetPri[cnt] = 0;
         vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
         cnt++;
        }
      }
     else
      {
       // x
       for(int c = 0; c < numDis__; c++)
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             for(int r = 0; r < numSal__; r++)
              {
               vetInd[cnt] = cnt;
               vetPri[cnt] = 1;
               vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
               cnt++;
              }
       // v
       for(int u = 0; u < numTur__; u++)
         for(int p = 0; p < numPerTot__; p++)
          {
           vetInd[cnt] = cnt;
           vetPri[cnt] = 0;
           vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
           cnt++;
          }
       // w
       for(int c = 0; c < numDis__; c++)
        {
         vetInd[cnt] = cnt;
         vetPri[cnt] = 0;
         vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
          cnt++;
        }     
       // y
       for(int c = 0; c < numDis__; c++)
         for(int r = 0; r < numSal__; r++)
          {
          vetInd[cnt] = cnt;
          vetPri[cnt] = 0;
          vetDir[cnt] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
          cnt++;
         }
      }
     for(int i = cnt; i < vetCpx__[0].numVar_; i++)
      {
       vetInd[i] = i;
       vetPri[i] = 0;
       vetDir[i] = CPX_BRANCH_GLOBAL; // CPX_BRANCH_GLOBAL;	CPX_BRANCH_DOWN; CPX_BRANCH_UP
      }
     //CPXcopyorder(env__,vetCpx__[0].lp_,cnt,vetInd,vetPri,vetDir);
     CPXcopyorder(env__,vetCpx__[0].lp_,cnt,vetInd,vetPri,NULL);
     delete[] vetInd;
     delete[] vetPri;
     delete[] vetDir;
*/
    }
   if(MODELO < 0)
     if(TEM_LIM != 0)
       CPXsetdblparam(env__,CPX_PARAM_TILIM,0.8*TEM_LIM);
   h1 = time(NULL);
   CPXmipopt(env__,vetCpx__[0].lp_);
   h2 = time(NULL);
   vetCpx__[0].tempo_ = difftime(h2,h1);
   CPXgetmipobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limSup_);
   CPXgetbestobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limInf_);
   if((PESOS[3] != 0) && ((MODELO == 0) || (MODELO > 2)))
    {
     vetCpx__[0].limSup_ -= numDis__;
     vetCpx__[0].limInf_ -= numDis__;
    }
   vetCpx__[0].gap_ = INT_MAX;
   if(vetCpx__[0].limInf_ != 0)
     vetCpx__[0].gap_ = ((vetCpx__[0].limSup_ - vetCpx__[0].limInf_)/vetCpx__[0].limSup_)*100;
   if(vetCpx__[0].limSup_ - vetCpx__[0].limInf_ <= 0.005)
     vetCpx__[0].gap_ = 0;
   if((MODELO == 3) || (MODELO == 4) || (MODELO == 5))
     CPXgetmipx(env__,vetCpx__[0].lp_,s.vetSol_,0,vetQtdVar__[0]+vetQtdVar__[6]-1);
   else
     CPXgetmipx(env__,vetCpx__[0].lp_,s.vetSol_,0,vetQtdVar__[0]-1);
   if(MODELO < 0)
    {
     exeF2(id,s);
     sprintf(arq,"Saidas\\%sF.res",id);
    } 
   else
    {
     sprintf(arq,"Saidas\\%s.res",id);
     cpxToSol(s);
    } 
  }
 CPXfreeprob(env__,&vetCpx__[0].lp_);
 f = fopen(arq,"w");
 escResCpx(f,rl);
 if(!rl)
   escSolucao(s,f);
 fclose(f);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void exeF2(char *id,Solucao &s)
{
 time_t h1,h2;
 char arq[50];
 int sts,per,dia;
 double *vetSol;
 bool **matVarX;
 bool ***matVarY;
 bool ***matDisPerSal;
 strcpy(arq,"Saidas\\");
 strcat(arq,id);
 strcat(arq,"F.lp");
 // montar a matriz com as vari�veis x
 matVarX = new bool*[numDis__];
 for(int c = 0; c < numDis__; c++)
   matVarX[c] = new bool[numPerTot__];
 sts = 0;
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     matVarX[c][p] = 0;
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
      {
       if(s.vetSol_[sts] >= 0.5)
         matVarX[c][p] = 1;
       sts++;
      }
 // montar a matriz com as vari�veis y
 vetSol = new double[vetQtdVar__[3]];
 CPXgetmipx(env__,vetCpx__[0].lp_,vetSol,vetQtdVar__[0],vetQtdVar__[0]+vetQtdVar__[3]-1);
 matVarY = new bool**[numPerTot__];
 for(int p = 0; p < numPerTot__; p++)
  {
   matVarY[p] = new bool*[numSalDif__];
   for(int s = 0; s < numSalDif__; s++)
     matVarY[p][s] = new bool[numDis__];
  }
 for(int p = 0; p < numPerTot__; p++)
   for(int s = 0; s < numSalDif__; s++)
     for(int c = 0; c < numDis__; c++)
       matVarY[p][s][c] = 0;
 sts = 0;
 for(int p = 0; p < numPerTot__; p++)
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
      if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
       {
        if(vetSol[sts] >= 0.5)
          matVarY[p][s][c] = 1;
        sts++;
       }
 delete[] vetSol;
 // montar a matriz com as vari�veis uv
 matDisPerSal = new bool**[numDis__];
 for(int c = 0; c < numDis__; c++)
  {
   matDisPerSal[c] = new bool*[numPerTot__];
   for(int p = 0; p < numPerTot__; p++)
     matDisPerSal[c][p] = new bool[numSal__];
  }
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     for(int r = 0; r < numSal__; r++)
       matDisPerSal[c][p][r] = 0;
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     if(matVarX[c][p])
      {
       for(int s = 0; s < (numSalDif__-1); s++)
        {
         if(!matVarY[p][s][c])
          {
           for(int r = 0; r < numSal__; r++)
             if(vetDisciplinas__[c].numAlu_ <= vetSalas__[r].capacidade_)
               matDisPerSal[c][p][r] = 1;
          }
         else
          {
           sts = 0;
           for(int r = 0; r < numSal__; r++)
             if((vetSalas__[r].capacidade_ > sts) && (vetSalas__[r].capacidade_ < vetDisciplinas__[c].numAlu_))
               sts = vetSalas__[r].capacidade_;
           for(int r = 0; r < numSal__; r++)
             if((vetDisciplinas__[c].numAlu_ > vetSalas__[r].capacidade_) && (vetSalas__[r].capacidade_ == sts))
               matDisPerSal[c][p][r] = 1;
          }
        }
      } 
  }
 // montar e resolver o modelo da FASE 2
 if(TEM_LIM != 0)
   CPXsetdblparam(env__,CPX_PARAM_TILIM,TEM_LIM-vetCpx__[0].tempo_);
 monModLacLub12_F2(arq,matDisPerSal);
 vetCpx__[1].lp_ = CPXcreateprob(env__,&sts,"");
 CPXreadcopyprob(env__,vetCpx__[1].lp_,arq,NULL);
 vetCpx__[1].numVar_ = CPXgetnumcols(env__,vetCpx__[1].lp_);
 vetCpx__[1].numRes_ = CPXgetnumrows(env__,vetCpx__[1].lp_);
 h1 = time(NULL);
 CPXmipopt(env__,vetCpx__[1].lp_);
 h2 = time(NULL);
 vetCpx__[1].tempo_ = difftime(h2,h1);
 CPXgetmipobjval(env__,vetCpx__[1].lp_,&vetCpx__[1].limSup_);
 CPXgetbestobjval(env__,vetCpx__[1].lp_,&vetCpx__[1].limInf_);
 if(PESOS[3] != 0)
  {
   vetCpx__[1].limSup_ -= numDis__;
   vetCpx__[1].limInf_ -= numDis__;
  }
 vetCpx__[1].gap_ = INT_MAX;
 if(vetCpx__[1].limInf_ != 0)
   vetCpx__[1].gap_ = ((vetCpx__[1].limSup_ - vetCpx__[1].limInf_)/vetCpx__[1].limSup_)*100;
 if(vetCpx__[1].limSup_ - vetCpx__[1].limInf_ <= 0.005)
   vetCpx__[1].gap_ = 0;
 // pegar a solu��o da FASE 2
 memset(s.matSolSal_,-1,sizeof(s.matSolSal_));
 memset(s.matSolTur_,-1,sizeof(s.matSolTur_));
 vetSol = new double[vetCpx__[1].numVar_-(numDis__*numSal__)];
 CPXgetmipx(env__,vetCpx__[1].lp_,vetSol,(numDis__*numSal__),vetCpx__[1].numVar_-1);
 sts = 0;
 for(int c = 0; c < numDis__; c++)
   for(int r = 0; r < numSal__; r++)
     for(int p = 0; p < numPerTot__; p++)
       if(matDisPerSal[c][p][r])
        {
         if(vetSol[sts] >= 0.5)
          {
           dia = p / numPerDia__;
           per = p % numPerDia__;
           if(s.matSolSal_[per][dia][r] != -1)
             printf("\n\nERRO - SALA %d - p%d d%d c%d!\n\n",r,per,dia,c);
           else
             s.matSolSal_[per][dia][r] = c;
           for(int u = 0; u < numTur__; u++)
            {
             if(matDisTur__[c][u])
              {
               if(s.matSolTur_[per][dia][u] != -1)
                 printf("\n\nERRO - TURMA %d - p%d d%d c%d!\n\n",u,per,dia,c);
               else
                 s.matSolTur_[per][dia][u] = c;
              }
            }
          } 
         sts++;
        }
 delete[] vetSol;
 // limpar a mem�ria
 for(int c = 0; c < numDis__; c++)
   delete[] matVarX[c];
 delete[] matVarX;
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int s = 0; s < numSalDif__; s++)
     delete[] matVarY[p][s];
   delete[] matVarY[p];
  }
 delete[] matVarY;
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     delete[] matDisPerSal[c][p];
   delete[] matDisPerSal[c];
  }
 delete[] matDisPerSal;
 CPXfreeprob(env__,&vetCpx__[1].lp_);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void escResCpx(FILE *f,const int &rl)
{
 if(f == NULL)
   f = stdout;
 fprintf(f,"\n< ----------------------------------- CPLEX ---------------------------------- >\n");
 if(MODELO == -1)
   fprintf(f,"Modelo utilizado...............................................: Lach e Lubbecke (2012) - duas fases\n");
 else if(MODELO == -2)
   fprintf(f,"Modelo utilizado...............................................: Lach e Lubbecke (2012) - adaptado - duas fases\n");
 else if(MODELO == 0)
   fprintf(f,"Modelo utilizado...............................................: Burke et al. (2010)\n");
 else if(MODELO == 1)
   fprintf(f,"Modelo utilizado...............................................: Lach e Lubbecke (2012)\n");
 else if(MODELO == 2)
   fprintf(f,"Modelo utilizado...............................................: Lach e Lubbecke (2012) - adaptado\n");
 else if(MODELO == 3)
   fprintf(f,"Modelo utilizado...............................................: Novo 1\n");
 else if(MODELO == 4)
   fprintf(f,"Modelo utilizado...............................................: Novo 2\n");
 else if(MODELO == 5)
   fprintf(f,"Modelo utilizado...............................................: Novo 3\n");
 else if(MODELO == 6)
   fprintf(f,"Modelo utilizado...............................................: Novo 4\n");
 else if(MODELO == 7)
   fprintf(f,"Modelo utilizado...............................................: Novo 5\n");
 else
   fprintf(f,"Modelo utilizado...............................................: Novo 6\n");
 if(PESOS[3] == 0)
   fprintf(f,"Salas diferentes ignoradas (peso 0 na FO)......................: SIM\n");
 else
   fprintf(f,"Salas diferentes ignoradas (peso 0 na FO)......................: NAO\n");
 if(!rl)
  {
   if(OPT_CPX == 0)
     fprintf(f,"Cplex usado com parametros default.............................: SIM\n");
   else
     fprintf(f,"Cplex usado com parametros default.............................: NAO\n");
  } 
 fprintf(f,"Numero de variaveis............................................: %d\n",vetCpx__[0].numVar_);
 fprintf(f,"Numero de restricoes...........................................: %d\n",vetCpx__[0].numRes_);
 if(rl)
  {
   fprintf(f,"Valor do melhor no (limitante inferior)........................: %.2f\n",vetCpx__[0].limInf_);
   fprintf(f,"Tempo (segundos)...............................................: %.2f\n",vetCpx__[0].tempo_);
  }
 else
  {
   fprintf(f,"Valor da solucao (limitante superior)..........................: %.2f\n",vetCpx__[0].limSup_);
   fprintf(f,"Valor do melhor no (limitante inferior)........................: %.2f\n",vetCpx__[0].limInf_);
   fprintf(f,"GAP............................................................: %.2f\n",vetCpx__[0].gap_);
   fprintf(f,"Tempo (segundos)...............................................: %.2f\n",vetCpx__[0].tempo_);
   if(MODELO < 0)
    {
     fprintf(f,"\n\nNumero de variaveis - FASE 2...................................: %d\n",vetCpx__[1].numVar_);
     fprintf(f,"Numero de restricoes - FASE 2..................................: %d\n",vetCpx__[1].numRes_);
     fprintf(f,"Valor da solucao (limitante superior) - FASE 2.................: %.2f\n",vetCpx__[1].limSup_);
     fprintf(f,"Valor do melhor no (limitante inferior) - FASE 2...............: %.2f\n",vetCpx__[1].limInf_);
     fprintf(f,"GAP - FASE 2...................................................: %.2f\n",vetCpx__[1].gap_);
     fprintf(f,"Tempo (segundos) - FASE 2......................................: %.2f\n",vetCpx__[1].tempo_);
     fprintf(f,"\n\nNumero TOTAL de variaveis......................................: %d\n",vetCpx__[0].numVar_+vetCpx__[1].numVar_);
     fprintf(f,"Numero TOTAL de restricoes.....................................: %d\n",vetCpx__[0].numRes_+vetCpx__[1].numRes_);
     fprintf(f,"Valor FINAL da solucao (limitante superior)....................: %.2f\n",vetCpx__[0].limSup_+vetCpx__[1].limSup_);
     fprintf(f,"Valor FINAL do melhor no (limitante inferior)..................: %.2f\n",vetCpx__[0].limInf_);
     fprintf(f,"GAP FINAL......................................................: %.2f\n",
             (((vetCpx__[0].limSup_+vetCpx__[1].limSup_)-vetCpx__[0].limInf_)/(vetCpx__[0].limSup_+vetCpx__[1].limSup_))*100);
     fprintf(f,"Tempo TOTAL (segundos).........................................: %.2f\n",vetCpx__[0].tempo_+vetCpx__[1].tempo_);
    }
  }
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void cpxToSol(Solucao &s)
{
 int pos,dia,per;
 memset(s.matSolSal_,-1,sizeof(s.matSolSal_));
 memset(s.matSolTur_,-1,sizeof(s.matSolTur_));
 if(MODELO == 0)
  {
   pos = 0;
   for(int r = 0; r < numSal__; r++)
     for(int p = 0; p < numPerTot__; p++)
       for(int c = 0; c < numDis__; c++)
        {
         if(s.vetSol_[pos] >= 0.5)
          {
           dia = p / numPerDia__;
           per = p % numPerDia__;
           if(s.matSolSal_[per][dia][r] != -1)
             printf("\n\nERRO - SALA %d - p%d d%d c%d!\n\n",r,per,dia,c);
           else
             s.matSolSal_[per][dia][r] = c;
           for(int u = 0; u < numTur__; u++)
            {
             if(matDisTur__[c][u])
              {
               if(s.matSolTur_[per][dia][u] != -1)
                 printf("\n\nERRO - TURMA %d - p%d d%d c%d!\n\n",u,per,dia,c);
               else
                 s.matSolTur_[per][dia][u] = c;
              }
            }
          }
         pos++;
        } 
  }
 else if(MODELO < 6)
  {
   pos = 0;
   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
        {
         if(s.vetSol_[pos] >= 0.5)
          {
           dia = p / numPerDia__;
           per = p % numPerDia__;
           for(int u = 0; u < numTur__; u++)
            {
             if(matDisTur__[c][u])
              {
               if(s.matSolTur_[per][dia][u] != -1)
                 printf("\n\nERRO - TURMA %d - p%d d%d c%d!\n\n",u,per,dia,c);
               else
                 s.matSolTur_[per][dia][u] = c;
              }
            }
          } 
         pos++;
        }
   if(MODELO > 2)
    {
     for(int c = 0; c < numDis__; c++)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           for(int r = 0; r < numSal__; r++)
            {
             if(s.vetSol_[pos] >= 0.5)
              {
               dia = p / numPerDia__;
               per = p % numPerDia__;
               if(s.matSolSal_[per][dia][r] != -1)
                 printf("\n\nERRO - SALA %d - p%d d%d c%d!\n\n",r,per,dia,c);
               else
                 s.matSolSal_[per][dia][r] = c;
              }
             pos++;
            } 
    }
  }
 else
  {
   pos = 0;
   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
         for(int r = 0; r < numSal__; r++)
          {
           if(s.vetSol_[pos] >= 0.5)
            {
             dia = p / numPerDia__;
             per = p % numPerDia__;
             if(s.matSolSal_[per][dia][r] != -1)
               printf("\n\nERRO - SALA %d - p%d d%d c%d!\n\n",r,per,dia,c);
             else
               s.matSolSal_[per][dia][r] = c;
             for(int u = 0; u < numTur__; u++)
              {
               if(matDisTur__[c][u])
                {
                 if(s.matSolTur_[per][dia][u] != -1)
                   printf("\n\nERRO - TURMA %d - p%d d%d c%d!\n\n",u,per,dia,c);
                 else
                   s.matSolTur_[per][dia][u] = c;
                }
              }
            }
           pos++;
          } 
  }
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monModBurEal10(char *arq,const int &rl)
{
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 fprintf(f,"\n\\Capacidade das salas\n");
 for(int r = 0; r < numSal__; r++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
      if(vetDisciplinas__[c].numAlu_ > vetSalas__[r].capacidade_)
        fprintf(f,"+ %d x_%d_%d_%d ",PESOS[0]*(vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),p,r,c);
      else
        fprintf(f,"+ 0 x_%d_%d_%d ",p,r,c);
     fprintf(f,"\n");
    }
  }
 fprintf(f,"\n\n\\Aulas isoladas\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int s = 0; s < numPerDia__; s++)
       fprintf(f,"+ %d z_%d_%d_%d ",PESOS[1],u,d,s);
     fprintf(f,"\n");
    }
  }
 fprintf(f,"\n\\Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"+ %d q_%d ",PESOS[2],c);
 fprintf(f,"\n\n\\Salas diferentes\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"+ %d y_%d_%d ",PESOS[3],r,c);
   fprintf(f,"\n");
  }
 // ------------------ restri��es
 memset(vetQtdRes__,0,sizeof(vetQtdRes__));
 fprintf(f,"\nST\n");
 fprintf(f,"\n\\ ------------------------------------ HARD------------------------------------\n");
 fprintf(f,"\n\\R1 - Numero de aulas\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     for(int r = 0; r < numSal__; r++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
    fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_);
    vetQtdRes__[0]++;
  }
 fprintf(f,"\n\\R2 - Aulas na mesma sala no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int r = 0; r < numSal__; r++)
    {
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n"); 
     vetQtdRes__[1]++;
    }
 fprintf(f,"\n\\R3 - Aulas de uma disciplina no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int c = 0; c < numDis__; c++)
    {
     for(int r = 0; r < numSal__; r++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
     vetQtdRes__[2]++;
    }
 fprintf(f,"\n\\R4 - Aulas de um professor no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int t = 0; t < numPro__; t++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
         if(vetDisciplinas__[c].professor_ == t)
           fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
     vetQtdRes__[3]++; 
    }
 fprintf(f,"\n\\R5 - Aulas de uma turma no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int u = 0; u < numTur__; u++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
         if(matDisTur__[c][u])
           fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
     vetQtdRes__[4]++;
    }
 fprintf(f,"\n\\R6 - Restricoes de oferta (alocacao)\n");
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(matResDisPer__[c][p])
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
       fprintf(f,"= 0\n");
       vetQtdRes__[5]++;
      }
 fprintf(f,"\n\\ ------------------------------------ SOFT------------------------------------\n");
 fprintf(f,"\n\\R7 - Dias m�nimos 1\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
       fprintf(f,"- v_%d_%d <= 0\n",d,c);
       vetQtdRes__[6]++;
      }
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R8 - Dias m�nimos 2\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"- v_%d_%d >= 0\n",d,c);
     vetQtdRes__[6]++;
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R9 - Dias minimos 3\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"+ v_%d_%d ",d,c);
   fprintf(f,"+ q_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
   vetQtdRes__[6]++;
  }
 fprintf(f,"\n\\R10 a R13+#PER_DIA - Aulas isoladas no horario das turmas\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int c = 0; c < numDis__; c++)
       if(matDisTur__[c][u])
        {
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__),r,c,(d*numPerDia__)+1,r,c);
        }
     fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,0);
     vetQtdRes__[7]++; 
    }
  }
 fprintf(f,"\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int c = 0; c < numDis__; c++)
       if(matDisTur__[c][u])
        {
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+numPerDia__-1,r,c,(d*numPerDia__)+numPerDia__-2,r,c);
        }
     fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,1);
     vetQtdRes__[7]++;
    }
  }
 fprintf(f,"\n");
 for(int s = 2; s < numPerDia__; s++)
  {
   for(int u = 0; u < numTur__; u++)
    {
     for(int d = 0; d < numDia__; d++)
      {
       for(int c = 0; c < numDis__; c++)
         if(matDisTur__[c][u])
          {
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+s-1,r,c,(d*numPerDia__)+s-2,r,c,(d*numPerDia__)+s,r,c);
          }
       fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,s);
       vetQtdRes__[7]++;
      }
    }
  fprintf(f,"\n");
 }
 fprintf(f,"\n\\R14 - Salas utilizadas por disciplina 1\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int r = 0; r < numSal__; r++)
     for(int c = 0; c < numDis__; c++)
      { 
       fprintf(f,"x_%d_%d_%d - y_%d_%d <= 0\n",p,r,c,r,c);
       vetQtdRes__[8]++;
      }
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R15 - Salas utilizadas por disciplina 2\n");
 for(int r = 0; r < numSal__; r++)
  {
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"- y_%d_%d >= 0\n",r,c);
     vetQtdRes__[8]++; 
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Variaveis x\n");
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerTot__; p++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"0 <= x_%d_%d_%d <= 1\n",p,r,c);
 fprintf(f,"\n\\Variaveis z\n");
 for(int u = 0; u < numTur__; u++)
   for(int d = 0; d < numDia__; d++)
     for(int s = 0; s < numPerDia__; s++)
        fprintf(f,"0 <= z_%d_%d_%d <= 1\n",u,d,s);
 fprintf(f,"\n\\Variaveis q\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"0 <= q_%d <= %d\n",c,numDia__);
 fprintf(f,"\n\\Variaveis y\n");
 for(int r = 0; r < numSal__; r++)
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"0 <= y_%d_%d <= 1\n",r,c);
 fprintf(f,"\n\\Variaveis v\n");
 for(int d = 0; d < numDia__; d++)
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"0 <= v_%d_%d <= 1\n",d,c);
 if(!rl)
  {
   memset(vetQtdVar__,0,sizeof(vetQtdVar__));
   fprintf(f,"\nBINARIES\n");
   for(int r = 0; r < numSal__; r++)
     for(int p = 0; p < numPerTot__; p++)
       for(int c = 0; c < numDis__; c++)
        {
         fprintf(f,"x_%d_%d_%d\n",p,r,c);
         vetQtdVar__[0]++;
        }
   for(int u = 0; u < numTur__; u++)
     for(int d = 0; d < numDia__; d++)
       for(int s = 0; s < numPerDia__; s++)
        {
         fprintf(f,"z_%d_%d_%d\n",u,d,s);
         vetQtdVar__[1]++;
        }
   for(int r = 0; r < numSal__; r++)
     for(int c = 0; c < numDis__; c++)
      {
       fprintf(f,"y_%d_%d\n",r,c);
       vetQtdVar__[3]++;
      }
   for(int d = 0; d < numDia__; d++)
     for(int c = 0; c < numDis__; c++)
      {
       fprintf(f,"v_%d_%d\n",d,c);
       vetQtdVar__[4]++;
      }
   fprintf(f,"\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"q_%d\n",c);
   vetQtdVar__[2] = numDis__;
  }
 fprintf(f,"\nEND");
 fclose(f);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monModLacLub12_F1(char *arq,const int &rl,const int &adp)
{
 int aux;
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 fprintf(f,"\n\\Variaveis x\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ 0 x_%d_%d ",c,p);
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\Capacidade das salas\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
         if(adp)
           fprintf(f,"+ %d y_%d_%d_%d ",PESOS[0] * (vetDisciplinas__[c].numAlu_ - vetTamSal__[s]),s,c,p);
         else
           fprintf(f,"+ %d y_%d_%d_%d ",PESOS[0] * MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"+ %d w_%d ",PESOS[2],c); 
 fprintf(f,"\n\n\\Aulas isoladas\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"+ %d v_%d_%d ",PESOS[1],u,p); 
   fprintf(f,"\n"); 
  }
 // ------------------ restri��es
 memset(vetQtdRes__,0,sizeof(vetQtdRes__));
 fprintf(f,"\nST\n");
 fprintf(f,"\n\\R1 - Numero de aulas\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ x_%d_%d ",c,p); 
   fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
   vetQtdRes__[0]++;
  }
 fprintf(f,"\n\\R2 - Numero de salas\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int c = 0; c < numDis__; c++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ x_%d_%d ",c,p); 
   fprintf(f,"<= %d\n",numSal__); 
   vetQtdRes__[1]++;
  }
 fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
 for(int s = 0; s < (numSalDif__-1); s++)
  {
   for(int c = 0; c < numDis__; c++)
     if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
          { 
           fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
           vetQtdRes__[9]++;
          }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
 for(int s = 0; s < (numSalDif__-1); s++)
  {
   aux = 0;
   for(int i = 0; i < numSal__; i++)
     if(vetSalas__[i].capacidade_ > vetTamSal__[s])
       aux++;
   for(int p = 0; p < numPerTot__; p++) 
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
         fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
     fprintf(f,"<= %d\n",aux); 
     vetQtdRes__[9]++;
    } 
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R5 - Dias minimos 1\n");
 for(int c = 0; c < numDis__; c++)
   for(int d = 0; d < numDia__; d++)
    {
     for(int q = 0; q < numPerDia__; q++)
       if(!matResDisPer__[c][(d*numPerDia__)+q])
         fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
     fprintf(f,"- z_%d_%d >= 0\n",c,d);
     vetQtdRes__[6]++;
    }
 fprintf(f,"\n\\R6 - Dias minimos 2\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"+ z_%d_%d ",c,d);
   fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
   vetQtdRes__[6]++;
  }
 fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int k = 0; k < vetTurmas__[u].numDis_; k++)
       if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
         fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
     fprintf(f,"- r_%d_%d = 0\n",u,p);
     vetQtdRes__[7]++;
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
     vetQtdRes__[7]++;
     for(int q = 1; q < numPerDia__-1; q++)
      {
       fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
       vetQtdRes__[7]++;
      }
     fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
     vetQtdRes__[7]++;
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
 for(int t = 0; t < numPro__; t++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
         fprintf(f,"+ x_%d_%d ",c,p);
     fprintf(f,"<= 1\n");
     vetQtdRes__[3]++;
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Variaveis x\n");
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
 fprintf(f,"\n\\Variaveis y\n");
 for(int s = 0; s < (numSalDif__-1); s++)
   for(int c = 0; c < numDis__; c++)
     if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
 fprintf(f,"\n\\Variaveis w\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
 fprintf(f,"\n\\Variaveis v\n");
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
 fprintf(f,"\n\\Variaveis z\n");
 for(int c = 0; c < numDis__; c++)
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
 fprintf(f,"\n\\Variaveis r\n");
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
 if(!rl)
  {
   memset(vetQtdVar__,0,sizeof(vetQtdVar__));
   fprintf(f,"\nBINARIES\n");
   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
        {
         fprintf(f,"x_%d_%d\n",c,p);
         vetQtdVar__[0]++;
        } 
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
            {
             fprintf(f,"y_%d_%d_%d\n",s,c,p);
             vetQtdVar__[3]++;
            } 
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     for(int p = 0; p < numPerTot__; p++)
      {
       fprintf(f,"v_%d_%d\n",u,p);
       vetQtdVar__[1]++;
      } 
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     for(int d = 0; d < numDia__; d++)
      {
       fprintf(f,"z_%d_%d\n",c,d);
       vetQtdVar__[4]++;
      } 
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     for(int p = 0; p < numPerTot__; p++)
      {
       fprintf(f,"r_%d_%d\n",u,p);
       vetQtdVar__[5]++;
      } 
   fprintf(f,"\n");
   fprintf(f,"\n\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"w_%d\n",c);
   vetQtdVar__[2] = numDis__;
  }
 fprintf(f,"\nEND");
 fclose(f);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monModLacLub12_F2(char *arq,bool ***matDisPerSal)
{
 int flag;
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 fprintf(f,"\n\\Variaveis y\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"+ y_%d_%d ",c,r);
   fprintf(f,"\n"); 
  }
 // ------------------ restri��es
 fprintf(f,"\nST\n");
 fprintf(f,"\n\\R19 - Salas diferentes\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int r = 0; r < numSal__; r++)
    {
     flag = 0; 
     for(int p = 0; p < numPerTot__; p++)
       if(matDisPerSal[c][p][r])
        {
         fprintf(f,"+ u_%d_%d_v_%d_%d ",c,p,r,p); 
         flag = 1;
        }
     if(flag)
       fprintf(f,"- %d y_%d_%d <= 0\n",numPerTot__,c,r); 
    } 
  }
 fprintf(f,"\n\\R20 - Apenas uma sala por disciplina por periodo\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     flag = 0;
     for(int r = 0; r < numSal__; r++)
       if(matDisPerSal[c][p][r])
        {
         fprintf(f,"+ u_%d_%d_v_%d_%d ",c,p,r,p); 
         flag = 1;
        }
     if(flag)
       fprintf(f,"= 1\n"); 
    } 
  }
 fprintf(f,"\n\\R21 - Apenas uma disciplina por sala em cada periodo\n");
 for(int r = 0; r < numSal__; r++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     flag = 0;
     for(int c = 0; c < numDis__; c++)
       if(matDisPerSal[c][p][r])
        {
         fprintf(f,"+ u_%d_%d_v_%d_%d ",c,p,r,p); 
         flag++;
        }
     if(flag)
       fprintf(f,"<= 1\n"); 
    } 
  }
 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Variaveis y\n");
 for(int c = 0; c < numDis__; c++)
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"0 <= y_%d_%d <= 1\n",c,r);
 fprintf(f,"\n\\Variaveis uv\n");
 for(int c = 0; c < numDis__; c++)
   for(int r = 0; r < numSal__; r++)
     for(int p = 0; p < numPerTot__; p++)
       if(matDisPerSal[c][p][r])
         fprintf(f,"0 <= u_%d_%d_v_%d_%d <= 1\n",c,p,r,p); 
 fprintf(f,"\nBINARIES\n");
 for(int c = 0; c < numDis__; c++)
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"y_%d_%d\n",c,r);
 fprintf(f,"\n");
 for(int c = 0; c < numDis__; c++)
   for(int r = 0; r < numSal__; r++)
     for(int p = 0; p < numPerTot__; p++)
       if(matDisPerSal[c][p][r])
         fprintf(f,"u_%d_%d_v_%d_%d\n",c,p,r,p); 
 fprintf(f,"\nEND");
 fclose(f);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monModNovo(char *arq,const int &rl)
{
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 if(MODELO < 6)
  {
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
         fprintf(f,"+ 0 x_%d_%d ",c,p);
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       if(!matResDisPer__[c][p])
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ %d q_%d_%d_%d ",PESOS[0] * MAX(0,vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),c,p,r); 
       fprintf(f,"\n"); 
      }
     fprintf(f,"\n"); 
    }
  }
 else
  {
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       if(!matResDisPer__[c][p])
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ %d x_%d_%d_%d ",PESOS[0] * MAX(0,vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),c,p,r); 
       fprintf(f,"\n"); 
      }
     fprintf(f,"\n");
    }
  }
 fprintf(f,"\n\n\\Aulas isoladas\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"+ %d v_%d_%d ",PESOS[1],u,p); 
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"+ %d w_%d ",PESOS[2],c); 
 fprintf(f,"\n\n\\Salas diferentes\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"+ %d y_%d_%d ",PESOS[3],c,r); 
   fprintf(f,"\n"); 
  }
 // ------------------ restri��es
 memset(vetQtdRes__,0,sizeof(vetQtdRes__));
 fprintf(f,"\nST\n");
 if(MODELO < 6)
  {
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
         fprintf(f,"+ x_%d_%d ",c,p); 
     fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
     vetQtdRes__[0]++;
    }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     for(int d = 0; d < numDia__; d++)
      {
       for(int q = 0; q < numPerDia__; q++)
         if(!matResDisPer__[c][(d*numPerDia__)+q])
           fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
       fprintf(f,"- z_%d_%d >= 0\n",c,d);
       vetQtdRes__[6]++;
      }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"+ z_%d_%d ",c,d);
     fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
     vetQtdRes__[6]++;
    }
   if(MODELO != 5)
    {
     fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
     for(int u = 0; u < numTur__; u++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
         fprintf(f,"- r_%d_%d = 0\n",u,p);
         vetQtdRes__[7]++;
        }
       fprintf(f,"\n"); 
      }
     fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
     for(int u = 0; u < numTur__; u++)
      {
       for(int d = 0; d < numDia__; d++)
        {
         fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
         vetQtdRes__[7]++;
         for(int q = 1; q < numPerDia__-1; q++)
          {
           fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
           vetQtdRes__[7]++;
          }
         fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
         vetQtdRes__[7]++;
        }
       fprintf(f,"\n"); 
      }
    }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
           fprintf(f,"+ x_%d_%d ",c,p);
       fprintf(f,"<= 1\n");
       vetQtdRes__[3]++;
      }
     fprintf(f,"\n");
    }
   fprintf(f,"\n\\Nova 1 - Aulas simultaneas na mesma sala\n");
   for(int r = 0; r < numSal__; r++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
       fprintf(f,"<= 1\n"); 
       vetQtdRes__[1]++;
      } 
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\Nova 2 - Mesma aula em mais de uma sala\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
        {
         fprintf(f,"x_%d_%d ",c,p); 
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"- q_%d_%d_%d ",c,p,r); 
         fprintf(f,"= 0\n"); 
         vetQtdRes__[1]++;
        }
     fprintf(f,"\n"); 
    }
   if(MODELO == 3)
    {
     fprintf(f,"\n\\Nova 3 - Numero de salas usadas por disciplina 1\n");
     for(int c = 0; c < numDis__; c++)
      {
       for(int r = 0; r < numSal__; r++)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
         fprintf(f,"- y_%d_%d >= 0\n",c,r); 
         vetQtdRes__[8]++;
        } 
       fprintf(f,"\n"); 
      }
     fprintf(f,"\n\\Nova 4 - Numero de salas usadas por disciplina 2\n");
     for(int c = 0; c < numDis__; c++)
      {
       for(int r = 0; r < numSal__; r++)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
            {
             fprintf(f,"q_%d_%d_%d - y_%d_%d <= 0\n",c,p,r,c,r); 
             vetQtdRes__[8]++;
            }
        } 
       fprintf(f,"\n"); 
      }
    }
   else 
    {
     fprintf(f,"\n\\Nova 5 - Numero de salas usadas por disciplina\n");
     for(int c = 0; c < numDis__; c++)
      {
       for(int r = 0; r < numSal__; r++)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"+ q_%d_%d_%d ",c,p,r); 
         fprintf(f,"- %d y_%d_%d <= 0\n",vetDisciplinas__[c].numPer_,c,r); 
         vetQtdRes__[8]++;
        } 
       fprintf(f,"\n"); 
      }
     if(MODELO == 5)
      {
       fprintf(f,"\n\\Nova 6 - Aulas isoladas\n");
       for(int u = 0; u < numTur__; u++)
        {
         for(int d = 0; d < numDia__; d++)
          {
           for(int k = 0; k < vetTurmas__[u].numDis_; k++)
            {
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)])
               fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__));
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+1])
               fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+1);
            } 
           fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__));
           vetQtdRes__[7]++;
           for(int q = 1; q < numPerDia__ - 1; q++)
            {
             for(int k = 0; k < vetTurmas__[u].numDis_; k++)
              {
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q-1])
                 fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q-1);
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q])
                 fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q);
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q+1])
                 fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q+1);
              } 
             fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+q);
             vetQtdRes__[7]++;
            }
           for(int k = 0; k < vetTurmas__[u].numDis_; k++)
            {
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-2])
               fprintf(f,"- x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-2);
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-1])
               fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-1);
            } 
           fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-1);
           vetQtdRes__[7]++;
          }
         fprintf(f,"\n"); 
        }
       fprintf(f,"\n\\Nova 7 - Aulas de uma turma no mesmo periodo\n");
       for(int u = 0; u < numTur__; u++)
        {
         for(int p = 0; p < numPerTot__; p++)
          {
           for(int k = 0; k < vetTurmas__[u].numDis_; k++)
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
               fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
           fprintf(f,"<= 1\n");
           vetQtdRes__[4]++;
          }
         fprintf(f,"\n"); 
        }
      }
    }
  }
 else
  {
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
       for(int r = 0; r < numSal__; r++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
     fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
     vetQtdRes__[0]++;
    }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     for(int d = 0; d < numDia__; d++)
      {
       for(int q = 0; q < numPerDia__; q++)
         if(!matResDisPer__[c][(d*numPerDia__)+q])
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"+ x_%d_%d_%d ",c,(d*numPerDia__)+q,r);
       fprintf(f,"- z_%d_%d >= 0\n",c,d);
       vetQtdRes__[6]++;
      }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"+ z_%d_%d ",c,d);
     fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
     vetQtdRes__[6]++;
    }
   if(MODELO != 8)
    {
     fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
     for(int u = 0; u < numTur__; u++)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],p,r);
         fprintf(f,"- r_%d_%d = 0\n",u,p);
         vetQtdRes__[7]++;
        }
       fprintf(f,"\n"); 
      }
     fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
     for(int u = 0; u < numTur__; u++)
      {
       for(int d = 0; d < numDia__; d++)
        {
         fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
         vetQtdRes__[7]++;
         for(int q = 1; q < numPerDia__-1; q++)
          {
           fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
           vetQtdRes__[7]++;
          }
         fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
         vetQtdRes__[7]++;
        }
       fprintf(f,"\n"); 
      }
    }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"+ x_%d_%d_%d ",c,p,r);
       fprintf(f,"<= 1\n");
       vetQtdRes__[3]++;
      }
     fprintf(f,"\n");
    }
   fprintf(f,"\n\\Nova 1 - Aulas simultaneas na mesma sala\n");
   for(int r = 0; r < numSal__; r++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
       fprintf(f,"<= 1\n"); 
       vetQtdRes__[1]++;
      } 
     fprintf(f,"\n"); 
    }
   if(MODELO == 6)
    {
     fprintf(f,"\n\\Nova 3 - Numero de salas usadas por disciplina 1\n");
     for(int c = 0; c < numDis__; c++)
      {
       for(int r = 0; r < numSal__; r++)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
         fprintf(f,"- y_%d_%d >= 0\n",c,r); 
         vetQtdRes__[8]++;
        } 
       fprintf(f,"\n"); 
      }
     fprintf(f,"\n\\Nova 4 - Numero de salas usadas por disciplina 2\n");
     for(int c = 0; c < numDis__; c++)
      {
       for(int r = 0; r < numSal__; r++)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
            {
             fprintf(f,"x_%d_%d_%d - y_%d_%d <= 0\n",c,p,r,c,r); 
             vetQtdRes__[8]++;
            }  
        } 
       fprintf(f,"\n"); 
      }
    }
   else 
    {
     fprintf(f,"\n\\Nova 5 - Numero de salas usadas por disciplina\n");
     for(int c = 0; c < numDis__; c++)
      {
       for(int r = 0; r < numSal__; r++)
        {
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"+ x_%d_%d_%d ",c,p,r); 
         fprintf(f,"- %d y_%d_%d <= 0\n",vetDisciplinas__[c].numPer_,c,r); 
         vetQtdRes__[8]++;
        } 
       fprintf(f,"\n"); 
      }
     if(MODELO == 8)
      {
       fprintf(f,"\n\\Nova 6 - Aulas isoladas\n");
       for(int u = 0; u < numTur__; u++)
        {
         for(int d = 0; d < numDia__; d++)
          {
           for(int k = 0; k < vetTurmas__[u].numDis_; k++)
            {
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__),r);
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+1])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+1,r);
            } 
           fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__));
           vetQtdRes__[7]++;
           for(int q = 1; q < numPerDia__ - 1; q++)
            {
             for(int k = 0; k < vetTurmas__[u].numDis_; k++)
              {
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q-1])
                 for(int r = 0; r < numSal__; r++)
                   fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q-1,r);
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q])
                 for(int r = 0; r < numSal__; r++)
                   fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q,r);
               if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+q+1])
                 for(int r = 0; r < numSal__; r++)
                   fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+q+1,r);
              } 
             fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+q);
             vetQtdRes__[7]++;
            }
           for(int k = 0; k < vetTurmas__[u].numDis_; k++)
            {
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-2])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"- x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-2,r);
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][(d*numPerDia__)+numPerDia__-1])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],(d*numPerDia__)+numPerDia__-1,r);
            } 
           fprintf(f,"- v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-1);
           vetQtdRes__[7]++;
          }
         fprintf(f,"\n"); 
        }
       fprintf(f,"\n\\Nova 7 - Aulas de uma turma no mesmo periodo\n");
       for(int u = 0; u < numTur__; u++)
        {
         for(int p = 0; p < numPerTot__; p++)
          {
           for(int k = 0; k < vetTurmas__[u].numDis_; k++)
             if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
               for(int r = 0; r < numSal__; r++)
                 fprintf(f,"+ x_%d_%d_%d ",vetTurmas__[u].vetDis_[k],p,r);
           fprintf(f,"<= 1\n");
           vetQtdRes__[4]++;
          }
         fprintf(f,"\n"); 
        }
      }
    }
  }
 // ------------------ limites
 fprintf(f,"\nBOUNDS\n");
 if(MODELO < 6)
  {
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
         fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
   fprintf(f,"\n\\Variaveis q\n");
   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"0 <= q_%d_%d_%d <= 1\n",c,p,r); 
  }
 else
  {
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"0 <= x_%d_%d_%d <= 1\n",c,p,r); 
  }
 fprintf(f,"\n\\Variaveis v\n");
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
 fprintf(f,"\n\\Variaveis w\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_); // ? ou numDia__ ou CPX_INFBOUND (+ correto, por�m resultado pior) 
 fprintf(f,"\n\\Variaveis y\n");
 for(int c = 0; c < numDis__; c++)
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"0 <= y_%d_%d <= 1\n",c,r); 
 fprintf(f,"\n\\Variaveis z\n");
 for(int c = 0; c < numDis__; c++)
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
 if((MODELO != 5) && (MODELO != 8))
  {
   fprintf(f,"\n\\Variaveis r\n");
   for(int u = 0; u < numTur__; u++)
     for(int p = 0; p < numPerTot__; p++)
       fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
  }
 if(!rl)
  {
   // ------------------ vari�veis
   fprintf(f,"\nBINARIES\n");
   if(MODELO < 6)
    {
     for(int c = 0; c < numDis__; c++)
      for(int p = 0; p < numPerTot__; p++)
        if(!matResDisPer__[c][p])
         { 
          fprintf(f,"x_%d_%d\n",c,p);
          vetQtdVar__[0]++;
         } 
     fprintf(f,"\n");
     for(int c = 0; c < numDis__; c++)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           for(int r = 0; r < numSal__; r++)
            {
             fprintf(f,"q_%d_%d_%d\n",c,p,r); 
             vetQtdVar__[6]++;
            } 
     fprintf(f,"\n");
    }
   else
    {
     for(int c = 0; c < numDis__; c++)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           for(int r = 0; r < numSal__; r++)
            {
             fprintf(f,"x_%d_%d_%d\n",c,p,r); 
             vetQtdVar__[0]++;
            } 
     fprintf(f,"\n");
    }
   for(int u = 0; u < numTur__; u++)
     for(int p = 0; p < numPerTot__; p++)
      {
       fprintf(f,"v_%d_%d\n",u,p);
       vetQtdVar__[1]++;
      } 
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     for(int r = 0; r < numSal__; r++)
      {
       fprintf(f,"y_%d_%d\n",c,r); 
       vetQtdVar__[3]++;
      } 
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     for(int d = 0; d < numDia__; d++)
      {
       fprintf(f,"z_%d_%d\n",c,d);
       vetQtdVar__[4]++;
      } 
   fprintf(f,"\n");
   if((MODELO != 5) && (MODELO != 8))
    {
     for(int u = 0; u < numTur__; u++)
       for(int p = 0; p < numPerTot__; p++)
        {
         fprintf(f,"r_%d_%d\n",u,p);
         vetQtdVar__[5]++;
        } 
     fprintf(f,"\n");
    } 
   fprintf(f,"\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"w_%d\n",c);
   vetQtdVar__[2] = numDis__;
  }
 fprintf(f,"\nEND");
 fclose(f); 
}
//------------------------------------------------------------------------------

//==============================================================================


//================================== SOLU��O ===================================

//------------------------------------------------------------------------------
void escSolucao(Solucao &s,FILE *f)
{
 int aux,flag;
 if(f == NULL)
   f = stdout;
 fprintf(f,"\n\n< ---------------------------------- SOLUCAO --------------------------------- >\n");
 fprintf(f,"Instancia (%s)................................................: %s\n\n\n",INST,nomInst__);
 //-------------------------------- 
 // matrizes de turmas
 for(int u = 0; u < numTur__; u++)
  {
   fprintf(f,"TURMA %d - %s\n",u,vetTurmas__[u].nome_);
   for(int p = 0; p < numPerDia__; p++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"%d  ",s.matSolTur_[p][d][u]);
     fprintf(f,"\n");
    }
   fprintf(f,"\n");
  } 
 //-------------------------------- 
 // matrizes de salas
 if((MODELO != 1) && (MODELO != 2))
  {
   fprintf(f,"\n");
   for(int r = 0; r < numSal__; r++)
    {
     fprintf(f,"SALA %d - %s\n",r,vetSalas__[r].nome_);
     for(int p = 0; p < numPerDia__; p++)
      {
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"%d  ",s.matSolSal_[p][d][r]);
       fprintf(f,"\n");
      }
     fprintf(f,"\n");
    }
  } 
 //-------------------------------- 
 // verificar a solu��o
 s.funObj_ = 0;
 //-------------------------------- 
 // HARD
 s.vioNumAul_ = s.vioAulSim_ = s.vioDisSim_ = s.vioProSim_ = s.vioTurSim_ = 0;
 //-------------------------------- 
 // n�mero de aulas
 for(int c = 0; c < numDis__; c++)
  {
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u])
      {
       aux = 0;
       for(int p = 0; p < numPerDia__; p++)
         for(int d = 0; d < numDia__; d++)
           if(s.matSolTur_[p][d][u] == c)
             aux++;
       s.vioNumAul_ += abs(vetDisciplinas__[c].numPer_ - aux);    
       break;
      }
  }
 //-------------------------------- 
 // aulas simult�neas --> n�o precisa calcular (garantido na modelagem)
 //-------------------------------- 
 // professor simult�neo
 for(int u = 0; u < numTur__; u++)
   for(int d = 0; d < numDia__; d++)
    {
     for(int p = 0; p < numPerDia__; p++)
       if(s.matSolTur_[p][d][u] != -1)
         for(int u2 = u+1; u2 < numTur__; u2++)
           if(s.matSolTur_[p][d][u2] != -1)
             if((s.matSolTur_[p][d][u2] != s.matSolTur_[p][d][u]) && (vetDisciplinas__[s.matSolTur_[p][d][u2]].professor_ == vetDisciplinas__[s.matSolTur_[p][d][u]].professor_))
               s.vioProSim_++;
    } 
 //-------------------------------- 
 // disciplinas e turmas simult�neas
 if((MODELO != 1) && (MODELO != 2))
  {
   for(int r = 0; r < numSal__; r++)
    {
     for(int d = 0; d < numDia__; d++)
      {
       for(int p = 0; p < numPerDia__; p++)
        {
         if(s.matSolSal_[p][d][r] != -1)
           for(int r2 = r+1; r2 < numSal__; r2++)
            {
             if(s.matSolSal_[p][d][r2] != -1)
              {
               if(s.matSolSal_[p][d][r2] == s.matSolSal_[p][d][r])
                 s.vioDisSim_++;
               for(int u = 0; u < numTur__; u++)
                 if((matDisTur__[s.matSolSal_[p][d][r2]][u]) && (matDisTur__[s.matSolSal_[p][d][r]][u]))
                   s.vioTurSim_++;
              }
            }
        } 
      }
    }
  }
 //-------------------------------- 
 // SOFT
 s.capSal_ = s.janHor_ = s.diaMin_ = s.salDif_ = 0;
 //-------------------------------- 
 // dias m�nimos
 for(int c = 0; c < numDis__; c++)
  {
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u])
      {
       aux = 0;
       for(int d = 0; d < numDia__; d++)
        {
         for(int p = 0; p < numPerDia__; p++)
           if(s.matSolTur_[p][d][u] == c)
            {
             aux++;
             break;
            }
        } 
       s.diaMin_ += MAX(0,vetDisciplinas__[c].diaMin_-aux);
       break;
      }
  }
 //-------------------------------- 
 // aulas isoladas
 for(int u = 0; u < numTur__; u++)
   for(int d = 0; d < numDia__; d++)
    {
     if((s.matSolTur_[0][d][u] != -1) && (s.matSolTur_[1][d][u] == -1))
       s.janHor_++;
     if((s.matSolTur_[numPerDia__-1][d][u] != -1) && (s.matSolTur_[numPerDia__-2][d][u] == -1))
       s.janHor_++;
     for(int p = 2; p < numPerDia__; p++)
       if((s.matSolTur_[p-1][d][u] != -1) && (s.matSolTur_[p-2][d][u] == -1) && (s.matSolTur_[p][d][u] == -1))
         s.janHor_++;
    } 
 //-------------------------------- 
 // capacidade das salas e salas diferentes
 if((MODELO != 1) && (MODELO != 2)) 
  {
   for(int r = 0; r < numSal__; r++)
     for(int p = 0; p < numPerDia__; p++)
       for(int d = 0; d < numDia__; d++)
         {
          if(s.matSolSal_[p][d][r] != -1)
            if(vetDisciplinas__[s.matSolSal_[p][d][r]].numAlu_ > vetSalas__[r].capacidade_)
              s.capSal_ += (vetDisciplinas__[s.matSolSal_[p][d][r]].numAlu_ - vetSalas__[r].capacidade_);
         }
   for(int c = 0; c < numDis__; c++)
    {
     aux = 0;
     for(int r = 0; r < numSal__; r++)
      {
       flag = 0;
       for(int d = 0; d < numDia__; d++)
        {
         for(int p = 0; p < numPerDia__; p++)
          {
           if(s.matSolSal_[p][d][r] == c)
            {
             flag = 1;
             break;
            }
          } 
         if(flag)
           break;
        } 
       aux += flag;
      }
     s.salDif_ += aux - 1;
    }
  }
 else
  {
   for(int d = 0; d < numDia__; d++)
     for(int p = 0; p < numPerDia__; p++)
      {
       aux = 0;
       for(int u = 0; u < numTur__; u++)
         if(s.matSolTur_[p][d][u] != -1)
           aux++;
       s.capSal_ = MAX(0,aux - numSal__);
      } 
  }
 s.funObj_ = PESOS[0] * s.capSal_ + PESOS[1] * s.janHor_ + PESOS[2] * s.diaMin_ + PESOS[3] * s.salDif_;   
 fprintf(f,"\n>>> RESULTADOS CALCULADOS\n\n");
 fprintf(f,"Func. obj.....: %d\n",s.funObj_);
 fprintf(f,"\n\nHARD----------------------------------\n\n");
 fprintf(f,"Num. aulas....: %d\n",s.vioNumAul_);
 fprintf(f,"Aulas simul...: %d\n",s.vioAulSim_);
 fprintf(f,"Prof. simul...: %d\n",s.vioProSim_);
 if((MODELO != 1) && (MODELO != 2))
  {
   fprintf(f,"Disc. simul...: %d\n",s.vioDisSim_);
   fprintf(f,"Turm. simul...: %d\n",s.vioTurSim_);
  }
 else
  {
   fprintf(f,"Disc. simul...: -\n");
   fprintf(f,"Turm. simul...: -\n");
  }
 fprintf(f,"\n\nSOFT------------------------------------\n\n");
 fprintf(f,"Jan. turmas (%d)....: %d\n",PESOS[1],s.janHor_);
 fprintf(f,"Dias minimos (%d)...: %d\n",PESOS[2],s.diaMin_);
 if((MODELO != 1) && (MODELO != 2))
  {
   fprintf(f,"Cap. salas (%d).....: %d\n",PESOS[0],s.capSal_);
   fprintf(f,"Salas difer (%d)....: %d\n",PESOS[3],s.salDif_);
  }
 else
  {
   fprintf(f,"Cap. salas (%d).....: %d (adaptado)\n",PESOS[0],s.capSal_);
   fprintf(f,"Salas difer (%d)....: -\n",PESOS[3]);
  }
 fclose(f);
}
//------------------------------------------------------------------------------

//==============================================================================


//================================= AUXILIARES =================================

//------------------------------------------------------------------------------
void lerInstancia(char *arq)
{
 int pos,per;
 char aux[50];
 FILE *f = fopen(arq,"r");
 fscanf(f,"Name: %s\n",&nomInst__);
 fscanf(f,"Courses: %d\n",&numDis__);
 fscanf(f,"Rooms: %d\n",&numSal__);
 fscanf(f,"Days: %d\n",&numDia__);
 fscanf(f,"Periods_per_day: %d\n",&numPerDia__);
 fscanf(f,"Curricula: %d\n",&numTur__);
 fscanf(f,"Constraints: %d\n",&numRes__);
 fscanf(f,"\nCOURSES:\n");
 numPerTot__ = numDia__ * numPerDia__;
 numPro__ = 0;
 for(int i = 0; i < numDis__; i++)
  {
   fscanf(f,"%s %s %d %d %d\n",&vetDisciplinas__[i].nome_,&aux,
          &vetDisciplinas__[i].numPer_,&vetDisciplinas__[i].diaMin_,&vetDisciplinas__[i].numAlu_);
   pos = -1;
   for(int p = 0; p < numPro__; p++)
     if(strcmp(aux,vetProfessores__[p].nome_) == 0)
      {
       vetDisciplinas__[i].professor_ = p;
       pos = p;
       break;
      }
   if(pos == -1)
    {
     vetDisciplinas__[i].professor_ = numPro__;
     strcpy(vetProfessores__[numPro__].nome_,aux);
     numPro__++;
    }
  }
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numTur__; j++)
     matDisTur__[i][j] = 0;
 fscanf(f,"\nROOMS:\n");
 for(int i = 0; i < numSal__; i++)
   fscanf(f,"%s %d\n",&vetSalas__[i].nome_,&vetSalas__[i].capacidade_);
 fscanf(f,"\nCURRICULA:\n");
 for(int i = 0; i < numTur__; i++)
  {
   fscanf(f,"%s %d",&vetTurmas__[i].nome_,&vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
    {
     fscanf(f, "%s ",&aux);
     vetTurmas__[i].vetDis_[j] = -1;
     for(int k = 0; k < numDis__; k++)
       if(strcmp(aux,vetDisciplinas__[k].nome_) == 0)
        {
         vetTurmas__[i].vetDis_[j] = k;
         matDisTur__[k][i] = 1;
         break;
        }
    }
  }
 fscanf(f,"\nUNAVAILABILITY_CONSTRAINTS:\n");
 memset(matResDisPer__,0,sizeof(matResDisPer__));
 for(int i = 0; i < numRes__; i++) 
  {
   fscanf(f,"%s %d %d\n",&aux,&pos,&per);
   for(int j = 0; j < numDis__; j++)
     if(strcmp(aux,vetDisciplinas__[j].nome_) == 0)
      {
       matResDisPer__[j][(pos * numPerDia__) + per] = 1;
       break;
      }
  }
 fclose(f);
 //---------------
 // preeencher e ordenar o vetor com as diferentes capacidades de sala
 ordVetCapSal();
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void testarEntrada()
{
 int aux;

 printf("Name: %s\n",nomInst__);
 printf("Courses: %d\n",numDis__);
 printf("Rooms: %d\n",numSal__);
 printf("Days: %d\n",numDia__);
 printf("Periods_per_day: %d\n",numPerDia__);
 printf("Curricula: %d\n",numTur__);
 printf("Constraints: %d\n",numRes__);

 printf("\nDISCIPLINAS:\n");
 for(int i = 0; i < numDis__; i++)
   printf("%d  %s  %d  %d  %d  %d\n",i,vetDisciplinas__[i].nome_,vetDisciplinas__[i].professor_,vetDisciplinas__[i].numPer_,vetDisciplinas__[i].diaMin_,vetDisciplinas__[i].numAlu_);

 printf("\nTURMAS:\n");
 for(int i = 0; i < numTur__; i++)
  {
   printf("%d  %s  %d --> ",i,vetTurmas__[i].nome_,vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
     printf("%d  ",vetTurmas__[i].vetDis_[j]);
   printf("\n");
  } 

 printf("\nPROFESSORES:\n");
 for(int i = 0; i < numPro__; i++)
   printf("%d  %s\n",i,vetProfessores__[i].nome_);

 printf("\nSALAS:\n");
 for(int i = 0; i < numSal__; i++)
   printf("%d  %s  %d\n",i,vetSalas__[i].nome_,vetSalas__[i].capacidade_);

 aux = 0;
 printf("\nRESTRICOES:\n");
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numPerTot__; j++)
     if(matResDisPer__[i][j])
      {
       printf("%d  %d  %d\n",aux,i,j);
       aux++;
      } 

 printf("\nDISC x TURMA:\n");
 for(int i = 0; i < numDis__; i++)
  {
   for(int j = 0; j < numTur__; j++)
     printf("%d  ",matDisTur__[i][j]);
   printf("\n");
  }
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void ordVetCapSal()
{
 int aux,flag;
 numSalDif__ = 0;
 memset(vetTamSal__,0,sizeof(vetTamSal__));
 for(int i = 0; i < numSal__; i++)
  {
   flag = 0;
   for(int j = 0; j < numSalDif__; j++)
     if(vetSalas__[i].capacidade_ == vetTamSal__[j])
      {
       flag = 1; 
       break;
      }
   if(!flag)
    {
     vetTamSal__[numSalDif__] = vetSalas__[i].capacidade_;
     numSalDif__++; 
    }
  }
 flag = 1;
 while(flag)
  {
   flag = 0;
   for(int i = 0; i < numSalDif__-1; i++)
     if(vetTamSal__[i] > vetTamSal__[i+1])
      {
       aux = vetTamSal__[i];
       vetTamSal__[i] = vetTamSal__[i+1];
       vetTamSal__[i+1] = aux;
       flag = 1;
      }
  }
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void configuraParametros(int piNParam, char **piParams){
	
	for (int i = 2; i < piNParam; i++){
		if (strncmp("model", piParams[i], 5) == 0){
			MODELO = atoi(piParams[i] + 6);
		} else if (strncmp("metodo", piParams[i], 6) == 0){
			METODO = atoi(piParams[i] + 7);
		}	
	}
	
	for (int i = 2; i < piNParam; i++){
	   if(METODO == -1){
			 if (strncmp("saida", piParams[i], 5) == 0)
				strcpy(SAIDA,piParams[i] + 6);
	   } else if(METODO == 0){
			 if (strncmp("estsala", piParams[i], 7) == 0)
				PESOS[3] = atoi(piParams[i] + 8);
			 else if (strncmp("timeout", piParams[i], 7) == 0)
				TEM_LIM = atof(piParams[i] + 8);
			 else if (strncmp("saida", piParams[i], 5) == 0)
				strcpy(SAIDA,piParams[i] + 6);
	   } else if(METODO == 1){
			 if (strncmp("estsala", piParams[i], 7) == 0)
				PESOS[3] = atoi(piParams[i] + 8);
			 else if (strncmp("optcpx", piParams[i], 6) == 0)
				OPT_CPX = atoi(piParams[i] + 7);
			 else if (strncmp("timeout", piParams[i], 7) == 0)
				TEM_LIM = atof(piParams[i] + 8);
			 else if (strncmp("saida", piParams[i], 5) == 0)
				strcpy(SAIDA,piParams[i] + 6);
	   } else if((METODO == 2) || (METODO == 3)){
			 if (strncmp("sps", piParams[i], 3) == 0)
				SPS = atoi(piParams[i] + 4);
			 else if (strncmp("fd", piParams[i], 2) == 0)
				FD = atoi(piParams[i] + 3);
			 else if (strncmp("pes", piParams[i], 3) == 0)
				PES = atoi(piParams[i] + 4);
			 else if (strncmp("timeout", piParams[i], 7) == 0)
				TEM_LIM = atof(piParams[i] + 8);
			 else if (strncmp("saida", piParams[i], 5) == 0)
				strcpy(SAIDA,piParams[i] + 6);
	   } else{
			 if (strncmp("estsala", piParams[i], 7) == 0)
				PESOS[3] = atoi(piParams[i] + 8);
			 else if (strncmp("optcpx", piParams[i], 6) == 0)
				OPT_CPX = atoi(piParams[i] + 7);
			 else if (strncmp("sps", piParams[i], 3) == 0)
				SPS = atoi(piParams[i] + 4);
			 else if (strncmp("fd", piParams[i], 2) == 0)
				FD = atoi(piParams[i] + 3);
			 else if (strncmp("pes", piParams[i], 3) == 0)
				PES = atoi(piParams[i] + 4);
			 else if (strncmp("timeout", piParams[i], 7) == 0)
				TEM_LIM = atof(piParams[i] + 8);
			 else if (strncmp("saida", piParams[i], 5) == 0)
				strcpy(SAIDA,piParams[i] + 6);
	   }
	}
}
//------------------------------------------------------------------------------

//==============================================================================
