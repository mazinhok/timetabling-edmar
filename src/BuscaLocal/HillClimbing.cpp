/*
 * HillClimbing.cpp
 *
 *  Created on: May 26, 2015
 *      Author: erika
 */

#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <string.h>

#include "HillClimbing.h"
#include "../Vizinhancas/Move.h"
#include "../Vizinhancas/Swap.h"
#include "../Vizinhancas/TimeMove.h"
#include "../Vizinhancas/TimeMove2.h"
#include "../Vizinhancas/CadeiadeKempe.h"
#include "../Vizinhancas/CadeiadeKempeRestricaoSala.h"
#include "../Vizinhancas/CadeiaKempeCompletaSalaVazia.h"
#include "../Vizinhancas/CadeiadeKempeEstendidaRestrSala.h"
#include "../Vizinhancas/CadeiadeKempeExtendido.h"
#include "../Vizinhancas/RoomMove.h"
#include "../Vizinhancas/RoomMove2.h"
#include "../Vizinhancas/LectureMove.h"
#include "../Vizinhancas/LectureMove2.h"
#include "../Vizinhancas/LectureMove3.h"
#include "../Vizinhancas/LectureMove4.h"
#include "../Vizinhancas/RoomStabilityMove.h"
#include "../Vizinhancas/MinWorkingDaysMove.h"
#include "../Vizinhancas/CurriculumCompactnessMove.h"

const int _MOVE = 1;
const int _SWAP = 2;
const int _KEMPE = 3;
const int _TIMEMOVE = 4;
const int _ROOMMOVE = 5;
const int _LECTUREMOVE = 6;
const int _ROOMSTABILITYMOVE = 7;
const int _MINWORKINGDAYSMOVE = 8;
const int _CURRICULUMCOMPACTNESSMOVE = 9;
const int _LECTURE3  = 10;
const int _LECTURE4 = 11;
const int _KEMPEEXT = 12;
const int _KEMPERESTSALA  = 13;
const int _KEMPESALAVAZIA = 14;
const int _KEMPEEXTRESTSALA = 15;
const int _ROOMMOVE2 = 16;

const int _MOVE_SWAP = 0;
const int _MOVE_SWAP_KEMPE_30 = 20;
const int _MOVE_SWAP_KEMPE_20 = 21;
const int _MOVE_SWAP_KEMPE_10 = 22;
const int _TIME_ROOM = 30;

HillClimbing::HillClimbing(Problema* p, int piN, int piK) {
	nIteracoes = piN;
	k = piK;
	problema = p;
	tamMedioKempeChain = 0;
}

HillClimbing::~HillClimbing() {}

Movimento* HillClimbing::obtemMelhorMovimento(Individuo* ind, int movimento){
	Movimento* melhor = NULL;
	Movimento* mov;
	CadeiadeKempe* kmp;
	CadeiadeKempeExtendido* kmpext;
	CadeiadeKempeRestricaoSala* kmprs;
	CadeiaKempeCompletaSalaVazia* kmpcsv;
	CadeiadeKempeEstendidaRestrSala* kmpextrs;
	float sorteio;
	int movARealizar;
	int numTentativas = k;

	while( numTentativas-- ){

		//Escolhe um movimento a partir de uma vizinhança composta por mais de um movimento
		switch( movimento ){
			case _MOVE_SWAP:
				sorteio = (float) rand() / RAND_MAX;
				if( sorteio < 0.5 ) movARealizar = _MOVE;
				else  movARealizar = _SWAP;
				break;
			case _MOVE_SWAP_KEMPE_30:
				sorteio = (float) rand() / RAND_MAX;
				if( sorteio < 0.35 ) movARealizar = _MOVE;
				else if( sorteio < 0.7 ) movARealizar = _SWAP;
				else movARealizar = _KEMPEEXT;
				break;
			case _MOVE_SWAP_KEMPE_20:
				sorteio = (float) rand() / RAND_MAX;
				if( sorteio < 0.4 ) movARealizar = _MOVE;
				else if( sorteio < 0.8 ) movARealizar = _SWAP;
				else movARealizar = _KEMPEEXT;
				break;
			case _MOVE_SWAP_KEMPE_10:
				sorteio = (float) rand() / RAND_MAX;
				if( sorteio < 0.45 ) movARealizar = _MOVE;
				else if( sorteio < 0.9 ) movARealizar = _SWAP;
				else movARealizar = _KEMPEEXT;
				break;
			case _TIME_ROOM:
				sorteio = (float) rand() / RAND_MAX;
				if( sorteio < 0.5 ) movARealizar = _TIMEMOVE;
				else  movARealizar = _ROOMMOVE;
				break;
			default: movARealizar=movimento;
		}

		//Cria movimento
		switch(movARealizar){
		case _MOVE:  mov = new Move(problema, ind);
					 nMoves++;
				     if( mov->deltaFit < 0 ){
				    	 nMovesMelhora++;
				     }
				     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
				 	 	nMovesValidos++;
				 	 }
				     else nMovesInvalido++;
					 break;
		case _SWAP:  mov = new Swap(problema, ind);
					 nSwaps++;
				     if( mov->deltaFit < 0 ){
				    	 nSwapsMelhora++;
				     }
				     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
				 	 	nSwapsValidos++;
				 	 }
				     else nSwapsInvalido++;
			   	   	 break;
		case _KEMPE: mov = new CadeiadeKempe(problema, ind);
					 kmp = (CadeiadeKempe*) mov;
		 	 	 	 nKempes++;
				     if( mov->deltaFit < 0 ){
				    	 nKempesMelhora++;
			 	 	 	 tamMedioKempeChain += kmp->movimentos.size();
				     }
				     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
				 	 	nKempesValidos++;
			 	 	 	tamMedioKempeChain += kmp->movimentos.size();
				 	 }
				     else nKempesInvalido++;
			   	   	 break;
		case _KEMPEEXT: mov = new CadeiadeKempeExtendido(problema, ind);
					 kmpext = (CadeiadeKempeExtendido*) mov;
					 nKempes++;
				     if( mov->deltaFit < 0 ){
				    	 nKempesMelhora++;
			 	 	 	 tamMedioKempeChain += kmpext->movimentos.size();
				     }
				     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
				 	 	nKempesValidos++;
			 	 	 	tamMedioKempeChain += kmpext->movimentos.size();
				 	 }
				     else nKempesInvalido++;
			         break;	         
		case _KEMPERESTSALA: mov = new CadeiadeKempeRestricaoSala(problema, ind);
					 kmprs = (CadeiadeKempeRestricaoSala*) mov;
					 nKempes++;
				     if( mov->deltaFit < 0 ){
				    	 nKempesMelhora++;
			 	 	 	 tamMedioKempeChain += kmprs->movimentos.size();
				     }
				     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
				 	 	nKempesValidos++;
			 	 	 	tamMedioKempeChain += kmprs->movimentos.size();
				 	 }
				     else nKempesInvalido++;
			         break;         
		case _KEMPESALAVAZIA: mov = new CadeiaKempeCompletaSalaVazia(problema, ind);
					 kmpcsv = (CadeiaKempeCompletaSalaVazia*) mov;
					 nKempes++;
				     if( mov->deltaFit < 0 ){
				    	 nKempesMelhora++;
			 	 	 	 tamMedioKempeChain += kmpcsv->movimentos.size();
				     }
				     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
				 	 	nKempesValidos++;
			 	 	 	tamMedioKempeChain += kmpcsv->movimentos.size();
				 	 }
				     else nKempesInvalido++;
			         break;			         
		case _KEMPEEXTRESTSALA: mov = new CadeiadeKempeEstendidaRestrSala(problema, ind);
					 kmpextrs = (CadeiadeKempeEstendidaRestrSala*) mov;
					 nKempes++;
				     if( mov->deltaFit < 0 ){
				    	 nKempesMelhora++;
			 	 	 	 tamMedioKempeChain += kmpextrs->movimentos.size();
				     }
				     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
				 	 	nKempesValidos++;
			 	 	 	tamMedioKempeChain += kmpextrs->movimentos.size();
				 	 }
				     else nKempesInvalido++;
			         break;				         		         
		case _TIMEMOVE: mov = new TimeMove2(problema, ind);
						nTimeMoves++;
						if( mov->deltaFit < 0 ){
							nTimeMovesMelhora++;
						}
						else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
							nTimeMovesValidos++;
						}
						else nTimeMovesInvalido++;
						break;
		case _ROOMMOVE: mov = new RoomMove2(problema, ind);
						nRoomMoves++;
						if( mov->deltaFit < 0 ){
							nRoomMovesMelhora++;
						}
						else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
							nRoomMovesValidos++;
						}
						else nRoomMovesInvalido++;
						break;
		case _LECTUREMOVE: mov = new LectureMove4(problema, ind);
						nLectureMoves++;
						if( mov->deltaFit < 0 ){
							nLectureMovesMelhora++;
						}
						else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
							nLectureMovesValidos++;
						}
						else nLectureMovesInvalido++;
						break;
		case _ROOMSTABILITYMOVE: mov = new RoomStabilityMove(problema, ind);
						nRoomStabilityMoves++;
						if( mov->deltaFit < 0 ){
							nRoomStabilityMovesMelhora++;
						}
						else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
							nRoomStabilityMovesValidos++;
						}
						else nRoomStabilityMovesInvalido++;
						break;
		case _MINWORKINGDAYSMOVE: mov = new MinWorkingDaysMove(problema, ind);
						nMinWorkingDaysMoves++;
						if( mov->deltaFit < 0 ){
							nMinWorkingDaysMovesMelhora++;
						}
						else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
							nMinWorkingDaysMovesValidos++;
						}
						else nMinWorkingDaysMovesInvalido++;
						break;
		case _CURRICULUMCOMPACTNESSMOVE: mov = new CurriculumCompactnessMove(problema, ind);
						nCurriculumCompactnessMoves++;
						if( mov->deltaFit < 0 ){
							nCurriculumCompactnessMovesMelhora++;
						}
						else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
							nCurriculumCompactnessMovesValidos++;
						}
						else nCurriculumCompactnessMovesInvalido++;
						break;
		default: fprintf(stderr, "Movimento %d invalido!\n\n", movARealizar);
				 exit(0);
		}

		//nMovimentosRealizados++;
		if( melhor == NULL ) {
			melhor = mov;
		}
		else{
			if( mov->deltaFit < melhor->deltaFit ){
				delete(melhor);
				melhor = mov;
			}
			else delete(mov);
		}
	}

	return melhor;
}

Individuo* HillClimbing::executa(Individuo* bestInd, int movimento, int nMaxMovimentosSemMelhora){
	int i;
	Movimento *mov;
	int nIteracoesSemMelhora = 0;
	nMovimentosRealizados = 0;
	tamMedioKempeChain    = 0;
	nIteracoesExecutadas  = 0;

	for( i=0; i<nIteracoes; i++ ){
		mov = obtemMelhorMovimento(bestInd, movimento);
		nIteracoesSemMelhora++;
		nIteracoesExecutadas++;

		if( mov->deltaFit < 0 ){
			mov->aplicaMovimento();
			//printf("%d;%d;%d;%d;%d;\n", (mov->deltaFit * (-1)),bestInd->soft1,bestInd->soft2,bestInd->soft3,bestInd->soft4);
			nMovimentosRealizados++;
			nIteracoesSemMelhora = 0;
		}
		delete(mov);

		if( nIteracoesSemMelhora >= nMaxMovimentosSemMelhora ) break;
	}

	if( nKempesMelhora+nKempesValidos > 0 ) tamMedioKempeChain /= (nKempesValidos+nKempesMelhora);
	else tamMedioKempeChain = 0;
	//printf("tamMedioKempeChain = %d\n\n", tamMedioKempeChain);

//	printf("*********************************************\n");
	return bestInd;
}

void HillClimbing::imprimeExecucao(){
	printf("Maximo de Iteracoes Busca Local: %d\n", nIteracoes);
	printf("k:  %d\n", k);
	printf("\nNumero de Movimentos Realizados: %d\n", nMovimentosRealizados);
	printf("Total de Movimentos   :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n", nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves, nRoomStabilityMoves, nMinWorkingDaysMoves, nCurriculumCompactnessMoves);
	printf("Movimentos Invalidos  :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n", nMovesInvalido, nSwapsInvalido, nKempesInvalido, nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido, nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido, nCurriculumCompactnessMovesInvalido);
	printf("Movimentos Validos    :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n", nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos, nRoomMovesValidos, nLectureMovesValidos, nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos, nCurriculumCompactnessMovesValidos);
	printf("Movimentos com Melhora:  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n", nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora, nRoomMovesMelhora, nLectureMovesMelhora, nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora, nCurriculumCompactnessMovesMelhora);
}
