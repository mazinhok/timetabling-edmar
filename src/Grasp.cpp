/*
 * Grasp.cpp
 *
 *  Created on: May 17, 2015
 *      Author: erika
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>

#include "Grasp.h"
#include "GeraSolucacoInicial.h"
#include "GeraSolucaoInicialGulosa.h"
#include "GeraSolucaoInicialGulosaAleatoria.h"
#include "Model/Problema.h"
#include "BuscaLocal/HillClimbing.h"
#include "BuscaLocal/SimulatedAnnealing.h"
#include "BuscaLocal/SteepestDescent.h"
#include "BuscaLocal/SDConjugado.h"
#include "BuscaLocal/BuscaTeste.h"

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

Grasp::Grasp() {
	pesoHard = 10000;
	vizinhancasUtilizadas = 0; //move 50% + swap 50%

	//Construcao GRASP
	threshold = 0.10;
	tipoConstrucaoInicial = 0;	// 0 = normal; 1 = gulosa; 2 = teste com os 2
	//Iterações GRASP
	nMaxIter = 9999;
	//HC
	nMaxIterSemMelhora = 10000;
	nMaxIterBuscaLocal = 999999;
	k = 10;
	//SA
	temperaturaInicial = 7.6;
	temperaturaFinal = 0.005;
	taxaDecaimentoTemperatura = 0.999;
	aceitaPioraSA = 1;

	mediaSolucoes = 0;
	soft1 = soft2 = soft3 = soft4 = 0;
	f1 = f2 = f3 = 0;
	opBuscaLocalGrasp = 2;	//busca padrao = HC
	info = 0;
	infoSD = 0;
	imprimeGrafico = 0;
	imprimeFO = 0;
	iteracoesExecutadas = 0;
	tempoExecucao = 0;
	tempoLimite = 999999999;
	seed = time(NULL);
	nomeArquivoInstanciaInicial[0] = '\0';
	nMovimentosRealizados = 0;

	nMoves = nSwaps = nKempes = nTimeMoves = nRoomMoves = nLectureMoves = nRoomStabilityMoves = nMinWorkingDaysMoves = nCurriculumCompactnessMoves = 0;
	nMovesValidos = nSwapsValidos = nKempesValidos = nTimeMovesValidos =
			nRoomMovesValidos = nLectureMovesValidos = nRoomStabilityMovesValidos = nMinWorkingDaysMovesValidos = nCurriculumCompactnessMovesValidos = 0;
	nMovesMelhora = nSwapsMelhora = nKempesMelhora = nTimeMovesMelhora =
			nRoomMovesMelhora = nLectureMovesMelhora = nRoomStabilityMovesMelhora = nMinWorkingDaysMovesMelhora = nCurriculumCompactnessMovesMelhora = 0;
	nMovesInvalido = nSwapsInvalido = nKempesInvalido = nTimeMovesInvalido =
			nRoomMovesInvalido = nLectureMovesInvalido = nRoomStabilityMovesInvalido = nMinWorkingDaysMovesInvalido = nCurriculumCompactnessMovesInvalido = 0;

	//Geração de Coluna (Edmar)
	geraMatrizGC = 0;
}

Individuo* Grasp::executa(Problema* p) {
	int i, nTentativas ;	
	Individuo *ind, *bestInd = NULL, *indConstrucao;
	GeraSolucacoInicial* geraSolucaoI;
	GeraSolucaoInicialGulosa* geraSolucaoG;
	GeraSolucaoInicialGulosaAleatoria* geraSolucaoA;
	BuscaLocal* buscalocal;
	float tempo_inicial, tempo_construcao, tempo_busca_local, sorteio;

	if (geraMatrizGC)
		AlocaMatrizes_GC(p, 500);

	srand(seed);

	tempoExecucao = clock() / CLOCKS_PER_SEC;
	
	for (i = 0; i < nMaxIter; i++) {

		tempo_inicial = clock() / CLOCKS_PER_SEC;

		//CONSTRUCAO
		indConstrucao = new Individuo(p);
		if (i == 0 && nomeArquivoInstanciaInicial[0] != '\0') {
			if (info)
				printf("\tLe Solucao Inicial do arquivo '%s'\n",
						nomeArquivoInstanciaInicial);
			indConstrucao->LeDoArquivo(nomeArquivoInstanciaInicial);
		} else {
			if (tipoConstrucaoInicial == 0) {
				geraSolucaoI = new GeraSolucacoInicial(p, threshold);
				nTentativas = 1;
				while (geraSolucaoI->CriaIndividuoInicial(p, indConstrucao)){
					delete(indConstrucao);
					delete (geraSolucaoI);
					geraSolucaoI = new GeraSolucacoInicial(p, threshold);
					indConstrucao = new Individuo(p);
					nTentativas++;
				}
				delete (geraSolucaoI);
			} else if (tipoConstrucaoInicial == 1) {
				geraSolucaoG = new GeraSolucaoInicialGulosa(p, threshold);
				geraSolucaoG->CriaIndividuoInicial(p, indConstrucao);
				delete (geraSolucaoG);
			} else if (tipoConstrucaoInicial == 2) {
				geraSolucaoA = new GeraSolucaoInicialGulosaAleatoria(p, threshold);
				geraSolucaoA->CriaIndividuoInicial(p, indConstrucao);
				delete (geraSolucaoA);
			}
		}

		p->CalculaFuncaoObjetivo(indConstrucao);
		f1 += indConstrucao->fitness;
		printf("%d\n",indConstrucao->fitness);

		if (indConstrucao->hard > 0) {
			fprintf(stderr, "ERRO Construcao Grasp: Restricao Forte Conflitante\n");
			fprintf(stderr, "fo: %5d\n", indConstrucao->fitness);
			fprintf(stderr, "%4d %4d %4d %4d %4d\n", indConstrucao->hard, indConstrucao->soft1, indConstrucao->soft2, indConstrucao->soft3, indConstrucao->soft4);
			return indConstrucao;
		}

		tempo_construcao = clock() / CLOCKS_PER_SEC;
		if (imprimeGrafico){
			printf("%3d;", seed);
			printf("%6d;%6d;%6d;%6.2f;", i+1, nTentativas, indConstrucao->fitness, tempo_construcao - tempo_inicial);
		}
		if (info) {
			printf("Construção %3d: %5d em %4.2f s\n", i, indConstrucao->fitness, tempo_construcao - tempo_inicial);
		}
		if (infoSD) printf("%d\n", indConstrucao->fitness);

		//BUSCA LOCAL
		
		if (opBuscaLocalGrasp == 2) {
			ind = executaHillClimbing(p, indConstrucao, &buscalocal);
		} else if (opBuscaLocalGrasp == 6) {
			ind = executaSimulatedAnnealing(p, indConstrucao, &buscalocal);
		} else if (opBuscaLocalGrasp == 3) {
			ind = executaHillClimbing(p, indConstrucao, &buscalocal);
			atualizaEstatisticasBuscaLocal(buscalocal);
			delete (buscalocal);
			ind = executaSimulatedAnnealing(p, ind, &buscalocal);
		} else if (opBuscaLocalGrasp == 1) {
			ind = executaSteepestDescent(p, indConstrucao, &buscalocal);
		} else if (opBuscaLocalGrasp == 5) {
			ind = executaBuscaTeste(p, indConstrucao, &buscalocal);
		} else if (opBuscaLocalGrasp == 4) {
			buscalocal = NULL;
			ind = indConstrucao;
			printf("%d\n",i+1);
		} else if (opBuscaLocalGrasp == 7) {
			sorteio = (float) rand() / RAND_MAX;
			if (sorteio < 0.5)
				ind = executaSimulatedAnnealing(p, indConstrucao, &buscalocal);
			else{
				ind = executaSteepestDescent(p, indConstrucao, &buscalocal);
				//buscalocal = NULL;
				//ind = indConstrucao;
			}
		}else {
			fprintf(stderr, "Tipo de busca local nao foi informada!\n");
			exit(1);
		}

		f2 += ind->fitness;
		if (ind->fitness >= 10000) {
			fprintf(stderr, "ERRO Busca local: Restricao Forte Conflitantes: %d\n", ind->hard);
			exit(0);
		}
		atualizaEstatisticasBuscaLocal(buscalocal);
		soft1 += ind->soft1;
		soft2 += ind->soft2;
		soft3 += ind->soft3;
		soft4 += ind->soft4;

		tempo_busca_local = (clock() / CLOCKS_PER_SEC);
		if (imprimeGrafico){
			printf("%5d;%6.2f;", ind->fitness, tempo_busca_local - tempo_construcao);
		}
		if (info) {
			printf("Busca Local   : %5d em %4.2f s\n", ind->fitness, tempo_busca_local - tempo_construcao);
		}
		if (geraMatrizGC){				
			InsereElementosMatriz_GC(p, ind, i);
		}
		
		if (imprimeGrafico){
			printf("%7d;", buscalocal->nIteracoesExecutadas);
			printf("%10d;", buscalocal->nMovimentosRealizados);
			printf("%10d;", (buscalocal->nMovesValidos+
							 buscalocal->nSwapsValidos+
							 buscalocal->nKempesValidos+
							 buscalocal->nTimeMovesValidos+
							 buscalocal->nRoomMovesValidos+
							 buscalocal->nLectureMovesValidos+
							 buscalocal->nRoomStabilityMovesValidos+
							 buscalocal->nMinWorkingDaysMovesValidos+
							 buscalocal->nCurriculumCompactnessMovesValidos
							 ) 
					);
			printf("%10d;", (buscalocal->nMovesMelhora+
							 buscalocal->nSwapsMelhora+
							 buscalocal->nKempesMelhora+
							 buscalocal->nTimeMovesMelhora+
							 buscalocal->nRoomMovesMelhora+
							 buscalocal->nLectureMovesMelhora+
							 buscalocal->nRoomStabilityMovesMelhora+
							 buscalocal->nMinWorkingDaysMovesMelhora+
							 buscalocal->nCurriculumCompactnessMovesMelhora
							 ) 
					);
			printf("%10d;", (buscalocal->nMovesInvalido+
							 buscalocal->nSwapsInvalido+
							 buscalocal->nKempesInvalido+
							 buscalocal->nTimeMovesInvalido+
							 buscalocal->nRoomMovesInvalido+
							 buscalocal->nLectureMovesInvalido+
							 buscalocal->nRoomStabilityMovesInvalido+
							 buscalocal->nMinWorkingDaysMovesInvalido+
							 buscalocal->nCurriculumCompactnessMovesInvalido
							 ) 
					);
			printf("%10d;", (buscalocal->nMoves+buscalocal->nSwaps+buscalocal->nKempes+buscalocal->nTimeMoves+
							 buscalocal->nRoomMoves+buscalocal->nLectureMoves+buscalocal->nRoomStabilityMoves+
							 buscalocal->nMinWorkingDaysMoves+buscalocal->nCurriculumCompactnessMoves) );
			printf("%10d;", buscalocal->nMoves+buscalocal->nLectureMoves);
			printf("%10d;", buscalocal->nSwaps);
			printf("%10d;", buscalocal->nKempes+buscalocal->nRoomStabilityMoves+buscalocal->nMinWorkingDaysMoves);
		}
		delete (buscalocal);

		f3 += ind->fitness;
		mediaSolucoes += ind->fitness;

		//DELETE
		if ((bestInd == NULL) || (ind->fitness < bestInd->fitness)) {
			if (bestInd != NULL)
				delete (bestInd);
			bestInd = ind;
		} else
			delete (ind);

		if (info) {
			printf("\n");
		}
		//FIM ITERACAO
		iteracoesExecutadas++;
		if (imprimeGrafico){
			printf("%6.2f;", (clock() / CLOCKS_PER_SEC) - tempo_inicial);
			printf("%3d;%6.0f\n", iteracoesExecutadas, (clock() / CLOCKS_PER_SEC) - tempoExecucao);
		}
		if (this->tempoLimite <= (clock() / CLOCKS_PER_SEC) - tempoExecucao)
			break;

	}
	tempoExecucao = (clock() / CLOCKS_PER_SEC) - tempoExecucao;


	if (info) {
		printf("Executou %d iteracoes em %f s\n", iteracoesExecutadas, tempoExecucao);
		printf("Num Movimentos Realizados: %d\n", nMovimentosRealizados);
	}
	if (infoSD) {
		printf("%f\n", tempoExecucao);
		printf("%d\n", nMovimentosRealizados);
	}
	if (geraMatrizGC){
		ImprimeMatriz_GC(p, i, tempoExecucao);
		DesalocaMatrizes_GC(p);
	}

	mediaSolucoes /= iteracoesExecutadas;

	soft1 /= iteracoesExecutadas;
	soft2 /= iteracoesExecutadas;
	soft3 /= iteracoesExecutadas;
	soft4 /= iteracoesExecutadas;

	f1 /= iteracoesExecutadas;
	f2 /= iteracoesExecutadas;
	f3 /= iteracoesExecutadas;

	return bestInd;
}

Individuo* Grasp::executaHillClimbing(Problema* p, Individuo* indInicial, BuscaLocal** bl) {
	BuscaLocal* buscalocal;
	HillClimbing* hc;
	Individuo* ind;

	hc = new HillClimbing(p, nMaxIterBuscaLocal, k);
	ind = hc->executa(indInicial, vizinhancasUtilizadas, nMaxIterSemMelhora);
	buscalocal = hc;
	*bl = buscalocal;
	return ind;
}

Individuo* Grasp::executaSteepestDescent(Problema* p, Individuo* indInicial, BuscaLocal** bl) {
	BuscaLocal* buscalocal;
	SDConjugado* sd;
	Individuo* ind;

	sd = new SDConjugado(p);
	ind = sd->executa(indInicial, vizinhancasUtilizadas);
	buscalocal = sd;
	*bl = buscalocal;
	return ind;
}

Individuo* Grasp::executaSimulatedAnnealing(Problema* p, Individuo* indInicial, BuscaLocal** bl) {
	BuscaLocal* buscalocal;
	SimulatedAnnealing* sa;
	Individuo* ind;

	sa = new SimulatedAnnealing(p, nMaxIterSemMelhora, temperaturaInicial, temperaturaFinal, taxaDecaimentoTemperatura);
	if (vizinhancasUtilizadas == 100){
		ind = sa->executa(indInicial, 10, info);
		ind = sa->executa(ind, 15, info);
	}
	else ind = sa->executa(indInicial, vizinhancasUtilizadas, info);
	buscalocal = sa;
	*bl = buscalocal;
	return ind;
}

Individuo* Grasp::executaBuscaTeste(Problema* p, Individuo* indInicial, BuscaLocal** bl) {
	BuscaLocal* buscalocal;
	BuscaTeste* sd;
	Individuo* ind;

	sd = new BuscaTeste(p);
	ind = sd->executa(indInicial, vizinhancasUtilizadas);
	buscalocal = sd;
	*bl = buscalocal;
	return ind;
}

void Grasp::atualizaEstatisticasBuscaLocal(BuscaLocal* buscalocal){
	if( buscalocal != NULL ){
		nMovimentosRealizados 		+= buscalocal->nMovimentosRealizados;
		nMoves 						+= buscalocal->nMoves;
		nSwaps 						+= buscalocal->nSwaps;
		nKempes 					+= buscalocal->nKempes;
		nTimeMoves 					+= buscalocal->nTimeMoves;
		nRoomMoves 					+= buscalocal->nRoomMoves;
		nLectureMoves 				+= buscalocal->nLectureMoves;
		nRoomStabilityMoves 		+= buscalocal->nRoomStabilityMoves;
		nMinWorkingDaysMoves 		+= buscalocal->nMinWorkingDaysMoves;
		nCurriculumCompactnessMoves += buscalocal->nCurriculumCompactnessMoves;
		nMovesMelhora 				+= buscalocal->nMovesMelhora;
		nSwapsMelhora 				+= buscalocal->nSwapsMelhora;
		nKempesMelhora 				+= buscalocal->nKempesMelhora;
		nTimeMovesMelhora 			+= buscalocal->nTimeMovesMelhora;
		nRoomMovesMelhora 			+= buscalocal->nRoomMovesMelhora;
		nLectureMovesMelhora 		+= buscalocal->nLectureMovesMelhora;
		nRoomStabilityMovesMelhora 	+= buscalocal->nRoomStabilityMovesMelhora;
		nMinWorkingDaysMovesMelhora += buscalocal->nMinWorkingDaysMovesMelhora;
		nCurriculumCompactnessMovesMelhora += buscalocal->nCurriculumCompactnessMovesMelhora;
		nMovesValidos 				+= buscalocal->nMovesValidos;
		nSwapsValidos 				+= buscalocal->nSwapsValidos;
		nKempesValidos 				+= buscalocal->nKempesValidos;
		nTimeMovesValidos 			+= buscalocal->nTimeMovesValidos;
		nRoomMovesValidos 			+= buscalocal->nRoomMovesValidos;
		nLectureMovesValidos 		+= buscalocal->nLectureMovesValidos;
		nRoomStabilityMovesValidos 	+= buscalocal->nRoomStabilityMovesValidos;
		nMinWorkingDaysMovesValidos += buscalocal->nMinWorkingDaysMovesValidos;
		nCurriculumCompactnessMovesValidos += buscalocal->nCurriculumCompactnessMovesValidos;
		nMovesInvalido 				+= buscalocal->nMovesInvalido;
		nSwapsInvalido 				+= buscalocal->nSwapsInvalido;
		nKempesInvalido 			+= buscalocal->nKempesInvalido;
		nTimeMovesInvalido 			+= buscalocal->nTimeMovesInvalido;
		nRoomMovesInvalido 			+= buscalocal->nRoomMovesInvalido;
		nLectureMovesInvalido 		+= buscalocal->nLectureMovesInvalido;
		nRoomStabilityMovesInvalido 	+= buscalocal->nRoomStabilityMovesInvalido;
		nMinWorkingDaysMovesInvalido 	+= buscalocal->nMinWorkingDaysMovesInvalido;
		nCurriculumCompactnessMovesInvalido += buscalocal->nCurriculumCompactnessMovesInvalido;
	}

}

void Grasp::ImprimeExecucao() {
	printf("\nSeed:  %d\n", seed);
	printf("Numero maximo de iteracoes:  %d\n", nMaxIter);

	if (opBuscaLocalGrasp == 2) {
		printf("Busca Local: HC\n");
		printf("Maximo de Iteracoes Busca Local sem Melhora: %d\n", nMaxIterSemMelhora);
		printf("Maximo de Iteracoes Busca Local Geral: %d\n", nMaxIterBuscaLocal);
		printf("k:  %d\n", k);
	} else if (opBuscaLocalGrasp == 6) {
		printf("Busca Local: SA\n");
		printf("Variacao de temperatura:  %lf a %lf, a uma taxa de %lf\n", temperaturaInicial, temperaturaFinal, taxaDecaimentoTemperatura);
		printf("N:  500\n");
		printf("Na: %3d\n", nMaxIterSemMelhora);
	} else if (opBuscaLocalGrasp == 1) {
		printf("Busca Local: SD\n");
	} else if (opBuscaLocalGrasp == 5) {
		printf("Busca Local: Teste\n");
	} else
		printf("Busca Local Invalida (%d)\n", opBuscaLocalGrasp);
	printf("Opcao de Vizinhanca:  %d\n", vizinhancasUtilizadas);
	printf("\nNumero de Movimentos Realizados: %d\n", nMovimentosRealizados);
	if( nMovimentosRealizados > 100 ){
	if (nMoves >= 100000 || nSwaps >= 100000) {
		printf(
				"Total de Movimentos   :  nMv: %6d nSw: %6d nK: %6d nTimeMv: %4d nRoomMv: %4d nLectMv: %4d nRoomStbMv: %4d nMinWkMv: %4d nCurCompMv: %4d\n",
				nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves,
				nRoomStabilityMoves, nMinWorkingDaysMoves,
				nCurriculumCompactnessMoves);
		printf(
				"Movimentos Invalidos  :  nMv: %6d nSw: %6d nK: %6d nTimeMv: %4d nRoomMv: %4d nLectMv: %4d nRoomStbMv: %4d nMinWkMv: %4d nCurCompMv: %4d\n",
				nMovesInvalido, nSwapsInvalido, nKempesInvalido,
				nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido,
				nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido,
				nCurriculumCompactnessMovesInvalido);
		printf(
				"Movimentos Validos    :  nMv: %6d nSw: %6d nK: %6d nTimeMv: %4d nRoomMv: %4d nLectMv: %4d nRoomStbMv: %4d nMinWkMv: %4d nCurCompMv: %4d\n",
				nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos,
				nRoomMovesValidos, nLectureMovesValidos,
				nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos,
				nCurriculumCompactnessMovesValidos);
		printf(
				"Movimentos com Melhora:  nMv: %6d nSw: %6d nK: %6d nTimeMv: %4d nRoomMv: %4d nLectMv: %4d nRoomStbMv: %4d nMinWkMv: %4d nCurCompMv: %4d",
				nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora,
				nRoomMovesMelhora, nLectureMovesMelhora,
				nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora,
				nCurriculumCompactnessMovesMelhora);
	} else {
		printf(
				"Total de Movimentos   :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n",
				nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves,
				nRoomStabilityMoves, nMinWorkingDaysMoves,
				nCurriculumCompactnessMoves);
		printf(
				"Movimentos Invalidos  :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n",
				nMovesInvalido, nSwapsInvalido, nKempesInvalido,
				nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido,
				nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido,
				nCurriculumCompactnessMovesInvalido);
		printf(
				"Movimentos Validos    :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n",
				nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos,
				nRoomMovesValidos, nLectureMovesValidos,
				nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos,
				nCurriculumCompactnessMovesValidos);
		printf(
				"Movimentos com Melhora:  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d",
				nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora,
				nRoomMovesMelhora, nLectureMovesMelhora,
				nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora,
				nCurriculumCompactnessMovesMelhora);
	}
	}

	printf("\nIteracoes do GRASP executadas: %d em %lf s\n", iteracoesExecutadas, tempoExecucao);
	printf("Media das solucoes:  %d\n", mediaSolucoes);

}

void Grasp::ImprimeExecucaoResumida() {

	if (nMoves > 0) {
		printf("Move\n");
		printf("%6d\n", nMoves);
		printf("%6d\n", nMovesInvalido);
		printf("%6d\n", nMovesValidos);
		printf("%6d\n", nMovesMelhora);
	}
	if (nSwaps > 0) {
		printf("Swap\n");
		printf("%6d\n", nSwaps);
		printf("%6d\n", nSwapsInvalido);
		printf("%6d\n", nSwapsValidos);
		printf("%6d\n", nSwapsMelhora);
	}
	if (nKempes > 0) {
		printf("Kempe\n");
		printf("%6d\n", nKempes);
		printf("%6d\n", nKempesInvalido);
		printf("%6d\n", nKempesValidos);
		printf("%6d\n", nKempesMelhora);
	}

}

void Grasp::AlocaMatrizes_GC(Problema* p, int maxIter){

  int i, j;

  matrizGC = (int**) malloc( (p->nVarTotal+1) * sizeof(int*) );
  for(i = 0; i < p->nVarTotal+1; i++) {
	matrizGC[i] = (int*) malloc( maxIter * sizeof(int) );
	for(j = 0; j < maxIter; j++)
		matrizGC[i][j] = 0;
  }
  
   /*int numres, numvar;

   numvar = p->nDisciplinas * p->nDias * p->nPerDias * p->nSalas;
   printf("%d;",numvar);
   numvar = p->nCurriculos * p->nDias * p->nPerDias;
   printf("%d;",numvar);
   numvar = p->nDisciplinas;
   printf("%d;",numvar);
   numvar = p->nDisciplinas * p->nSalas;
   printf("%d;",numvar);
   numvar = p->nDisciplinas * p->nDias;
   printf("%d;",numvar);
   numvar = 1;
   printf("%d;",numvar);
   
   numres = 1;
   printf("%d;",numres);
   numres = p->nDisciplinas;
   printf("%d;",numres);
   numres = p->nDias * p->nPerDias * p->nSalas;
   printf("%d;",numres);  
   numres = p->nDias * p->nPerDias * p->nDisciplinas;
   printf("%d;",numres);
   numres = p->nDias * p->nPerDias * p->professores.size();
   printf("%d;",numres); 
   numres = p->nDias * p->nPerDias * p->nCurriculos;
   printf("%d;",numres);
   numres = p->nRestricoes;
   printf("%d;",numres); 
   
   numres = p->nDisciplinas * p->nDias * p->nPerDias;
   printf("%d;",numres);
   numres = p->nDisciplinas * p->nDias;
   printf("%d;",numres);  
   numres = p->nDisciplinas;
   printf("%d;",numres);
   numres = p->nCurriculos * p->nDias * p->nPerDias;
   printf("%d;",numres); 
   numres = p->nDisciplinas * p->nDias * p->nPerDias * p->nSalas;
   printf("%d;",numres);
   numres = p->nDisciplinas * p->nSalas;
   printf("%d;",numres);
   printf("\n");*/
}

void Grasp::DesalocaMatrizes_GC(Problema* p){

  int i;

  for(i = 0; i < p->nVarTotal+1; i++) {
	free(matrizGC[i]);
  }
  free(matrizGC);
}

void Grasp::InsereElementosMatriz_GC(Problema* p, Individuo* ind, int col){
	
   int *varX, *varZ, *varW, *varY, *varV, *varR;
   int i, j, k, aux, indice, valFO;
   vector<Alocacao*>::iterator it;
   
   valFO = 0;
   /************ Variaveis X **************/
   varX = (int*) malloc( p->numVarX * sizeof(int) );
   for(i = 0; i < p->numVarX; i++)
      varX[i] = 0;

   for( it = ind->aulasAlocadas.begin(); it != ind->aulasAlocadas.end(); it++ ) {
	    indice = 0;
	    for (i = 0; i < (*it)->aula->disciplina->numeroSequencial; i++)
	      for (j = 0; j < p->nHorarios; j++)
	        if (!p->indisponibilidades->EhIndisponivel(i,j))
	           indice++;
	    for (j = 0; j < (*it)->horario->horario; j++)
	      if (!p->indisponibilidades->EhIndisponivel((*it)->aula->disciplina->numeroSequencial,j))
	         indice++;
		varX[indice] = 1;
   } 
   
   /************ Variaveis Y **************/
   aux = 0;
   varY = (int*) malloc(p->numVarY * sizeof(int));
   for(i = 0; i < p->numVarY; i++)
      varY[i] = 0;
	    
   for( it = ind->aulasAlocadas.begin(); it != ind->aulasAlocadas.end(); it++ ) {
	    if ((*it)->sala->capacidade < (*it)->aula->disciplina->nAlunos) {
			indice = 0;
			for (k = 0; k < p->nSalasDiff; k++)
				if (p->vetTamSal[k] == (*it)->sala->capacidade)
					aux = k;

			for (k = 0; k < aux; k++){ 
			  for (i = 0; i < p->nDisciplinas; i++)
				if(p->vetNumAlun[i] > p->vetTamSal[k])
				  for (j = 0; j < p->nHorarios; j++)
					 if (!p->indisponibilidades->EhIndisponivel(i,j))
						indice++;
			} 
        
			  for (i = 0; i < (*it)->aula->disciplina->numeroSequencial; i++){
				if(p->vetNumAlun[i] > p->vetTamSal[aux])
				  for (j = 0; j < p->nHorarios; j++)
					 if (!p->indisponibilidades->EhIndisponivel(i,j))
						indice++;
			  }         
			  for (j = 0; j < (*it)->horario->horario; j++){
					 if (!p->indisponibilidades->EhIndisponivel((*it)->aula->disciplina->numeroSequencial,j))
						indice++;
			  }
			varY[indice] = 1;
			if (aux != p->nSalasDiff-1)
			  valFO += MIN((*it)->aula->disciplina->nAlunos - p->vetTamSal[aux], p->vetTamSal[aux+1] - p->vetTamSal[aux]);
			else
			  valFO += ((*it)->aula->disciplina->nAlunos - p->vetTamSal[aux]);
		}
   }
   
   /************ Variaveis W **************/
   varW = (int*) malloc(p->numVarW * sizeof(int));
   for(i = 0; i < p->numVarW; i++)
      varW[i] = 0;

   int contaDias;
   list<Disciplina*>::iterator it1;
   for (j = 0; j < p->nDisciplinas; j++) {
		contaDias = 0;
		it1 = p->disciplinas.begin();
			advance(it1, j);
		for( i = 0; i < p->nDias; i++) {
			if( ind->Alocacao_dias_utilizados[j][i] > 0 )
				contaDias++;
		}
		if( contaDias < (*it1)->minDiasSemana ){
			varW[j] = (*it1)->minDiasSemana - contaDias;
			valFO += (varW[j]*5);
		}    
   }
   
   /************ Variaveis V **************/
   varV = (int*) malloc(p->numVarV * sizeof(int));
   for(i = 0; i < p->numVarV; i++)
      varV[i] = 0;
      
   for (i = 0; i < p->nCurriculos; i++) {
     for (j = 0; j < p->nDias; j++) {
       for (k = 0; k < p->nPerDias; k++) {
			Alocacao* a = ind->matrizAlocacaoCurriculoDiasPeriodos[i][j][k];
			if (a == NULL) 
				continue;
			Alocacao* prev = ( k == 0 ? NULL : ind->matrizAlocacaoCurriculoDiasPeriodos[i][j][k-1]);
			Alocacao* next = ( k+1 == p->nPerDias ? NULL : ind->matrizAlocacaoCurriculoDiasPeriodos[i][j][k+1]);
			if (next==NULL && prev==NULL) {
				indice = i * p->nDias * p->nPerDias;
				indice += j * p->nPerDias;
				indice += k;
				varV[indice] = 1;
				valFO += 2;
			}
       }
     }
   }

   /************ Variaveis Z **************/
   varZ = (int*) malloc(p->numVarZ * sizeof(int));
   for(i = 0; i < p->numVarZ; i++)
      varZ[i] = 0;  
   
   for (j = 0; j < p->nDisciplinas; j++) {
		it1 = p->disciplinas.begin();
			advance(it1, j);
		for( i = 0; i < p->nDias; i++) {
			if( ind->Alocacao_dias_utilizados[j][i] > 0 ){
				varZ[(j*p->nDias)+i] =1;
			}
		}
   }
   
   /************ Variaveis R **************/
   varR = (int*) malloc(p->numVarR * sizeof(int));
   for(i = 0; i < p->numVarR; i++)
      varR[i] = 0;
      
   for (i = 0; i < p->nCurriculos; i++) {
     for (j = 0; j < p->nDias; j++) {
       for (k = 0; k < p->nPerDias; k++) {
			Alocacao* a = ind->matrizAlocacaoCurriculoDiasPeriodos[i][j][k];
			if (a != NULL){
				indice = i * p->nDias * p->nPerDias;
				indice += j * p->nPerDias;
				indice += k;
				varR[indice] = 1;
			}
       }
     }
   }
      
   /*******************Insere Coluna*******************/
   i = 0;

   //matrizGC[i][col] = ind->fitness;
   matrizGC[i][col] = valFO;
   i++;

   j = 0;
   while (j < p->numVarX){
      matrizGC[i][col] = varX[j];
      i++;
      j++;
   }

   j = 0;
   while (j < p->numVarY){
      matrizGC[i][col] = varY[j];
      i++;
      j++;
   }

   j = 0;
   while (j < p->numVarW){
      matrizGC[i][col] = varW[j];
      i++;
      j++;
   }

   j = 0;
   while (j < p->numVarV){
      matrizGC[i][col] = varV[j];
      i++;
      j++;
   }

   j = 0;
   while (j < p->numVarZ){
      matrizGC[i][col] = varZ[j];
      i++;
      j++;
   }
   
   j = 0;
   while (j < p->numVarR){
      matrizGC[i][col] = varR[j];
      i++;
      j++;
   }

   free(varX);
   free(varZ);
   free(varW);
   free(varY);
   free(varV);
   free(varR);
}

void Grasp::ImprimeMatriz_GC(Problema* p, int maxIter, double tempoExecucao){
   
   char arq[50];
   strcpy(arq, "GeracaoColuna-2/colunas/");
   strcat(arq, p->nome);
   strcat(arq, ".gc");
    
   FILE *f;
   // Abre Arquivo
   f = fopen(arq, "w");
   if (f == NULL) {
        printf("Erro de abertura do arquivo %s\n", arq);
        exit(1);
   }

   fprintf(f, "%d\n", p->nVarTotal);
   fprintf(f, "%d\n", maxIter);
   fprintf(f, "%lf\n", tempoExecucao);
   
   for( int i = 0; i < p->nVarTotal+1; i++) {
		for (int j = 0; j < maxIter; j++) {
			fprintf(f, "%d ", matrizGC[i][j]);
		}
		fprintf(f, "\n");
		 
   }

   fclose(f);
}

/* Modelo Burke
void Grasp::InsereElementosMatriz_GC(Problema* p, Individuo* ind, int col){

   int *varX, *varZ, *varQ, *varY, *varV;
   int i, j, u, d, r, s, c, indice, numVarX, numVarZ, numVarQ, numVarY, numVarV;

   ************ Variaveis X **************
   numVarX = p->nDias * p->nPerDias * p->nSalas * p->nDisciplinas;
   varX = (int*) malloc( numVarX * sizeof(int) );
   for(i = 0; i < numVarX; i++)
	varX[i] = 0;

   vector<Alocacao*>::iterator it;
   for( it = ind->aulasAlocadas.begin(); it != ind->aulasAlocadas.end(); it++ ) {
		indice = (*it)->horario->horario * p->nSalas * p->nDisciplinas;
		indice += (*it)->sala->numeroSequencial * p->nDisciplinas;
		indice += (*it)->aula->disciplina->numeroSequencial;
		varX[indice] = 1;
   }

   ************ Variaveis Z **************
   numVarZ = p->nDias * p->nPerDias * p->nCurriculos;
   varZ = (int*) malloc( numVarZ * sizeof(int) );
   for(i = 0; i < numVarZ; i++)
	varZ[i] = 0;

   for (u = 0; u < p->nCurriculos; u++) {
     for (d = 0; d < p->nDias; d++) {
       for (s = 0; s < p->nPerDias; s++) {
            Alocacao* a = ind->matrizAlocacaoCurriculoDiasPeriodos[u][d][s];
	    if (a == NULL) 
		continue;
	    Alocacao* prev = ( s == 0 ? NULL : ind->matrizAlocacaoCurriculoDiasPeriodos[u][d][s-1]);
	    Alocacao* next = ( s+1 == p->nPerDias ? NULL : ind->matrizAlocacaoCurriculoDiasPeriodos[u][d][s+1]);
	    if (next==NULL && prev==NULL) {
		indice = u * p->nDias * p->nPerDias;
		indice += d * p->nPerDias;
		indice += s;
		varZ[indice] = 1;
		
	    }
       }
     }
   }

   ************ Variaveis Q **************
   numVarQ = p->nDisciplinas;
   varQ = (int*) malloc( numVarQ * sizeof(int) );
   for(i = 0; i < numVarQ; i++)
	varQ[i] = 0;

   int contaDias;
   list<Disciplina*>::iterator it1;
   for (c = 0; c < p->nDisciplinas; c++) {
	contaDias = 0;
	it1 = p->disciplinas.begin();
        advance(it1, c);
	for( i = 0; i < p->nDias; i++) {
		if( ind->Alocacao_dias_utilizados[c][i] > 0 )
			contaDias++;
	}
	if( contaDias < (*it1)->minDiasSemana )
		varQ[c] = (*it1)->minDiasSemana - contaDias;    
   }

   ************ Variaveis Y **************
   numVarY = p->nDisciplinas * p->nSalas;
   varY = (int*) malloc( numVarY * sizeof(int) );
   for(i = 0; i < numVarY; i++)
	varY[i] = 0;

   for( r = 0; r < p->nSalas; r++) {
	for (c = 0; c < p->nDisciplinas; c++) {
		if( ind->Alocacao_salas_utilizadas[c][r] > 0 ){
			indice = r * p->nDisciplinas;
			indice += c;
			varY[indice] = 1;
		}
	} 
   }

   ************ Variaveis V **************
   numVarV = p->nDisciplinas * p->nDias;
   varV = (int*) malloc( numVarV * sizeof(int) );
   for(i = 0; i < numVarV; i++)
	varV[i] = 0;

   vector<Alocacao*>::iterator it2;
   for( it2 = ind->aulasAlocadas.begin(); it2 != ind->aulasAlocadas.end(); it2++ ) {
		indice = (*it2)->horario->dia *  p->nDisciplinas;
		indice += (*it2)->aula->disciplina->numeroSequencial;
		varV[indice] = 1;
   }

   *******************Insere Coluna*******************
   i = 0;

   matrizGC[i][col] = ind->fitness;
   i++;

   j = 0;
   while (j < numVarX){
      matrizGC[i][col] = varX[j];
      i++;
      j++;
   }

   j = 0;
   while (j < numVarZ){
      matrizGC[i][col] = varZ[j];
      i++;
      j++;
   }

   j = 0;
   while (j < numVarQ){
      matrizGC[i][col] = varQ[j];
      i++;
      j++;
   }

   j = 0;
   while (j < numVarY){
      matrizGC[i][col] = varY[j];
      i++;
      j++;
   }

   j = 0;
   while (j < numVarV){
      matrizGC[i][col] = varV[j];
      i++;
      j++;
   }

   matrizGC[i][col] = p->nDisciplinas;

   free(varX);
   free(varZ);
   free(varQ);
   free(varY);
   free(varV);
}

void Grasp::ImprimeMatriz_GC(Problema* p, int maxIter, double tempoExecucao){
   
   char arq[50];
   strcpy(arq, "GeracaoColuna/colunas/");
   strcat(arq, p->nome);
   strcat(arq, ".gc");
    
   FILE *f;
   // Abre Arquivo
   f = fopen(arq, "w");
   if (f == NULL) {
        printf("Erro de abertura do arquivo %s\n", arq);
        exit(1);
   }

   fprintf(f, "%d\n", p->nVarTotal);
   fprintf(f, "%d\n", maxIter);
   fprintf(f, "%lf\n", tempoExecucao);

   for( int i = 0; i < p->nVarTotal+1; i++) {
	for (int j = 0; j < maxIter; j++) {
	    fprintf(f, "%d ", matrizGC[i][j]);
	}
        fprintf(f, "\n"); 
   }

   fclose(f);
}*/
