#include "multiStart.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "/opt/ibm/ILOG/CPLEX_Studio1271/cplex/include/ilcplex/cplex.h"
#include "/home/edmar/Documentos/timetabling-edmar/metis-5.0.1-X64/include/metis.h"

#define MAX(x,y) (((x) > (y)) ? (x) : (y))

int PESOS[4] = {1,2,5,1};
double f_pmr, f_sp;
double erro = 0.000001;
CPXENVptr env; 
CPXLPptr pmr, sp;
char *localInst = "../../instances/";
double totalSol = 0;
int extraTime = 0;
int indiceMelhorColuna, bestFO, newFO;
int pesoMAX = 99999;
int minnumCur = 99999, maxnumCur = 0, minnumDis = 99999, maxnumDis = 0;

//------------------------------------------------------------------------------
int main(int argc, char **argv){

 facDes = 1;     // Fator de Desbalanceamento
 numSubProb = 2; // Número de Subproblemas
 comPeso = 0;    // Booleano para considerar arestas/vértices com peso na partição
 time_t hi,hf;
 
 /**********************************************/
 hi = time(NULL); 
 lerInstancia(argv[1]);
 configuraParametros(argc, argv);
 hf = time(NULL);
 //printf("Tempo Leitura Instância: %.2f\n\n",difftime(hf,hi));
 
 aloca(); 
 
 /**********************************************/
 hi = time(NULL);  
 partGrafo(numSubProb, facDes);
 hf = time(NULL);
 //printf("Tempo Particionar Grafo: %.2f\n",difftime(hf,hi));
 //printf("Fat. Desb. = %d, Num. Subp. = %d, Num. Cortes = %d, Com Peso = %d\n\n", facDes, numSubProb, numCortes, comPeso);
 
 /**********************************************/
 hi = time(NULL); 
 for(int i = 0; i < numSubProb; i++)
    montarModelosSBP(i);
 hf = time(NULL);
 //printf("Tempo Montar Modelos SBP: %.2f\n\n",difftime(hf,hi)); 
 
 /**********************************************/
 hi = time(NULL); 
 for(int i = 0; i < numSubProb; i++)
    if (vetQuantDisPrt[i] > 0)
		execCpx_SubProblemas(i);
 hf = time(NULL);
 //printf("Tempo Total Execução SBP no CPX: %.2f\n",difftime(hf,hi));
 if (extraTime) 
	//printf("EXTRAPOLOU TEMPO: SIM\n");
	 printf("%d;%d;%.2f;%.2f;SIM;\n", comPeso, numSubProb, totalSol, difftime(hf,hi));
 else
 	//printf("EXTRAPOLOU TEMPO: NÃO\n"); 
 	printf("%d;%d;%.2f;%.2f;NÃO;\n", comPeso,numSubProb, totalSol, difftime(hf,hi));
 //printf("TOTAL LB: %.2f\n",totalSol);*/
 
 /******************************************************************
  * IMPRESSÃO ******************************************************
  *****************************************************************/
  //printf("%d;%d;%d;\n", facDes, numSubProb, numCortes);
  /*int numCurriculos, numd;
  for(int i = 0; i < numSubProb; i++){
	  numCurriculos = 0;
	  for(int j = 0; j < numTur__; j++){
		  if (vetTurmasPrt[j] == i){
			 numCurriculos++;
			 for(int k = 0; k < vetTurmas__[j].numDis_; k++){
				vetDisciplinasPrt[vetTurmas__[j].vetDis_[k]] = i+1;
			 }
		  }
	  }
	  	    
	  numd = 0;
	  for(int j = 0; j < numDis__; j++)
		  if (vetDisciplinasPrt[j] == i+1)
			 numd++;
	  //printf("P%d;%d;%d;\n", i+1, numCurriculos, numd);
	  if (minnumCur > numCurriculos)
	     minnumCur = numCurriculos;
	  if (maxnumCur < numCurriculos)
	     maxnumCur = numCurriculos;
	     
	  if (minnumDis > numd)
	     minnumDis = numd;
	  if (maxnumDis < numd)
	     maxnumDis = numd;
  }
  
  int totalCortado = 0;
  for(int i = 0; i < numTur__-1; i++){ 
	   for(int j = i+1; j < numTur__; j++){
		  if (vetTurmasPrt[i] != vetTurmasPrt[j]){
				    for(int k = 0; k < vetTurmas__[i].numDis_; k++){
						for(int l = 0; l < vetTurmas__[j].numDis_; l++){
							if (vetTurmas__[i].vetDis_[k] == vetTurmas__[j].vetDis_[l]){
								if ((vetTurmas__[i].numDis_ > 1) && (vetTurmas__[j].numDis_ > 1))
									totalCortado++;
								else
								    totalCortado += pesoMAX; 
							}
						}
					}
		  } 
	   }
   }
   
   int numCopias = 0;
   for(int j = 0; j < numDis__; j++){
	 if (vetDisciplinasPrtDiff[j]){
	    numCopias += ContarParticoesPorDisc(j);
	 }  
   }
   int numtotalDisc = numDis__+numCopias;
   float mediaDiscSub = numtotalDisc/numSubProb;
   printf("%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%f;\n", facDes, numSubProb, comPeso, numCortes, totalCortado, minnumCur, maxnumCur, minnumDis, maxnumDis, numDis__, numCopias, numtotalDisc, (float) numtotalDisc/numSubProb);
   */

 /******************************************************************
  * FIM IMPRESSÃO **************************************************
  *****************************************************************/
  
 desaloca();
 return 0;
}
int ContarParticoesPorDisc(int d){

   for(int i = 0; i < numSubProb; i++){
	  vetTurmasPrtAux[i] = 0;   
   }
   for(int j = 0; j < numTur__; j++)
     for(int k = 0; k < vetTurmas__[j].numDis_; k++)
        if (vetTurmas__[j].vetDis_[k] == d)
           vetTurmasPrtAux[vetTurmasPrt[j]] = 1;
   int soma = 0;
   for(int i = 0; i < numSubProb; i++){
	  soma += vetTurmasPrtAux[i];   
   }  
  return (soma-1);
}
//------------------------------------------------------------------------------
void partGrafo(int sbp, int fd){

    idx_t *vetPrt;         // Tamanho: |V| = número de Currículos (Resposta da Metis em qual partição ficou cada vértice	
	idx_t *vetPosAdj;      // Tamanho: |V| + 1 --> Preenchido com a posição de início dos adjacentes de i na lista abaixo 
	idx_t *vetListAdj;     // Tamanho: |V| x |V| (pior caso) --> Preenchido com o índice dos vértices adjacentes
	idx_t *vwgt;           // Tamanho: |V| --> Peso de cada Vértice
	idx_t *adjwgt;         // Tamanho: |V| x |V| (pior caso) --> Peso de cada aresta
	
	int wgtFlag = 0;  // flag para indicar a utilizacao ou nao de pesos (0: sem pesos)
    int numFlag = 0;  // flag para indicar o estilo dos contadores (comeca em 0: 0) 
    
    idx_t *opts = new idx_t[METIS_NOPTIONS];
    METIS_SetDefaultOptions(opts);
    opts[METIS_OPTION_UFACTOR] = fd;
    
    idx_t *nCon = new idx_t[1];
    nCon[0] = 1; 
    
	vetPrt = new idx_t[numTur__];
    vetPosAdj = new idx_t[numTur__+1];
    vetListAdj = new idx_t[numTur__*numTur__];
    vwgt = new idx_t[numTur__];
    adjwgt = new idx_t[numTur__*numTur__];
    
    int pos = 0;
    int cont;
    vetPosAdj[0] = 0;
    
    for(int i = 0; i < numTur__; i++){
		for(int j = 0; j < numTur__; j++){
			
			//Verifica se dois currículos diferentes (i e j) possuem disciplinas em comum
			cont = 0;
			if (i != j){
				    for(int k = 0; k < vetTurmas__[i].numDis_; k++){
						for(int l = 0; l < vetTurmas__[j].numDis_; l++){
							if (vetTurmas__[i].vetDis_[k] == vetTurmas__[j].vetDis_[l])
								cont++;
						}
					}				
			}
			
			if (cont != 0){
				vetListAdj[pos] = j;
				if ((vetTurmas__[i].numDis_ > 1) && (vetTurmas__[j].numDis_ > 1))
				   adjwgt[pos] = cont;
				else
				   adjwgt[pos] = pesoMAX;
                pos++;			
			}
		}
		vetPosAdj[i+1] = pos;
		vwgt[i] = vetTurmas__[i].numDis_; 
	}
	
   idx_t aux1 = numTur__;
   idx_t aux2 = sbp;
   idx_t aux3;
   if (comPeso)
		METIS_PartGraphRecursive(&aux1,nCon,vetPosAdj,vetListAdj,vwgt,NULL,adjwgt,&aux2,NULL,NULL,opts,&aux3,vetPrt);
   else  
		METIS_PartGraphRecursive(&aux1,nCon,vetPosAdj,vetListAdj,NULL,NULL,NULL,&aux2,NULL,NULL,opts,&aux3,vetPrt);

   numCortes = aux3;
      
   // Atribui em qual partição cada currículo vai estar
   for(int i = 0; i < numTur__; i++){
	     vetTurmasPrt[i] = vetPrt[i];
   }
   
   // Inicializa vetor de disciplinas com uma partição inicial  
   for(int i = 0; i < numDis__; i++){
	vetDisciplinasPrt[i] = 0;
	vetDisciplinasPrtDiff[i] = 0;
   }

   /*for(int j = 0; j < numTur__; j++){
	 for(int k = 0; k < vetTurmas__[j].numDis_; k++){
	     vetDisciplinasPrt[vetTurmas__[j].vetDis_[k]] = vetPrt[j];
     }
   }*/ 
   
   // Determina Disciplinas que estão em currículos de partições diferentes (cortes)
   for(int i = 0; i < numTur__-1; i++){ 
	   for(int j = i+1; j < numTur__; j++){
		  if (vetTurmasPrt[i] != vetTurmasPrt[j]){
			for(int k = 0; k < numDis__; k++){
				if (matDisTur__[k][i] && matDisTur__[k][j]) {
					vetDisciplinasPrtDiff[k] = 1;
				}
			}
		  } 
	   }
   } 
   
    // Verifica em qual partição a disciplina mais aparece
    /*int maiorPrt;
	for(int i = 0; i < numDis__; i++){
		if (vetDisciplinasPrtDiff[i]){
			for(int j = 0; j < numSubProb; j++){
				vetQuantDisPrt[j] = 0;
				for(int k = 0; k < numTur__; k++){
					if ((vetTurmasPrt[k] == j) && (matDisTur__[i][k])){
						vetQuantDisPrt[j]++;
					}
				}
			}
			// Define partição em que a disciplina mais aparece
			maiorPrt = 0;
			for(int j = 1; j < numSubProb; j++){
				if (vetQuantDisPrt[j] > vetQuantDisPrt[maiorPrt]){
					maiorPrt = j;
				}
			}
			vetDisciplinasPrt[i] = maiorPrt;	
		}
	} */
	/***************************** INICIO IMPRIME GRAFO ***********************
	
	printf("VETOR LISTA DE ADJACENCIAS\n");
	for(int i = 0; i < pos; i++){
		aux = vetListAdj[i];
		printf("%d ",aux);
	}
	printf("\n");
	
	printf("VETOR POSICAO DE ADJACENCIAS\n");
 	for(int i = 0; i < numTur__+1; i++){
		aux = vetPosAdj[i];
		printf("%d ",aux);
	}
	printf("\n");
	
	printf("VETOR DE PARTIÇÕES\n");
 	for(int i = 0; i < numTur__; i++){
		printf("%d ",vetPrt[i]);
	}
	printf("\n");
	
	printf("VETOR DE TURMAS PARTIÇÕES\n");
 	for(int i = 0; i < numTur__; i++){
		printf("%d;\n",vetTurmasPrt[i]);
	}
	printf("\n");
	
	printf("VETOR DE PARTIÇÕES DAS DISCIPLINAS\n");
 	for(int i = 0; i < numDis__; i++){
		printf("%d;\n",vetDisciplinasPrt[i]);
	}
	printf("\n");
	
	printf("VETOR DE DISCIPLINAS EM PARTIÇÕES DIFERENTES\n");
 	for(int i = 0; i < numDis__; i++){
		printf("%d ",vetDisciplinasPrtDiff[i]);
	}
	printf("\n");  
    /***************************** FIM IMPRIME GRAFO ***********************/  
    
    delete[] vetPrt;
	delete[] vetPosAdj;
	delete[] vetListAdj;
	delete[] vwgt;
	delete[] adjwgt;
	delete[] opts;
	delete[] nCon;
	
}
//------------------------------------------------------------------------------
void execCpx_PMR(Solucao &s){
 int sts;
 double f;
 clock_t h; 

 //sts = CPXwriteprob(env,pmr,"Verifica_pmr.lp",NULL);
 //sts = CPXreadcopyprob(env,pmr,"Verifica_pmr.lp",NULL);

 h = clock();
 sts = CPXprimopt(env,pmr);
 h = clock() - h;

 s.tempo_ = (double)h/CLOCKS_PER_SEC;
 sts = CPXgetpi(env,pmr,VecDual__,0,CPXgetnumrows(env,pmr)-1);
 
 //sts = CPXgetx (env, pmr,x,0,CPXgetnumcols(env,pmr)-1);
 //sts = CPXgetmipobjval(env,pmr,&s.valSol_);
 //sts = CPXgetbestobjval(env,pmr,&s.bstNod_);
 //sts = CPXgetmipx(env,pmr,s.vetSol_,0,(numPerTot__*numSal__*numDis__-1));

}
//------------------------------------------------------------------------------
void execCpx_SP(Solucao &s){
 int sts;
 clock_t h; 

 //sts = CPXwriteprob(env,sp,"Verifica_sp.lp",NULL);
 //sts = CPXreadcopyprob(env,sp,"Verifica_sp.lp",NULL);

 h = clock();
 sts = CPXmipopt(env,sp);
 h = clock() - h;

 s.tempo_ = (double)h/CLOCKS_PER_SEC;
}
//------------------------------------------------------------------------------
void execCpx_SubProblemas(int sbp){
 Solucao s;
 int sts;
 char modelo[50];
 time_t hi,hf;

 sprintf(modelo,"SBP%d-",sbp+1);
 strcat(modelo,nomArqInst__);
 strcat(modelo,".lp");
 
 CPXENVptr env; 
 CPXLPptr lp;   
 env = CPXopenCPLEX(&sts);
 sts = CPXsetintparam(env,CPX_PARAM_SCRIND,CPX_OFF);
 sts = CPXsetdblparam(env,CPX_PARAM_TILIM,300);
 lp = CPXcreateprob(env,&sts,"");
 sts = CPXreadcopyprob(env,lp,modelo,NULL);
 s.numVar_ = CPXgetnumcols(env,lp);
 s.numRes_ = CPXgetnumrows(env,lp);
 
 hi = time(NULL);
 sts = CPXmipopt(env,lp);
 hf = time(NULL);
 s.tempo_ = difftime(hf,hi);
 sts = CPXgetmipobjval(env,lp,&s.valSol_);
 sts = CPXgetbestobjval(env,lp,&s.bstNod_);
 //sts = CPXgetmipx(env,lp,s.vetSol_,0,(numPerTot__*numSal__*numDis__-1));
 sts = CPXgetmipx(env,lp,s.vetSol_,0,CPXgetnumcols(env,lp)-1);
 sts = CPXfreeprob(env,&lp);
 sts = CPXcloseCPLEX(&env);
 
 /*printf(">>> RESULTADOS DO CPLEX\n");
 printf("Num. var....: %d\n",s.numVar_);
 printf("Num. res....: %d\n",s.numRes_);
 printf("Val. sol....: %f\n",s.valSol_);
 printf("Melhor no...: %f\n",s.bstNod_);
 if(s.valSol_ != 0)
   printf("GAP.........: %.2f\n",((s.valSol_ - s.bstNod_)/s.valSol_)*100);
 else
   printf("GAP.........: -\n");
 printf("Tempo.......: %.2f\n\n",s.tempo_);*/
 
 totalSol += s.valSol_;
 if (s.tempo_ >= 300)
	extraTime = 1;

 /*printf("SOLUÇÃO\n");	
 for(int i = 0; i < s.numVar_; i++){
    if (s.vetSol_[i] > 0){
      printf("%f %d;\n",s.vetSol_[i], i+1);
    }
 } */
 ContarDisciplinasSBP(sbp);
 escreverSol(s,sbp);
}
//------------------------------------------------------------------------------
void escreverSol(Solucao &s, int sbp){
 char saidaSol[50];
 
 sprintf(saidaSol,"SBP%d-",sbp+1);
 strcat(saidaSol,nomArqInst__);
 strcat(saidaSol,".res");
 
 int pos = 0;
 FILE *f = fopen(saidaSol,"w");
  
 for(int d = 0; d < numDia__; d++){
    for (int i = 0; i < numPerDia__; i++) {
        for (int j = 0; j < numSal__; j++) {
            for (int k = 0; k < numDis__; k++) {
				if (vetDisciplinasPrt[k] == sbp) {
					if (s.vetSol_[pos] > 0){
						fprintf(f,"%s %s %d %d\n", vetDisciplinas__[k].nome_, vetSalas__[j].nome_, d, i);		
					}
					pos++;
				}
            }
        }
    }
 }
 fclose(f);
}
//-----------------------------------------------------------------------------*/
void ContarDisciplinasSBP(int sbp){
	
 	for(int i = 0; i < numDis__; i++){
		vetDisciplinasPrt[i] = -1;;
	}
	for(int j = 0; j < numTur__; j++){
	 if(vetTurmasPrt[j] == sbp){
		for(int k = 0; k < vetTurmas__[j].numDis_; k++){
			vetDisciplinasPrt[vetTurmas__[j].vetDis_[k]] = vetTurmasPrt[j];
		}
	 }
	}

	vetQuantDisPrt[sbp] = 0;
    for(int i = 0; i < numDis__; i++)
		if (vetDisciplinasPrt[i] == sbp)
			vetQuantDisPrt[sbp]++;
}
//-----------------------------------------------------------------------------*/
void montarModelosSBP(int sbp){
 
 char arq[50];
 sprintf(arq,"SBP%d-",sbp+1);
 strcat(arq,nomArqInst__);
 strcat(arq,".lp");
 
 ContarDisciplinasSBP(sbp);
  
 int val;
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int p = 0; p < numPerTot__; p++){
	 for(int r = 0; r < numSal__; r++){
       for(int c = 0; c < numDis__; c++)
        if (vetDisciplinasPrt[c] == sbp) {
			if(vetDisciplinas__[c].numAlu_ > vetSalas__[r].capacidade_)
			  fprintf(f,"+ %d x_%d_%d_%d ",PESOS[0]*(vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),p,r,c);
			else
			  fprintf(f,"+ 0 x_%d_%d_%d ",p,r,c);
        }
       fprintf(f,"\n");
      }
    }
   fprintf(f,"\n\n\\Janelas de hor\E1rios\n");
   for(int u = 0; u < numTur__; u++){
	 if (vetTurmasPrt[u] == sbp){
		 for(int d = 0; d < numDia__; d++){
		   for(int s = 0; s < numPerDia__; s++)
			  fprintf(f,"+ %d z_%d_%d_%d ",PESOS[1],u,d,s);
		   fprintf(f,"\n");
		 }
	 }
    }
   fprintf(f,"\n\\Dias m\EDnimos\n");
   for(int c = 0; c < numDis__; c++)
     if (vetDisciplinasPrt[c] == sbp) 
		fprintf(f,"+ %d q_%d ",PESOS[2],c);
   fprintf(f,"\n\n\\Salas diferentes\n");
   for(int r = 0; r < numSal__; r++){
     for(int c = 0; c < numDis__; c++)
       if (vetDisciplinasPrt[c] == sbp) 
         fprintf(f,"+ %d y_%d_%d ",PESOS[3],r,c);
     fprintf(f,"\n");
    }
    fprintf(f,"\n\n\\Variaveis V\n");
   for(int d = 0; d < numDia__; d++){
     for(int c = 0; c < numDis__; c++)
       if (vetDisciplinasPrt[c] == sbp) 
		fprintf(f,"+ 0 v_%d_%d ",d,c);
     fprintf(f,"\n");
    }
   val = PESOS[3] * vetQuantDisPrt[sbp];
   fprintf(f,"- val\n");
   fprintf(f,"\nST\n");
   fprintf(f,"\n\\Valor constante\n");
   fprintf(f,"val = %d\n",val);

 fprintf(f,"\n\\ ------------------------------------ HARD------------------------------------\n");
 fprintf(f,"\n\\R1 - N\FAmero de aulas\n");
 for(int c = 0; c < numDis__; c++)
   if (vetDisciplinasPrt[c] == sbp){
	   for(int p = 0; p < numPerTot__; p++)
		 for(int r = 0; r < numSal__; r++)
		   fprintf(f,"+ x_%d_%d_%d ",p,r,c);
	   fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_);
   }
 fprintf(f,"\n\\R2 - Aulas na mesma sala no mesmo per\EDodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int r = 0; r < numSal__; r++){
     for(int c = 0; c < numDis__; c++)
      if (vetDisciplinasPrt[c] == sbp)
		fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
   }
 fprintf(f,"\n\\R3 - Aulas de uma disciplina no mesmo per\EDodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int c = 0; c < numDis__; c++)
	   if (vetDisciplinasPrt[c] == sbp){
		 for(int r = 0; r < numSal__; r++)
		  fprintf(f,"+ x_%d_%d_%d ",p,r,c);
		 fprintf(f,"<= 1\n");
	   }
 fprintf(f,"\n\\R4 - Aulas de um professor no mesmo per\EDodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int t = 0; t < numPro__; t++){
       for(int c = 0; c < numDis__; c++)
         if(vetDisciplinas__[c].professor_ == t)
           if (vetDisciplinasPrt[c] == sbp)
				for(int r = 0; r < numSal__; r++)
					fprintf(f,"+ x_%d_%d_%d ",p,r,c);
	   fprintf(f,"<= 1\n");
   }   
 fprintf(f,"\n\\R5 - Aulas de uma turma no mesmo per\EDodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int u = 0; u < numTur__; u++)
	 if (vetTurmasPrt[u] == sbp){  
		 for(int r = 0; r < numSal__; r++)
		   for(int c = 0; c < numDis__; c++)
		   if (vetDisciplinasPrt[c] == sbp){
				 for(int k = 0; k < vetTurmas__[u].numDis_; k++)
				   if(vetTurmas__[u].vetDis_[k] == c){
					 fprintf(f,"+ x_%d_%d_%d ",p,r,c);
					 break;
				   }
		   }
		 fprintf(f,"<= 1\n");
     }
 fprintf(f,"\n\\R6 - Restri\E7\F5es de oferta (aloca\E7\E3o)\n");
 for(int s = 0; s < numRes__; s++)
   if (vetDisciplinasPrt[vetRestricoes__[s].disciplina_] == sbp){
	   for(int r = 0; r < numSal__; r++)
		 fprintf(f,"+ x_%d_%d_%d ",vetRestricoes__[s].periodo_,r,vetRestricoes__[s].disciplina_);
	   fprintf(f,"= 0\n");
   }

   fprintf(f,"\n\\ ------------------------------------ SOFT------------------------------------\n");
   fprintf(f,"\n\\R7 - N\FAmero de salas usadas por disciplina\n");
   for(int c = 0; c < numDis__; c++)
     if (vetDisciplinasPrt[c] == sbp){
		 for(int d = 0; d < numDia__; d++)
		   for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++){
			 for(int r = 0; r < numSal__; r++)
			   fprintf(f,"+ x_%d_%d_%d ",p,r,c);
			 fprintf(f,"- v_%d_%d <= 0\n",d,c);
			}
		 fprintf(f,"\n");
     }
   fprintf(f,"\n\\R8 - N\FAmero de salas usadas por disciplina\n");
   for(int d = 0; d < numDia__; d++){
     for(int c = 0; c < numDis__; c++)
       if (vetDisciplinasPrt[c] == sbp){
		   for(int r = 0; r < numSal__; r++)
			 for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
			   fprintf(f,"+ x_%d_%d_%d ",p,r,c);
		   fprintf(f,"- v_%d_%d >= 0\n",d,c);
       }
     fprintf(f,"\n");
    }
   fprintf(f,"\n\\R9 - Dias m\EDnimos\n");
   for(int c = 0; c < numDis__; c++)
     if (vetDisciplinasPrt[c] == sbp){
		 for(int d = 0; d < numDia__; d++)
		   fprintf(f,"+ v_%d_%d ",d,c);
		 fprintf(f,"+ q_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
     }
   fprintf(f,"\n\\R10 a R13+#PER_DIA - Janelas no hor\E1rio das turmas\n");
   for(int u = 0; u < numTur__; u++)
     if (vetTurmasPrt[u] == sbp){
		 for(int d = 0; d < numDia__; d++){
		   for(int c = 0; c < numDis__; c++)
			 if(matDisTur__[c][u] == 1){
			   if (vetDisciplinasPrt[c] == sbp){
					for(int r = 0; r < numSal__; r++)
						fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__),r,c,(d*numPerDia__)+1,r,c);
			   }
			 }
		   fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,0);
		 }
     }
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     if (vetTurmasPrt[u] == sbp){
		 for(int d = 0; d < numDia__; d++){
		   for(int c = 0; c < numDis__; c++)
			 if(matDisTur__[c][u] == 1){
			   if (vetDisciplinasPrt[c] == sbp){
				for(int r = 0; r < numSal__; r++)
					fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+numPerDia__-1,r,c,(d*numPerDia__)+numPerDia__-2,r,c);
			   }
			 }
		   fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,numPerDia__-1);
		 }
     }
  fprintf(f,"\n");
  for(int s = 1; s < numPerDia__-1; s++){
     for(int u = 0; u < numTur__; u++)
       if (vetTurmasPrt[u] == sbp){
		   for(int d = 0; d < numDia__; d++){
			 for(int c = 0; c < numDis__; c++)
			   if(matDisTur__[c][u] == 1){
				if (vetDisciplinasPrt[c] == sbp){
					for(int r = 0; r < numSal__; r++)
						fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+s,r,c,(d*numPerDia__)+s-1,r,c,(d*numPerDia__)+s+1,r,c);
				 }
				}
			 fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,s);
			}
       }
    fprintf(f,"\n");
   }
   fprintf(f,"\n\\R14 - Salas utilizadas por disciplina\n");
   for(int p = 0; p < numPerTot__; p++){
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
         if (vetDisciplinasPrt[c] == sbp)
			fprintf(f,"x_%d_%d_%d - y_%d_%d <= 0\n",p,r,c,r,c);
     fprintf(f,"\n");
    }
   fprintf(f,"\n\\R15 - Salas utilizadas por disciplina\n");
   for(int r = 0; r < numSal__; r++){
     for(int c = 0; c < numDis__; c++)
       if (vetDisciplinasPrt[c] == sbp){
		   for(int p = 0; p < numPerTot__; p++)
			 fprintf(f,"+ x_%d_%d_%d ",p,r,c);
		   fprintf(f,"- y_%d_%d >= 0\n",r,c);
       }
     fprintf(f,"\n");
    }

 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Vari\E1veis x\n");
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerTot__; p++)
     for(int c = 0; c < numDis__; c++)
       if (vetDisciplinasPrt[c] == sbp)
		fprintf(f,"0 <= x_%d_%d_%d <= 1\n",p,r,c);
   fprintf(f,"\n\\Vari\E1veis v\n");
   for(int d = 0; d < numDia__; d++)
     for(int c = 0; c < numDis__; c++)
	   if (vetDisciplinasPrt[c] == sbp)
         fprintf(f,"0 <= v_%d_%d <= 1\n",d,c);
   fprintf(f,"\n\\Vari\E1veis q\n");
   for(int c = 0; c < numDis__; c++)
     if (vetDisciplinasPrt[c] == sbp)
       fprintf(f,"0 <= q_%d <= %d\n",c,numDia__);
   fprintf(f,"\n\\Vari\E1veis z\n");
   for(int u = 0; u < numTur__; u++)
     if (vetTurmasPrt[u] == sbp)
		 for(int d = 0; d < numDia__; d++)
		   for(int s = 0; s < numPerDia__; s++)
			  fprintf(f,"0 <= z_%d_%d_%d <= 1\n",u,d,s);
   fprintf(f,"\n\\Vari\E1veis y\n");
   for(int r = 0; r < numSal__; r++)
     for(int c = 0; c < numDis__; c++)
       if (vetDisciplinasPrt[c] == sbp)
		 fprintf(f,"0 <= y_%d_%d <= 1\n",r,c);
       
   fprintf(f,"\nGENERALS\n");
   fprintf(f,"\n\\Vari\E1veis q\n");
   for(int c = 0; c < numDis__; c++)
	if (vetDisciplinasPrt[c] == sbp)
     fprintf(f,"q_%d\n",c);
 
	fprintf(f,"\nBINARIES\n");
	 for(int r = 0; r < numSal__; r++)
	   for(int p = 0; p < numPerTot__; p++)
		 for(int c = 0; c < numDis__; c++)
		   if (vetDisciplinasPrt[c] == sbp)
		     fprintf(f,"x_%d_%d_%d\n",p,r,c);
	   fprintf(f,"\n\\Vari\E1veis v\n");
	   for(int d = 0; d < numDia__; d++)
		 for(int c = 0; c < numDis__; c++)
		   if (vetDisciplinasPrt[c] == sbp)
		     fprintf(f,"v_%d_%d\n",d,c);
	   fprintf(f,"\n\\Vari\E1veis z\n");
	   for(int u = 0; u < numTur__; u++)
		 if (vetTurmasPrt[u] == sbp)
		   for(int d = 0; d < numDia__; d++)
		     for(int s = 0; s < numPerDia__; s++)
			   fprintf(f,"z_%d_%d_%d\n",u,d,s);
	   fprintf(f,"\n\\Vari\E1veis y\n");
	   for(int r = 0; r < numSal__; r++)
		 for(int c = 0; c < numDis__; c++)
		   if (vetDisciplinasPrt[c] == sbp)
		     fprintf(f,"y_%d_%d\n",r,c);

 fprintf(f,"\nEND");
 fclose(f);
 
}
//------------------------------------------------------------------------------
void lerInstancia(char *arq){
	
 int pos;
 char aux[50];
 char nArq[50];
 
 strcpy(nArq,localInst);
 strcat(nArq,arq);
 strcat(nArq,".ctt");
 
 strcpy(nomArqInst__,arq);
 
 FILE *f = fopen(nArq,"r");
 
 if (f == NULL) {
	printf("Erro de abertura do arquivo %s\n", arq);
	exit(1);
 }

 fscanf(f,"Name: %s\n",&nomInst__);
 fscanf(f,"Courses: %d\n",&numDis__);
 fscanf(f,"Rooms: %d\n",&numSal__);
 fscanf(f,"Days: %d\n",&numDia__);
 fscanf(f,"Periods_per_day: %d\n",&numPerDia__);
 fscanf(f,"Curricula: %d\n",&numTur__);
 fscanf(f,"Constraints: %d\n",&numRes__);
 fscanf(f,"\nCOURSES:\n");
 numPerTot__ = numDia__ * numPerDia__;
 numPro__ = 0;
 for(int i = 0; i < numDis__; i++)
  {
   fscanf(f,"%s %s %d %d %d\n",&vetDisciplinas__[i].nome_,&aux,
          &vetDisciplinas__[i].numPer_,&vetDisciplinas__[i].diaMin_,&vetDisciplinas__[i].numAlu_);
   pos = -1;
   for(int p = 0; p < numPro__; p++)
     if(strcmp(aux,vetProfessores__[p].nome_) == 0)
      {
       vetDisciplinas__[i].professor_ = p;
       pos = p;
       break;
      }
   if(pos == -1)
    {
     vetDisciplinas__[i].professor_ = numPro__;
     strcpy(vetProfessores__[numPro__].nome_,aux);
     numPro__++;
    }
  }
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numTur__; j++)
     matDisTur__[i][j] = 0;
 fscanf(f,"\nROOMS:\n");
 for(int i = 0; i < numSal__; i++)
   fscanf(f,"%s %d\n",&vetSalas__[i].nome_,&vetSalas__[i].capacidade_);
 fscanf(f,"\nCURRICULA:\n");
 for(int i = 0; i < numTur__; i++)
  {
   fscanf(f,"%s %d",&vetTurmas__[i].nome_,&vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
    {
     fscanf(f, "%s ",&aux);
     vetTurmas__[i].vetDis_[j] = -1;
     for(int k = 0; k < numDis__; k++)
       if(strcmp(aux,vetDisciplinas__[k].nome_) == 0)
        {
         vetTurmas__[i].vetDis_[j] = k;
         matDisTur__[k][i] = 1;
         break;
        }
    }
  }
 fscanf(f,"\nUNAVAILABILITY_CONSTRAINTS:\n");
 for(int i = 0; i < numRes__; i++) 
  {
   fscanf(f,"%s %d %d\n",&aux,&pos,&vetRestricoes__[i].periodo_);
   vetRestricoes__[i].periodo_ = (pos * numPerDia__) + vetRestricoes__[i].periodo_;
   vetRestricoes__[i].disciplina_ = -1;
   for(int j = 0; j < numDis__; j++)
     if(strcmp(aux,vetDisciplinas__[j].nome_) == 0)
      {
       vetRestricoes__[i].disciplina_ = j;
       break;
      }
  }
 fclose(f);
}
//------------------------------------------------------------------------------
void configuraParametros(int piNParam, char **piParams){
	
	for (int i = 2; i < piNParam; i++){
		if (strncmp("facDes", piParams[i], 6) == 0){
			facDes = atoi(piParams[i] + 7);
		} else if (strncmp("numSubProb", piParams[i], 10) == 0){
			numSubProb = atoi(piParams[i] + 11);
		} else if (strncmp("comPeso", piParams[i], 7) == 0){
			comPeso = atoi(piParams[i] + 8);
		}	
	}
}
//------------------------------------------------------------------------------
void testarEntrada(){
	
 printf("Name: %s\n",nomInst__);
 printf("Courses: %d\n",numDis__);
 printf("Rooms: %d\n",numSal__);
 printf("Days: %d\n",numDia__);
 printf("Periods_per_day: %d\n",numPerDia__);
 printf("Curricula: %d\n",numTur__);
 printf("Constraints: %d\n",numRes__);

 printf("\nDISCIPLINAS:\n");
 for(int i = 0; i < numDis__; i++)
   printf("%d  %s  %d  %d  %d  %d\n",i,vetDisciplinas__[i].nome_,vetDisciplinas__[i].professor_,vetDisciplinas__[i].numPer_,vetDisciplinas__[i].diaMin_,vetDisciplinas__[i].numAlu_);

 printf("\nTURMAS:\n");
 for(int i = 0; i < numTur__; i++)
  {
   printf("%d  %s  %d --> ",i,vetTurmas__[i].nome_,vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
     printf("%d  ",vetTurmas__[i].vetDis_[j]);
   printf("\n");
  } 

 printf("\nPROFESSORES:\n");
 for(int i = 0; i < numPro__; i++)
   printf("%d  %s\n",i,vetProfessores__[i].nome_);

 printf("\nSALAS:\n");
 for(int i = 0; i < numSal__; i++)
   printf("%d  %s  %d\n",i,vetSalas__[i].nome_,vetSalas__[i].capacidade_);

 printf("\nRESTRICOES:\n");
 for(int i = 0; i < numRes__; i++)
   printf("%d  %d  %d\n",i,vetRestricoes__[i].disciplina_,vetRestricoes__[i].periodo_);

 printf("\nDISC x TURMA:\n");
 for(int i = 0; i < numDis__; i++)
  {
   for(int j = 0; j < numTur__; j++)
     printf("%d  ",matDisTur__[i][j]);
   printf("\n");
  }
}
//------------------------------------------------------------------------------
void lerSolucoesIniciais(char *arq){
	int i, j;
	char aux[200];
	strcpy(aux, "colunas/");
	if (arq != NULL){
		strcat(aux, arq);
	    strcat(aux, "/");
    }
	strcat(aux, nomInst__);
	strcat(aux, ".gc");

	matFO__ = (double*)malloc(MAX_COL * sizeof(double));

	matGC__ = (int**)malloc(MAX_VAR * sizeof(int*));
	for (i = 0; i < MAX_VAR; i++) {
		matGC__[i] = (int*)malloc(MAX_COL * sizeof(int));
	}
	
	FILE *f = fopen(aux, "r");
	if (f == NULL) {
		printf("Erro de abertura do arquivo %s\n", aux);
		exit(1);
	}

	fscanf(f, "%d\n", &numVar__);
	fscanf(f, "%d\n", &numCol__);
	fscanf(f, "%lf\n", &tempoGCIniciais_);

	for (j = 0; j < numCol__; j++) {
		fscanf(f, "%lf ", &matFO__[j]);
	}
	fscanf(f, "\n");

	for (i = 0; i < numVar__; i++) {
		for (j = 0; j < numCol__; j++) {
			fscanf(f, "%d ", &matGC__[i][j]);
		}
		fscanf(f, "\n");
	}
	fclose(f);
	
	/* Guarda Indice Melhor Coluna Inicial*/
	indiceMelhorColuna = 0;
	bestFO = matFO__[0];
	for (j = 1; j < numCol__; j++) {
		if(matFO__[j] < bestFO){
		    bestFO = matFO__[j];
			indiceMelhorColuna = j;
		}
	}
	
    bestSol = (double*)malloc(numVar__ * sizeof(double));
    for (i = 0; i < numVar__; i++) {
		bestSol[i] = matGC__[i][indiceMelhorColuna];
	}
}
//------------------------------------------------------------------------------
void aloca(){
	vetQuantDisPrt = (int*) malloc(numSubProb * sizeof(int));
	vetTurmasPrtAux = (int*) malloc(numSubProb *sizeof(int));
	//vetTurmasPrt = new int[numTur__];
	//vetDisciplinasPrt = new int[numDis__];
}
//------------------------------------------------------------------------------
void desaloca(){
	free(vetQuantDisPrt);
	free(vetTurmasPrtAux);
	//delete[] vetTurmasPrt;
	//delete[] vetDisciplinasPrt;
}
//------------------------------------------------------------------------------
void MontaMatrizesIndices(){

    int i, j, k, cont;

    cont = 0;
//////////VARIAVEIS X
    for (i = 0; i < numPerTot__; i++) {
        indVarX__[i] = (int**) malloc(numSal__ * sizeof (int*));
        for (j = 0; j < numSal__; j++) {
            indVarX__[i][j] = (int*) malloc(numDis__ * sizeof (int));
        }
    }

    for (i = 0; i < numPerTot__; i++) {
        for (j = 0; j < numSal__; j++) {
            for (k = 0; k < numDis__; k++) {
                indVarX__[i][j][k] = cont;
				cont++;
            }
        }
    }
    
//////////VARIAVEIS Z
    for (i = 0; i < numTur__; i++) {
        indVarZ__[i] = (int**) malloc(numDia__ * sizeof (int*));
        for (j = 0; j < numDia__; j++) {
            indVarZ__[i][j] = (int*) malloc(numPerDia__ * sizeof (int));
        }
    }

    for (i = 0; i < numTur__; i++) {
        for (j = 0; j < numDia__; j++) {
            for (k = 0; k < numPerDia__; k++) {
                indVarZ__[i][j][k] = cont;
		cont++;
            }
        }
    }

//////////VARIAVEIS Q
    for (i = 0; i < numDis__; i++) {
	     indVarQ__[i] = cont;
	     cont++;
    }

//////////VARIAVEIS Y
    for (i = 0; i < numSal__; i++) {
        indVarY__[i] = (int*) malloc(numDis__ * sizeof (int));
    }

    for (i = 0; i < numSal__; i++) {
        for (j = 0; j < numDis__; j++) {
		indVarY__[i][j] = cont;
		cont++;
        }
    }

//////////VARIAVEIS V
    for (i = 0; i < numDia__; i++) {
        indVarV__[i] = (int*) malloc(numDis__ * sizeof (int));
    }

    for (i = 0; i < numDia__; i++) {
        for (j = 0; j < numDis__; j++) {
		indVarV__[i][j] = cont;
		cont++;
        }
    }
}
//------------------------------------------------------------------------------
void ConstroiMatrizD(){
    int i, j, c, d, p, r, u, s, contRes;

	contRes = 0;

	/* R10 a ~R13+#PER_DIA - Janelas no horário das turmas */
	for(u = 0; u < numTur__; u++){
		for(d = 0; d < numDia__; d++){
			for(c = 0; c < numDis__; c++)
				if(matDisTur__[c][u] == 1){
					for(r = 0; r < numSal__; r++){
						matD__[contRes][indVarX__[(d*numPerDia__)][r][c]] = 1;
						matD__[contRes][indVarX__[(d*numPerDia__)+1][r][c]] = -1;
						mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)][r][c]] = 1;
						mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+1][r][c]] = 1;
					}
				}
			matD__[contRes][indVarZ__[u][d][0]] = -1;
			mat_Rss_Var__[contRes][indVarZ__[u][d][0]] = 1;
			matE__[contRes] = 0;
			matSNS_DE__[contRes] = 'L';
			contRes++;
		}
	}

	for(u = 0; u < numTur__; u++){
		for(d = 0; d < numDia__; d++){
			for(c = 0; c < numDis__; c++)
				if(matDisTur__[c][u] == 1){
					for(r = 0; r < numSal__; r++){
						matD__[contRes][indVarX__[(d*numPerDia__)+numPerDia__-1][r][c]] = 1;
						matD__[contRes][indVarX__[(d*numPerDia__)+numPerDia__-2][r][c]] = -1;
						mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+numPerDia__-1][r][c]] = 1;
						mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+numPerDia__-2][r][c]] = 1;
					}
						
				}
			matD__[contRes][indVarZ__[u][d][numPerDia__-1]] = -1;
			mat_Rss_Var__[contRes][indVarZ__[u][d][numPerDia__-1]] = 1;
			matE__[contRes] = 0;
			matSNS_DE__[contRes] = 'L';
			contRes++;
		}
	}

	for(s = 1; s < numPerDia__-1; s++)
		for(u = 0; u < numTur__; u++)
			for(d = 0; d < numDia__; d++){
				for(c = 0; c < numDis__; c++)
					if(matDisTur__[c][u] == 1){
						for(r = 0; r < numSal__; r++){
							matD__[contRes][indVarX__[(d*numPerDia__)+s][r][c]] = 1;
							matD__[contRes][indVarX__[(d*numPerDia__)+s-1][r][c]] = -1;
							matD__[contRes][indVarX__[(d*numPerDia__)+s+1][r][c]] = -1;
							mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+s][r][c]] = 1;
							mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+s-1][r][c]] = 1;
							mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+s+1][r][c]] = 1;
						}
					}
				matD__[contRes][indVarZ__[u][d][s]] = -1;
				mat_Rss_Var__[contRes][indVarZ__[u][d][s]] = 1;
				matE__[contRes] = 0;
				matSNS_DE__[contRes] = 'L';
				contRes++;
			}

	/* R14 - Salas utilizadas por disciplina */ 
	for(p = 0; p < numPerTot__; p++)
		for(r = 0; r < numSal__; r++)
			for(c = 0; c < numDis__; c++){
				matD__[contRes][indVarX__[p][r][c]] = 1;
				matD__[contRes][indVarY__[r][c]] = -1;
				mat_Rss_Var__[contRes][indVarX__[p][r][c]] = 1;
				mat_Rss_Var__[contRes][indVarY__[r][c]] = 1;
				matE__[contRes] = 0;
				matSNS_DE__[contRes] = 'L';
				contRes++;
			}

	/* R15 - Salas utilizadas por disciplina */
	for(r = 0; r < numSal__; r++)
		for(c = 0; c < numDis__; c++){
			for(p = 0; p < numPerTot__; p++){
				matD__[contRes][indVarX__[p][r][c]] = 1;
				mat_Rss_Var__[contRes][indVarX__[p][r][c]] = 1;
			}
			matD__[contRes][indVarY__[r][c]] = -1;
			mat_Rss_Var__[contRes][indVarY__[r][c]] = 1;
			matE__[contRes] = 0;
			matSNS_DE__[contRes] = 'G';
			contRes++;
		}

	numRss_ = contRes;
}
//------------------------------------------------------------------------------
void ConstroiMatrizLambdas(){

	int i, j, k, S, contMATVAL, contMATBEG, contMATCNT;
	
	//-----------------------------------------------------
	MAT_D_VAL__ = (int*)malloc(MAX_CVA * sizeof(int));
	MAT_D_IND__ = (int*)malloc(MAX_CVA * sizeof(int));	
	MAT_D_CNT__ = (int*)malloc((numRss_+1) * sizeof(int));
	
	contMATVAL = 0;
	for (i = 0; i < numRss_; i++) {
		contMATCNT = 0;
		for (j = 0; j < numVar__; j++) {	
			if(matD__[i][j] != 0){
				MAT_D_VAL__[contMATVAL] = matD__[i][j];
				MAT_D_IND__[contMATVAL] = j;
				contMATVAL++;
				contMATCNT++;
			}			
		}
		MAT_D_CNT__[i] = contMATCNT;
	}

	for (j = 0; j < numCol__; j++) {
		contMATVAL = 0;
		for (i = 0; i < numRss_; i++) {
			S = 0;
			for (k = 0; k < MAT_D_CNT__[i]; k++) {
				S += (MAT_D_VAL__[contMATVAL] * matGC__[MAT_D_IND__[contMATVAL]][j]);
				contMATVAL++;
			}
			matLambdas__[i][j] = S;
		}
	}
	
	/*for (i = 0; i < numRss_; i++) {
		for (j = 0; j < numCol__; j++) {
			S = 0;
			for (k = 0; k < numVar__; k++) {
				S += (matD__[i][k] * matGC__[k][j]);
			}
			matLambdas__[i][j] = S;
		}
	}*/

	/*********PREPARA MATRIZES PARA CPLEX****************/
	matE__[numRss_] = 1;
	matSNS_DE__[numRss_] = 'E';

	MATVAL__ = (double*)malloc(((numRss_ * numCol__)/2) * sizeof(double));
	MATIND__ = (int*)malloc(((numRss_ * numCol__)/2) * sizeof(int));
	MATBEG__ = (int*)malloc(numCol__ * sizeof(int));
	MATCNT__ = (int*)malloc(numCol__ * sizeof(int));
	lbVec__ = (double*)malloc(numCol__ * sizeof(double));
	ubVec__ = (double*)malloc(numCol__ * sizeof(double));

	contMATVAL = 0;
	contMATBEG = 0;
	for (j = 0; j < numCol__; j++) {
		MATBEG__[j] = contMATBEG;
		contMATCNT = 0;
		for (i = 0; i < numRss_; i++) {	
			if(matLambdas__[i][j] != 0){
				MATVAL__[contMATVAL] = matLambdas__[i][j];
				MATIND__[contMATVAL] = i;
				contMATVAL++;
				contMATCNT++;
			}			
		}
		MATVAL__[contMATVAL] = 1;
		MATIND__[contMATVAL] = numRss_;
		contMATVAL++;
		contMATCNT++;
		contMATBEG = contMATVAL;
		MATCNT__[j] = contMATCNT;
		lbVec__[j] = 0;
		ubVec__[j] = CPX_INFBOUND;
	}
	/*********************************************/
	
}
//------------------------------------------------------------------------------
void ConstroiMatrizA(){
        int i, j, k, u, c, p, r, t, s, d, contRes, contMATVAL, contMATBEG, contMATCNT;

  contRes = 0;

  /* R0 - Valor constante */
  matA__[contRes][numVar__-1] = 1;
  matB__[contRes] = numDis__;
  matSNS_AB__[contRes] = 'E';
  contRes++;

  /* R1 - Número de aulas */
 for(c = 0; c < numDis__; c++){
   for(p = 0; p < numPerTot__; p++)
     for(r = 0; r < numSal__; r++)
       matA__[contRes][indVarX__[p][r][c]] = 1;
   matB__[contRes] = vetDisciplinas__[c].numPer_;
   matSNS_AB__[contRes] = 'E';
   contRes++;
 }
  /* R2 - Aulas na mesma sala no mesmo período */
 for(p = 0; p < numPerTot__; p++)
   for(r = 0; r < numSal__; r++){
     for(c = 0; c < numDis__; c++)
       matA__[contRes][indVarX__[p][r][c]] = 1;
     matB__[contRes] = 1;
     matSNS_AB__[contRes] = 'L';
     contRes++;
   }
  /* R3 - Aulas de uma disciplina no mesmo período */
 for(p = 0; p < numPerTot__; p++)
   for(c = 0; c < numDis__; c++){
     for(r = 0; r < numSal__; r++)
      matA__[contRes][indVarX__[p][r][c]] = 1;
     matB__[contRes] = 1;
     matSNS_AB__[contRes] = 'L';
     contRes++;
   }
  /* R4 - Aulas de um professor no mesmo período */ 
 for(p = 0; p < numPerTot__; p++)
   for(t = 0; t < numPro__; t++){
     for(r = 0; r < numSal__; r++)
       for(c = 0; c < numDis__; c++)
         if(vetDisciplinas__[c].professor_ == t)
           matA__[contRes][indVarX__[p][r][c]] = 1;
     matB__[contRes] = 1;
     matSNS_AB__[contRes] = 'L';
     contRes++;
   }
  /* R5 - Aulas de uma turma no mesmo período */
 for(p = 0; p < numPerTot__; p++)
   for(u = 0; u < numTur__; u++){
     for(r = 0; r < numSal__; r++)
       for(c = 0; c < numDis__; c++){
         for(k = 0; k < vetTurmas__[u].numDis_; k++)
           if(vetTurmas__[u].vetDis_[k] == c){
             matA__[contRes][indVarX__[p][r][c]] = 1;
             break;
           }
       }
     matB__[contRes] = 1;
     matSNS_AB__[contRes] = 'L';
     contRes++;
    }
  /* R6 - Indisponibilidades */
 for(s = 0; s < numRes__; s++){
   for(r = 0; r < numSal__; r++)
     matA__[contRes][indVarX__[vetRestricoes__[s].periodo_][r][vetRestricoes__[s].disciplina_]] = 1;
   matB__[contRes] = 0;
   matSNS_AB__[contRes] = 'E';
   contRes++;
 }
  
  	/* R7 - Número de salas usadas por disciplina */
   	for(c = 0; c < numDis__; c++)
     		for(d = 0; d < numDia__; d++)
       			for(p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++){
        			for(r = 0; r < numSal__; r++)
						matA__[contRes][indVarX__[p][r][c]] = 1;
					matA__[contRes][indVarV__[d][c]] = -1;
					matB__[contRes] = 0;
					matSNS_AB__[contRes] = 'L';
					contRes++;
        		}

	/* R8 - Número de salas usadas por disciplina */
	for(c = 0; c < numDis__; c++)
		for(d = 0; d < numDia__; d++){
			for(r = 0; r < numSal__; r++)
				for(p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
					matA__[contRes][indVarX__[p][r][c]] = 1;
			matA__[contRes][indVarV__[d][c]] = -1;
			matB__[contRes] = 0;
			matSNS_AB__[contRes] = 'G';
			contRes++;
		}

	/* R9 - Dias mínimos */
	for(c = 0; c < numDis__; c++){
		for(d = 0; d < numDia__; d++)
			matA__[contRes][indVarV__[d][c]] = 1;
		matA__[contRes][indVarQ__[c]] = 1;
		matB__[contRes] = vetDisciplinas__[c].diaMin_;
		matSNS_AB__[contRes] = 'G';
		contRes++;
	}

  numRsh_ = contRes;
  matA__[numRsh_][numVar__] = 1;
  matSNS_AB__[contRes] = 'E';
  matB__[numRsh_] = 0;

	/*********PREPARA MATRIZES PARA CPLEX****************/
	MATVAL__ = (double*)malloc(((numRsh_ * numVar__)/2) * sizeof(double));
	MATIND__ = (int*)malloc(((numRsh_ * numVar__)/2) * sizeof(int));
	MATBEG__ = (int*)malloc((numVar__+1) * sizeof(int));
	MATCNT__ = (int*)malloc((numVar__+1) * sizeof(int));
	lbVec__ = (double*)malloc((numVar__+1) * sizeof(double));
	ubVec__ = (double*)malloc((numVar__+1)* sizeof(double));

	contMATVAL = 0;
	contMATBEG = 0;
	for (j = 0; j <= numVar__; j++) {
		MATBEG__[j] = contMATBEG;
		contMATCNT = 0;
		for (i = 0; i <= numRsh_; i++) {	
			if(matA__[i][j] != 0){
				MATVAL__[contMATVAL] = matA__[i][j];
				MATIND__[contMATVAL] = i;
				contMATVAL++;
				contMATCNT++;
			}			
		}
		contMATBEG = contMATVAL;
		MATCNT__[j] = contMATCNT;
		lbVec__[j] = 0;
		ubVec__[j] = 1;
	}
	ubVec__[numVar__-1] = CPX_INFBOUND;
	lbVec__[numVar__] = -CPX_INFBOUND;
	ubVec__[numVar__] = CPX_INFBOUND;
	/**********************************************/  
}
//------------------------------------------------------------------------------
void ConstroiCoefOrig(){

  int p,r,c,u,d,s;

   for(r = 0; r < numSal__; r++){
     for(p = 0; p < numPerTot__; p++){
       for(c = 0; c < numDis__; c++)
        if(vetDisciplinas__[c].numAlu_ > vetSalas__[r].capacidade_)
          matCoefOrig__[indVarX__[p][r][c]] = PESOS[0]*(vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_);
        else
          matCoefOrig__[indVarX__[p][r][c]] = 0;
      }
    }

   for(u = 0; u < numTur__; u++){
     for(d = 0; d < numDia__; d++){
       for(s = 0; s < numPerDia__; s++)

          matCoefOrig__[indVarZ__[u][d][s]] = PESOS[1];
      }
    }

   for(c = 0; c < numDis__; c++)
     matCoefOrig__[indVarQ__[c]] = PESOS[2];

   for(c = 0; c < numDis__; c++){
     for(r = 0; r < numSal__; r++)
       matCoefOrig__[indVarY__[r][c]] = PESOS[3];
    }

    for (d = 0; d < numDia__; d++) 
        for (c = 0; c < numDis__; c++) 
          matCoefOrig__[indVarV__[d][c]] = 0;

   matCoefOrig__[numVar__-1] = -1;
   matCoefOrig__[numVar__] = 0;

}
//------------------------------------------------------------------------------
void CalculaNovosCoef(){

   int i, j, contMATVAL;
   double S;

    contMATVAL = 0;
    for(j = 0; j < numVar__; j++){
		matNovosCoef__[j] = matCoefOrig__[j];
	}
	for (i = 0; i < numRss_; i++) {
		for (j = 0; j < MAT_D_CNT__[i]; j++) {
			matNovosCoef__[MAT_D_IND__[contMATVAL]] -= VecDual__[i] * mat_Rss_Var__[i][MAT_D_IND__[contMATVAL]] * MAT_D_VAL__[contMATVAL];	
			contMATVAL++;
		}
	}
	matNovosCoef__[numVar__] = -1;
}
//------------------------------------------------------------------------------
void obtemNovaColuna(){
	int i, j, contMATVAL;
	double S;
	
    contMATVAL = 0;
	for (i = 0; i < numRss_; i++) {
		S = 0;
		for (j = 0; j < MAT_D_CNT__[i]; j++) {
			S += (MAT_D_VAL__[contMATVAL] * VecX_SP[MAT_D_IND__[contMATVAL]]);	
			contMATVAL++;
		}
		VecNewCol[i] = S;
	}
	VecNewCol[numRss_] = 1;
	
	/*for(i = 0; i < numRss_; i++){
	    S = 0;
	    for(j = 0; j < numVar__; j++){
		   S += matD__[i][j]*VecX_SP[j];	
		}	
		VecNewCol[i] = S;
	}
	VecNewCol[numRss_] = 1;*/
	
	S = 0;
	for(j = 0; j < numVar__; j++){
		S += matCoefOrig__[j]*VecX_SP[j];
	}
	coladd_FO[0] = S;
	
}
//------------------------------------------------------------------------------
void desalocaMarizesCPX(){

	free(MATVAL__);
	free(MATIND__);
	free(MATBEG__);
	free(MATCNT__);

	free(lbVec__);
	free(ubVec__);
}
//------------------------------------------------------------------------------
void alocaAddCol(){
	
   coladd_FO = (double*)malloc(sizeof(double));
   coladd_IND  = (int*)malloc((numRss_+1) * sizeof(int));
   VecNewCol = (double*)malloc((numRss_+1) * sizeof(double));
   coladd_BEG  = (int*)malloc(sizeof(int));
   coladd_lb = (double*)malloc(sizeof(double));
   coladd_ub = (double*)malloc(sizeof(double));
   
   for(int i = 0; i <= numRss_; i++)
      coladd_IND[i] = i;
   
   coladd_BEG[0] = 0;
   coladd_lb[0] = 0;
   coladd_ub[0] = CPX_INFBOUND;
}
//------------------------------------------------------------------------------
int EhViavel(){
   int i, j, contMATVAL = 0;
   double S;
   
    newFO = 0;
	for (i = 0; i < numVar__; i++) {
		newFO = newFO + (matCoefOrig__[i]*VecX_SP[i]);
	}
    
	for (i = 0; i < numRss_; i++) {
		S = 0;
		for (j = 0; j < MAT_D_CNT__[i]; j++) {
			S = S + (MAT_D_VAL__[contMATVAL] * VecX_SP[MAT_D_IND__[contMATVAL]]);
			contMATVAL++;	
		}
		
		if ((matSNS_DE__[i] == 'L') && (S > matE__[i])){
			return 0;
		}
		else if ((matSNS_DE__[i] == 'G') && (S < matE__[i])){
			return 0;
		}
	}
	
   return 1;	
}
//------------------------------------------------------------------------------
void execCpx_RelLinear(){
 Solucao s;
 int sts;
 char aux[50];
 time_t hi,hf;


 strcpy(aux,nomInst__);
 strcat(aux,".lp");
 //montarModeloPLI(aux,0);
 
 CPXENVptr env; 
 CPXLPptr lp;   
 env = CPXopenCPLEX(&sts);
 sts = CPXsetintparam(env,CPX_PARAM_SCRIND,CPX_ON);
 lp = CPXcreateprob(env,&sts,"");
 sts = CPXreadcopyprob(env,lp,aux,NULL);
 s.numVar_ = CPXgetnumcols(env,lp);
 s.numRes_ = CPXgetnumrows(env,lp);
 
 hi = time(NULL);
 sts = CPXmipopt(env,lp);
 hf = time(NULL);
 s.tempo_ = difftime(hf,hi);
 sts = CPXgetmipobjval(env,lp,&s.valSol_);
 sts = CPXgetbestobjval(env,lp,&s.bstNod_);
 sts = CPXgetmipx(env,lp,s.vetSol_,0,(numPerTot__*numSal__*numDis__-1));
 sts = CPXfreeprob(env,&lp);
 sts = CPXcloseCPLEX(&env);
 
 printf(">>> RESULTADOS DO CPLEX\n\n");
 printf("Num. var....: %d\n",s.numVar_);
 printf("Num. res....: %d\n",s.numRes_);
 printf("Val. sol....: %f\n",s.valSol_);
 printf("Melhor no...: %f\n",s.bstNod_);
 if(s.valSol_ != 0)
   printf("GAP.........: %.2f\n",((s.valSol_ - s.bstNod_)/s.valSol_)*100);
 else
   printf("GAP.........: -\n");
 printf("Tempo.......: %.2f\n",s.tempo_);
}
