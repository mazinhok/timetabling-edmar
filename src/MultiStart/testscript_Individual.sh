#!/bin/bash

reset
make

numSP=( 2 3 4 5 6 7 8 9 10 )
fatorDes=( 1 50 100 500 1000 )
instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )
#comp19
./multiStart comp19 facDes=1 numSubProb=6 >> comp19-1-6.tx
./multiStart comp19 facDes=1 numSubProb=10 >> comp19-1-10.tx
./multiStart comp19 facDes=500 numSubProb=10 >> comp19-500-10.txt
./multiStart comp19 facDes=1000 numSubProb=4 >> comp19-1000-4.txt
./multiStart comp19 facDes=1000 numSubProb=6 >> comp19-1000-6.txt
./multiStart comp19 facDes=1000 numSubProb=7 >> comp19-1000-7.txt
./multiStart comp19 facDes=1000 numSubProb=10 >> comp19-1000-10.txt
#comp20
./multiStart comp20 facDes=1 numSubProb=10 >> comp20-1-10.txt
./multiStart comp20 facDes=500 numSubProb=9 >> comp20-500-9.txt
./multiStart comp20 facDes=500 numSubProb=10 >> comp20-500-10.txt
./multiStart comp20 facDes=1000 numSubProb=4 >> comp20-1000-4.txt
./multiStart comp20 facDes=1000 numSubProb=8 >> comp20-1000-8.txt
#comp21
./multiStart comp21 facDes=1 numSubProb=10 >> comp21-1-10.txt
./multiStart comp21 facDes=500 numSubProb=6 >> comp21-500-6.txt
./multiStart comp21 facDes=500 numSubProb=9 >> comp21-500-9.txt
./multiStart comp21 facDes=500 numSubProb=10 >> comp21-500-10.txt
./multiStart comp21 facDes=1000 numSubProb=6 >> comp21-1000-6.txt
./multiStart comp21 facDes=1000 numSubProb=7 >> comp21-1000-7.txt
./multiStart comp21 facDes=1000 numSubProb=9 >> comp21-1000-9.txt
./multiStart comp21 facDes=1000 numSubProb=10 >> comp21-1000-10.txt
