#!/bin/bash

reset
make

numSP=( 2 3 4 5 6 7 8 9 10 15 20 25 30 )
#fatorDes=( 1 50 100 500 1000 )
fatorDes=( 1 )
#instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )
instances=( comp04 comp08 )
#comPesos=( 0 1 )
comPesos=( 1 )


	for instance in ${instances[@]} ; do
	   echo "Inicio $instance"
           for cp in ${comPesos[@]} ; do
	   	for fd in ${fatorDes[@]} ; do
	      		for nsp in ${numSP[@]} ; do
		  		./multiStart $instance facDes=$fd numSubProb=$nsp comPeso=$cp >> $instance-$fd-$nsp-$cp.txt
	      		done
	   	done
	   done
	   cat $instance-* > $instance.csv
	   echo "... fim $instance"
	done

