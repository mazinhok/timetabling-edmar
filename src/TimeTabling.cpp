//============================================================================
// Name        : TimeTabling.cpp
// Author      : Erika
// Version     :
// Copyright   :
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <string.h>
#include "Model/Problema.h"
#include "Model/Individuo.h"
#include "GraspReativo.h"
#include "Grasp.h"

using namespace std;

void configuraParametros(Grasp *grasp, int piNParam, char **piParams) {
	int i;

	for (i = 3; i < piNParam; i++) {
		if (strncmp("seed", piParams[i], 4) == 0) {
			grasp->seed = atoi(piParams[i] + 5);
		}
		else if (strncmp("maxIter", piParams[i], 7) == 0) {
			grasp->nMaxIter = atoi(piParams[i] + 8);
		}
		else if (strncmp("bl", piParams[i], 2) == 0) {
			if (strncmp(piParams[i] + 3, "sa", 2) == 0) {
				grasp->opBuscaLocalGrasp = 6; // SA
			}
			else if (strncmp(piParams[i] + 3, "hc", 2) == 0) {
				grasp->opBuscaLocalGrasp = 2; // HC
			}
			else if (strncmp(piParams[i] + 3, "sd", 2) == 0) {
				grasp->opBuscaLocalGrasp = 1; // SD
			}
			else grasp->opBuscaLocalGrasp = atoi(piParams[i] + 3);
		}
		else if (strncmp("n", piParams[i], 1) == 0) {
			grasp->nMaxIterSemMelhora = atoi(piParams[i] + 2);
		}
		else if (strncmp("maxhc", piParams[i], 5) == 0) {
			grasp->nMaxIterBuscaLocal = atoi(piParams[i] + 6);
		}
		else if (strncmp("k", piParams[i], 1) == 0) {
			grasp->k = atoi(piParams[i] + 2);
		}
		else if (strncmp("alfa", piParams[i], 4) == 0) {
			grasp->threshold = atof(piParams[i] + 5);
		}
		else if (strncmp("timeout", piParams[i], 7) == 0) {
			grasp->tempoLimite = atoi(piParams[i] + 8);
		}
		else if (strncmp("info", piParams[i], 4) == 0) {
			grasp->info = atoi(piParams[i] + 5);
		}
		else if (strncmp("geraInicial", piParams[i], 11) == 0) {
			strcpy(grasp->nomeArquivoInstanciaInicial, piParams[i] + 12);
		}
		else if (strncmp("viz", piParams[i], 3) == 0) {
			grasp->vizinhancasUtilizadas = atoi(piParams[i] + 4);
		}
		else if (strncmp("t0", piParams[i], 2) == 0) {
			grasp->temperaturaInicial = atof(piParams[i] + 3);
		}
		else if (strncmp("tFinal", piParams[i], 6) == 0) {
			grasp->temperaturaFinal = atof(piParams[i] + 7);
		}
		else if (strncmp("beta", piParams[i], 4) == 0) {
			grasp->taxaDecaimentoTemperatura = atof(piParams[i] + 5);
		}
		else if (strncmp("const", piParams[i], 5) == 0) {
			grasp->tipoConstrucaoInicial = atoi(piParams[i] + 6);
		}
		else if (strncmp("grafico", piParams[i], 7) == 0) {
			grasp->imprimeGrafico = atoi(piParams[i] + 8);
		}
		else if (strncmp("impfo", piParams[i], 5) == 0) {
			grasp->imprimeFO = atoi(piParams[i] + 6);
		}
		else if (strncmp("gc", piParams[i], 2) == 0) {
			grasp->geraMatrizGC = atoi(piParams[i] + 3);
		}

	}
	srand(grasp->seed);

}

int main(int argc, char** argv) {
	Problema* p;
	Individuo* best;
	Grasp* grasp;

	if( argc < 2 ){
		fprintf(stderr, "Modo de executar:\n\n");
		fprintf(stderr, "\t./grasp arquivo_problema_entrada arquivo_saida [parametros]\n\n\tonde os parametros podem ser:\n");
		fprintf(stderr, "\t\tseed=VALOR_SEED (recebe time(NULL) caso nao informado)\n");
		fprintf(stderr, "\t\tmaxIter=NUMERO_MAXIMO_ITERACOES_GRASP\n");
		fprintf(stderr, "\t\tbl=ALGORITMO_BUSCA_LOCAL (atualmente somente 2 para HillClimbing e sa para simulated annealing)\n");
		fprintf(stderr, "\t\tmaxhc=NUMERO_MAXIMO_ITERACOES_HILL_CLIMBING\n");
		fprintf(stderr, "\t\tn=NUMERO_MAXIMO_ITERACOES_SEM_MELHORA\n");
		fprintf(stderr, "\t\tk=NUMERO_VIZINHANCAS_POR_ITERACAO_HILL_CLIMBING\n");
		fprintf(stderr, "\t\talfa=ALFA_CONSTRUCAO_SOLUCAO (alfa entre 0 e 1)\n");
		fprintf(stderr, "\t\ttimeout=TEMPO_LIMITE_EXECUCAO (em segundos)\n");
		fprintf(stderr, "\t\tinfo=1 para imprimir debug\n");
		fprintf(stderr, "\t\timpfo=1 para imprimir somente o valor da solucao final\n");
		fprintf(stderr, "\t\tgrafico=1 para imprimir informacoes para geracao de grafico\n");
		fprintf(stderr, "\t\tgeraInicial=NOME_ARQUIVO_SOLUCAO_INICIAL (caso a solucao inicial seja lida de um arquivo)\n");
		fprintf(stderr, "\t\tviz=VIZINHANCA_A_SER_USADA\n");
		fprintf(stderr, "\t\tgc=1 para gerar arquivo com as soluçõespara o GC\n");
		fprintf(stderr, "\t\t\t(0 para 50%% move e 50%% swap; -[x] para x%% de kempe sobre a vizinhanca 0;\n\t\t\t 1 para move; 2 para swap; 3 para kempe; \n\t\t\t 4 para TimeMove; 5 para RoomMove; 6 para LectureMove)\n");
		fprintf(stderr, "\n\tobs: O numero maximo total de iteracoes da busca local foi fixado em 150.000\n\n");

		exit(0);
	}


	p = new Problema(argv[1]);
	grasp = new Grasp();
	configuraParametros(grasp, argc, argv);

	best = grasp->executa(p);

	if( grasp->tipoConstrucaoInicial!=2 ){
		if( grasp->info ) 
			printf("\nentrada: %s\n", argv[1]);
		if( grasp->info ) {
			grasp->ImprimeExecucao();
			fprintf(stderr, "%d\n", best->fitness);
		}
		if( grasp->info ) 
			best->ImprimeIndividuo();
	}
	if( argc >= 3 ) 
		best->ImprimeInstancia(argv[2]);
	if( grasp->imprimeFO ) 
		printf("%d\n", best->fitness);

	delete(grasp);
	delete(best);
	delete(p);

	return 0;
}
