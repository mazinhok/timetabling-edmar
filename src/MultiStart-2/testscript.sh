#!/bin/bash

reset
make
numAlg=( 2 1 )
numSP=( 10 9 8 7 6 5 4 3 2 )
instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )
#instances=( Udine1 Udine2 Udine3 Udine4 Udine5 Udine6 Udine7 Udine8 Udine9 UUMCAS_A131 )
#instances=( DDS1 DDS2 DDS3 DDS4 DDS5 DDS6 DDS7 )
#instances=( EA01 EA02 EA03 EA04 EA05 EA06 EA07 EA08 EA09 EA10 EA11 EA12 )
#instances=( erlangen2011_2 erlangen2012_1 erlangen2012_2 erlangen2013_1 erlangen2013_2 erlangen2014_1 )
#instances=( test1 test2 test3 test4 )

for nalg in ${numAlg[@]} ; do
    for instance in ${instances[@]} ; do
	   echo "Inicio $instance"
           for nsp in ${numSP[@]} ; do
	            ./multiStart $instance numSubProb=$nsp tGrafo=$nalg tempLim=9880 >> $instance-$nalg.csv
           done
	   echo "... fim $instance"
	done
done
rm sp*.lp

#./multiStart comp01 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp02 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp03 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp04 numSubProb=8 tGrafo=1  >> resumo.csv
#./multiStart comp05 numSubProb=4 tGrafo=1  >> resumo.csv
#./multiStart comp06 numSubProb=3 tGrafo=1  >> resumo.csv
#./multiStart comp07 numSubProb=7 tGrafo=1  >> resumo.csv
#./multiStart comp08 numSubProb=4 tGrafo=1  >> resumo.csv
#./multiStart comp09 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp10 numSubProb=5 tGrafo=1  >> resumo.csv
#./multiStart comp11 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp12 numSubProb=5 tGrafo=1  >> resumo.csv
#./multiStart comp13 numSubProb=5 tGrafo=1  >> resumo.csv
#./multiStart comp14 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp15 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp16 numSubProb=4 tGrafo=1  >> resumo.csv
#./multiStart comp17 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp18 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp19 numSubProb=3 tGrafo=1  >> resumo.csv
#./multiStart comp20 numSubProb=2 tGrafo=1  >> resumo.csv
#./multiStart comp21 numSubProb=2 tGrafo=1  >> resumo.csv
#rm sp*.lp
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
