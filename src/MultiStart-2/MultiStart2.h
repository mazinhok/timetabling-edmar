#ifndef MultiStart2
#define MultiStart2

#include <stdio.h>
#include "/opt/ibm/ILOG/CPLEX_Studio128/cplex/include/ilcplex/cplex.h"

// Valores limites baseados nas inst�ncias comp
#define MAX_DIS 850   // 131
#define MAX_TUR 3691  // 150
#define MAX_PRO 135  // 135
#define MAX_SAL 176   // 20
#define MAX_DIA 6    // 6
#define MAX_PER 18    // 9
#define MAX_RES 11948 // 1368
#define MAX_SPS 150  // subproblemas

//================================ ESTRUTURAS ================================\\

//------------------------------------------------------------------------------
typedef struct tDisciplina
{
 char nome_[50]; // nome da disciplina
 int professor_; // id do professor
 int numPer_;    // n�mero de per�odos de oferta
 int diaMin_;    // dias m�nimos para distribui��o
 int numAlu_;    // n�mero de alunos
}Disciplina;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tTurma
{
 char nome_[50];       // nome da turma
 int numDis_;          // n�mero de disciplinas
 int vetDis_[MAX_DIS]; // vetor com as disciplinas (ids)
}Turma;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tProfessor
{
 char nome_[50]; // nome da turma
}Professor;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tSala
{
 char nome_[50];  // nome da sala
 int capacidade_; // capacidade
}Sala;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tSolucao
{
 // FO (restri��es SOFT)
 int capSal_; // n�mero de alunos que violam a capacidade da sala
 int janHor_; // n�mero de janelas no hor�rio das turmas
 int diaMin_; // n�mero "dias m�nimos" n�o respeitados
 int salDif_; // n�mero de salas diferentes usadas para as disciplinas
 int funObj_; // valor da fun��o objetivo

 // restri��es HARD
 int vioNumAul_; // viola��es na restri��o 1 - n�mero de aulas
 int vioAulSim_; // viola��es na restri��o 2 - aulas simultaneas
 int vioDisSim_; // viola��es na restri��o 3 - disciplinas simultaneas
 int vioProSim_; // viola��es na restri��o 4 - professores simultaneos
 int vioTurSim_; // viola��es na restri��o 5 - turmas simultaneas

 // vetor de solu��o (vari�veis x)
 double vetSol_[MAX_PER*MAX_DIA*MAX_SAL*MAX_DIS];

 // matriz de solu��o (per�odo x dia x sala)
 int matSolSal_[MAX_PER][MAX_DIA][MAX_SAL];

 // matriz de solu��o (per�odo x dia x turma)
 int matSolTur_[MAX_PER][MAX_DIA][MAX_TUR];
}Solucao;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tCpx
{
 CPXLPptr lp_;   // problema no CPLEX
 int numVar_;    // n�mero de vari�veis do problema
 int numRes_;    // n�mero de restri��es do problema
 double limSup_; // valor do limitante superior (solu��o do problema)
 double limInf_; // valor do limitante inferior (melhor n�)
 double gap_;    // valor do gap
 double tempo_;  // tempo de execu��o
}Cpx;
//------------------------------------------------------------------------------

typedef struct tMetRel
{
 int numSps_;    // n�mero de subproblemas
 int fatDes_;    // fator de desbalanceamento - usado pela METIS na parti��o do "grafo"
 int numCtsM_;   // n�mero de arestas cortadas (valor retornado pela METIS)
 int numCts_;    // n�mero de arestas cortadas
 int pesCts_;    // peso das arestas cortadas
 double limSup_; // valor do limitante superior (solu��o real - vi�vel)
 double limInf_; // valor do limitante inferior (solu��o relaxada)
 double gap_;    // valor do gap
 double temPrt_; // tempo de particionamento do problema
 double temSub_; // tempo para montar o(s) subproblema(s)
 double temExe_; // tempo de execu��o do algoritmo de subgradientes ou gera��o de colunas
 double temTot_; // tempo total de execu��o
 int vetSpDis_[MAX_DIS]; // vetor com o id do subproblema em que cada disciplina ficou
 int vetSpTur_[MAX_TUR]; // vetor com o id do subproblema em que cada turma ficou
}MetRel;
//---------------------------------------------------------------------------

//==============================================================================


//============================= VARI�VEIS GLOBAIS ==============================
// ------------ Dados de entrada
char nomInst__[150]; // nome da inst�ncia
int numDis__;        // n�mero de disciplinas 
int numTur__;        // n�mero de turmas
int numPro__;        // n�mero de professores
int numSal__;        // n�mero de salas
int numDia__;        // n�mero de dias
int numPerDia__;     // n�mero de per�odos por dia
int numPerTot__;     // n�mero de per�odos total
int numRes__;        // n�mero de restri��es
bool matResDisPer__[MAX_DIS][MAX_DIA*MAX_PER]; // matriz que indica as restri��es de oferta (disciplina proibidas nos per�odos)
Disciplina vetDisciplinas__[MAX_DIS];
Turma vetTurmas__[MAX_TUR];
Professor vetProfessores__[MAX_PRO];
Sala vetSalas__[MAX_SAL];
// ------------ CPLEX
CPXENVptr env__;              // ambiente do CPLEX
static Cpx vetCpx__[MAX_SPS]; // vetor de problemas
static MetRel mr__;       // m�todos de relaxa��o
// ------------ METIS
int comPeso = 1;
int numSubProb = 2;
int facDes = 1;
int tGrafo = 1;               // Escolhe o m�todo de constru��o do grafo e parti��o: 1 - Curr�culos s�o V�rtices/ "?" - Disciplinas s�o V�rtices
int tempLim = 0;
// ------------ Auxiliares
bool matDisTur__[MAX_DIS][MAX_TUR]; // matriz que indica se uma turma possui uma disciplina
int numSalDif__;                    // n�mero de "tamanhos" diferentes de sala  
int vetTamSal__[MAX_SAL];           // vetor com os tamanhos diferentes das salas
//==============================================================================


//===================================== M�TODOS ================================
void configuraParametros(int piNParam, char **piParams);
// ------------ M�todos de relaxa��o
void excPrtTur(const int &sps,const int &fat,const int &pes);
void excPrtDis(const int &sps,const int &fat,const int &pes);
void monSpsTur();
void monSpsDis();
void parGraTur(const int &sps,const int &fat,const int &pes);
void parGraDis(const int &sps,const int &fat,const int &pes);
// ------------ CPLEX
void exeCpx(Solucao &s,char *arq,const int &rl);
void escResCpx(FILE *f,const int &rl);
void monModBurEal10(char *arq,const int &rl);
void monModLacLub12(char *arq,const int &rl);
void monModLacLub12latex(char *arq,const int &rl);
// ------------ Solu��o
void escSolBurEal10(Solucao &s,FILE *f);
void escSolLacLub12(Solucao &s,FILE *f);
// ------------ Auxiliares
void lerInstancia(char *arq);
void testarEntrada();
void ordVetCapSal();
//==============================================================================

#endif
