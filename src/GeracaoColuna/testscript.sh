#!/bin/bash

reset
make

#testes=( 100COL-BLOFF 100COL-10_90-SA_BLOFF 100COL-10_90-SA_SD 500COL-BLOFF 500COL-10_90-SA_BLOFF 500COL-10_90-SA_SD )
testes=( 500COL-10_90-SA_SD )
#instances=( comp01 comp02 comp05 comp07 comp11 comp12 comp14 comp17 comp20 )
instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )

#for teste in ${testes[@]} ; do
   #for ((j=1;j<=21;j++));do
   for instance in ${instances[@]} ; do
      #instancia=$(printf "../../instances/comp%02d.ctt" $j)
      #arqsaida=$(printf "comp%02d.txt" $j)
      #arqBestInd=$(printf "resultados/comp_%02d.res" $j)
      echo "Inicio $instance"
      ./geraCol ../../instances/$instance.ctt >> $instance.txt
      echo "... fim $instance"
   done
#done

