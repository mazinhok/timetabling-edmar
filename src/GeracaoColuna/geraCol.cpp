#include "geraCol.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "/opt/ibm/ILOG/CPLEX_Studio1271/cplex/include/ilcplex/cplex.h"

#define MAX(x,y) (((x) > (y)) ? (x) : (y))

#define RES_SOFT

int PESOS[4] = {1,2,5,1};
double f_pmr, f_sp;
double erro = 0.000001;
CPXENVptr env; 
CPXLPptr pmr, sp;
int indiceMelhorColuna, bestFO, newFO;

//------------------------------------------------------------------------------
int main(int argc, char *argv[]){
 Solucao s_pmr, s_sp;
 int sts, iter = 1, flag = 1, i, j;
 time_t hi,hf;

 lerInstancia(argv[1]);

 //execCpx_RelLinear();
 //execCpx_Original();
 
 //lerSolucoesIniciais(argv[2]);
 
 indices = (int*)malloc((numVar__+1) * sizeof(int));
    for (i = 0; i <= numVar__; i++) {
      indices[i] = i;
   }
 
 /*aloca();
 MontaMatrizesIndices();
 ConstroiCoefOrig();
 ConstroiMatrizD();
 ConstroiMatrizLambdas();
 
 pmr = CPXcreateprob(env,&sts,"");
 sts = CPXcopylp(env,pmr,numCol__,numRss_+1,1,matFO__,matE__,matSNS_DE__,MATBEG__,MATCNT__,MATIND__,MATVAL__,lbVec__,ubVec__,NULL);
 desalocaMarizesCPX();

 ConstroiMatrizA();
 
 sp = CPXcreateprob(env,&sts,"");
 sts = CPXcopylp(env,sp,numVar__+1,numRsh_+1,1,matCoefOrig__,matB__,matSNS_AB__,MATBEG__,MATCNT__,MATIND__,MATVAL__,lbVec__,ubVec__,NULL);
 sts = CPXcopyctype(env,sp,ctype__);
 sts = CPXchgprobtype(env,sp,1);
 desalocaMarizesCPX();
 
 alocaAddCol();
 
 hi = hf = time(NULL);
 while((difftime(hf,hi) <= 3600) && (flag)){
 	execCpx_PMR(s_pmr);
 	sts = CPXgetobjval(env,pmr,&f_pmr);
	 	
 	CalculaNovosCoef();

    sts = CPXchgobj(env,sp,numVar__+1,indices,matNovosCoef__);
    sts = CPXchgcoef(env,sp,numRsh_,-1,VecDual__[numRss_]);
    
    execCpx_SP(s_sp);
    sts = CPXgetmipobjval(env,sp,&f_sp);
        
    printf("********** ITERAÇÃO %d **********\n",iter);
    printf("PMR = %lf\n",f_pmr);
    printf("SBP = %lf\n",f_sp);
    printf("\n");
    fflush(stdout);
    
    sts = CPXgetmipx(env,sp,VecX_SP,0,numVar__);
    
    if (EhViavel()){
		printf("***************************************************NOVA SOLUÇÃO VIAVEL --> %d************************************************\n", newFO);
		if (newFO < bestFO) {
			bestFO = newFO;
			for (i = 0; i < numVar__; i++) {
				bestSol[i] = VecX_SP[i];
			}
			printf("***************************************************MELHOR SOLUÇÃO VIAVEL************************************************\n");
		}
	}
    
    if (fabs(f_sp) > erro){
		obtemNovaColuna();
		sts = CPXaddcols(env, pmr, 1, numRss_+1, coladd_FO, coladd_BEG, coladd_IND, VecNewCol, coladd_lb, coladd_ub, NULL);
	}
	else
		flag = 0;	
	iter++;
	
	hf = time(NULL);
 }
 
 printf("***************************************************MELHOR SOLUÇÃO VIÁVEL --> %d************************************************\n", bestFO);
 printf("TEMPO GC: %.2f\n\n",difftime(hf,hi));*/
 
 //execCpx_Original();
 //execCpx_RelLinear();
 
 //desaloca();
 
 	/*for (i = 0; i < MAX_VAR; i++) {
		free(matGC__[i]);
	}
	free(matGC__);
	free(matFO__);
	free(bestSol);
	free(indices);*/

 return 0;
}
//------------------------------------------------------------------------------
void execCpx_PMR(Solucao &s){
 int sts;
 double f;
 clock_t h; 

 //sts = CPXwriteprob(env,pmr,"Verifica_pmr.lp",NULL);
 //sts = CPXreadcopyprob(env,pmr,"Verifica_pmr.lp",NULL);

 h = clock();
 sts = CPXprimopt(env,pmr);
 h = clock() - h;

 s.tempo_ = (double)h/CLOCKS_PER_SEC;
 sts = CPXgetpi(env,pmr,VecDual__,0,CPXgetnumrows(env,pmr)-1);
 
 //sts = CPXgetx (env, pmr,x,0,CPXgetnumcols(env,pmr)-1);
 //sts = CPXgetmipobjval(env,pmr,&s.valSol_);
 //sts = CPXgetbestobjval(env,pmr,&s.bstNod_);
 //sts = CPXgetmipx(env,pmr,s.vetSol_,0,(numPerTot__*numSal__*numDis__-1));

}
//------------------------------------------------------------------------------
void execCpx_SP(Solucao &s){
 int sts;
 clock_t h; 

 //sts = CPXwriteprob(env,sp,"Verifica_sp.lp",NULL);
 //sts = CPXreadcopyprob(env,sp,"Verifica_sp.lp",NULL);

 h = clock();
 sts = CPXmipopt(env,sp);
 h = clock() - h;

 s.tempo_ = (double)h/CLOCKS_PER_SEC;
}
//------------------------------------------------------------------------------
void execCpx_Original(){
 Solucao s;
 int sts;
 char modelo[50];
 time_t hi,hf;
 int beg[1]; 
 
 beg[0] = 0;
 
 strcpy(modelo,nomInst__);
 strcat(modelo,".lp");
 montarModeloPLI(modelo,1);
 
 CPXENVptr env; 
 CPXLPptr lp;   
 env = CPXopenCPLEX(&sts);
 sts = CPXsetintparam(env,CPX_PARAM_SCRIND,CPX_OFF);
 //sts = CPXsetdblparam(env,CPX_PARAM_TILIM,14400);
 lp = CPXcreateprob(env,&sts,"");
 sts = CPXreadcopyprob(env,lp,modelo,NULL);
 s.numVar_ = CPXgetnumcols(env,lp);
 s.numRes_ = CPXgetnumrows(env,lp);
 //sts = CPXaddmipstarts(env,lp,1,s.numVar_,beg,indices,bestSol,NULL,NULL);
 
 hi = time(NULL);
 sts = CPXmipopt(env,lp);
 hf = time(NULL);
 s.tempo_ = difftime(hf,hi);
 sts = CPXgetmipobjval(env,lp,&s.valSol_);
 sts = CPXgetbestobjval(env,lp,&s.bstNod_);
 sts = CPXgetmipx(env,lp,s.vetSol_,0,(numPerTot__*numSal__*numDis__-1));
 sts = CPXfreeprob(env,&lp);
 sts = CPXcloseCPLEX(&env);
 
 printf(">>> RESULTADOS DO CPLEX\n\n");
 printf("Num. var....: %d\n",s.numVar_);
 printf("Num. res....: %d\n",s.numRes_);
 printf("Val. sol....: %f\n",s.valSol_);
 printf("Melhor no...: %f\n",s.bstNod_);
 if(s.valSol_ != 0)
   printf("GAP.........: %.2f\n",((s.valSol_ - s.bstNod_)/s.valSol_)*100);
 else
   printf("GAP.........: -\n");
 printf("Tempo.......: %.2f\n",s.tempo_);
 
 escreverSol(s);
}
//------------------------------------------------------------------------------
void escreverSol(Solucao &s){
 char saidaSol[50];
 
 strcpy(saidaSol,nomInst__);
 strcat(saidaSol,".res");
 
 int pos = 0;
 FILE *f = fopen(saidaSol,"w");
  
 for(int d = 0; d < numDia__; d++){
    for (int i = 0; i < numPerDia__; i++) {
        for (int j = 0; j < numSal__; j++) {
            for (int k = 0; k < numDis__; k++) {
                if (s.vetSol_[pos] > 0){
				   fprintf(f,"%s %s %d %d\n", vetDisciplinas__[k].nome_, vetSalas__[j].nome_, d, i);		
				}
				pos++;
            }
        }
    }
 }
 fclose(f);
}
//-----------------------------------------------------------------------------*/
void montarModeloPLI(char *arq, int RelLinear){
 int val;
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 #ifndef RES_SOFT
   fprintf(f,"\n\\Auxiliar\n");
   for(int p = 0; p < numPerTot__; p++){
	 for(int r = 0; r < numSal__; r++){
       for(int c = 0; c < numDis__; c++)
          fprintf(f,"+ 0 x_%d_%d_%d ",p,r,c);
       fprintf(f,"\n");
      }
    }
   fprintf(f,"\nST\n");
 #else
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int p = 0; p < numPerTot__; p++){
	 for(int r = 0; r < numSal__; r++){
       for(int c = 0; c < numDis__; c++)
        if(vetDisciplinas__[c].numAlu_ > vetSalas__[r].capacidade_)
          fprintf(f,"+ %d x_%d_%d_%d ",PESOS[0]*(vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),p,r,c);
        else
          fprintf(f,"+ 0 x_%d_%d_%d ",p,r,c);
       fprintf(f,"\n");
      }
    }
   fprintf(f,"\n\n\\Janelas de hor\E1rios\n");
   for(int u = 0; u < numTur__; u++)
    {
     for(int d = 0; d < numDia__; d++)
      {
       for(int s = 0; s < numPerDia__; s++)
          fprintf(f,"+ %d z_%d_%d_%d ",PESOS[1],u,d,s);
       fprintf(f,"\n");
      }
    }
   fprintf(f,"\n\\Dias m\EDnimos\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"+ %d q_%d ",PESOS[2],c);
   fprintf(f,"\n\n\\Salas diferentes\n");
   for(int r = 0; r < numSal__; r++){
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"+ %d y_%d_%d ",PESOS[3],r,c);
     fprintf(f,"\n");
    }
    fprintf(f,"\n\n\\Variaveis V\n");
   for(int d = 0; d < numDia__; d++){
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"+ 0 v_%d_%d ",d,c);
     fprintf(f,"\n");
    }
   val = PESOS[3] * numDis__;
   fprintf(f,"- val\n");
   fprintf(f,"\nST\n");
   fprintf(f,"\n\\Valor constante\n");
   fprintf(f,"val = %d\n",val);
 #endif
 fprintf(f,"\n\\ ------------------------------------ HARD------------------------------------\n");
 fprintf(f,"\n\\R1 - N\FAmero de aulas\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     for(int r = 0; r < numSal__; r++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
    fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_);
  }
 fprintf(f,"\n\\R2 - Aulas na mesma sala no mesmo per\EDodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int r = 0; r < numSal__; r++)
    {
     for(int c = 0; c < numDis__; c++)
      fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R3 - Aulas de uma disciplina no mesmo per\EDodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int c = 0; c < numDis__; c++)
    {
     for(int r = 0; r < numSal__; r++)
      fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R4 - Aulas de um professor no mesmo per\EDodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int t = 0; t < numPro__; t++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
         if(vetDisciplinas__[c].professor_ == t)
           fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R5 - Aulas de uma turma no mesmo per\EDodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int u = 0; u < numTur__; u++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(vetTurmas__[u].vetDis_[k] == c)
            {
             fprintf(f,"+ x_%d_%d_%d ",p,r,c);
             break;
            }
        }
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R6 - Restri\E7\F5es de oferta (aloca\E7\E3o)\n");
 for(int s = 0; s < numRes__; s++)
  {
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"+ x_%d_%d_%d ",vetRestricoes__[s].periodo_,r,vetRestricoes__[s].disciplina_);
   fprintf(f,"= 0\n");
  }
 #ifdef RES_SOFT
   fprintf(f,"\n\\ ------------------------------------ SOFT------------------------------------\n");
   fprintf(f,"\n\\R7 - N\FAmero de salas usadas por disciplina\n");
   for(int c = 0; c < numDis__; c++)
    {
     for(int d = 0; d < numDia__; d++)
       for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
        {
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ x_%d_%d_%d ",p,r,c);
         fprintf(f,"- v_%d_%d <= 0\n",d,c);
        }
     fprintf(f,"\n");
    }
   fprintf(f,"\n\\R8 - N\FAmero de salas usadas por disciplina\n");
   for(int d = 0; d < numDia__; d++){
     for(int c = 0; c < numDis__; c++){
       for(int r = 0; r < numSal__; r++)
         for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
           fprintf(f,"+ x_%d_%d_%d ",p,r,c);
       fprintf(f,"- v_%d_%d >= 0\n",d,c);
      }
     fprintf(f,"\n");
    }
   fprintf(f,"\n\\R9 - Dias m\EDnimos\n");
   for(int c = 0; c < numDis__; c++){
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"+ v_%d_%d ",d,c);
     fprintf(f,"+ q_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
    }
   fprintf(f,"\n\\R10 a R13+#PER_DIA - Janelas no hor\E1rio das turmas\n");
   for(int u = 0; u < numTur__; u++)
    {
     for(int d = 0; d < numDia__; d++)
      {
       for(int c = 0; c < numDis__; c++)
         if(matDisTur__[c][u] == 1)
          {
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__),r,c,(d*numPerDia__)+1,r,c);
          }
       fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,0);
      }
    }
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
    {
     for(int d = 0; d < numDia__; d++)
      {
       for(int c = 0; c < numDis__; c++)
         if(matDisTur__[c][u] == 1)
          {
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+numPerDia__-1,r,c,(d*numPerDia__)+numPerDia__-2,r,c);
          }
       fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,numPerDia__-1);
      }
    }
  fprintf(f,"\n");
  for(int s = 1; s < numPerDia__-1; s++)
   {
     for(int u = 0; u < numTur__; u++)
      {
       for(int d = 0; d < numDia__; d++)
        {
         for(int c = 0; c < numDis__; c++)
           if(matDisTur__[c][u] == 1)
            {
             for(int r = 0; r < numSal__; r++)
               fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+s,r,c,(d*numPerDia__)+s-1,r,c,(d*numPerDia__)+s+1,r,c);
            }
         fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,s);
        }
      }
    fprintf(f,"\n");
   }
   fprintf(f,"\n\\R14 - Salas utilizadas por disciplina\n");
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
         fprintf(f,"x_%d_%d_%d - y_%d_%d <= 0\n",p,r,c,r,c);
     fprintf(f,"\n");
    }
   fprintf(f,"\n\\R15 - Salas utilizadas por disciplina\n");
   for(int r = 0; r < numSal__; r++)
    {
     for(int c = 0; c < numDis__; c++)
      {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
       fprintf(f,"- y_%d_%d >= 0\n",r,c);
      }
     fprintf(f,"\n");
    }
 #endif
 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Vari\E1veis x\n");
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerTot__; p++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"0 <= x_%d_%d_%d <= 1\n",p,r,c);
 #ifdef RES_SOFT
   fprintf(f,"\n\\Vari\E1veis v\n");
   for(int d = 0; d < numDia__; d++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"0 <= v_%d_%d <= 1\n",d,c);
   fprintf(f,"\n\\Vari\E1veis q\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"0 <= q_%d <= %d\n",c,numDia__);
   fprintf(f,"\n\\Vari\E1veis z\n");
   for(int u = 0; u < numTur__; u++)
     for(int d = 0; d < numDia__; d++)
       for(int s = 0; s < numPerDia__; s++)
          fprintf(f,"0 <= z_%d_%d_%d <= 1\n",u,d,s);
   fprintf(f,"\n\\Vari\E1veis y\n");
   for(int r = 0; r < numSal__; r++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"0 <= y_%d_%d <= 1\n",r,c);
 #endif
 #ifdef RES_SOFT
   fprintf(f,"\nGENERALS\n");
   fprintf(f,"\n\\Vari\E1veis q\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"q_%d\n",c);
 #endif
 
 if (RelLinear == 1){
	 fprintf(f,"\nBINARIES\n");
	 for(int r = 0; r < numSal__; r++)
	   for(int p = 0; p < numPerTot__; p++)
		 for(int c = 0; c < numDis__; c++)
		   fprintf(f,"x_%d_%d_%d\n",p,r,c);
	   fprintf(f,"\n\\Vari\E1veis v\n");
	   for(int d = 0; d < numDia__; d++)
		 for(int c = 0; c < numDis__; c++)
		   fprintf(f,"v_%d_%d\n",d,c);
	   fprintf(f,"\n\\Vari\E1veis z\n");
	   for(int u = 0; u < numTur__; u++)
		 for(int d = 0; d < numDia__; d++)
		   for(int s = 0; s < numPerDia__; s++)
			  fprintf(f,"z_%d_%d_%d\n",u,d,s);
	   fprintf(f,"\n\\Vari\E1veis y\n");
	   for(int r = 0; r < numSal__; r++)
		 for(int c = 0; c < numDis__; c++)
		   fprintf(f,"y_%d_%d\n",r,c);
 }
 fprintf(f,"\nEND");
 fclose(f);
}
//------------------------------------------------------------------------------
void lerInstancia(char *arq){
	
 int pos;
 char aux[50];
 FILE *f = fopen(arq,"r");
 
 if (f == NULL) {
	printf("Erro de abertura do arquivo %s\n", arq);
	exit(1);
 }

 fscanf(f,"Name: %s\n",&nomInst__);
 fscanf(f,"Courses: %d\n",&numDis__);
 fscanf(f,"Rooms: %d\n",&numSal__);
 fscanf(f,"Days: %d\n",&numDia__);
 fscanf(f,"Periods_per_day: %d\n",&numPerDia__);
 fscanf(f,"Curricula: %d\n",&numTur__);
 fscanf(f,"Constraints: %d\n",&numRes__);
 fscanf(f,"\nCOURSES:\n");
 numPerTot__ = numDia__ * numPerDia__;
 numPro__ = 0;
 for(int i = 0; i < numDis__; i++)
  {
   fscanf(f,"%s %s %d %d %d\n",&vetDisciplinas__[i].nome_,&aux,
          &vetDisciplinas__[i].numPer_,&vetDisciplinas__[i].diaMin_,&vetDisciplinas__[i].numAlu_);
   pos = -1;
   for(int p = 0; p < numPro__; p++)
     if(strcmp(aux,vetProfessores__[p].nome_) == 0)
      {
       vetDisciplinas__[i].professor_ = p;
       pos = p;
       break;
      }
   if(pos == -1)
    {
     vetDisciplinas__[i].professor_ = numPro__;
     strcpy(vetProfessores__[numPro__].nome_,aux);
     numPro__++;
    }
  }
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numTur__; j++)
     matDisTur__[i][j] = 0;
 fscanf(f,"\nROOMS:\n");
 for(int i = 0; i < numSal__; i++)
   fscanf(f,"%s %d\n",&vetSalas__[i].nome_,&vetSalas__[i].capacidade_);
 fscanf(f,"\nCURRICULA:\n");
 for(int i = 0; i < numTur__; i++)
  {
   fscanf(f,"%s %d",&vetTurmas__[i].nome_,&vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
    {
     fscanf(f, "%s ",&aux);
     vetTurmas__[i].vetDis_[j] = -1;
     for(int k = 0; k < numDis__; k++)
       if(strcmp(aux,vetDisciplinas__[k].nome_) == 0)
        {
         vetTurmas__[i].vetDis_[j] = k;
         matDisTur__[k][i] = 1;
         break;
        }
    }
  }
 fscanf(f,"\nUNAVAILABILITY_CONSTRAINTS:\n");
 for(int i = 0; i < numRes__; i++) 
  {
   fscanf(f,"%s %d %d\n",&aux,&pos,&vetRestricoes__[i].periodo_);
   vetRestricoes__[i].periodo_ = (pos * numPerDia__) + vetRestricoes__[i].periodo_;
   vetRestricoes__[i].disciplina_ = -1;
   for(int j = 0; j < numDis__; j++)
     if(strcmp(aux,vetDisciplinas__[j].nome_) == 0)
      {
       vetRestricoes__[i].disciplina_ = j;
       break;
      }
  }
 fclose(f);
}
//------------------------------------------------------------------------------
void testarEntrada(){
	
 printf("Name: %s\n",nomInst__);
 printf("Courses: %d\n",numDis__);
 printf("Rooms: %d\n",numSal__);
 printf("Days: %d\n",numDia__);
 printf("Periods_per_day: %d\n",numPerDia__);
 printf("Curricula: %d\n",numTur__);
 printf("Constraints: %d\n",numRes__);

 printf("\nDISCIPLINAS:\n");
 for(int i = 0; i < numDis__; i++)
   printf("%d  %s  %d  %d  %d  %d\n",i,vetDisciplinas__[i].nome_,vetDisciplinas__[i].professor_,vetDisciplinas__[i].numPer_,vetDisciplinas__[i].diaMin_,vetDisciplinas__[i].numAlu_);

 printf("\nTURMAS:\n");
 for(int i = 0; i < numTur__; i++)
  {
   printf("%d  %s  %d --> ",i,vetTurmas__[i].nome_,vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
     printf("%d  ",vetTurmas__[i].vetDis_[j]);
   printf("\n");
  } 

 printf("\nPROFESSORES:\n");
 for(int i = 0; i < numPro__; i++)
   printf("%d  %s\n",i,vetProfessores__[i].nome_);

 printf("\nSALAS:\n");
 for(int i = 0; i < numSal__; i++)
   printf("%d  %s  %d\n",i,vetSalas__[i].nome_,vetSalas__[i].capacidade_);

 printf("\nRESTRICOES:\n");
 for(int i = 0; i < numRes__; i++)
   printf("%d  %d  %d\n",i,vetRestricoes__[i].disciplina_,vetRestricoes__[i].periodo_);

 printf("\nDISC x TURMA:\n");
 for(int i = 0; i < numDis__; i++)
  {
   for(int j = 0; j < numTur__; j++)
     printf("%d  ",matDisTur__[i][j]);
   printf("\n");
  }
}
//------------------------------------------------------------------------------
void lerSolucoesIniciais(char *arq){
	int i, j;
	char aux[200];
	strcpy(aux, "colunas/");
	if (arq != NULL){
		strcat(aux, arq);
	    strcat(aux, "/");
    }
	strcat(aux, nomInst__);
	strcat(aux, ".gc");

	matFO__ = (double*)malloc(MAX_COL * sizeof(double));

	matGC__ = (int**)malloc(MAX_VAR * sizeof(int*));
	for (i = 0; i < MAX_VAR; i++) {
		matGC__[i] = (int*)malloc(MAX_COL * sizeof(int));
	}
	
	FILE *f = fopen(aux, "r");
	if (f == NULL) {
		printf("Erro de abertura do arquivo %s\n", aux);
		exit(1);
	}

	fscanf(f, "%d\n", &numVar__);
	fscanf(f, "%d\n", &numCol__);
	fscanf(f, "%lf\n", &tempoGCIniciais_);

	for (j = 0; j < numCol__; j++) {
		fscanf(f, "%lf ", &matFO__[j]);
	}
	fscanf(f, "\n");

	for (i = 0; i < numVar__; i++) {
		for (j = 0; j < numCol__; j++) {
			fscanf(f, "%d ", &matGC__[i][j]);
		}
		fscanf(f, "\n");
	}
	fclose(f);
	
	/* Guarda Indice Melhor Coluna Inicial*/
	indiceMelhorColuna = 0;
	bestFO = matFO__[0];
	for (j = 1; j < numCol__; j++) {
		if(matFO__[j] < bestFO){
		    bestFO = matFO__[j];
			indiceMelhorColuna = j;
		}
	}
	
    bestSol = (double*)malloc(numVar__ * sizeof(double));
    for (i = 0; i < numVar__; i++) {
		bestSol[i] = matGC__[i][indiceMelhorColuna];
	}
}
//------------------------------------------------------------------------------
void aloca(){
	int i, sts;
	
	env = CPXopenCPLEX(&sts);
	sts = CPXsetintparam(env,CPX_PARAM_SCRIND,CPX_OFF);
	
	indVarX__ = (int***) malloc(numPerTot__ * sizeof (int**));
	indVarZ__ = (int***) malloc(numTur__ * sizeof (int**));
	indVarQ__ = (int*) malloc(numDis__ * sizeof (int*));
	indVarY__ = (int**) malloc(numSal__ * sizeof (int*));
	indVarV__ = (int**) malloc(numDia__ * sizeof (int*));

	matD__ = (int**)malloc(MAX_RSS * sizeof(int*));
	mat_Rss_Var__ = (int**)malloc(MAX_RSS * sizeof(int*));
	matE__ = (double*)malloc(MAX_RSS * sizeof(double));
	matSNS_DE__ = (char*)malloc(MAX_RSS * sizeof(char));
	for (i = 0; i < MAX_RSS; i++) {
		matD__[i] = (int*)malloc(MAX_VAR * sizeof(int));
		mat_Rss_Var__[i] = (int*)malloc(MAX_VAR * sizeof(int));
	}
	
	matLambdas__ = (int**)malloc(MAX_RSS * sizeof(int*));
	for (i = 0; i < MAX_RSS; i++) {
		matLambdas__[i] = (int*)malloc(MAX_COL * sizeof(int));
	}
	
	VecDual__ = (double*)malloc((MAX_RSS+1) * sizeof(double));
	VecX_SP   = (double*)malloc(MAX_VAR * sizeof(double));
	
    matA__ = (int**)malloc((MAX_RSH+1) * sizeof(int*));
    matB__ = (double*)malloc((MAX_RSH+1) * sizeof(double));
    matSNS_AB__ = (char*)malloc((MAX_RSH+1) * sizeof(char));
    for (i = 0; i < (MAX_RSH+1); i++) {
      matA__[i] = (int*)malloc((MAX_VAR+1) * sizeof(int));
    }
    
   indices = (int*)malloc((numVar__+1) * sizeof(int));
   ctype__ = (char*)malloc((numVar__+1) * sizeof(char));
   for (i = 0; i <= numVar__; i++) {
      indices[i] = i;
      ctype__[i] = 'B';
   }
   ctype__[numVar__-1] = 'I';
   ctype__[numVar__] = 'C';
   
   matCoefOrig__ = (double*)malloc((MAX_VAR + 1) * sizeof(double));
   matNovosCoef__ = (double*)malloc((MAX_VAR + 1) * sizeof(double));
}
//------------------------------------------------------------------------------
void desaloca(){
	int i, j, sts;

	for (i = 0; i < MAX_VAR; i++) {
		free(matGC__[i]);
	}
	free(matGC__);

	for (i = 0; i < numPerTot__; i++) {
		for (j = 0; j < numSal__; j++) { 
			free(indVarX__[i][j]);
		}    
		free(indVarX__[i]);
	}
	free(indVarX__);

	for (i = 0; i < numTur__; i++) {
		for (j = 0; j < numDia__; j++) { 
			free(indVarZ__[i][j]);
		}    
		free(indVarZ__[i]);
	}
	free(indVarZ__);

	free(indVarQ__);

	for (i = 0; i < numSal__; i++) {
		free(indVarY__[i]);
	}
	free(indVarY__);

	for (i = 0; i < numDia__; i++) {
		free(indVarV__[i]);
	}
	free(indVarV__);

	for (i = 0; i < MAX_RSS; i++) {
		free(matD__[i]);
		free(mat_Rss_Var__[i]);
	}
	free(matD__);
	free(MAT_D_VAL__);
    free(MAT_D_IND__);
    free(MAT_D_CNT__);
	free(mat_Rss_Var__);

	free(matE__);

	free(matSNS_DE__);

	for (i = 0; i < MAX_RSS; i++) {
		free(matLambdas__[i]);
	}
	free(matLambdas__);

	free(matFO__);
	
	free(bestSol);

	for (i = 0; i < MAX_RSH; i++) {
	  free(matA__[i]);
	}
	free(matA__);

	free(matB__);

	free(matSNS_AB__);

	free(matCoefOrig__);

	free(VecDual__);
	free(VecX_SP);
	
	free(matNovosCoef__);
	
	free(indices);
	
	free(ctype__);
	
	free(coladd_FO);
	free(coladd_IND);
	free(VecNewCol);
	free(coladd_BEG);
	free(coladd_lb);
	free(coladd_ub);
	
	sts = CPXfreeprob(env,&pmr);
    sts = CPXfreeprob(env,&sp);
    sts = CPXcloseCPLEX(&env);
}
//------------------------------------------------------------------------------
void MontaMatrizesIndices(){

    int i, j, k, cont;

    cont = 0;
//////////VARIAVEIS X
    for (i = 0; i < numPerTot__; i++) {
        indVarX__[i] = (int**) malloc(numSal__ * sizeof (int*));
        for (j = 0; j < numSal__; j++) {
            indVarX__[i][j] = (int*) malloc(numDis__ * sizeof (int));
        }
    }

    for (i = 0; i < numPerTot__; i++) {
        for (j = 0; j < numSal__; j++) {
            for (k = 0; k < numDis__; k++) {
                indVarX__[i][j][k] = cont;
				cont++;
            }
        }
    }
    
//////////VARIAVEIS Z
    for (i = 0; i < numTur__; i++) {
        indVarZ__[i] = (int**) malloc(numDia__ * sizeof (int*));
        for (j = 0; j < numDia__; j++) {
            indVarZ__[i][j] = (int*) malloc(numPerDia__ * sizeof (int));
        }
    }

    for (i = 0; i < numTur__; i++) {
        for (j = 0; j < numDia__; j++) {
            for (k = 0; k < numPerDia__; k++) {
                indVarZ__[i][j][k] = cont;
		cont++;
            }
        }
    }

//////////VARIAVEIS Q
    for (i = 0; i < numDis__; i++) {
	     indVarQ__[i] = cont;
	     cont++;
    }

//////////VARIAVEIS Y
    for (i = 0; i < numSal__; i++) {
        indVarY__[i] = (int*) malloc(numDis__ * sizeof (int));
    }

    for (i = 0; i < numSal__; i++) {
        for (j = 0; j < numDis__; j++) {
			indVarY__[i][j] = cont;
			cont++;
        }
    }

//////////VARIAVEIS V
    for (i = 0; i < numDia__; i++) {
        indVarV__[i] = (int*) malloc(numDis__ * sizeof (int));
    }

    for (i = 0; i < numDia__; i++) {
        for (j = 0; j < numDis__; j++) {
			indVarV__[i][j] = cont;
			cont++;
        }
    }
}
//------------------------------------------------------------------------------
void ConstroiMatrizD(){
    int i, j, c, d, p, r, u, s, contRes;

	contRes = 0;

	/* R10 a ~R13+#PER_DIA - Janelas no horário das turmas */
	for(u = 0; u < numTur__; u++){
		for(d = 0; d < numDia__; d++){
			for(c = 0; c < numDis__; c++)
				if(matDisTur__[c][u] == 1){
					for(r = 0; r < numSal__; r++){
						matD__[contRes][indVarX__[(d*numPerDia__)][r][c]] = 1;
						matD__[contRes][indVarX__[(d*numPerDia__)+1][r][c]] = -1;
						mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)][r][c]] = 1;
						mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+1][r][c]] = 1;
					}
				}
			matD__[contRes][indVarZ__[u][d][0]] = -1;
			mat_Rss_Var__[contRes][indVarZ__[u][d][0]] = 1;
			matE__[contRes] = 0;
			matSNS_DE__[contRes] = 'L';
			contRes++;
		}
	}

	for(u = 0; u < numTur__; u++){
		for(d = 0; d < numDia__; d++){
			for(c = 0; c < numDis__; c++)
				if(matDisTur__[c][u] == 1){
					for(r = 0; r < numSal__; r++){
						matD__[contRes][indVarX__[(d*numPerDia__)+numPerDia__-1][r][c]] = 1;
						matD__[contRes][indVarX__[(d*numPerDia__)+numPerDia__-2][r][c]] = -1;
						mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+numPerDia__-1][r][c]] = 1;
						mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+numPerDia__-2][r][c]] = 1;
					}
						
				}
			matD__[contRes][indVarZ__[u][d][numPerDia__-1]] = -1;
			mat_Rss_Var__[contRes][indVarZ__[u][d][numPerDia__-1]] = 1;
			matE__[contRes] = 0;
			matSNS_DE__[contRes] = 'L';
			contRes++;
		}
	}

	for(s = 1; s < numPerDia__-1; s++)
		for(u = 0; u < numTur__; u++)
			for(d = 0; d < numDia__; d++){
				for(c = 0; c < numDis__; c++)
					if(matDisTur__[c][u] == 1){
						for(r = 0; r < numSal__; r++){
							matD__[contRes][indVarX__[(d*numPerDia__)+s][r][c]] = 1;
							matD__[contRes][indVarX__[(d*numPerDia__)+s-1][r][c]] = -1;
							matD__[contRes][indVarX__[(d*numPerDia__)+s+1][r][c]] = -1;
							mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+s][r][c]] = 1;
							mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+s-1][r][c]] = 1;
							mat_Rss_Var__[contRes][indVarX__[(d*numPerDia__)+s+1][r][c]] = 1;
						}
					}
				matD__[contRes][indVarZ__[u][d][s]] = -1;
				mat_Rss_Var__[contRes][indVarZ__[u][d][s]] = 1;
				matE__[contRes] = 0;
				matSNS_DE__[contRes] = 'L';
				contRes++;
			}

	/* R14 - Salas utilizadas por disciplina */ 
	for(p = 0; p < numPerTot__; p++)
		for(r = 0; r < numSal__; r++)
			for(c = 0; c < numDis__; c++){
				matD__[contRes][indVarX__[p][r][c]] = 1;
				matD__[contRes][indVarY__[r][c]] = -1;
				mat_Rss_Var__[contRes][indVarX__[p][r][c]] = 1;
				mat_Rss_Var__[contRes][indVarY__[r][c]] = 1;
				matE__[contRes] = 0;
				matSNS_DE__[contRes] = 'L';
				contRes++;
			}

	/* R15 - Salas utilizadas por disciplina */
	for(r = 0; r < numSal__; r++)
		for(c = 0; c < numDis__; c++){
			for(p = 0; p < numPerTot__; p++){
				matD__[contRes][indVarX__[p][r][c]] = 1;
				mat_Rss_Var__[contRes][indVarX__[p][r][c]] = 1;
			}
			matD__[contRes][indVarY__[r][c]] = -1;
			mat_Rss_Var__[contRes][indVarY__[r][c]] = 1;
			matE__[contRes] = 0;
			matSNS_DE__[contRes] = 'G';
			contRes++;
		}

	numRss_ = contRes;
}
//------------------------------------------------------------------------------
void ConstroiMatrizLambdas(){

	int i, j, k, S, contMATVAL, contMATBEG, contMATCNT;
	
	//-----------------------------------------------------
	MAT_D_VAL__ = (int*)malloc(MAX_CVA * sizeof(int));
	MAT_D_IND__ = (int*)malloc(MAX_CVA * sizeof(int));	
	MAT_D_CNT__ = (int*)malloc((numRss_+1) * sizeof(int));
	
	contMATVAL = 0;
	for (i = 0; i < numRss_; i++) {
		contMATCNT = 0;
		for (j = 0; j < numVar__; j++) {	
			if(matD__[i][j] != 0){
				MAT_D_VAL__[contMATVAL] = matD__[i][j];
				MAT_D_IND__[contMATVAL] = j;
				contMATVAL++;
				contMATCNT++;
			}			
		}
		MAT_D_CNT__[i] = contMATCNT;
	}

	for (j = 0; j < numCol__; j++) {
		contMATVAL = 0;
		for (i = 0; i < numRss_; i++) {
			S = 0;
			for (k = 0; k < MAT_D_CNT__[i]; k++) {
				S += (MAT_D_VAL__[contMATVAL] * matGC__[MAT_D_IND__[contMATVAL]][j]);
				contMATVAL++;
			}
			matLambdas__[i][j] = S;
		}
	}
	
	/*for (i = 0; i < numRss_; i++) {
		for (j = 0; j < numCol__; j++) {
			S = 0;
			for (k = 0; k < numVar__; k++) {
				S += (matD__[i][k] * matGC__[k][j]);
			}
			matLambdas__[i][j] = S;
		}
	}*/

	/*********PREPARA MATRIZES PARA CPLEX****************/
	matE__[numRss_] = 1;
	matSNS_DE__[numRss_] = 'E';

	MATVAL__ = (double*)malloc(((numRss_ * numCol__)/2) * sizeof(double));
	MATIND__ = (int*)malloc(((numRss_ * numCol__)/2) * sizeof(int));
	MATBEG__ = (int*)malloc(numCol__ * sizeof(int));
	MATCNT__ = (int*)malloc(numCol__ * sizeof(int));
	lbVec__ = (double*)malloc(numCol__ * sizeof(double));
	ubVec__ = (double*)malloc(numCol__ * sizeof(double));

	contMATVAL = 0;
	contMATBEG = 0;
	for (j = 0; j < numCol__; j++) {
		MATBEG__[j] = contMATBEG;
		contMATCNT = 0;
		for (i = 0; i < numRss_; i++) {	
			if(matLambdas__[i][j] != 0){
				MATVAL__[contMATVAL] = matLambdas__[i][j];
				MATIND__[contMATVAL] = i;
				contMATVAL++;
				contMATCNT++;
			}			
		}
		MATVAL__[contMATVAL] = 1;
		MATIND__[contMATVAL] = numRss_;
		contMATVAL++;
		contMATCNT++;
		contMATBEG = contMATVAL;
		MATCNT__[j] = contMATCNT;
		lbVec__[j] = 0;
		ubVec__[j] = CPX_INFBOUND;
	}
	/*********************************************/
	
}
//------------------------------------------------------------------------------
void ConstroiMatrizA(){
        int i, j, k, u, c, p, r, t, s, d, contRes, contMATVAL, contMATBEG, contMATCNT;

  contRes = 0;

  /* R0 - Valor constante */
  matA__[contRes][numVar__-1] = 1;
  matB__[contRes] = numDis__;
  matSNS_AB__[contRes] = 'E';
  contRes++;

  /* R1 - Número de aulas */
 for(c = 0; c < numDis__; c++){
   for(p = 0; p < numPerTot__; p++)
     for(r = 0; r < numSal__; r++)
       matA__[contRes][indVarX__[p][r][c]] = 1;
   matB__[contRes] = vetDisciplinas__[c].numPer_;
   matSNS_AB__[contRes] = 'E';
   contRes++;
 }
  /* R2 - Aulas na mesma sala no mesmo período */
 for(p = 0; p < numPerTot__; p++)
   for(r = 0; r < numSal__; r++){
     for(c = 0; c < numDis__; c++)
       matA__[contRes][indVarX__[p][r][c]] = 1;
     matB__[contRes] = 1;
     matSNS_AB__[contRes] = 'L';
     contRes++;
   }
  /* R3 - Aulas de uma disciplina no mesmo período */
 for(p = 0; p < numPerTot__; p++)
   for(c = 0; c < numDis__; c++){
     for(r = 0; r < numSal__; r++)
      matA__[contRes][indVarX__[p][r][c]] = 1;
     matB__[contRes] = 1;
     matSNS_AB__[contRes] = 'L';
     contRes++;
   }
  /* R4 - Aulas de um professor no mesmo período */ 
 for(p = 0; p < numPerTot__; p++)
   for(t = 0; t < numPro__; t++){
     for(r = 0; r < numSal__; r++)
       for(c = 0; c < numDis__; c++)
         if(vetDisciplinas__[c].professor_ == t)
           matA__[contRes][indVarX__[p][r][c]] = 1;
     matB__[contRes] = 1;
     matSNS_AB__[contRes] = 'L';
     contRes++;
   }
  /* R5 - Aulas de uma turma no mesmo período */
 for(p = 0; p < numPerTot__; p++)
   for(u = 0; u < numTur__; u++){
     for(r = 0; r < numSal__; r++)
       for(c = 0; c < numDis__; c++){
         for(k = 0; k < vetTurmas__[u].numDis_; k++)
           if(vetTurmas__[u].vetDis_[k] == c){
             matA__[contRes][indVarX__[p][r][c]] = 1;
             break;
           }
       }
     matB__[contRes] = 1;
     matSNS_AB__[contRes] = 'L';
     contRes++;
    }
  /* R6 - Indisponibilidades */
 for(s = 0; s < numRes__; s++){
   for(r = 0; r < numSal__; r++)
     matA__[contRes][indVarX__[vetRestricoes__[s].periodo_][r][vetRestricoes__[s].disciplina_]] = 1;
   matB__[contRes] = 0;
   matSNS_AB__[contRes] = 'E';
   contRes++;
 }
  
  	/* R7 - Número de salas usadas por disciplina */
   	for(c = 0; c < numDis__; c++)
     		for(d = 0; d < numDia__; d++)
       			for(p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++){
        			for(r = 0; r < numSal__; r++)
						matA__[contRes][indVarX__[p][r][c]] = 1;
					matA__[contRes][indVarV__[d][c]] = -1;
					matB__[contRes] = 0;
					matSNS_AB__[contRes] = 'L';
					contRes++;
        		}

	/* R8 - Número de salas usadas por disciplina */
	for(c = 0; c < numDis__; c++)
		for(d = 0; d < numDia__; d++){
			for(r = 0; r < numSal__; r++)
				for(p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
					matA__[contRes][indVarX__[p][r][c]] = 1;
			matA__[contRes][indVarV__[d][c]] = -1;
			matB__[contRes] = 0;
			matSNS_AB__[contRes] = 'G';
			contRes++;
		}

	/* R9 - Dias mínimos */
	for(c = 0; c < numDis__; c++){
		for(d = 0; d < numDia__; d++)
			matA__[contRes][indVarV__[d][c]] = 1;
		matA__[contRes][indVarQ__[c]] = 1;
		matB__[contRes] = vetDisciplinas__[c].diaMin_;
		matSNS_AB__[contRes] = 'G';
		contRes++;
	}

  numRsh_ = contRes;
  matA__[numRsh_][numVar__] = 1;
  matSNS_AB__[contRes] = 'E';
  matB__[numRsh_] = 0;

	/*********PREPARA MATRIZES PARA CPLEX****************/
	MATVAL__ = (double*)malloc(((numRsh_ * numVar__)/2) * sizeof(double));
	MATIND__ = (int*)malloc(((numRsh_ * numVar__)/2) * sizeof(int));
	MATBEG__ = (int*)malloc((numVar__+1) * sizeof(int));
	MATCNT__ = (int*)malloc((numVar__+1) * sizeof(int));
	lbVec__ = (double*)malloc((numVar__+1) * sizeof(double));
	ubVec__ = (double*)malloc((numVar__+1)* sizeof(double));

	contMATVAL = 0;
	contMATBEG = 0;
	for (j = 0; j <= numVar__; j++) {
		MATBEG__[j] = contMATBEG;
		contMATCNT = 0;
		for (i = 0; i <= numRsh_; i++) {	
			if(matA__[i][j] != 0){
				MATVAL__[contMATVAL] = matA__[i][j];
				MATIND__[contMATVAL] = i;
				contMATVAL++;
				contMATCNT++;
			}			
		}
		contMATBEG = contMATVAL;
		MATCNT__[j] = contMATCNT;
		lbVec__[j] = 0;
		ubVec__[j] = 1;
	}
	ubVec__[numVar__-1] = CPX_INFBOUND;
	lbVec__[numVar__] = -CPX_INFBOUND;
	ubVec__[numVar__] = CPX_INFBOUND;
	/**********************************************/  
}
//------------------------------------------------------------------------------
void ConstroiCoefOrig(){

  int p,r,c,u,d,s;

   for(r = 0; r < numSal__; r++){
     for(p = 0; p < numPerTot__; p++){
       for(c = 0; c < numDis__; c++)
        if(vetDisciplinas__[c].numAlu_ > vetSalas__[r].capacidade_)
          matCoefOrig__[indVarX__[p][r][c]] = PESOS[0]*(vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_);
        else
          matCoefOrig__[indVarX__[p][r][c]] = 0;
      }
    }

   for(u = 0; u < numTur__; u++){
     for(d = 0; d < numDia__; d++){
       for(s = 0; s < numPerDia__; s++)

          matCoefOrig__[indVarZ__[u][d][s]] = PESOS[1];
      }
    }

   for(c = 0; c < numDis__; c++)
     matCoefOrig__[indVarQ__[c]] = PESOS[2];

   for(c = 0; c < numDis__; c++){
     for(r = 0; r < numSal__; r++)
       matCoefOrig__[indVarY__[r][c]] = PESOS[3];
    }

    for (d = 0; d < numDia__; d++) 
        for (c = 0; c < numDis__; c++) 
          matCoefOrig__[indVarV__[d][c]] = 0;

   matCoefOrig__[numVar__-1] = -1;
   matCoefOrig__[numVar__] = 0;

}
//------------------------------------------------------------------------------
void CalculaNovosCoef(){

   int i, j, contMATVAL;
   double S;

    contMATVAL = 0;
    for(j = 0; j < numVar__; j++){
		matNovosCoef__[j] = matCoefOrig__[j];
	}
	for (i = 0; i < numRss_; i++) {
		for (j = 0; j < MAT_D_CNT__[i]; j++) {
			matNovosCoef__[MAT_D_IND__[contMATVAL]] -= VecDual__[i] * mat_Rss_Var__[i][MAT_D_IND__[contMATVAL]] * MAT_D_VAL__[contMATVAL];	
			contMATVAL++;
		}
	}
	matNovosCoef__[numVar__] = -1;
}
//------------------------------------------------------------------------------
void obtemNovaColuna(){
	int i, j, contMATVAL;
	double S;
	
    contMATVAL = 0;
	for (i = 0; i < numRss_; i++) {
		S = 0;
		for (j = 0; j < MAT_D_CNT__[i]; j++) {
			S += (MAT_D_VAL__[contMATVAL] * VecX_SP[MAT_D_IND__[contMATVAL]]);	
			contMATVAL++;
		}
		VecNewCol[i] = S;
	}
	VecNewCol[numRss_] = 1;
	
	/*for(i = 0; i < numRss_; i++){
	    S = 0;
	    for(j = 0; j < numVar__; j++){
		   S += matD__[i][j]*VecX_SP[j];	
		}	
		VecNewCol[i] = S;
	}
	VecNewCol[numRss_] = 1;*/
	
	S = 0;
	for(j = 0; j < numVar__; j++){
		S += matCoefOrig__[j]*VecX_SP[j];
	}
	coladd_FO[0] = S;
	
}
//------------------------------------------------------------------------------
void desalocaMarizesCPX(){

	free(MATVAL__);
	free(MATIND__);
	free(MATBEG__);
	free(MATCNT__);

	free(lbVec__);
	free(ubVec__);
}
//------------------------------------------------------------------------------
void alocaAddCol(){
	
   coladd_FO = (double*)malloc(sizeof(double));
   coladd_IND  = (int*)malloc((numRss_+1) * sizeof(int));
   VecNewCol = (double*)malloc((numRss_+1) * sizeof(double));
   coladd_BEG  = (int*)malloc(sizeof(int));
   coladd_lb = (double*)malloc(sizeof(double));
   coladd_ub = (double*)malloc(sizeof(double));
   
   for(int i = 0; i <= numRss_; i++)
      coladd_IND[i] = i;
   
   coladd_BEG[0] = 0;
   coladd_lb[0] = 0;
   coladd_ub[0] = CPX_INFBOUND;
}
//------------------------------------------------------------------------------
int EhViavel(){
   int i, j, contMATVAL = 0;
   double S;
   
    newFO = 0;
	for (i = 0; i < numVar__; i++) {
		newFO = newFO + (matCoefOrig__[i]*VecX_SP[i]);
	}
    
	for (i = 0; i < numRss_; i++) {
		S = 0;
		for (j = 0; j < MAT_D_CNT__[i]; j++) {
			S = S + (MAT_D_VAL__[contMATVAL] * VecX_SP[MAT_D_IND__[contMATVAL]]);
			contMATVAL++;	
		}
		
		if ((matSNS_DE__[i] == 'L') && (S > matE__[i])){
			return 0;
		}
		else if ((matSNS_DE__[i] == 'G') && (S < matE__[i])){
			return 0;
		}
	}
	
   return 1;	
}
//------------------------------------------------------------------------------
void execCpx_RelLinear(){
 Solucao s;
 int sts;
 char aux[50];
 time_t hi,hf;


 strcpy(aux,nomInst__);
 strcat(aux,".lp");
 montarModeloPLI(aux,0);
 
 CPXENVptr env; 
 CPXLPptr lp;   
 env = CPXopenCPLEX(&sts);
 sts = CPXsetintparam(env,CPX_PARAM_SCRIND,CPX_ON);
 lp = CPXcreateprob(env,&sts,"");
 sts = CPXreadcopyprob(env,lp,aux,NULL);
 s.numVar_ = CPXgetnumcols(env,lp);
 s.numRes_ = CPXgetnumrows(env,lp);
 
 hi = time(NULL);
 sts = CPXmipopt(env,lp);
 hf = time(NULL);
 s.tempo_ = difftime(hf,hi);
 sts = CPXgetmipobjval(env,lp,&s.valSol_);
 sts = CPXgetbestobjval(env,lp,&s.bstNod_);
 sts = CPXgetmipx(env,lp,s.vetSol_,0,(numPerTot__*numSal__*numDis__-1));
 sts = CPXfreeprob(env,&lp);
 sts = CPXcloseCPLEX(&env);
 
 printf(">>> RESULTADOS DO CPLEX\n\n");
 printf("Num. var....: %d\n",s.numVar_);
 printf("Num. res....: %d\n",s.numRes_);
 printf("Val. sol....: %f\n",s.valSol_);
 printf("Melhor no...: %f\n",s.bstNod_);
 if(s.valSol_ != 0)
   printf("GAP.........: %.2f\n",((s.valSol_ - s.bstNod_)/s.valSol_)*100);
 else
   printf("GAP.........: -\n");
 printf("Tempo.......: %.2f\n",s.tempo_);
}
