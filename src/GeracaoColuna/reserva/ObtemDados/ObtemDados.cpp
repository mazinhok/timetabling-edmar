#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

//------------------------------------------------------------------------------
int main(int argc, char *argv[]){

 int menor = 9999999;
 int p1,p2,p3,p4,p6,p8,p9,p10,p11,p12,p13,p14,p15,p16,p18;
 float p5,p7,p17,p19;
   
   FILE *f;
   // Abre Arquivo
   f = fopen(argv[1], "r");
   if (f == NULL) {
        printf("Erro de abertura do arquivo %s\n", argv[1]);
        exit(1);
   }

   while(fscanf(f,"%d;%d;%d;%d;%f;%d;%f;%d;%d;%d;%d;%d;%d;%d;%d;%d;%f;%d;%f\n",
                  &p1,&p2,&p3,&p4,&p5,&p6,&p7,&p8,&p9,&p10,&p11,&p12,&p13,&p14,&p15,&p16,&p17,&p18,&p19) != EOF){
		if (p6 < menor){
			menor = p6;
	    }
   }
   printf("%d;\n",menor);

   fclose(f);
 return 0;
}
