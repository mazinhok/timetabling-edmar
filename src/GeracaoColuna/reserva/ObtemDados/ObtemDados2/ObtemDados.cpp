#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

//------------------------------------------------------------------------------
int main(int argc, char *argv[]){

   char Linha[40];
   char *result;
   
   FILE *f;
   // Abre Arquivo
   f = fopen(argv[1], "r");
   if (f == NULL) {
        printf("Erro de abertura do arquivo %s\n", argv[1]);
        exit(1);
   }

  while (!feof(f))
  {
      result = fgets(Linha, 40, f);
      if (result){
		if ((Linha[0] == 'S') && (Linha[1] == 'u'))
		   printf("%s",Linha);
	  }
  }

   fclose(f);
 return 0;
}
