/*
 * Grasp.h
 *
 *  Created on: May 17, 2015
 *      Author: erika
 */

#ifndef DISS_VETOR_GRASP_H_
#define DISS_VETOR_GRASP_H_

#include "Model/Individuo.h"
#include "BuscaLocal/BuscaLocal.h"

class Problema;

class Grasp {
public:
	int nMaxIter;
	int nMaxIterSemMelhora;
	int pesoHard;
	int seed;
	float threshold;//Para construcao da solu;'ao inicial
	int tipoConstrucaoInicial;
    int mediaSolucoes;
    int soft1, soft2, soft3, soft4;
    int f1, f2, f3;
    int opBuscaLocalGrasp;
    int info, infoSD;
    int imprimeGrafico;
    int imprimeFO;
    //Parametros da Busca Local
    int nMaxIterBuscaLocal;
    int k;
    int vizinhancasUtilizadas;
    char nomeArquivoInstanciaInicial[30];
    //Parametros do SA
	double temperaturaInicial, temperaturaFinal;
	double taxaDecaimentoTemperatura;
	int aceitaPioraSA;

    double tempoLimite;
    double tempoExecucao;
	int iteracoesExecutadas;
	int nMovimentosRealizados;
	int nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves, nRoomStabilityMoves, nMinWorkingDaysMoves, nCurriculumCompactnessMoves;
	int nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos, nRoomMovesValidos, nLectureMovesValidos, nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos, nCurriculumCompactnessMovesValidos;
	int nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora, nRoomMovesMelhora, nLectureMovesMelhora, nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora, nCurriculumCompactnessMovesMelhora;
	int nMovesInvalido, nSwapsInvalido, nKempesInvalido, nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido, nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido, nCurriculumCompactnessMovesInvalido;

	//Geração de Coluna (Edmar)
	int geraMatrizGC;
	int** matrizGC;
        //////////////////////////////
	Grasp();

	Individuo* executa(Problema* p);
	void ImprimeExecucao();
	void ImprimeExecucaoResumida();
	//Geração de Coluna (Edmar)
        void AlocaMatrizes_GC(Problema* p, int maxIter);
	void DesalocaMatrizes_GC(Problema* p);
	void InsereElementosMatriz_GC(Problema* p, Individuo* ind, int col);
	void ImprimeMatriz_GC(Problema* p, int maxIter, double tempoExecucao);
	//////////////////////////////

private:
	Individuo* executaHillClimbing(Problema* p, Individuo* indInicial, BuscaLocal** bl);
	Individuo* executaSimulatedAnnealing(Problema* p, Individuo* indInicial, BuscaLocal** bl);
	Individuo* executaSteepestDescent(Problema* p, Individuo* indInicial, BuscaLocal** bl);
	Individuo* executaBuscaTeste(Problema* p, Individuo* indInicial, BuscaLocal** bl);
	void atualizaEstatisticasBuscaLocal(BuscaLocal* buscalocal);

};

#endif /* DISS_VETOR_GRASP_H_ */
