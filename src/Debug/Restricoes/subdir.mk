################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Restricoes/Restricao.cpp \
../Restricoes/RestricaoForte3Indisponibilidade.cpp \
../Restricoes/RestricaoForte4AulasConflitantes.cpp \
../Restricoes/RestricaoFraca2MinimumWorkingDays.cpp \
../Restricoes/RestricaoFraca3CurriculumCompactness.cpp \
../Restricoes/RestricaoFraca4RoomStability.cpp 

OBJS += \
./Restricoes/Restricao.o \
./Restricoes/RestricaoForte3Indisponibilidade.o \
./Restricoes/RestricaoForte4AulasConflitantes.o \
./Restricoes/RestricaoFraca2MinimumWorkingDays.o \
./Restricoes/RestricaoFraca3CurriculumCompactness.o \
./Restricoes/RestricaoFraca4RoomStability.o 

CPP_DEPS += \
./Restricoes/Restricao.d \
./Restricoes/RestricaoForte3Indisponibilidade.d \
./Restricoes/RestricaoForte4AulasConflitantes.d \
./Restricoes/RestricaoFraca2MinimumWorkingDays.d \
./Restricoes/RestricaoFraca3CurriculumCompactness.d \
./Restricoes/RestricaoFraca4RoomStability.d 


# Each subdirectory must supply rules for building sources it contributes
Restricoes/%.o: ../Restricoes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


