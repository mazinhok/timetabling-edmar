#ifndef geracol
#define geracol

#include <stdio.h>
#include "/opt/ibm/ILOG/CPLEX_Studio1271/cplex/include/ilcplex/cplex.h"

// Valores limites baseados nas inst�ncias comp
#define MAX_DIS 131    // 131
#define MAX_TUR 150    // 150
#define MAX_PRO 135    // 135
#define MAX_SAL 20     // 20
#define MAX_DIA 6      // 6
#define MAX_PER 9      // 9
#define MAX_RES 1368   // 1368
#define MAX_SPS 150    // subproblemas
#define MAX_COL 50000  // colunas
#define MAX_VAR 20000  // 19255
#define MAX_VVr 11000  // 10800
#define MAX_RSS 11100  // 11088-Restri��es Relaxadas-PMR
#define MAX_RSH 20000  // 19603
#define MAX_CVA 776000 // 775136-Mesmo valor usado em GC1

//================================ ESTRUTURAS ================================\\

//------------------------------------------------------------------------------
typedef struct tDisciplina
{
 char nome_[50]; // nome da disciplina
 int professor_; // id do professor
 int numPer_;    // n�mero de per�odos de oferta
 int diaMin_;    // dias m�nimos para distribui��o
 int numAlu_;    // n�mero de alunos
}Disciplina;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tTurma
{
 char nome_[50];       // nome da turma
 int numDis_;          // n�mero de disciplinas
 int vetDis_[MAX_DIS]; // vetor com as disciplinas (ids)
}Turma;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tProfessor
{
 char nome_[50]; // nome da turma
}Professor;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tSala
{
 char nome_[50];  // nome da sala
 int capacidade_; // capacidade
}Sala;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tSolucao
{
 // FO (restri��es SOFT)
 int capSal_; // n�mero de alunos que violam a capacidade da sala
 int janHor_; // n�mero de janelas no hor�rio das turmas
 int diaMin_; // n�mero "dias m�nimos" n�o respeitados
 int salDif_; // n�mero de salas diferentes usadas para as disciplinas
 int funObj_; // valor da fun��o objetivo

 // restri��es HARD
 int vioNumAul_; // viola��es na restri��o 1 - n�mero de aulas
 int vioAulSim_; // viola��es na restri��o 2 - aulas simultaneas
 int vioDisSim_; // viola��es na restri��o 3 - disciplinas simultaneas
 int vioProSim_; // viola��es na restri��o 4 - professores simultaneos
 int vioTurSim_; // viola��es na restri��o 5 - turmas simultaneas

 // vetor de solu��o (vari�veis x)
 double vetSol_[MAX_PER*MAX_DIA*MAX_SAL*MAX_DIS];

 // matriz de solu��o (per�odo x dia x sala)
 int matSolSal_[MAX_PER][MAX_DIA][MAX_SAL];

 // matriz de solu��o (per�odo x dia x turma)
 int matSolTur_[MAX_PER][MAX_DIA][MAX_TUR];
}Solucao;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct tCpx
{
 CPXLPptr lp_;   // problema no CPLEX
 int numVar_;    // n�mero de vari�veis do problema
 int numRes_;    // n�mero de restri��es do problema
 double limSup_; // valor do limitante superior (solu��o do problema)
 double limInf_; // valor do limitante inferior (melhor n�)
 double gap_;    // valor do gap
 double tempo_;  // tempo de execu��o
}Cpx;
//------------------------------------------------------------------------------

typedef struct tMetRel
{
 int numSps_;    // n�mero de subproblemas
 int fatDes_;    // fator de desbalanceamento - usado pela METIS na parti��o do "grafo"
 int numCtsM_;   // n�mero de arestas cortadas (valor retornado pela METIS)
 int numCts_;    // n�mero de arestas cortadas
 int pesCts_;    // peso das arestas cortadas
 double limSup_; // valor do limitante superior (solu��o real - vi�vel)
 double limInf_; // valor do limitante inferior (solu��o relaxada)
 double gap_;    // valor do gap
 double temPrt_; // tempo de particionamento do problema
 double temSub_; // tempo para montar o(s) subproblema(s)
 double temExe_; // tempo de execu��o do algoritmo de subgradientes ou gera��o de colunas
 double temTot_; // tempo total de execu��o
 int vetSpDis_[MAX_DIS]; // vetor com o id do subproblema em que cada disciplina ficou
 int vetSpTur_[MAX_TUR]; // vetor com o id do subproblema em que cada turma ficou
}MetRel;
//---------------------------------------------------------------------------

//==============================================================================


//============================= VARI�VEIS GLOBAIS ==============================
// ------------ Dados de entrada
char nomInst__[150]; // nome da inst�ncia
int numDis__;        // n�mero de disciplinas 
int numTur__;        // n�mero de turmas
int numPro__;        // n�mero de professores
int numSal__;        // n�mero de salas
int numDia__;        // n�mero de dias
int numPerDia__;     // n�mero de per�odos por dia
int numPerTot__;     // n�mero de per�odos total
int numRes__;        // n�mero de restri��es
bool matResDisPer__[MAX_DIS][MAX_DIA*MAX_PER]; // matriz que indica as restri��es de oferta (disciplina proibidas nos per�odos)
Disciplina vetDisciplinas__[MAX_DIS];
Turma vetTurmas__[MAX_TUR];
Professor vetProfessores__[MAX_PRO];
Sala vetSalas__[MAX_SAL];
double pmrMAX;
double spsMAX;
// ------------ CPLEX
CPXENVptr env__;              // ambiente do CPLEX
static Cpx vetCpx__[MAX_SPS]; // vetor de problemas
static Cpx pmr;               // Ambiente Cplex do PMR
static MetRel mr__;           // m�todos de relaxa��o
// ------------ METIS
int comPeso = 1;
int numSubProb = 2;
int facDes = 1;
int tGrafo = 1;               // Escolhe o m�todo de constru��o do grafo e parti��o: 1 - Curr�culos s�o V�rtices/ "?" - Disciplinas s�o V�rtices
// ------------ Auxiliares
int numCol__;                       // n�mero de colunas
int numColFinal__;
int numVar__;                       // n�mero de vari�veis
int numVarCopy__;                   // n�mero de vari�veis de c�pia
int numRss__;                       // n�mero de restri��es relaxadas
int *varSPS__;
int **indVarX__;                    // Matriz de �ndices das vari�veis X
int ***indVarY__;                   // Matriz de �ndices das vari�veis Y
int *indVarW__;                     // Vetor de �ndices das vari�veis W
int **indVarV__;                    // Matriz de �ndices das vari�veis V
int **indVarZ__;                    // Matriz de �ndices das vari�veis Z
int **indVarr__;                    // Matriz de �ndices das vari�veis r
double tempoGCIniciais_;            // tempo para gera��o das colunas iniciais
int *indices;                       // Vetor de Indices para atualiza��o dos coeficientes
char ctype__[MAX_COL];
bool matDisTur__[MAX_DIS][MAX_TUR]; // matriz que indica se uma turma possui uma disciplina
int numSalDif__;                    // n�mero de "tamanhos" diferentes de sala  
int vetTamSal__[MAX_SAL];           // vetor com os tamanhos diferentes das salas
int **matColInicial__;                      // matriz com as colunas iniciais
int **matColFinal__;                // matriz com as colunas finais
double *matFO__;                    // vetor com as FO's das colunas iniciais 
int **matD__;                       // Matriz com as restri��es relaxadas no PMR
double *matE__;                     // Vetor com o lado esquerdo das restri��es relaxadas no PMR
char *matSNS_DE__;                  // Vetor com o sinal das restri��es relaxadas no PMR
int **mat_Rss_Var__;                // Matriz bin�ria com valores iguais a 1 se a vari�vel aparece na restri��o e 0 caso contr�rio
double *matCoefOrig__;              // Vetor com os coeficientes originais do modelo completo
double *matNovosCoef__;             // Vetor com os novos coeficientes para atualiza��o do SPP
double *vecDualD__;
int **matLambdas__;                 // Matriz Inicial resultado da multiplica��o de matD__ * matColFinal
int vetAux[MAX_TUR];                // Vetor auxiliar para defini��o de qual subproblema vai ficar cada curr�culo
int vetAuxSps[MAX_SPS];
int vetTurDis[MAX_TUR];             // Vetor bin�rio para definir se um curr�culo foi cortado
int vetProfDis[MAX_PRO];            // Vetor bin�rio para definir se um professor ficou em subproblemas diferentes
int **vetVarOrigXCopy;
int *vetVarSPSSeq;
int vetQntVarSPS[MAX_SPS];
double *VecDual__;
double *VecSPx__;
double *VecNewX__;
double *VecNewCol__;
double *VecSPxPMR__;

int *MAT_D_VAL__;
int *MAT_D_IND__;
int *MAT_D_CNT__;

double *MATVAL__;
int *MATBEG__;
int *MATIND__;
int *MATCNT__;
double *lbVec__;
double *ubVec__;

double *coladd_FO;
int *coladd_IND;
int *coladd_BEG;
double *coladd_lb;
double *coladd_ub;
//==============================================================================


//===================================== M�TODOS ================================
void configuraParametros(int piNParam, char **piParams);
// ------------ Gera��o de Coluna
void lerSolucoesIniciais();
void ConstroiCoefOrig();
void CalculaNovosCoef();
void MontaMatrizesIndices();
void ConstroiMatrizD();
void ConstroiMatrizRssVar();
void ConstroiMatrizColFinal();
void ConstroiMatrizLambdas();
void alteraCoefSubProb();
void monColX(int sp);
void addNewCol(int sp);
double defineX(double x);
// ------------ M�todos de relaxa��o
void excPrtTur(const int &sps,const int &fat,const int &pes);
void excPrtDis();
void monSpsTur();
void monSpsDis();
void parGraTur(const int &sps,const int &fat,const int &pes);
void parGraDis(const int &sps,const int &fat,const int &pes);
// ------------ CPLEX
void execCpx_PMR();
void exeCpx(Solucao &s,char *arq,const int &rl);
void escResCpx(FILE *f,const int &rl);
void monModBurEal10(char *arq,const int &rl);
void monModLacLub12(char *arq,const int &rl);
// ------------ Solu��o
void escSolBurEal10(Solucao &s,FILE *f);
void escSolLacLub12(Solucao &s,FILE *f);
// ------------ Auxiliares
void ajustaParticao();
void lerInstancia(char *arq);
void testarEntrada();
void ordVetCapSal();
void contaVarRestricoes();
void aloca();
void desaloca();
void desalocaMarizesCPX();
void alocaAddCol();
void convertExcPRM();
//==============================================================================

#endif
