#include "geracol.h"

#include <string.h>
#include <time.h>
#include <math.h>
#include "/home/edmar/Documentos/timetabling-edmar/metis-5.0.1-X64/include/metis.h"

#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))


int PESOS[4] = {1,2,5,1};
double erro = 0.0000001;

//================================== PRINCIPAL ================================= 

//------------------------------------------------------------------------------
int main(int argc, char *argv[]){
 int sts, flag = 1, iter = 1, qntColAdic;
 FILE *arq;
 char aux[50];
 Solucao sol;
 time_t hi, hf;
 
 spsMAX = -CPX_INFBOUND;
 
 strcpy(aux,"../../instances/");
 strcat(aux,argv[1]);
 strcat(aux,".ctt");
 lerInstancia(aux);
 configuraParametros(argc, argv);
 lerSolucoesIniciais();
 MontaMatrizesIndices();
 
 parGraDis(numSubProb,facDes,comPeso);
 ajustaParticao();
 
 aloca();
 
 ConstroiCoefOrig();
   
 ConstroiMatrizD();
 ConstroiMatrizRssVar();
 ConstroiMatrizColFinal();
 ConstroiMatrizLambdas();
 
 ////// MONTA PMR //////
 pmr.lp_ = CPXcreateprob(env__,&sts,"");
 sts = CPXcopylp(env__,pmr.lp_,(numCol__*mr__.numSps_),(numRss__+mr__.numSps_),1,matFO__,matE__,matSNS_DE__,MATBEG__,MATCNT__,MATIND__,MATVAL__,lbVec__,ubVec__,NULL);
 desalocaMarizesCPX();
 ///////////////////////
 
 ////// MONTA SP ////// 
 monSpsDis();
 ///////////////////////
 
 alocaAddCol();
 
 hi = hf = time(NULL);
 pmr.limInf_ = -1.0;
 mr__.limInf_ = -2.0;
 //&& (fabs(pmr.limInf_ - spsMAX) > erro)
 while((difftime(hf,hi) <= 3600) && (flag) && (numColFinal__ < MAX_COL) ){
	 
	// Executa o PMR
	execCpx_PMR();
	
	// Define e altera coeficientes dos subproblemas
	CalculaNovosCoef();
	alteraCoefSubProb();
	
    // Executa cada subproblema armazenando o LB e o UB
    excPrtDis();
      
    printf("********** ITERACAO %d **********\n",iter);
    printf("PMR = %lf \tLB-SPB = %lf --> %lf\n",pmr.limInf_,mr__.limInf_,spsMAX);
    for(int i = 0; i < mr__.numSps_; i++)
      printf("SP-%d  UB = %lf  LB = %lf\n",i+1,vetCpx__[i].limSup_,vetCpx__[i].limInf_);
	
	// Adiciona colunas no PMR
	qntColAdic = 0;
	for(int i = 0; i < mr__.numSps_; i++){
		if((vetCpx__[i].limSup_ - VecDual__[numRss__+i]) < 0){
			monColX(i);
			addNewCol(i);
			qntColAdic++;
		}
			
	}
	if (qntColAdic == 0)
		flag = 0;
	iter++; 
    hf = time(NULL);
 }
 
 convertExcPRM();
 
 desaloca();
 return 0;
}
//------------------------------------------------------------------------------

//==============================================================================


//=========================== M�TODOS DE RELAXA��O =============================

//------------------------------------------------------------------------------
void excPrtTur(const int &sps,const int &fat,const int &pes){
 int sts;
 time_t hi, hf;
 char arq[50];
 parGraTur(sps,fat,pes);
 monSpsTur();
 for(int i = 0; i < mr__.numSps_; i++)
  {
   sprintf(arq,"sp%d.lp",i+1);
   vetCpx__[i].lp_ = CPXcreateprob(env__,&sts,"");
   CPXreadcopyprob(env__,vetCpx__[i].lp_,arq,NULL);
  }
 mr__.limInf_ = 0.0;
 hi = time(NULL); 
 for(int i = 0; i < mr__.numSps_; i++)
  {
   CPXmipopt(env__,vetCpx__[i].lp_);
   CPXgetmipobjval(env__,vetCpx__[i].lp_,&vetCpx__[i].limSup_);
   mr__.limInf_ += vetCpx__[i].limSup_;
  }
 hf = time(NULL); 
 mr__.temExe_ = difftime(hf,hi);
 for(int i = 0; i < mr__.numSps_; i++)
   CPXfreeprob(env__,&vetCpx__[i].lp_);
}
//------------------------------------------------------------------------------
void excPrtDis(){
 time_t hi, hf;

 mr__.limInf_ = 0.0;
 hi = time(NULL); 
 for(int i = 0; i < mr__.numSps_; i++){
   CPXmipopt(env__,vetCpx__[i].lp_);
   CPXgetmipobjval(env__,vetCpx__[i].lp_,&vetCpx__[i].limSup_);
   CPXgetbestobjval(env__,vetCpx__[i].lp_,&vetCpx__[i].limInf_);
   mr__.limInf_ += vetCpx__[i].limSup_;
 }
 for (int i = 0; i < numRss__; i++)
    mr__.limInf_ += (VecDual__[i] * matE__[i]);
 hf = time(NULL);
 mr__.temExe_ = difftime(hf,hi);
 
  if (mr__.limInf_ > spsMAX)
   spsMAX = mr__.limInf_; 
}
//------------------------------------------------------------------------------
void monSpsTur(){
 FILE *f;
 time_t hi, hf;
 char arq[50];
 int aux,flag;
 int vetAux[MAX_DIS];
 int vetTurDis[MAX_DIS];
 hi = time(NULL);
 // ------------------ 
 // definir em qual turma cada disciplina ser� "contada" na FO
 memset(vetTurDis,0,sizeof(vetTurDis));
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u] && (vetTurmas__[u].numDis_ > aux))
      {
       aux = vetTurmas__[u].numDis_;
       vetTurDis[c] = u;
      }
  } 
 for(int sp = 0; sp < mr__.numSps_; sp++)
  {
   // ------------------ 
   // definir as disciplinas das turmas do subproblema sp
   memset(vetAux,0,sizeof(vetAux));
   for(int c = 0; c < numDis__; c++)
     for(int u = 0; u < numTur__; u++)
       if((mr__.vetSpTur_[u] == sp) && matDisTur__[c][u])
        {
         vetAux[c] = 1;
         break;
        }
   sprintf(arq,"sp%d.lp",sp+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
      {
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ 0 x_%d_%d ",c,p);
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int s = 0; s < (numSalDif__-1); s++)
      {
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])          
          {
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
             fprintf(f,"+ %d y_%d_%d_%d ",MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
          }
      }
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\Dias minimos\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpTur_[vetTurDis[c]] == sp) 
       fprintf(f,"+ 5 w_%d ",c); 
   fprintf(f,"\n\n\\Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++)
    {
     if(mr__.vetSpTur_[u] == sp) 
      {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ 2 v_%d_%d ",u,p); 
       fprintf(f,"\n"); 
      }
    }
   // ------------------ restri��es
   fprintf(f,"\nST\n");
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++)
    {
     if(vetAux[c])
      {
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d ",c,p); 
       fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
      } 
    }
   fprintf(f,"\n\\R2 - Numero de salas\n");
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
      {
       if(vetAux[c])         
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d ",c,p); 
      }
     fprintf(f,"<= %d\n",numSal__); 
    }
   fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     for(int c = 0; c < numDis__; c++)
      {
       if(vetAux[c])
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
      }
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     aux = 0;
     for(int i = 0; i < numSal__; i++)
       if(vetSalas__[i].capacidade_ > vetTamSal__[s])
         aux++;
     for(int p = 0; p < numPerTot__; p++) 
      {
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
             fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
       fprintf(f,"<= %d\n",aux); 
      } 
     fprintf(f,"\n"); 
    }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
        {
         for(int q = 0; q < numPerDia__; q++)
           if(!matResDisPer__[c][(d*numPerDia__)+q])
             fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
         fprintf(f,"- z_%d_%d >= 0\n",c,d);
        }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
      {
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"+ z_%d_%d ",c,d);
       fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
      }
   fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
      {
       for(int p = 0; p < numPerTot__; p++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
         fprintf(f,"- r_%d_%d = 0\n",u,p);
        }
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
      {
       for(int d = 0; d < numDia__; d++)
        { 
         fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
         for(int q = 1; q < numPerDia__-1; q++)
           fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
         fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
        }
       fprintf(f,"\n"); 
      }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++)
    {
     for(int p = 0; p < numPerTot__; p++)
      {
       for(int c = 0; c < numDis__; c++)
         if(vetAux[c])
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
             fprintf(f,"+ x_%d_%d ",c,p);
       fprintf(f,"<= 1\n");
      }
     fprintf(f,"\n");
    }
   fprintf(f,"\nBOUNDS\n");
   fprintf(f,"\n\\Variaveis w\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
   fprintf(f,"\n\\Variaveis r\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis v\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
   fprintf(f,"\n\\Variaveis y\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
   fprintf(f,"\n\\Variaveis z\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);

   fprintf(f,"\n\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nBINARIES\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"r_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     if(mr__.vetSpTur_[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"x_%d_%d\n",c,p);
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetAux[c])
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"y_%d_%d_%d\n",s,c,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetAux[c])
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
   fprintf(f,"\nEND");
   fclose(f);
  }
 hf = time(NULL);
 mr__.temSub_ = difftime(hf,hi);
}
//------------------------------------------------------------------------------
void monSpsDis(){
 FILE *f;
 time_t hi, hf;
 char arq[50];
 int aux,flag,sts;
 hi = time(NULL);
   
  for(int sp = 0; sp < mr__.numSps_; sp++){
   // ------------------   
   sprintf(arq,"sp%d.lp",sp+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp){
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ 0 x_%d_%d ",c,p);
       fprintf(f,"\n"); 
     }
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int s = 0; s < (numSalDif__-1); s++){
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"+ %d y_%d_%d_%d ",MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
     fprintf(f,"\n"); 
   }            
   fprintf(f,"\n\\Dias minimos\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp) 
       fprintf(f,"+ 5 w_%d ",c); 
   fprintf(f,"\n\n\\Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp) {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ 2 v_%d_%d ",u,p); 
       fprintf(f,"\n"); 	
     }
   }
   fprintf(f,"\n\n\\Variaveis Auxiliares\n");
   for(int c = 0; c < numDis__; c++){
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"+ 0 z_%d_%d ",c,d);
     fprintf(f,"\n");
   }
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp) {
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ 0 r_%d_%d ",u,p); 
       fprintf(f,"\n"); 	
     }
   }
   fprintf(f,"\n\n\\Variaveis de Copia\n");
   for(int u = 0; u < numTur__; u++){
     if ((vetAux[u] != sp) && (vetTurDis[u] == 1)){
        aux = 0;
        for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)
            aux++;
        if (aux != 0){
          for(int p = 0; p < numPerTot__; p++)
            fprintf(f,"+ 0 v_%d_%d_c ",u,p); 
          fprintf(f,"\n");
        } 	
     }
   }
   for(int u = 0; u < numTur__; u++){
     if ((vetAux[u] != sp) && (vetTurDis[u] == 1)){
        aux = 0;
        for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)
            aux++;
        if (aux != 0){
          for(int p = 0; p < numPerTot__; p++)
            fprintf(f,"+ 0 r_%d_%d_c ",u,p); 
          fprintf(f,"\n");
        } 	
     }
   }

   // ------------------ restri��es
   fprintf(f,"\n\nST\n");
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++){
     if(mr__.vetSpDis_[c] == sp){
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d ",c,p); 
       fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
     } 
   }
   fprintf(f,"\n\\R2 - Numero de salas\n");
   for(int p = 0; p < numPerTot__; p++){
     for(int c = 0; c < numDis__; c++){
       if(mr__.vetSpDis_[c] == sp)         
         if(!matResDisPer__[c][p])
           fprintf(f,"+ x_%d_%d ",c,p); 
     }
     fprintf(f,"<= %d\n",numSal__); 
   }
   fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
   for(int s = 0; s < (numSalDif__-1); s++){
     for(int c = 0; c < numDis__; c++){
       if(mr__.vetSpDis_[c] == sp)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
     }
     fprintf(f,"\n"); 
   }
   fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
   for(int s = 0; s < (numSalDif__-1); s++){
     aux = 0;
     for(int i = 0; i < numSal__; i++)
       if(vetSalas__[i].capacidade_ > vetTamSal__[s])
         aux++;
     for(int p = 0; p < numPerTot__; p++){
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
             fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
       fprintf(f,"<= %d\n",aux); 
     } 
     fprintf(f,"\n"); 
   }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++){
         for(int q = 0; q < numPerDia__; q++)
           if(!matResDisPer__[c][(d*numPerDia__)+q])
             fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
         fprintf(f,"- z_%d_%d >= 0\n",c,d);
       }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp){
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"+ z_%d_%d ",c,d);
       fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
     }  
   fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp) {
       for(int p = 0; p < numPerTot__; p++){
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if((!matResDisPer__[vetTurmas__[u].vetDis_[k]][p]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)) 
             fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
         fprintf(f,"- r_%d_%d = 0\n",u,p);
       }
       fprintf(f,"\n"); 
     }else if(vetTurDis[u] == 1) {
        aux = 0;
        for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)
            aux++;
        if (aux != 0){		  
          for(int p = 0; p < numPerTot__; p++){
			flag = 0;
		    for(int k = 0; k < vetTurmas__[u].numDis_; k++)
		      if((!matResDisPer__[vetTurmas__[u].vetDis_[k]][p]) && (mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)){ 
		        fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
		        flag++;
		      }
		    if(flag != 0)
		      fprintf(f,"- r_%d_%d_c = 0\n",u,p);
          }
          fprintf(f,"\n"); 
        }
     } 
   }
   fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp) {
       for(int d = 0; d < numDia__; d++){ 
         fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
         for(int q = 1; q < numPerDia__-1; q++)
           fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
         fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
       }
       fprintf(f,"\n");
     }else if(vetTurDis[u] == 1) {
        aux = 0;
        for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)
            aux++;
        if (aux != 0){
          for(int d = 0; d < numDia__; d++){ 
            fprintf(f,"+ r_%d_%d_c - r_%d_%d_c - v_%d_%d_c <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
            for(int q = 1; q < numPerDia__-1; q++)
              fprintf(f,"- r_%d_%d_c + r_%d_%d_c - r_%d_%d_c - v_%d_%d_c <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
            fprintf(f,"- r_%d_%d_c + r_%d_%d_c - v_%d_%d_c <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
          }	
          fprintf(f,"\n");
	    }
     } 
   }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++){
     for(int p = 0; p < numPerTot__; p++){
       for(int c = 0; c < numDis__; c++)
         if(mr__.vetSpDis_[c] == sp)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
             fprintf(f,"+ x_%d_%d ",c,p);
       fprintf(f,"<= 1\n");
     }
     fprintf(f,"\n");
   }
   fprintf(f,"\nBOUNDS\n");
   fprintf(f,"\n\\Variaveis w\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
   fprintf(f,"\n\\Variaveis r\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp){
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
     }else if(vetTurDis[u] == 1){
		aux = 0;
        for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)
            aux++;
        if (aux != 0){
	      for(int p = 0; p < numPerTot__; p++)
            fprintf(f,"0 <= r_%d_%d_c <= 1\n",u,p);
	    }
     }
   }
   fprintf(f,"\n\\Variaveis v\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp){
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
     }else if(vetTurDis[u] == 1){
		aux = 0;
        for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)
            aux++;
        if (aux != 0){
	      for(int p = 0; p < numPerTot__; p++)
            fprintf(f,"0 <= v_%d_%d_c <= 1\n",u,p);
	    }
     }
   }
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
   fprintf(f,"\n\\Variaveis y\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
   fprintf(f,"\n\\Variaveis z\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);  
   fprintf(f,"\n\nGENERALS\n\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nBINARIES\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp){
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"r_%d_%d\n",u,p);
     }else if(vetTurDis[u] == 1){
		aux = 0;
        for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)
            aux++;
        if (aux != 0){
	      for(int p = 0; p < numPerTot__; p++)
            fprintf(f,"r_%d_%d_c\n",u,p);
	    }
     }
   }
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp){
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
     }else if(vetTurDis[u] == 1){
		aux = 0;
        for(int k = 0; k < vetTurmas__[u].numDis_; k++)
          if(mr__.vetSpDis_[vetTurmas__[u].vetDis_[k]] == sp)
            aux++;
        if (aux != 0){
	      for(int p = 0; p < numPerTot__; p++)
            fprintf(f,"v_%d_%d_c\n",u,p);
	    }
     }
   }
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"x_%d_%d\n",c,p);
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(mr__.vetSpDis_[c] == sp)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"y_%d_%d_%d\n",s,c,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(mr__.vetSpDis_[c] == sp)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
         
   fprintf(f,"\nEND");
   fclose(f);
   
   // Cria subproblemas no CPX
   vetCpx__[sp].lp_ = CPXcreateprob(env__,&sts,"");
   sts = CPXreadcopyprob(env__,vetCpx__[sp].lp_,arq,NULL);
 }
  
 hf = time(NULL);
 mr__.temSub_ = difftime(hf,hi);
}
//------------------------------------------------------------------------------
void parGraTur(const int &sps,const int &fat,const int &pes){
 time_t hi, hf;
 int aux,flag;
 int areCor,pesAre;
 int matAdj[MAX_TUR][MAX_TUR];
 hi = time(NULL);
 mr__.numSps_ = MIN(sps,numTur__);
 mr__.fatDes_ = fat;
 memset(mr__.vetSpTur_,-1,sizeof(mr__.vetSpTur_));
 memset(matAdj,0,sizeof(matAdj));
 for(int i = 0; i < numTur__; i++)
   for(int j = 0; j < numTur__; j++)
     if(j != i)
       for(int k = 0; k < numDis__; k++)
         if(matDisTur__[k][i] && matDisTur__[k][j])
           matAdj[i][j]++;
 //-------------------------
 // todas as turmas em um �nico subproblema
 if(mr__.numSps_ <= 1)
   memset(mr__.vetSpTur_,0,sizeof(mr__.vetSpTur_));
 //-------------------------
 // cada turma em um subproblema
 else if(mr__.numSps_ == numTur__)
  {
   for(int i = 0; i < numTur__; i++)
     mr__.vetSpTur_[i] = i;
  }
 //-------------------------
 // particionamento usando a METIS
 else
  {
   int pos,are;
   idx_t *vetPrt;    // vetor de identifica��o das parti��es
   idx_t *vetNumAdj; // vetor com o n�mero de adjac�ncias de cada turma
   idx_t *vetTurAdj; // vetor com as turmas adjacentes de cada turma
   idx_t *vetPesAre; // vetor com o peso das arestas
   int wgtFlag = 0;  // flag para indicar a utilizacao ou nao de pesos (0: sem pesos)
   int numFlag = 0;  // flag para indicar o estilo dos contadores (comeca em 0: 0)
   idx_t *opts = new idx_t[METIS_NOPTIONS];
   METIS_SetDefaultOptions(opts);
   opts[METIS_OPTION_UFACTOR] = mr__.fatDes_;
   idx_t *nCon = new idx_t[1];
   nCon[0] = 1;
   vetPrt = new idx_t[numTur__];
   vetNumAdj = new idx_t[numTur__+1];
   vetTurAdj = new idx_t[numTur__*numTur__];
   vetPesAre = new idx_t[numTur__*numTur__];
   pos = 0;
   vetNumAdj[0] = 0;
   for(int i = 0; i < numTur__; i++)
    {
     are = 0;
     for(int j = 0; j < numTur__; j++)
      {
       if(j != i)
        {
         if(matAdj[i][j] > 0)
          {
           vetTurAdj[pos] = j;
           vetPesAre[pos] = matAdj[i][j];
           pos++;
           are++;
          }
        }
      }
     vetNumAdj[i+1] = vetNumAdj[i] + are;
    } 
   idx_t aux1 = numTur__;
   idx_t aux2 = mr__.numSps_;
   idx_t aux3; 
   if(pes)
     METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetTurAdj,NULL,NULL,vetPesAre,&aux2,NULL,NULL,opts,&aux3,vetPrt);
   else
     METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetTurAdj,NULL,NULL,NULL,&aux2,NULL,NULL,opts,&aux3,vetPrt);
   mr__.numCtsM_ = aux3;
   for(int i = 0; i < numTur__; i++)
     mr__.vetSpTur_[i] = vetPrt[i];
   delete[] vetPrt;
   delete[] vetNumAdj;
   delete[] vetTurAdj;
   delete[] vetPesAre;
   delete[] opts;
   delete[] nCon;
  }
 mr__.numCts_ = mr__.pesCts_ = 0;
 if(mr__.numSps_ > 1)
   for(int i = 0; i < (numTur__-1); i++)
     for(int j = i+1; j < numTur__; j++)
       if(mr__.vetSpTur_[j] != mr__.vetSpTur_[i])
        {
         mr__.numCts_ += MIN(1,matAdj[i][j]);
         mr__.pesCts_ += matAdj[i][j];
        } 
 hf = time(NULL);
 mr__.temPrt_ = difftime(hf,hi);
}
//------------------------------------------------------------------------------
void parGraDis(const int &sps,const int &fat,const int &pes){
 time_t hi, hf;
 int aux,flag;
 int areCor,pesAre;
 int matAdj[MAX_DIS][MAX_DIS];
 hi = time(NULL);
 mr__.numSps_ = MIN(sps,numDis__);
 mr__.fatDes_ = fat;
 memset(mr__.vetSpDis_,-1,sizeof(mr__.vetSpDis_));
 memset(matAdj,0,sizeof(matAdj));
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numDis__; j++)
     if(j != i)
       for(int k = 0; k < numTur__; k++)
         if(matDisTur__[i][k] && matDisTur__[j][k])
           matAdj[i][j]++;
 //-------------------------
 // todas as disciplinas em um �nico subproblema
 if(mr__.numSps_ <= 1)
   memset(mr__.vetSpDis_,0,sizeof(mr__.vetSpDis_));
 //-------------------------
 // cada disciplina em um subproblema
 else if(mr__.numSps_ == numDis__){
   for(int i = 0; i < numDis__; i++)
     mr__.vetSpDis_[i] = i;
  }
 //-------------------------
 // particionamento usando a METIS
 else{
   int pos,are;
   idx_t *vetPrt;    // vetor de identifica��o das parti��es
   idx_t *vetNumAdj; // vetor com o n�mero de adjac�ncias de cada disciplina
   idx_t *vetDisAdj; // vetor com as disciplinas adjacentes de cada disciplina
   idx_t *vetPesAre; // vetor com o peso das arestas
   int wgtFlag = 0;  // flag para indicar a utilizacao ou nao de pesos (0: sem pesos)
   int numFlag = 0;  // flag para indicar o estilo dos contadores (comeca em 0: 0)
   idx_t *opts = new idx_t[METIS_NOPTIONS];
   METIS_SetDefaultOptions(opts);
   opts[METIS_OPTION_UFACTOR] = mr__.fatDes_;
   idx_t *nCon = new idx_t[1];
   nCon[0] = 1;
   vetPrt = new idx_t[numDis__];
   vetNumAdj = new idx_t[numDis__+1];
   vetDisAdj = new idx_t[numDis__*numDis__];
   vetPesAre = new idx_t[numDis__*numDis__];
   pos = 0;
   vetNumAdj[0] = 0;
   for(int i = 0; i < numDis__; i++)
    {
     are = 0;
     for(int j = 0; j < numDis__; j++)
      {
       if(j != i)
        {
         if(matAdj[i][j] > 0)
          {
           vetDisAdj[pos] = j;
           vetPesAre[pos] = matAdj[i][j];
           pos++;
           are++;
          }
        }
      }
     vetNumAdj[i+1] = vetNumAdj[i] + are;
    } 
   idx_t aux1 = numDis__;
   idx_t aux2 = mr__.numSps_;
   idx_t aux3; 
   if(pes)
      METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetDisAdj,NULL,NULL,vetPesAre,&aux2,NULL,NULL,opts,&aux3,vetPrt);   
   else
      METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetDisAdj,NULL,NULL,NULL,&aux2,NULL,NULL,opts,&aux3,vetPrt);
   mr__.numCtsM_ = aux3;
   for(int i = 0; i < numDis__; i++)
     mr__.vetSpDis_[i] = vetPrt[i];
   delete[] vetPrt;
   delete[] vetNumAdj;
   delete[] vetDisAdj;
   delete[] vetPesAre;
   delete[] opts;
   delete[] nCon;
 }
 mr__.numCts_ = mr__.pesCts_ = 0;
 if(mr__.numSps_ > 1)
   for(int i = 0; i < (numDis__-1); i++)
     for(int j = i+1; j < numDis__; j++)
       if(mr__.vetSpDis_[j] != mr__.vetSpDis_[i]){
         mr__.numCts_ += MIN(1,matAdj[i][j]);
         mr__.pesCts_ += matAdj[i][j];
       } 
                  
 hf = time(NULL);
 mr__.temPrt_ = difftime(hf,hi);
 
}
//------------------------------------------------------------------------------
void ConstroiCoefOrig(){
	
         
 int val, cont;

 cont = 0;
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p]){
       matCoefOrig__[cont] = 0;
       cont++;
     }
 
 for(int s = 0; s < (numSalDif__-1); s++)
   for(int c = 0; c < numDis__; c++)
    for(int p = 0; p < numPerTot__; p++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
         matCoefOrig__[cont] = MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]);
         cont++;
       }

 for(int c = 0; c < numDis__; c++){
   matCoefOrig__[cont] = 5;
   cont++;
 }
 
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++){
     matCoefOrig__[cont] = 2;
     cont++;
   }
   
 for(int c = 0; c < numDis__; c++)
   for(int d = 0; d < numDia__; d++){
     matCoefOrig__[cont] = 0;
     cont++;
   }
   
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++){
     matCoefOrig__[cont] = 0;
     cont++; 
   }
   
  for(int i = 0; i < numVarCopy__; i++){
     matCoefOrig__[cont] = 0;
     cont++;
  }
    
}
//------------------------------------------------------------------------------
void CalculaNovosCoef(){

   int contMATVAL;
   double S;

	contMATVAL = 0;
    for(int j = 0; j < (numVar__+numVarCopy__); j++){
		vecDualD__[j] = 0;
	}
	for (int i = 0; i < numRss__; i++) {
		for (int j = 0; j < MAT_D_CNT__[i]; j++) {
			vecDualD__[MAT_D_IND__[contMATVAL]] += VecDual__[i] * mat_Rss_Var__[i][MAT_D_IND__[contMATVAL]] * MAT_D_VAL__[contMATVAL];	
			contMATVAL++;
		}
	}
    for(int j = 0; j < (numVar__+numVarCopy__); j++){
	   matNovosCoef__[j] =	matCoefOrig__[j] - vecDualD__[j];
	}
	
	
	
}
//------------------------------------------------------------------------------
void MontaMatrizesIndices(){
	
	indVarX__ = (int**) malloc(numDis__ * sizeof (int*));
	indVarY__ = (int***) malloc((numSalDif__-1) * sizeof (int**));
	indVarW__ = (int*) malloc(numDis__ * sizeof (int*));
	indVarV__ = (int**) malloc(numTur__ * sizeof (int*));
	indVarZ__ = (int**) malloc(numDis__ * sizeof (int*));
	indVarr__ = (int**) malloc(numTur__ * sizeof (int*));

    int i, j, k, cont;

    cont = 0;
//////////VARIAVEIS X
    for (i = 0; i < numDis__; i++) {
        indVarX__[i] = (int*) malloc(numPerTot__ * sizeof (int*));
    }

   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p]){
		 indVarX__[c][p] = cont;
		 cont++;
       }
    
//////////VARIAVEIS Y
    for (i = 0; i < (numSalDif__-1); i++) {
        indVarY__[i] = (int**) malloc(numDis__ * sizeof (int*));
        for (j = 0; j < numDis__; j++) {
            indVarY__[i][j] = (int*) malloc(numPerTot__ * sizeof (int));
        }
    }

    for(int s = 0; s < (numSalDif__-1); s++)
      for(int c = 0; c < numDis__; c++)
        for(int p = 0; p < numPerTot__; p++)
          if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
            indVarY__[s][c][p] = cont;
		    cont++;
          }

//////////VARIAVEIS W
    for (i = 0; i < numDis__; i++) {
	     indVarW__[i] = cont;
	     cont++;
    }

//////////VARIAVEIS V
    for (i = 0; i < numTur__; i++) {
        indVarV__[i] = (int*) malloc(numPerTot__ * sizeof (int));
    }
   
	for(int u = 0; u < numTur__; u++)
	  for(int p = 0; p < numPerTot__; p++){
			indVarV__[u][p] = cont;
			cont++; 
	  }

//////////VARIAVEIS Z
    for (i = 0; i < numDis__; i++) {
        indVarZ__[i] = (int*) malloc(numDia__ * sizeof (int));
    }
   
	 for(int c = 0; c < numDis__; c++)
	   for(int d = 0; d < numDia__; d++){
			indVarZ__[c][d] = cont;
			cont++; 
	   }
    
//////////VARIAVEIS r
    for (i = 0; i < numTur__; i++) {
        indVarr__[i] = (int*) malloc(numPerTot__ * sizeof (int));
    }
   
	for(int u = 0; u < numTur__; u++)
	  for(int p = 0; p < numPerTot__; p++){
			indVarr__[u][p] = cont;
			cont++; 
	  }
}
//------------------------------------------------------------------------------
void ConstroiMatrizD(){

    int val;
	numRss__ = 0;
	
	// R2 - Aulas na mesma sala no mesmo periodo
	for(int p = 0; p < numPerTot__; p++){
	   for(int c = 0; c < numDis__; c++)
		 if(!matResDisPer__[c][p])
		   matD__[numRss__][indVarX__[c][p]] = 1;
	   matE__[numRss__] = numSal__;
	   matSNS_DE__[numRss__] = 'L';
	   numRss__++;
	}
	// R4 - Capacidade das salas 2
	for(int s = 0; s < (numSalDif__-1); s++){
	   val = 0;
	   for(int i = 0; i < numSal__; i++)
		 if(vetSalas__[i].capacidade_ > vetTamSal__[s])
		   val++;
	   for(int p = 0; p < numPerTot__; p++){
		 for(int c = 0; c < numDis__; c++)
		   if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
			 matD__[numRss__][indVarX__[c][p]] = 1;
			 matD__[numRss__][indVarY__[s][c][p]] = -1;
		   }
	     matE__[numRss__] = val;
	     matSNS_DE__[numRss__] = 'L';
	     numRss__++;
	   } 
	}
   //R7 - Aulas isoladas 1;
   for(int u = 0; u < numTur__; u++)
     if(vetTurDis[u] == 1)
       for(int p = 0; p < numPerTot__; p++){
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             matD__[numRss__][indVarX__[vetTurmas__[u].vetDis_[k]][p]] = 1;
         matD__[numRss__][indVarr__[u][p]] = -1;
 	     matE__[numRss__] = 0;
	     matSNS_DE__[numRss__] = 'E';
	     numRss__++;
       }
   // R8 - Aulas isoladas 2
   /*for(int u = 0; u < numTur__; u++)
     if(vetTurDis[u] == 1)
       for(int d = 0; d < numDia__; d++){
		 matD__[numRss__][indVarr__[u][d*numPerDia__]] = 1;
		 matD__[numRss__][indVarr__[u][(d*numPerDia__)+1]] = -1;
		 matD__[numRss__][indVarV__[u][d*numPerDia__]] = -1;
 	     matE__[numRss__] = 0;
	     matSNS_DE__[numRss__] = 'L';
	     numRss__++;
         for(int q = 1; q < numPerDia__-1; q++){
		   matD__[numRss__][indVarr__[u][(d*numPerDia__)+q-1]] = -1;
		   matD__[numRss__][indVarr__[u][(d*numPerDia__)+q]] = 1;
		   matD__[numRss__][indVarr__[u][(d*numPerDia__)+q+1]] = -1;
		   matD__[numRss__][indVarV__[u][(d*numPerDia__)+q]] = -1;
 	       matE__[numRss__] = 0;
	       matSNS_DE__[numRss__] = 'L';
	       numRss__++;
         }
		 matD__[numRss__][indVarr__[u][(d*numPerDia__)+numPerDia__-2]] = -1;
		 matD__[numRss__][indVarr__[u][(d*numPerDia__)+numPerDia__-1]] = 1;
		 matD__[numRss__][indVarV__[u][(d*numPerDia__)+numPerDia__-1]] = -1;
 	     matE__[numRss__] = 0;
	     matSNS_DE__[numRss__] = 'L';
	     numRss__++;
       }*/
       
   // R9 - Aulas de um professor no mesmo periodo
   for(int t = 0; t < numPro__; t++)
     if (vetProfDis[t] == 1)
       for(int p = 0; p < numPerTot__; p++){
         for(int c = 0; c < numDis__; c++)
             if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
               matD__[numRss__][indVarX__[c][p]] = 1;
	     matE__[numRss__] = 1;
	     matSNS_DE__[numRss__] = 'L';
	     numRss__++;
       }
       
   // Restri��es de Igualdade das Vari�veis de c�pia
   for(int i = 0; i < numVarCopy__; i++){
	   matD__[numRss__][vetVarOrigXCopy[i][0]] = 1;
	   matD__[numRss__][vetVarOrigXCopy[i][1]] = -1;
	   matE__[numRss__] = 0;
	   matSNS_DE__[numRss__] = 'E';
 	   numRss__++;  
   }   
}
//------------------------------------------------------------------------------
void ConstroiMatrizRssVar(){

    int val;
	numRss__ = 0;
	
	// R2 - Aulas na mesma sala no mesmo periodo
	for(int p = 0; p < numPerTot__; p++){
	   for(int c = 0; c < numDis__; c++)
		 if(!matResDisPer__[c][p])
		   mat_Rss_Var__[numRss__][indVarX__[c][p]] = 1;
	   numRss__++;
	}
	// R4 - Capacidade das salas 2
	for(int s = 0; s < (numSalDif__-1); s++){
	   val = 0;
	   for(int i = 0; i < numSal__; i++)
		 if(vetSalas__[i].capacidade_ > vetTamSal__[s])
		   val++;
	   for(int p = 0; p < numPerTot__; p++){
		 for(int c = 0; c < numDis__; c++)
		   if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
			 mat_Rss_Var__[numRss__][indVarX__[c][p]] = 1;
			 mat_Rss_Var__[numRss__][indVarY__[s][c][p]] = 1;
		   }
	     numRss__++;
	   } 
	}
   //R7 - Aulas isoladas 1;
   for(int u = 0; u < numTur__; u++)
     if(vetTurDis[u] == 1)
       for(int p = 0; p < numPerTot__; p++){
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             mat_Rss_Var__[numRss__][indVarX__[vetTurmas__[u].vetDis_[k]][p]] = 1;
         mat_Rss_Var__[numRss__][indVarr__[u][p]] = 1;
	     numRss__++;
       }
   // R8 - Aulas isoladas 2
   /*for(int u = 0; u < numTur__; u++)
     if(vetTurDis[u] == 1)
       for(int d = 0; d < numDia__; d++){
		 mat_Rss_Var__[numRss__][indVarr__[u][d*numPerDia__]] = 1;
		 mat_Rss_Var__[numRss__][indVarr__[u][(d*numPerDia__)+1]] = 1;
		 mat_Rss_Var__[numRss__][indVarV__[u][d*numPerDia__]] = 1;
	     numRss__++;
         for(int q = 1; q < numPerDia__-1; q++){
		   mat_Rss_Var__[numRss__][indVarr__[u][(d*numPerDia__)+q-1]] = 1;
		   mat_Rss_Var__[numRss__][indVarr__[u][(d*numPerDia__)+q]] = 1;
		   mat_Rss_Var__[numRss__][indVarr__[u][(d*numPerDia__)+q+1]] = 1;
		   mat_Rss_Var__[numRss__][indVarV__[u][(d*numPerDia__)+q]] = 1;
	       numRss__++;
         }
		 mat_Rss_Var__[numRss__][indVarr__[u][(d*numPerDia__)+numPerDia__-2]] = 1;
		 mat_Rss_Var__[numRss__][indVarr__[u][(d*numPerDia__)+numPerDia__-1]] = 1;
		 mat_Rss_Var__[numRss__][indVarV__[u][(d*numPerDia__)+numPerDia__-1]] = 1;
	     numRss__++;
       }*/
       
   // R9 - Aulas de um professor no mesmo periodo
   for(int t = 0; t < numPro__; t++)
     if (vetProfDis[t] == 1)
       for(int p = 0; p < numPerTot__; p++){
         for(int c = 0; c < numDis__; c++)
             if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
               mat_Rss_Var__[numRss__][indVarX__[c][p]] = 1;
	     numRss__++;
       } 
       
   // Restri��es de Igualdade das Vari�veis de c�pia
   for(int i = 0; i < numVarCopy__; i++){
	   mat_Rss_Var__[numRss__][vetVarOrigXCopy[i][0]] = 1;
	   mat_Rss_Var__[numRss__][vetVarOrigXCopy[i][1]] = 1;
 	   numRss__++;  
   } 
         
}
//------------------------------------------------------------------------------
void ConstroiMatrizColFinal(){
	
	double valfo;
	
	matFO__ = (double*)malloc((numCol__*mr__.numSps_) * sizeof(double));
	
	matColFinal__ = (int**)malloc((numVar__+numVarCopy__) * sizeof(int*));
	for (int i = 0; i < (numVar__+numVarCopy__); i++) {
		matColFinal__[i] = (int*)malloc(MAX_COL * sizeof(int));
	}
	
	for (int i = 0; i < mr__.numSps_; i++)
		for (int j = 0; j < numCol__; j++){
			// Calcula FO da coluna j no subproblema i
			valfo = 0;
			for (int k = 0; k < numVar__; k++)
				if (varSPS__[k] == i)
				   valfo += (matCoefOrig__[k] * matColInicial__[k][j]);
			matFO__[i+(j*mr__.numSps_)] = valfo;
			
			for (int k = 0; k < numVar__; k++){
				if (varSPS__[k] == i)
				   matColFinal__[k][i+(j*mr__.numSps_)] = matColInicial__[k][j];
				else
				   matColFinal__[k][i+(j*mr__.numSps_)] = 0;
			}
			for (int k = 0; k < numVarCopy__; k++){
				if (varSPS__[vetVarOrigXCopy[k][1]] == i)
				   matColFinal__[vetVarOrigXCopy[k][1]][i+(j*mr__.numSps_)] = matColInicial__[vetVarOrigXCopy[k][0]][j];
				else
				   matColFinal__[vetVarOrigXCopy[k][1]][i+(j*mr__.numSps_)] = 0;
			}
		}
		
	numColFinal__ = numCol__ * 	mr__.numSps_;
}
//------------------------------------------------------------------------------
void ConstroiMatrizLambdas(){

	int S, contMATVAL, contMATBEG, contMATCNT;
	
	//-----------------------------------------------------
	MAT_D_VAL__ = (int*)malloc(numRss__*(numVar__+numVarCopy__) * sizeof(int));
	MAT_D_IND__ = (int*)malloc(numRss__*(numVar__+numVarCopy__) * sizeof(int));	
	MAT_D_CNT__ = (int*)malloc(numRss__ * sizeof(int));
	
	matLambdas__ = (int**)malloc((numRss__+mr__.numSps_) * sizeof(int*));
	for (int i = 0; i < (numRss__+mr__.numSps_); i++) {
		matLambdas__[i] = (int*)malloc((numCol__*mr__.numSps_) * sizeof(int));
		for (int j = 0; j < (numCol__*mr__.numSps_); j++)
		   matLambdas__[i][j] = 0;
	}
	
	contMATVAL = 0;
	for (int i = 0; i < numRss__; i++) {
		contMATCNT = 0;
		for (int j = 0; j < (numVar__+numVarCopy__); j++) {	
			if(matD__[i][j] != 0){
				MAT_D_VAL__[contMATVAL] = matD__[i][j];
				MAT_D_IND__[contMATVAL] = j;
				contMATVAL++;
				contMATCNT++;
			}			
		}
		MAT_D_CNT__[i] = contMATCNT;
	}

	for (int j = 0; j < (numCol__*mr__.numSps_); j++) {
		contMATVAL = 0;
		for (int i = 0; i < numRss__; i++) {
			S = 0;
			for (int k = 0; k < MAT_D_CNT__[i]; k++) {
				S += (MAT_D_VAL__[contMATVAL] * matColFinal__[MAT_D_IND__[contMATVAL]][j]);
				contMATVAL++;
			}
			matLambdas__[i][j] = S;
		}
	}
	
	for (int i = 0; i < mr__.numSps_; i++){
		for (int j = 0; j < numCol__; j++)
			matLambdas__[i+numRss__][i+(j*mr__.numSps_)] = 1;
		matE__[i+numRss__] = 1;
	    matSNS_DE__[i+numRss__] = 'E';
	}
	
	/*********PREPARA MATRIZES PARA CPLEX****************/
	MATVAL__ = (double*)malloc((((numRss__+mr__.numSps_) * (numCol__*mr__.numSps_))/2) * sizeof(double));
	MATIND__ = (int*)malloc((((numRss__+mr__.numSps_) * (numCol__*mr__.numSps_))/2) * sizeof(int));
	MATBEG__ = (int*)malloc((numCol__*mr__.numSps_) * sizeof(int));
	MATCNT__ = (int*)malloc((numCol__*mr__.numSps_) * sizeof(int));
	lbVec__ = (double*)malloc((numCol__*mr__.numSps_) * sizeof(double));
	ubVec__ = (double*)malloc((numCol__*mr__.numSps_) * sizeof(double));

	contMATVAL = 0;
	contMATBEG = 0;
	for (int j = 0; j < (numCol__*mr__.numSps_); j++) {
		MATBEG__[j] = contMATBEG;
		contMATCNT = 0;
		for (int i = 0; i < (numRss__+mr__.numSps_); i++) {	
			if(matLambdas__[i][j] != 0){
				MATVAL__[contMATVAL] = matLambdas__[i][j];
				MATIND__[contMATVAL] = i;
				contMATVAL++;
				contMATCNT++;
			}			
		}
		contMATBEG = contMATVAL;
		MATCNT__[j] = contMATCNT;
		lbVec__[j] = 0;
		ubVec__[j] = 1; //CPX_INFBOUND;
	}
	/*********************************************/
	
}
//------------------------------------------------------------------------------
void alteraCoefSubProb(){
	int cont = 0, sts;
	double *VecNewCoefSub;
	
	VecNewCoefSub = (double*)malloc((numVar__+numVarCopy__) * sizeof(double));
	for(int i = 0; i < mr__.numSps_; i++){
		for(int j = 0; j < vetQntVarSPS[i]; j++){
			VecNewCoefSub[j] = matNovosCoef__[vetVarSPSSeq[cont]];
			cont++;
	    } 
	    sts = CPXchgobj(env__,vetCpx__[i].lp_,vetQntVarSPS[i],indices,VecNewCoefSub);
    }

    free(VecNewCoefSub); 	
}
//------------------------------------------------------------------------------
void monColX(int sp){
	
	CPXgetmipx(env__,vetCpx__[sp].lp_,VecSPx__,0,vetQntVarSPS[sp]-1);
	for (int i = 0; i < vetQntVarSPS[sp]; i++){
		VecSPx__[i] = defineX(VecSPx__[i]);
	}
		
	for (int i = 0; i < (numVar__+numVarCopy__); i++)
	  VecNewX__[i] = 0;
	
	int inicio = 0;
	for (int i = 0; i < sp; i++)
	   inicio += vetQntVarSPS[i];
	
	int cont = 0;
	for (int i = inicio; i < (inicio+vetQntVarSPS[sp]); i++){
        VecNewX__[vetVarSPSSeq[i]] = VecSPx__[cont];
        cont++;
    }
       
}
//------------------------------------------------------------------------------
void addNewCol(int sp){
		
	int contMATVAL = 0;
	for (int i = 0; i < numRss__; i++) {
		VecNewCol__[i] = 0;
		for (int j = 0; j < MAT_D_CNT__[i]; j++) {
			VecNewCol__[i] += (MAT_D_VAL__[contMATVAL] * VecNewX__[MAT_D_IND__[contMATVAL]]);	
			contMATVAL++;
		}
	}
	for (int i = 0; i < mr__.numSps_; i++)
		VecNewCol__[numRss__+i] = 0;
		
	VecNewCol__[numRss__+sp] = 1;
	
	coladd_FO[0] = 0;
	for(int j = 0; j < (numVar__+numVarCopy__); j++){
		coladd_FO[0] += (matCoefOrig__[j]*VecNewX__[j]);
		matColFinal__[j][numColFinal__] = VecNewX__[j];
	}

	CPXaddcols(env__, pmr.lp_, 1, numRss__+mr__.numSps_, coladd_FO, coladd_BEG, coladd_IND, VecNewCol__, coladd_lb, coladd_ub, NULL);
	numColFinal__++;
}
//==============================================================================


//=================================== CPLEX ====================================
//------------------------------------------------------------------------------
void execCpx_PMR(){
 int sts;
 clock_t h; 

 h = clock();
 sts = CPXprimopt(env__,pmr.lp_);
 h = clock() - h;

 pmr.tempo_ = (double)h/CLOCKS_PER_SEC;
 sts = CPXgetpi(env__,pmr.lp_,VecDual__,0,CPXgetnumrows(env__,pmr.lp_)-1);
 sts = CPXgetobjval(env__,pmr.lp_,&pmr.limInf_);
}
//------------------------------------------------------------------------------
void exeCpx(Solucao &s,char *arq,const int &rl){
 int sts;
 time_t hi, hf;
 #ifdef DBG
   CPXsetintparam(env__,CPX_PARAM_SCRIND,CPX_ON);
 #endif
 vetCpx__[0].lp_ = CPXcreateprob(env__,&sts,"");
 CPXreadcopyprob(env__,vetCpx__[0].lp_,arq,NULL);
 vetCpx__[0].numVar_ = CPXgetnumcols(env__,vetCpx__[0].lp_);
 vetCpx__[0].numRes_ = CPXgetnumrows(env__,vetCpx__[0].lp_);
 hi = time(NULL);
 if(rl)
   CPXprimopt(env__,vetCpx__[0].lp_);
 else
   CPXmipopt(env__,vetCpx__[0].lp_);
 hf = time(NULL);
 vetCpx__[0].tempo_ = difftime(hf,hi);
 if(rl)
   CPXgetobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limInf_);
 else
  {
   CPXgetmipobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limSup_);
   CPXgetbestobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limInf_);
   vetCpx__[0].gap_ = INT_MAX;
   if(vetCpx__[0].limInf_ != 0)
     vetCpx__[0].gap_ = ((vetCpx__[0].limSup_ - vetCpx__[0].limInf_)/vetCpx__[0].limSup_)*100;
   #ifdef MOD_BUR
     CPXgetmipx(env__,vetCpx__[0].lp_,s.vetSol_,0,(numPerTot__*numSal__*numDis__-1));
   #else
     CPXgetmipx(env__,vetCpx__[0].lp_,s.vetSol_,0,(numPerTot__*numDis__-1));
   #endif
  }
 CPXfreeprob(env__,&vetCpx__[0].lp_);
}
//------------------------------------------------------------------------------
void escResCpx(FILE *f,const int &rl){
 if(f == NULL)
   f = stdout;
 fprintf(f,"\n< ----------------------------------- CPLEX ---------------------------------- >\n");
 #ifdef MOD_BUR
   fprintf(f,"Modelo utilizado...............................................: Burke et al. (2010)\n");
 #else
   fprintf(f,"Modelo utilizado...............................................: Lach e Lubbecke (2012)\n");
 #endif
 fprintf(f,"Numero de variaveis............................................: %d\n",vetCpx__[0].numVar_);
 fprintf(f,"Numero de restricoes...........................................: %d\n",vetCpx__[0].numRes_);
 if(rl)
   fprintf(f,"Valor do melhor no (limitante inferior)........................: %.2f\n",vetCpx__[0].limInf_);
 else
  {
   fprintf(f,"Valor da solucao (limitante superior)..........................: %.2f\n",vetCpx__[0].limSup_);
   fprintf(f,"Valor do melhor no (limitante inferior)........................: %.2f\n",vetCpx__[0].limInf_);
   fprintf(f,"GAP............................................................: %.2f\n",vetCpx__[0].gap_);
  }
 fprintf(f,"Tempo (segundos)...............................................: %.2f\n",vetCpx__[0].tempo_);
}
//------------------------------------------------------------------------------
void monModBurEal10(char *arq,const int &rl){
 int val;
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 fprintf(f,"\n\\Capacidade das salas\n");
 for(int r = 0; r < numSal__; r++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
      if(vetDisciplinas__[c].numAlu_ > vetSalas__[r].capacidade_)
        fprintf(f,"+ %d x_%d_%d_%d ",PESOS[0]*(vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),p,r,c);
      else
        fprintf(f,"+ 0 x_%d_%d_%d ",p,r,c);
     fprintf(f,"\n");
    }
  }
 fprintf(f,"\n\n\\Janelas de horarios\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int s = 0; s < numPerDia__; s++)
        fprintf(f,"+ %d z_%d_%d_%d ",PESOS[1],u,d,s);
     fprintf(f,"\n");
    }
  }
 fprintf(f,"\n\\Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"+ %d q_%d ",PESOS[2],c);
 fprintf(f,"\n\n\\Salas diferentes\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"+ %d y_%d_%d ",PESOS[3],r,c);
   fprintf(f,"\n");
  }
 val = PESOS[3] * numDis__;
 fprintf(f,"- val\n");
 // ------------------ restri��es
 fprintf(f,"\nST\n");
 fprintf(f,"\n\\Valor constante\n");
 fprintf(f,"val = %d\n",val);
 fprintf(f,"\n\\ ------------------------------------ HARD------------------------------------\n");
 fprintf(f,"\n\\R1 - Numero de aulas\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     for(int r = 0; r < numSal__; r++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
    fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_);
  }
 fprintf(f,"\n\\R2 - Aulas na mesma sala no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int r = 0; r < numSal__; r++)
    {
     for(int c = 0; c < numDis__; c++)
      fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R3 - Aulas de uma disciplina no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int c = 0; c < numDis__; c++)
    {
     for(int r = 0; r < numSal__; r++)
      fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R4 - Aulas de um professor no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int t = 0; t < numPro__; t++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
         if(vetDisciplinas__[c].professor_ == t)
           fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R5 - Aulas de uma turma no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int u = 0; u < numTur__; u++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(vetTurmas__[u].vetDis_[k] == c)
            {
             fprintf(f,"+ x_%d_%d_%d ",p,r,c);
             break;
            }
        }
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R6 - Restricoes de oferta (alocacao)\n");
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(matResDisPer__[c][p])
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
       fprintf(f,"= 0\n");
      }
 fprintf(f,"\n\\ ------------------------------------ SOFT------------------------------------\n");
 fprintf(f,"\n\\R7 - Numero de salas usadas por disciplina\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
       fprintf(f,"- v_%d_%d <= 0\n",d,c);
      }
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R8 - Numero de salas usadas por disciplina\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"- v_%d_%d >= 0\n",d,c);
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R9 - Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"+ v_%d_%d ",d,c);
   fprintf(f,"+ q_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
  }
 fprintf(f,"\n\\R10 a R13+#PER_DIA - Janelas no horario das turmas\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int c = 0; c < numDis__; c++)
       if(matDisTur__[c][u] == 1)
        {
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__),r,c,(d*numPerDia__)+1,r,c);
        }
     fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,0);
    }
  }
 fprintf(f,"\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int c = 0; c < numDis__; c++)
       if(matDisTur__[c][u] == 1)
        {
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+numPerDia__-1,r,c,(d*numPerDia__)+numPerDia__-2,r,c);
        }
     fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,1);
    }
  }
 fprintf(f,"\n");
 for(int s = 2; s < numPerDia__; s++)
  {
   for(int u = 0; u < numTur__; u++)
    {
     for(int d = 0; d < numDia__; d++)
      {
       for(int c = 0; c < numDis__; c++)
         if(matDisTur__[c][u] == 1)
          {
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+s-1,r,c,(d*numPerDia__)+s-2,r,c,(d*numPerDia__)+s,r,c);
          }
       fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,s);
      }
    }
  fprintf(f,"\n");
 }
 fprintf(f,"\n\\R14 - Salas utilizadas por disciplina\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int r = 0; r < numSal__; r++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"x_%d_%d_%d - y_%d_%d <= 0\n",p,r,c,r,c);
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R15 - Salas utilizadas por disciplina\n");
 for(int r = 0; r < numSal__; r++)
  {
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"- y_%d_%d >= 0\n",r,c);
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Variaveis x\n");
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerTot__; p++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"0 <= x_%d_%d_%d <= 1\n",p,r,c);
 fprintf(f,"\n\\Variaveis v\n");
 for(int d = 0; d < numDia__; d++)
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"0 <= v_%d_%d <= 1\n",d,c);
 fprintf(f,"\n\\Variaveis q\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"0 <= q_%d <= %d\n",c,numDia__);
 fprintf(f,"\n\\Variaveis z\n");
 for(int u = 0; u < numTur__; u++)
   for(int d = 0; d < numDia__; d++)
     for(int s = 0; s < numPerDia__; s++)
        fprintf(f,"0 <= z_%d_%d_%d <= 1\n",u,d,s);
 fprintf(f,"\n\\Variaveis y\n");
 for(int r = 0; r < numSal__; r++)
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"0 <= y_%d_%d <= 1\n",r,c);
 if(!rl)
  {
   fprintf(f,"\nGENERALS\n");
   fprintf(f,"\n\\Variaveis q\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"q_%d\n",c);
   fprintf(f,"\nBINARIES\n");
   for(int r = 0; r < numSal__; r++)
     for(int p = 0; p < numPerTot__; p++)
       for(int c = 0; c < numDis__; c++)
         fprintf(f,"x_%d_%d_%d\n",p,r,c);
   fprintf(f,"\n\\Variaveis v\n");
   for(int d = 0; d < numDia__; d++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"v_%d_%d\n",d,c);
   fprintf(f,"\n\\Variaveis z\n");
   for(int u = 0; u < numTur__; u++)
     for(int d = 0; d < numDia__; d++)
       for(int s = 0; s < numPerDia__; s++)
          fprintf(f,"z_%d_%d_%d\n",u,d,s);
   fprintf(f,"\n\\Variaveis y\n");
   for(int r = 0; r < numSal__; r++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"y_%d_%d\n",r,c);
  }
 fprintf(f,"\nEND");
 fclose(f);
}
//------------------------------------------------------------------------------
void monModLacLub12(char *arq,const int &rl){
 int val;
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 fprintf(f,"\n\\Variaveis x\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ 0 x_%d_%d ",c,p);
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\Capacidade das salas\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
         fprintf(f,"+ %d y_%d_%d_%d ",MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"+ 5 w_%d ",c); 
 fprintf(f,"\n\n\\Aulas isoladas\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"+ 2 v_%d_%d ",u,p); 
   fprintf(f,"\n"); 
  }
 // ------------------ restri��es
 fprintf(f,"\nST\n");
 fprintf(f,"\n\\R1 - Numero de aulas\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ x_%d_%d ",c,p); 
   fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
  }
 fprintf(f,"\n\\R2 - Numero de salas\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int c = 0; c < numDis__; c++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ x_%d_%d ",c,p); 
   fprintf(f,"<= %d\n",numSal__); 
  }
 fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
 for(int s = 0; s < (numSalDif__-1); s++)
  {
   for(int c = 0; c < numDis__; c++)
     if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
 for(int s = 0; s < (numSalDif__-1); s++)
  {
   val = 0;
   for(int i = 0; i < numSal__; i++)
     if(vetSalas__[i].capacidade_ > vetTamSal__[s])
       val++;
   for(int p = 0; p < numPerTot__; p++) 
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
         fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
     fprintf(f,"<= %d\n",val); 
    } 
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R5 - Dias minimos 1\n");
 for(int c = 0; c < numDis__; c++)
   for(int d = 0; d < numDia__; d++)
    {
     for(int q = 0; q < numPerDia__; q++)
       if(!matResDisPer__[c][(d*numPerDia__)+q])
         fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
     fprintf(f,"- z_%d_%d >= 0\n",c,d);
    }
 
 fprintf(f,"\n\\R6 - Dias minimos 2\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"+ z_%d_%d ",c,d);
   fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
  }
 fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int k = 0; k < vetTurmas__[u].numDis_; k++)
       if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
         fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
     fprintf(f,"- r_%d_%d = 0\n",u,p);
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
     for(int q = 1; q < numPerDia__-1; q++)
       fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
     fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
 for(int t = 0; t < numPro__; t++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
         fprintf(f,"+ x_%d_%d ",c,p);
     fprintf(f,"<= 1\n");
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Variaveis w\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
 fprintf(f,"\n\\Variaveis r\n");
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
 fprintf(f,"\n\\Variaveis v\n");
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
 fprintf(f,"\n\\Variaveis x\n");
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
 fprintf(f,"\n\\Variaveis y\n");
 for(int s = 0; s < (numSalDif__-1); s++)
   for(int c = 0; c < numDis__; c++)
     if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
 fprintf(f,"\n\\Variaveis z\n");
 for(int c = 0; c < numDis__; c++)
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
 if(!rl){
   fprintf(f,"\n\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"w_%d\n",c);
   fprintf(f,"\nBINARIES\n");
   for(int u = 0; u < numTur__; u++)
     for(int p = 0; p < numPerTot__; p++)
       fprintf(f,"r_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     for(int p = 0; p < numPerTot__; p++)
       fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
         fprintf(f,"x_%d_%d\n",c,p);
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"y_%d_%d_%d\n",s,c,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"z_%d_%d\n",c,d);
  }
 fprintf(f,"\nEND");
 fclose(f);
}
//------------------------------------------------------------------------------

//==============================================================================


//================================== SOLU��O ===================================

//------------------------------------------------------------------------------
void escSolBurEal10(Solucao &s,FILE *f){
 int pos,dia,per,aux;
 if(f == NULL)
   f = stdout;
 fprintf(f,"\n< ---------------------------------- SOLUCAO --------------------------------- >\n");
 fprintf(f,"Instancia......................................................: %s\n\n",nomInst__);
 //------------------------------------------------------------- 
 // montar a solucao
 memset(s.matSolSal_,-1,sizeof(s.matSolSal_));
 memset(s.matSolTur_,-1,sizeof(s.matSolTur_));
 pos = 0;
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerTot__; p++)
     for(int c = 0; c < numDis__; c++)
       {
        if(s.vetSol_[pos] > 0)
         {
          dia = p / numPerDia__;
          per = p % numPerDia__;
          fprintf(f,"x_%d_%d_%d = %.2f\n",p,r,c,s.vetSol_[pos]);
          if(s.matSolSal_[per][dia][r] != -1)
           {
            printf("\n\nERRO - SALA %d- p%d d%d c%d!\n\n",r,per,dia,c);
           }
          else
            s.matSolSal_[per][dia][r] = c;
          for(int u = 0; u < numTur__; u++)
           {
            if(matDisTur__[c][u] == 1)
             {
              if(s.matSolTur_[per][dia][u] != -1)
               {
                printf("\n\nERRO - TURMA %d - p%d d%d c%d!\n\n",u,per,dia,c);
               }
              else
                s.matSolTur_[per][dia][u] = c;
             }
           }
         }
        pos++;
       } 
 fprintf(f,"\n");
 for(int r = 0; r < numSal__; r++)
  {
   fprintf(f,"SALA %d\n",r);
   for(int p = 0; p < numPerDia__; p++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"%d  ",s.matSolSal_[p][d][r]);
     fprintf(f,"\n");
    }
   fprintf(f,"\n");
  } 
 fprintf(f,"\n");
 for(int u = 0; u < numTur__; u++)
  {
   fprintf(f,"TURMA %d\n",u);
   for(int p = 0; p < numPerDia__; p++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"%d  ",s.matSolTur_[p][d][u]);
     fprintf(f,"\n");
    }
   fprintf(f,"\n");
  } 
 //------------------------------------------------------------- 
 // verificar a solucao
 s.vioNumAul_ = 0;
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int r = 0; r < numSal__; r++)
    {
     for(int p = 0; p < numPerDia__; p++)
       for(int d = 0; d < numDia__; d++)
         if(s.matSolSal_[p][d][r] == c)
           aux++;
    }    
   s.vioNumAul_ += abs(vetDisciplinas__[c].numPer_ - aux);
  }
 s.vioAulSim_ = 0;
 s.vioDisSim_ = 0;
 s.vioProSim_ = 0;
 s.vioTurSim_ = 0;
 for(int r = 0; r < numSal__; r++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int p = 0; p < numPerDia__; p++)
      {
       if(s.matSolSal_[p][d][r] != -1)
         for(int r2 = r+1; r2 < numSal__; r2++)
          {
           if(s.matSolSal_[p][d][r2] != -1)
            {
             if(s.matSolSal_[p][d][r2] == s.matSolSal_[p][d][r])
               s.vioDisSim_++;
             if(vetDisciplinas__[s.matSolSal_[p][d][r2]].professor_ == vetDisciplinas__[s.matSolSal_[p][d][r]].professor_)
               s.vioProSim_++;
             for(int u = 0; u < numTur__; u++)
               if((matDisTur__[s.matSolSal_[p][d][r2]][u] == 1) && (matDisTur__[s.matSolSal_[p][d][r]][u] == 1))
                 s.vioTurSim_++;
            }
          }
      } 
    }
  }
 s.capSal_ = 0;
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerDia__; p++)
     for(int d = 0; d < numDia__; d++)
       {
        if(s.matSolSal_[p][d][r] != -1)
          if(vetDisciplinas__[s.matSolSal_[p][d][r]].numAlu_ > vetSalas__[r].capacidade_)
            s.capSal_ += (vetDisciplinas__[s.matSolSal_[p][d][r]].numAlu_ - vetSalas__[r].capacidade_);
       } 
 s.janHor_ = 0;
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     if((s.matSolTur_[0][d][u] != -1) && (s.matSolTur_[1][d][u] == -1))
       s.janHor_++;
     if((s.matSolTur_[numPerDia__-1][d][u] != -1) && (s.matSolTur_[numPerDia__-2][d][u] == -1))
       s.janHor_++;
     for(int p = 2; p < numPerDia__; p++)
       if((s.matSolTur_[p-1][d][u] != -1) && (s.matSolTur_[p-2][d][u] == -1) && (s.matSolTur_[p][d][u] == -1))
         s.janHor_++;
    }
  }       
 s.diaMin_ = 0;
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int d = 0; d < numDia__; d++)
    {
     pos = 0;
     for(int p = 0; p < numPerDia__; p++)
      {
       for(int r = 0; r < numSal__; r++)
        {
         if(s.matSolSal_[p][d][r] == c)
          {
           pos = 1;
           break;
          }
        } 
       if(pos == 1)
         break;
      } 
     aux += pos;
    }
   if(aux < vetDisciplinas__[c].diaMin_)
     s.diaMin_++;
  }
 s.salDif_ = 0;
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int r = 0; r < numSal__; r++)
    {
     pos = 0;
     for(int d = 0; d < numDia__; d++)
      {
       for(int p = 0; p < numPerDia__; p++)
        {
         if(s.matSolSal_[p][d][r] == c)
          {
           pos = 1;
           break;
          }
        } 
       if(pos == 1)
         break;
      } 
     aux += pos;
    }
   s.salDif_ += aux - 1;
  }
 s.funObj_ = PESOS[0] * s.capSal_ + PESOS[1] * s.janHor_ + PESOS[2] * s.diaMin_ + PESOS[3] * s.salDif_;   
 fprintf(f,"\n\n>>> RESULTADOS CALCULADOS\n\n");
 fprintf(f,"Func. obj.....: %d\n",s.funObj_);
 fprintf(f,"\n\nHARD----------------------------------\n\n");
 fprintf(f,"Num. aulas....: %d\n",s.vioNumAul_);
 fprintf(f,"Aulas simul...: %d\n",s.vioAulSim_);
 fprintf(f,"Disc. simul...: %d\n",s.vioDisSim_);
 fprintf(f,"Prof. simul...: %d\n",s.vioProSim_);
 fprintf(f,"Turm. simul...: %d\n",s.vioTurSim_);
 fprintf(f,"\n\nSOFT------------------------------------\n\n");
 fprintf(f,"Cap. salas....: %d\n",s.capSal_);
 fprintf(f,"Jan. turmas...: %d\n",s.janHor_);
 fprintf(f,"Dias minimo...: %d\n",s.diaMin_);
 fprintf(f,"Salas difer...: %d\n",s.salDif_);
 fclose(f);
}
//------------------------------------------------------------------------------
void escSolLacLub12(Solucao &s,FILE *f){
 int pos,dia,per,aux;
 if(f == NULL)
   f = stdout;
 fprintf(f,"\n< ---------------------------------- SOLUCAO --------------------------------- >\n");
 fprintf(f,"Instancia......................................................: %s\n\n",nomInst__);
 //------------------------------------------------------------- 
 // montar a solucao
 memset(s.matSolTur_,-1,sizeof(s.matSolTur_));
 pos = 0;
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
      {
       if(s.vetSol_[pos] > 0)
        {
         dia = p / numPerDia__;
         per = p % numPerDia__;
         fprintf(f,"x_%d_%d = %.2f\n",c,p,s.vetSol_[pos]);
         for(int u = 0; u < numTur__; u++)
          {
           if(matDisTur__[c][u] == 1)
            {
             if(s.matSolTur_[per][dia][u] != -1)
              {
               printf("\n\nERRO - TURMA %d - p%d d%d c%d!\n\n",u,per,dia,c);
              }
             else
               s.matSolTur_[per][dia][u] = c;
            }
          }
        } 
       pos++;
      }
 fprintf(f,"\n");
 for(int u = 0; u < numTur__; u++)
  {
   fprintf(f,"TURMA %d\n",u);
   for(int p = 0; p < numPerDia__; p++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"%d  ",s.matSolTur_[p][d][u]);
     fprintf(f,"\n");
    }
   fprintf(f,"\n");
  } 
 //------------------------------------------------------------- 
 // verificar a solucao
 s.vioNumAul_ = 0;
 s.diaMin_ = 0;
 for(int c = 0; c < numDis__; c++)
  {
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u])
      {
       aux = 0;
       for(int p = 0; p < numPerDia__; p++)
         for(int d = 0; d < numDia__; d++)
           if(s.matSolTur_[p][d][u] == c)
             aux++;
       s.vioNumAul_ += abs(vetDisciplinas__[c].numPer_ - aux);    

       aux = 0;
       for(int d = 0; d < numDia__; d++)
        {
         for(int p = 0; p < numPerDia__; p++)
           if(s.matSolTur_[p][d][u] == c)
            {
             aux++;
             break;
            }
        } 
       s.diaMin_ += MAX(0,vetDisciplinas__[c].diaMin_-aux);
       break;
      }
  }
 s.vioProSim_ = 0;
 s.janHor_ = 0;
 for(int u = 0; u < numTur__; u++)
   for(int d = 0; d < numDia__; d++)
    {
     for(int p = 0; p < numPerDia__; p++)
       if(s.matSolTur_[p][d][u] != -1)
         for(int u2 = u+1; u2 < numTur__; u2++)
           if(s.matSolTur_[p][d][u2] != -1)
             if((s.matSolTur_[p][d][u2] != s.matSolTur_[p][d][u]) && (vetDisciplinas__[s.matSolTur_[p][d][u2]].professor_ == vetDisciplinas__[s.matSolTur_[p][d][u]].professor_))
               s.vioProSim_++;
  
     if((s.matSolTur_[0][d][u] != -1) && (s.matSolTur_[1][d][u] == -1))
       s.janHor_++;
     if((s.matSolTur_[numPerDia__-1][d][u] != -1) && (s.matSolTur_[numPerDia__-2][d][u] == -1))
       s.janHor_++;
     for(int p = 2; p < numPerDia__; p++)
       if((s.matSolTur_[p-1][d][u] != -1) && (s.matSolTur_[p-2][d][u] == -1) && (s.matSolTur_[p][d][u] == -1))
         s.janHor_++;
    } 
 s.capSal_ = 0;
 s.salDif_ = 0;
 s.funObj_ = PESOS[0] * s.capSal_ + PESOS[1] * s.janHor_ + PESOS[2] * s.diaMin_ + PESOS[3] * s.salDif_;   
 fprintf(f,"\n\n>>> RESULTADOS CALCULADOS\n\n");
 fprintf(f,"Func. obj.....: %d\n",s.funObj_);
 fprintf(f,"\n\nHARD----------------------------------\n\n");
 fprintf(f,"Num. aulas....: %d\n",s.vioNumAul_);
 fprintf(f,"Aulas simul...: -\n");
 fprintf(f,"Disc. simul...: -\n");
 fprintf(f,"Prof. simul...: %d\n",s.vioProSim_);
 fprintf(f,"Turm. simul...: -\n");
 fprintf(f,"\n\nSOFT------------------------------------\n\n");
 fprintf(f,"Cap. salas....: -\n");
 fprintf(f,"Jan. turmas...: %d\n",s.janHor_);
 fprintf(f,"Dias minimo...: %d\n",s.diaMin_);
 fprintf(f,"Salas difer...: -\n");
 fclose(f);
}
//------------------------------------------------------------------------------

//==============================================================================


//================================= AUXILIARES =================================

//------------------------------------------------------------------------------
void lerInstancia(char *arq){
 int pos,per;
 char aux[50];
 FILE *f = fopen(arq,"r");
 fscanf(f,"Name: %s\n",&nomInst__);
 fscanf(f,"Courses: %d\n",&numDis__);
 fscanf(f,"Rooms: %d\n",&numSal__);
 fscanf(f,"Days: %d\n",&numDia__);
 fscanf(f,"Periods_per_day: %d\n",&numPerDia__);
 fscanf(f,"Curricula: %d\n",&numTur__);
 fscanf(f,"Constraints: %d\n",&numRes__);
 fscanf(f,"\nCOURSES:\n");
 numPerTot__ = numDia__ * numPerDia__;
 numPro__ = 0;
 for(int i = 0; i < numDis__; i++)
  {
   fscanf(f,"%s %s %d %d %d\n",&vetDisciplinas__[i].nome_,&aux,
          &vetDisciplinas__[i].numPer_,&vetDisciplinas__[i].diaMin_,&vetDisciplinas__[i].numAlu_);
   pos = -1;
   for(int p = 0; p < numPro__; p++)
     if(strcmp(aux,vetProfessores__[p].nome_) == 0)
      {
       vetDisciplinas__[i].professor_ = p;
       pos = p;
       break;
      }
   if(pos == -1)
    {
     vetDisciplinas__[i].professor_ = numPro__;
     strcpy(vetProfessores__[numPro__].nome_,aux);
     numPro__++;
    }
  }
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numTur__; j++)
     matDisTur__[i][j] = 0;
 fscanf(f,"\nROOMS:\n");
 for(int i = 0; i < numSal__; i++)
   fscanf(f,"%s %d\n",&vetSalas__[i].nome_,&vetSalas__[i].capacidade_);
 fscanf(f,"\nCURRICULA:\n");
 for(int i = 0; i < numTur__; i++)
  {
   fscanf(f,"%s %d",&vetTurmas__[i].nome_,&vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
    {
     fscanf(f, "%s ",&aux);
     vetTurmas__[i].vetDis_[j] = -1;
     for(int k = 0; k < numDis__; k++)
       if(strcmp(aux,vetDisciplinas__[k].nome_) == 0)
        {
         vetTurmas__[i].vetDis_[j] = k;
         matDisTur__[k][i] = 1;
         break;
        }
    }
  }
 fscanf(f,"\nUNAVAILABILITY_CONSTRAINTS:\n");
 memset(matResDisPer__,0,sizeof(matResDisPer__));
 for(int i = 0; i < numRes__; i++) 
  {
   fscanf(f,"%s %d %d\n",&aux,&pos,&per);
   for(int j = 0; j < numDis__; j++)
     if(strcmp(aux,vetDisciplinas__[j].nome_) == 0)
      {
       matResDisPer__[j][(pos * numPerDia__) + per] = 1;
       break;
      }
  }
 fclose(f);
 //---------------
 // preeencher e ordenar o vetor com as diferentes capacidades de sala
 ordVetCapSal();
}
//------------------------------------------------------------------------------
void testarEntrada(){
 int aux;

 printf("Name: %s\n",nomInst__);
 printf("Courses: %d\n",numDis__);
 printf("Rooms: %d\n",numSal__);
 printf("Days: %d\n",numDia__);
 printf("Periods_per_day: %d\n",numPerDia__);
 printf("Curricula: %d\n",numTur__);
 printf("Constraints: %d\n",numRes__);

 printf("\nDISCIPLINAS:\n");
 for(int i = 0; i < numDis__; i++)
   printf("%d  %s  %d  %d  %d  %d\n",i,vetDisciplinas__[i].nome_,vetDisciplinas__[i].professor_,vetDisciplinas__[i].numPer_,vetDisciplinas__[i].diaMin_,vetDisciplinas__[i].numAlu_);

 printf("\nTURMAS:\n");
 for(int i = 0; i < numTur__; i++)
  {
   printf("%d  %s  %d --> ",i,vetTurmas__[i].nome_,vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
     printf("%d  ",vetTurmas__[i].vetDis_[j]);
   printf("\n");
  } 

 printf("\nPROFESSORES:\n");
 for(int i = 0; i < numPro__; i++)
   printf("%d  %s\n",i,vetProfessores__[i].nome_);

 printf("\nSALAS:\n");
 for(int i = 0; i < numSal__; i++)
   printf("%d  %s  %d\n",i,vetSalas__[i].nome_,vetSalas__[i].capacidade_);

 aux = 0;
 printf("\nRESTRICOES:\n");
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numPerTot__; j++)
     if(matResDisPer__[i][j])
      {
       printf("%d  %d  %d\n",aux,i,j);
       aux++;
      } 

 printf("\nDISC x TURMA:\n");
 for(int i = 0; i < numDis__; i++)
  {
   for(int j = 0; j < numTur__; j++)
     printf("%d  ",matDisTur__[i][j]);
   printf("\n");
  }
}
//------------------------------------------------------------------------------
void ordVetCapSal(){
 int aux,flag;
 numSalDif__ = 0;
 memset(vetTamSal__,0,sizeof(vetTamSal__));
 for(int i = 0; i < numSal__; i++)
  {
   flag = 0;
   for(int j = 0; j < numSalDif__; j++)
     if(vetSalas__[i].capacidade_ == vetTamSal__[j])
      {
       flag = 1; 
       break;
      }
   if(!flag)
    {
     vetTamSal__[numSalDif__] = vetSalas__[i].capacidade_;
     numSalDif__++; 
    }
  }
 flag = 1;
 while(flag)
  {
   flag = 0;
   for(int i = 0; i < numSalDif__-1; i++)
     if(vetTamSal__[i] > vetTamSal__[i+1])
      {
       aux = vetTamSal__[i];
       vetTamSal__[i] = vetTamSal__[i+1];
       vetTamSal__[i+1] = aux;
       flag = 1;
      }
  }
}
//------------------------------------------------------------------------------
void configuraParametros(int piNParam, char **piParams){
	
	for (int i = 2; i < piNParam; i++){
		if (strncmp("facDes", piParams[i], 6) == 0){
			facDes = atoi(piParams[i] + 7);
		} else if (strncmp("numSubProb", piParams[i], 10) == 0){
			numSubProb = atoi(piParams[i] + 11);
		} else if (strncmp("comPeso", piParams[i], 7) == 0){
			comPeso = atoi(piParams[i] + 8);
		} else if (strncmp("tGrafo", piParams[i], 6) == 0){
			tGrafo = atoi(piParams[i] + 7);
		}	
	}
}
//------------------------------------------------------------------------------
void lerSolucoesIniciais(){
	int i, j;
	char aux[200];
	float faux;
	strcpy(aux, "colunas/");
	strcat(aux, nomInst__);
	strcat(aux, ".gc");

	matColInicial__ = (int**)malloc(MAX_VAR * sizeof(int*));
	for (i = 0; i < MAX_VAR; i++) {
		matColInicial__[i] = (int*)malloc(MAX_COL * sizeof(int));
	}
	
	FILE *f = fopen(aux, "r");
	if (f == NULL) {
		printf("Erro de abertura do arquivo %s\n", aux);
		exit(1);
	}

	fscanf(f, "%d\n", &numVar__);
	fscanf(f, "%d\n", &numCol__);
	fscanf(f, "%lf\n", &tempoGCIniciais_);

	for (j = 0; j < numCol__; j++) {
		fscanf(f, "%lf ", &faux);
	}
	fscanf(f, "\n");

	for (i = 0; i < numVar__; i++) {
		for (j = 0; j < numCol__; j++) {
			fscanf(f, "%d ", &matColInicial__[i][j]);
		}
		fscanf(f, "\n");
	}
	fclose(f);
}
//------------------------------------------------------------------------------
void contaVarRestricoes(){
 int val, nr;
 // ------------------ FO

   printf("%d;",(numDis__*numPerTot__)-numRes__);           // X
 
   nr = 0;                                                  // Y
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             nr++;
   printf("%d;",nr); 
  
   printf("%d;",numDis__);                                 // W 
   printf("%d;",numTur__*numPerTot__);                     // V 
   printf("%d;",numDis__*numDia__);                        // Z 
   printf("%d;",numTur__*numPerTot__);                     // r 

 // ------------------ restri��es

   printf("%d;",numDis__);                                 // R1
   printf("%d;",numPerTot__);                              // R2  
 
   nr = 0;                                                 // R3
   for(int s = 0; s < (numSalDif__-1); s++){
     for(int c = 0; c < numDis__; c++)
       if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             nr++;
   }
   printf("%d;",nr);
     
   printf("%d;",(numSalDif__-1)*numPerTot__);              // R4 
   printf("%d;",numDis__*numDia__);                        // R5
   printf("%d;",numDis__);                                 // R6 
   printf("%d;",numTur__*numPerTot__);                     // R7
   printf("%d;",numTur__*numPerTot__);                     // R8
   printf("%d;\n",numPro__*numPerTot__);                   // R9
}
//------------------------------------------------------------------------------
void aloca(){
	int i, j, sts;
	
	env__ = CPXopenCPLEX(&sts);
	sts = CPXsetintparam(env__,CPX_PARAM_SCRIND,CPX_OFF);
				
	matD__ = (int**)malloc((MAX_RSS + numVarCopy__) * sizeof(int*));
	mat_Rss_Var__ = (int**)malloc((MAX_RSS + numVarCopy__) * sizeof(int*));
	matE__ = (double*)malloc((MAX_RSS + numVarCopy__) * sizeof(double));
	matSNS_DE__ = (char*)malloc((MAX_RSS + numVarCopy__) * sizeof(char));
	for (i = 0; i < (MAX_RSS + numVarCopy__); i++) {
		matD__[i] = (int*)malloc((numVar__ + numVarCopy__) * sizeof(int));
		mat_Rss_Var__[i] = (int*)malloc((numVar__ + numVarCopy__) * sizeof(int));
		for(j = 0; j < (numVar__ + numVarCopy__); j++){
			matD__[i][j] = 0;
			mat_Rss_Var__[i][j] = 0;;			
		}
	}
			
	VecDual__ = (double*)malloc((MAX_RSS+numVarCopy__+mr__.numSps_) * sizeof(double));

    indices = (int*)malloc((numVar__+numVarCopy__+mr__.numSps_) * sizeof(int));
    for (i = 0; i <= (numVar__+numVarCopy__); i++) {
      indices[i] = i;
    }
    
    matCoefOrig__ = (double*)malloc((numVar__ + numVarCopy__) * sizeof(double));
    matNovosCoef__ = (double*)malloc((numVar__ + numVarCopy__) * sizeof(double)); 
    vecDualD__ = (double*)malloc((numVar__ + numVarCopy__) * sizeof(double));
}
//------------------------------------------------------------------------------
void desaloca(){
	int i, j, sts;

	for (i = 0; i < MAX_VAR; i++) {
		free(matColInicial__[i]);
	}
	free(matColInicial__);

	for (i = 0; i < (numVar__+numVarCopy__); i++) {
		free(matColFinal__[i]);
	}
	free(matColFinal__);
	
	free(matFO__);
	
	for (i = 0; i < numDis__; i++)  
		free(indVarX__[i]);
	free(indVarX__);

	for (i = 0; i < (numSalDif__-1); i++) {
		for (j = 0; j < numDis__; j++) 
			free(indVarY__[i][j]);    
		free(indVarY__[i]);
	}
	free(indVarY__);

	free(indVarW__);

	for (i = 0; i < numTur__; i++)
		free(indVarV__[i]);
	free(indVarV__);

	for (i = 0; i < numDis__; i++)
		free(indVarZ__[i]);
	free(indVarZ__);
	
	for (i = 0; i < numTur__; i++)
		free(indVarr__[i]);
	free(indVarr__);

	for (i = 0; i < (MAX_RSS + numVarCopy__); i++) {
		free(matD__[i]);
		free(mat_Rss_Var__[i]);
	}
	free(matD__);
	free(mat_Rss_Var__);
	free(MAT_D_VAL__);
    free(MAT_D_IND__);
    free(MAT_D_CNT__);

	free(matE__);

	free(matSNS_DE__);
	
	for (i = 0; i < (numRss__+mr__.numSps_); i++) {
		free(matLambdas__[i]);
	}
	free(matLambdas__);
	
	//free(bestSol);

	/*for (i = 0; i < MAX_RSH; i++) {
	  free(matA__[i]);
	}
	free(matA__);

	free(matB__);

	free(matSNS_AB__);*/

	free(VecDual__);
	/*free(VecX_SP);*/

	free(matCoefOrig__);	
	free(matNovosCoef__);
	free(vecDualD__);
	free(varSPS__);
	for (i = 0; i < numVarCopy__; i++) {
		free(vetVarOrigXCopy[i]);
	}
	free(vetVarOrigXCopy);
	free(vetVarSPSSeq);
	
	free(indices);
	
	/*free(ctype__);*/
	
	free(coladd_FO);
	free(coladd_IND);
	free(coladd_BEG);
	free(coladd_lb);
	free(coladd_ub);
	free(VecSPx__);
	free(VecNewX__);
	free(VecNewCol__);
	free(VecSPxPMR__);
	
	sts = CPXfreeprob(env__,&pmr.lp_);
    for(int i = 0; i < mr__.numSps_; i++)
        sts = CPXfreeprob(env__,&vetCpx__[i].lp_);
    sts = CPXcloseCPLEX(&env__);
}
//------------------------------------------------------------------------------
void desalocaMarizesCPX(){

	free(MATVAL__);
	free(MATIND__);
	free(MATBEG__);
	free(MATCNT__);

	free(lbVec__);
	free(ubVec__);
}
//------------------------------------------------------------------------------
void alocaAddCol(){
	
   coladd_FO = (double*)malloc(sizeof(double));
   coladd_IND  = (int*)malloc((numRss__+mr__.numSps_) * sizeof(int));
   coladd_BEG  = (int*)malloc(sizeof(int));
   coladd_lb = (double*)malloc(sizeof(double));
   coladd_ub = (double*)malloc(sizeof(double));
   
   for(int i = 0; i < (numRss__+mr__.numSps_); i++)
      coladd_IND[i] = i;
   
   coladd_BEG[0] = 0;
   coladd_lb[0] = 0;
   coladd_ub[0] = CPX_INFBOUND;
   
   VecSPx__ = (double*)malloc((numVar__+numVarCopy__) * sizeof(double));	
   VecNewX__ = (double*)malloc((numVar__+numVarCopy__) * sizeof(double));
   VecNewCol__ = (double*)malloc((numRss__+mr__.numSps_) * sizeof(double));
}
//------------------------------------------------------------------------------
void ajustaParticao(){

  // Define aqui qual curr�culo vai ser cortado e em qual subproblema vai ficar
 memset(vetTurDis,0,sizeof(vetTurDis));
 memset(vetAux,-1,sizeof(vetAux));

 int aux, sp_u, qnt_sps;
 numVarCopy__ = 0;
 for(int u = 0; u < numTur__; u++){
   // Verifica se todas as disciplinas de um curriculo ficaram no mesmo subproblema... Se o curriculo foi "cortado" ele � ignorado (0)
   for(int i = 0; i < vetTurmas__[u].numDis_-1; i++)
     if (mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]] != mr__.vetSpDis_[vetTurmas__[u].vetDis_[i+1]])
       vetTurDis[u] = 1;
   // Se o curr�culo N�O foi "cortado" define o subproblema em que ele vai aparecer = subproblema de uma disciplina dele
   if (vetTurDis[u] == 0)
       vetAux[u] = mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]]; 
   else{
	   memset(vetAuxSps,0,sizeof(vetAuxSps));
	   aux = 0;
	   qnt_sps = 0;
	   for(int i = 0; i < vetTurmas__[u].numDis_; i++){
		   if (vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]] == 0)
		      qnt_sps++;
		   vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]]++;
		   if (vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]] > aux){
			   aux = vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]];
			   sp_u = mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]];  
	       }  
       }
	   vetAux[u] = sp_u;
	   numVarCopy__ += (qnt_sps-1)*2*numPerTot__; 
   }   
 }
 
  // Define aqui qual professor ficou com disciplinas em subproblemas diferentes
  memset(vetProfDis,0,sizeof(vetProfDis));
  for(int i = 0; i < numDis__; i++)
    for(int j = i+1; j < numDis__; j++)
       if (vetDisciplinas__[i].professor_ == vetDisciplinas__[j].professor_)
         if (mr__.vetSpDis_[i] != mr__.vetSpDis_[j])
           vetProfDis[vetDisciplinas__[i].professor_] = 1;
             
   // Define em qual subproblema cada vari�vel vai ficar;
   varSPS__ = (int*)malloc((numVar__+numVarCopy__) * sizeof(int));
   vetVarOrigXCopy = (int**)malloc(numVarCopy__* sizeof(int*));
   vetVarSPSSeq = (int*)malloc((numVar__+numVarCopy__) * sizeof(int));
   for (int i = 0; i < numVarCopy__; i++) {
		vetVarOrigXCopy[i] = (int*)malloc(2 * sizeof(int));
   }
   int cont = 0;
   for(int c = 0; c < numDis__; c++)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p]){
           varSPS__[cont] = mr__.vetSpDis_[c];
           cont++;
         }
           
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p]){
               varSPS__[cont] = mr__.vetSpDis_[c];
               cont++;
             }
                   
   for(int c = 0; c < numDis__; c++){
     varSPS__[cont] = mr__.vetSpDis_[c];
     cont++;
   } 
     
   for(int u = 0; u < numTur__; u++)
       for(int p = 0; p < numPerTot__; p++){
         varSPS__[cont] = vetAux[u];
         cont++; 
       }

   for(int c = 0; c < numDis__; c++)
       for(int d = 0; d < numDia__; d++){
         varSPS__[cont] = mr__.vetSpDis_[c];
         cont++;
       } 
         
   for(int u = 0; u < numTur__; u++)
       for(int p = 0; p < numPerTot__; p++){
         varSPS__[cont] = vetAux[u];
         cont++;
       }
   // Variaveis V de Copia 
   int cont2 = 0;   
   for(int u = 0; u < numTur__; u++){
     if (vetTurDis[u] != 0){
       memset(vetAuxSps,0,sizeof(vetAuxSps));
	   for(int i = 0; i < vetTurmas__[u].numDis_; i++)
		  vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]]++; 
       for (int sp = 0; sp < mr__.numSps_; sp++){
         if ((vetAuxSps[sp] > 0) && (vetAux[u] != sp)){
		   for(int p = 0; p < numPerTot__; p++){
			 varSPS__[cont] = sp;
			 vetVarOrigXCopy[cont2][0] = indVarV__[u][p]; 
			 vetVarOrigXCopy[cont2][1] = cont;
			 
			 cont++;
			 cont2++; 
		   }
		 } 
	   }    
     }
   }
   // Variaveis r de Copia   
   for(int u = 0; u < numTur__; u++){
     if (vetTurDis[u] != 0){
       memset(vetAuxSps,0,sizeof(vetAuxSps));
	   for(int i = 0; i < vetTurmas__[u].numDis_; i++)
		  vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]]++; 
       for (int sp = 0; sp < mr__.numSps_; sp++){
         if ((vetAuxSps[sp] > 0) && (vetAux[u] != sp)){
		   for(int p = 0; p < numPerTot__; p++){
			 varSPS__[cont] = sp;
			 vetVarOrigXCopy[cont2][0] = indVarr__[u][p]; 
			 vetVarOrigXCopy[cont2][1] = cont;
			 
			 cont++;
			 cont2++; 
		   }
		 } 
	   }    
     }
   }
   cont2 = 0;
   for (int i = 0; i < mr__.numSps_; i++){
	   cont = 0;
	   for (int j = 0; j < (numVar__+numVarCopy__); j++)
	     if (varSPS__[j] == i){
	       vetVarSPSSeq[cont2] = j;
	       cont2++;
	       cont++;
	     }
	   vetQntVarSPS[i] = cont;
   }
     
}
//------------------------------------------------------------------------------
void convertExcPRM(){

  memset(ctype__,'B',sizeof(ctype__));
  CPXcopyctype(env__,pmr.lp_,ctype__);
  CPXchgprobtype(env__,pmr.lp_,1);
  CPXmipopt(env__,pmr.lp_);
  
  VecSPxPMR__ = (double*)malloc(CPXgetnumcols(env__,pmr.lp_) * sizeof(double));
  CPXgetmipx(env__,pmr.lp_,VecSPxPMR__,0,CPXgetnumcols(env__,pmr.lp_)-1);
}
//------------------------------------------------------------------------------
double defineX(double x){
	double p, t;
	p = floor(x);
	t = ceil(x);
	if(p == t)
	   return p;
	else if((x-p) > (t-x))
	   return t;
	else
	   return p;
}
