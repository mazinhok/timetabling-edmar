#include "CurriculumCompactnessMove.h"

void CurriculumCompactnessMove::imprimeListaAlocada(){
  vector<Alocacao*>::iterator it;
  printf("aulas alocadas\n");
  for(it = ind->aulasAlocadas.begin(); it != ind->aulasAlocadas.end(); it++){
    (*(it))->imprime();
  }
  printf("\n---------------\n");
}

CurriculumCompactnessMove::CurriculumCompactnessMove(Problema *p, Individuo* piInd){
	ind = piInd;
	restricao = new RestricaoFraca3CurriculumCompactness();

	list<Alocacao*>::iterator it;

    if (restricao->contaViolacaoRestricao(ind, p) <= 0){
    	deltaFit = 99999; deltaHard  = 0;
    	return;
    }

	imprimeListaAlocada();
	for(list<Curriculo*>::iterator it = p->curriculos.begin(); it != p->curriculos.end(); it++){
		if(restricao->contaViolacaoRestricao(ind, *it, p, NULL) > 0){
		  printf("%d\n", (*(it))->numeroSequencial);
		  break;
		}
	}
}

void CurriculumCompactnessMove::aplicaMovimento(){

}

CurriculumCompactnessMove::~CurriculumCompactnessMove(){
}
