
#include "MinWorkingDaysMove.h"
#include "../Model/Alocacao.h"
#include "../Model/Aula.h"
#include "../Model/Disciplina.h"

bool MinWorkingDaysMove::naoPossuiAulaNoDia(int dia)
{
	bool retorno = true;
	for(list<Alocacao*>::iterator it = ind->aulasAlocadas.begin(); it != ind->aulasAlocadas.end(); it++)
	{
		if ((*(it))->aula->disciplina->numeroSequencial == a1->aula->disciplina->numeroSequencial &&
			  (*(it))->horario->dia == dia)
				{
					retorno = false;
					break;
				}
	}

	return retorno;
}

MinWorkingDaysMove::MinWorkingDaysMove(Problema* p, Individuo* piInd)
{
  ind = piInd;
  list<Alocacao*>::iterator it, it2;
  restricao = new RestricaoFraca2MinimumWorkingDays();
  int posAulaAlocada;
  bool aulaNoMesmoDia = false;

  if (restricao->contaViolacaoRestricao(ind, p) > 0){
    do
    {
      posAulaAlocada = rand() % ind->aulasAlocadas.size();
      it = ind->aulasAlocadas.begin();
      advance(it, posAulaAlocada);
      a1 = *(it);

			if(restricao->contaViolacaoRestricao(ind, a1->aula->disciplina, p->nDias) > 0)
			{
				for(it2 = ind->aulasAlocadas.begin(); it2 != ind->aulasAlocadas.end(); it2++)
				{
					if (a1->aula->disciplina->numeroSequencial == (*(it2))->aula->disciplina->numeroSequencial &&
							a1->horario->dia == (*(it2))->horario->dia &&
							a1->horario->periodo != (*(it2))->horario->periodo)
							{
								aulaNoMesmoDia = true;
								break;
							}
				}
			}
    } while(!aulaNoMesmoDia);

		/* Monta a lista de horários disponíveis */
		list<Alocacao*> horariosPossiveis;
		for(it = ind->TodosHorarios.begin(); it != ind->TodosHorarios.end(); it++)
		{
			if( a1->horario->dia != (*(it))->horario->dia &&
					!p->HorarioIndisponivelDisciplina(a1->aula->disciplina, (*(it))->horario->horario) &&
					naoPossuiAulaNoDia((*(it))->horario->dia))
			{
					horariosPossiveis.push_front(*it);
			}
		}

		/* Escolhe um horário aleatório dentre os disponíveis */
		int posAula = rand() % horariosPossiveis.size();
		it = horariosPossiveis.begin();
		/* Avança até a posição escolhida */
		advance(it, posAula);
		a2 = *(it);

		if (a2->aula == NULL){
			m = new Move(ind, a1, a2);
		}
		else {
			m = new Swap(ind, a1, a2);
		}
		deltaFit = m->deltaFit;
	 } else {deltaFit = 99999; deltaHard  = 0;}
}

MinWorkingDaysMove::~MinWorkingDaysMove()
{
  delete(restricao);
}

void MinWorkingDaysMove::aplicaMovimento()
{
	m->aplicaMovimento();
}
