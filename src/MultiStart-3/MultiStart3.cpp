#include "MultiStart3.h"

#include <string.h>
#include <time.h>
#include <omp.h>
#include "/home/edmar/Documentos/timetabling-edmar/metis-5.0.1-X64/include/metis.h"

#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))

int PESOS[4] = {1,2,5,1};

//================================== PRINCIPAL ================================= 

//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
 int sts;
 FILE *arq;
 char aux[50];
 Solucao sol;
 strcpy(aux,"../../instances/");
 strcat(aux,argv[1]);
 strcat(aux,".ctt");
 lerInstancia(aux);
 configuraParametros(argc, argv); 
 env__ = CPXopenCPLEX(&sts);
 CPXsetintparam(env__,CPX_PARAM_SCRIND,CPX_OFF);
 if(tempLim != 0)
   CPXsetdblparam(env__,CPX_PARAM_TILIM,tempLim);
   
 strcpy(aux,nomInst__);
 strcat(aux,".lp");
 monModLacLub12(aux,0);
 double hi, hf;
 mr__.temExe_ = 0.0;
 mr__.limInf_ = 0.0;
 mr__.limSup_ = 0.0;
 
   vetCpx__[0].lp_ = CPXcreateprob(env__,&sts,"");
   CPXreadcopyprob(env__,vetCpx__[0].lp_,aux,NULL);

   hi = omp_get_wtime();
   CPXmipopt(env__,vetCpx__[0].lp_);
   CPXgetmipobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limSup_);
   CPXgetbestobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limInf_);
     
   mr__.limInf_ += vetCpx__[0].limInf_;
   mr__.limSup_ += vetCpx__[0].limSup_;
   hf = omp_get_wtime();
   mr__.temExe_ += (hf-hi);
   CPXfreeprob(env__,&vetCpx__[0].lp_);
 
   /*mr__.numSps_ = MIN(numSubProb,numDis__);
  sprintf(aux,"../../sps/%s-%d.txt",argv[1],numSubProb);
  FILE *f = fopen(aux,"r");
  fscanf(f,"%f\n",&mr__.temPrt_);
  for(int u = 0; u < numTur__; u++)
     fscanf(f,"%d ",&vetAux[u]);
  fscanf(f,"\n");
  for(int i = 0; i < numDis__; i++)
     fscanf(f,"%d ",&mr__.vetSpDis_[i]);
  fscanf(f,"\n");
  fclose(f);*/
 
  //excPrtDis(numSubProb,facDes,comPeso);
  
  mr__.temTot_ = mr__.temPrt_ + mr__.temSub_ + mr__.temExe_;
  //printf("%s;%d;%d;%d;%d;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f\n",argv[1],mr__.numSps_,mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,mr__.limInf_,mr__.limSup_,mr__.temPrt_,mr__.temSub_,mr__.temExe_,mr__.temTot_);
  
  printf("%s;%d;%d;%d;%d;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f\n",argv[1],1,mr__.numCtsM_,mr__.numCts_,mr__.pesCts_,mr__.limInf_,mr__.limSup_,mr__.temPrt_,mr__.temSub_,mr__.temExe_,mr__.temTot_);
 
 CPXcloseCPLEX(&env__);
 return 0;
}
//------------------------------------------------------------------------------

//==============================================================================


//=========================== M�TODOS DE RELAXA��O =============================

//------------------------------------------------------------------------------
void excPrtDis(const int &sps,const int &fat,const int &pes)
{
 int sts;
 double hi, hf;
 char arq[50];
 double *vetSol;
 parGraDis(sps,fat,pes);
 mr__.temSub_ = 0.0;
 mr__.temExe_ = 0.0;
 mr__.limInf_ = 0.0;
 mr__.limSup_ = 0.0;
 
 for(int i = 0; i < mr__.numSps_; i++){
   if (tMetodo == 1)
      monSpsDisOrig(i);
   else
      monSpsDisNull(i);
   sprintf(arq,"sp%d.lp",i+1);
   vetCpx__[i].lp_ = CPXcreateprob(env__,&sts,"");
   CPXreadcopyprob(env__,vetCpx__[i].lp_,arq,NULL);

   hi = omp_get_wtime();
   CPXmipopt(env__,vetCpx__[i].lp_);
   CPXgetmipobjval(env__,vetCpx__[i].lp_,&vetCpx__[i].limSup_);
   CPXgetbestobjval(env__,vetCpx__[i].lp_,&vetCpx__[i].limInf_);
   
   vetSol = new double[CPXgetnumcols(env__,vetCpx__[i].lp_)];
   CPXgetmipx(env__,vetCpx__[i].lp_,vetSol,0,CPXgetnumcols(env__,vetCpx__[i].lp_)-1);
   
   for(int j = 0; j < CPXgetnumcols(env__,vetCpx__[i].lp_); j++){
	  vetCpx__[i].limInf_ -= (vetVar[j]*vetSol[j]);
	  vetCpx__[i].limSup_ -= (vetVar[j]*vetSol[j]);
   }
   
   mr__.limInf_ += vetCpx__[i].limInf_;
   mr__.limSup_ += vetCpx__[i].limSup_;
   hf = omp_get_wtime();
   mr__.temExe_ += (hf-hi);
   delete vetSol;
 }
 
 //for(int i = 0; i < mr__.numSps_; i++)
    //printf("SUB %d --> LB = %.2f UB = %.2f\n",i+1,vetCpx__[i].limInf_,vetCpx__[i].limSup_);

 for(int i = 0; i < mr__.numSps_; i++)
   CPXfreeprob(env__,&vetCpx__[i].lp_);
 
}

//------------------------------------------------------------------------------
void monSpsDisOrig(int sp)
{
 FILE *f;
 double hi, hf;
 char arq[50];
 int aux,flag, sp_u, imp, cont = 0;
 int vetAux[MAX_TUR];
 int *vetTurDis; 
 vetTurDis = (int*)malloc(numTur__ * sizeof(int));

 int vetAuxSps[MAX_SPS];
 int vetDisSps[MAX_DIS];
 hi = omp_get_wtime();
 
 memset(vetAux,-1,sizeof(vetAux));
 
 for(int u = 0; u < numTur__; u++){
   vetTurDis[u] = 0;
   // Verifica se todas as disciplinas de um curriculo ficaram no mesmo subproblema... Se o curriculo foi "cortado" ele � ignorado (0)
   for(int i = 0; i < vetTurmas__[u].numDis_-1; i++)
     if (mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]] != mr__.vetSpDis_[vetTurmas__[u].vetDis_[i+1]])
       vetTurDis[u] = 1;
   // Se o curr�culo N�O foi "cortado" define o subproblema em que ele vai aparecer = subproblema de uma disciplina dele
   if (vetTurDis[u] == 0)
       vetAux[u] = mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]];
   else{
	   memset(vetAuxSps,0,sizeof(vetAuxSps));
	   aux = 0;
	   for(int i = 0; i < vetTurmas__[u].numDis_; i++){
		   vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]]++;
		   if (vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]] > aux){
			   aux = vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]];
			   sp_u = mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]];  
	       }  
       }
	   vetAux[u] = sp_u;
    }    
  }
    
   // ------------------ 
   memset(vetDisSps,-1,sizeof(vetDisSps));
   memset(vetVar,0,sizeof(vetVar));
   // Determina se a disciplina est� ou n�o (-1) no subproblema sp... e se � original (1) ou c�pia (2)
   for(int i = 0; i < numDis__; i++)
     if (mr__.vetSpDis_[i] == sp)
	   vetDisSps[i] = 1;  
   for(int u = 0; u < numTur__; u++)
	 if (vetAux[u] == sp)
	   for(int i = 0; i < vetTurmas__[u].numDis_; i++)
		   if (mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]] != sp)
		       vetDisSps[vetTurmas__[u].vetDis_[i]] = 2;

   // ------------------   
   sprintf(arq,"sp%d.lp",sp+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1){
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p]){
           fprintf(f,"+ 0 x_%d_%d ",c,p);
           cont++;
         }
       fprintf(f,"\n"); 
     }
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int p = 0; p < numPerTot__; p++){
     for(int s = 0; s < (numSalDif__-1); s++){
       for(int c = 0; c < numDis__; c++)
         if(vetDisSps[c] == 1){
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
             fprintf(f,"+ %d y_%d_%d_%d ",MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
             cont++;
           }
         }else if(vetDisSps[c] == 2){
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
             fprintf(f,"+ %d y_%d_%d_%d ",MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
             vetVar[cont] = MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]);
             cont++;
           }
         }
     }
     fprintf(f,"\n"); 
   }
   fprintf(f,"\n\\Dias minimos\n");
   for(int c = 0; c < numDis__; c++){
     if(vetDisSps[c] == 1){ 
       fprintf(f,"+ 5 w_%d ",c);
       cont++;
     }else if(vetDisSps[c] == 2){ 
       fprintf(f,"+ 5 w_%d ",c);
       vetVar[cont] = 5;
       cont++;
     }
   }
   fprintf(f,"\n\n\\Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp){
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ 2 v_%d_%d ",u,p); 
       fprintf(f,"\n"); 
     }
   }
   // ------------------ restri��es
   fprintf(f,"\nST\n");
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++){
	 imp = 0;
     if(vetDisSps[c] != -1){
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p]){
           fprintf(f,"+ x_%d_%d ",c,p);
           imp = 1;
         }
       if (imp) 
          fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
     } 
   }
   fprintf(f,"\n\\R2 - Numero de salas\n");
   for(int p = 0; p < numPerTot__; p++){
	 imp = 0;
     for(int c = 0; c < numDis__; c++){
       if(vetDisSps[c] != -1)         
         if(!matResDisPer__[c][p]){
           fprintf(f,"+ x_%d_%d ",c,p);
           imp = 1;
         } 
     }
     if (imp)
       fprintf(f,"<= %d\n",numSal__); 
   }
   fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
   for(int s = 0; s < (numSalDif__-1); s++){
     for(int c = 0; c < numDis__; c++){
       if(vetDisSps[c] != -1)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
     }
     fprintf(f,"\n"); 
   }
   fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
   for(int s = 0; s < (numSalDif__-1); s++){
     aux = 0;
     for(int i = 0; i < numSal__; i++)
       if(vetSalas__[i].capacidade_ > vetTamSal__[s])
         aux++;
     for(int p = 0; p < numPerTot__; p++) {
	   imp = 0;
       for(int c = 0; c < numDis__; c++)
         if(vetDisSps[c] != -1)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
             fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
             imp = 1;
           }
       if (imp)
         fprintf(f,"<= %d\n",aux); 
     } 
     fprintf(f,"\n"); 
   }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int d = 0; d < numDia__; d++){
         for(int q = 0; q < numPerDia__; q++)
           if(!matResDisPer__[c][(d*numPerDia__)+q])
             fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
         fprintf(f,"- z_%d_%d >= 0\n",c,d);
       }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1){
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"+ z_%d_%d ",c,d);
       fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
     }
   fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp){
       for(int p = 0; p < numPerTot__; p++){
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
         fprintf(f,"- r_%d_%d = 0\n",u,p);
       }
       fprintf(f,"\n"); 
     }
   fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp){
       for(int d = 0; d < numDia__; d++){ 
         fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
         for(int q = 1; q < numPerDia__-1; q++)
           fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
         fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
       }
       fprintf(f,"\n"); 
     }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++){
     for(int p = 0; p < numPerTot__; p++){
	   imp = 0;
       for(int c = 0; c < numDis__; c++)
         if(vetDisSps[c] != -1)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t)){
             fprintf(f,"+ x_%d_%d ",c,p);
             imp = 1;
           }
       if (imp)
		fprintf(f,"<= 1\n");
     }
     fprintf(f,"\n");
   }
   fprintf(f,"\nBOUNDS\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
   fprintf(f,"\n\\Variaveis y\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetDisSps[c] != -1)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
   fprintf(f,"\n\\Variaveis w\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
   fprintf(f,"\n\\Variaveis v\n");
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis z\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
   fprintf(f,"\n\\Variaveis r\n");
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
     
   fprintf(f,"\nBINARIES\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"x_%d_%d\n",c,p);
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetDisSps[c] != -1)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"y_%d_%d_%d\n",s,c,p);
   fprintf(f,"\n");   
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"r_%d_%d\n",u,p);
   fprintf(f,"\n");
         
   fprintf(f,"\n\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nEND");
   fclose(f);
   
 hf = omp_get_wtime();
 mr__.temSub_ += (hf-hi);
 
 free(vetTurDis);
}

//------------------------------------------------------------------------------
void monSpsDisNull(int sp)
{
 FILE *f;
 double hi, hf;
 char arq[50];
 int aux,flag, sp_u, imp, cont = 0;
 int vetAux[MAX_TUR];
 int vetTurDis[MAX_TUR];
 int vetAuxSps[MAX_SPS];
 int vetDisSps[MAX_DIS];
 hi = omp_get_wtime();
 
 memset(vetTurDis,0,sizeof(vetTurDis));
 memset(vetAux,-1,sizeof(vetAux));
 
 for(int u = 0; u < numTur__; u++){
   // Verifica se todas as disciplinas de um curriculo ficaram no mesmo subproblema... Se o curriculo foi "cortado" ele � ignorado (0)
   for(int i = 0; i < vetTurmas__[u].numDis_-1; i++)
     if (mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]] != mr__.vetSpDis_[vetTurmas__[u].vetDis_[i+1]])
       vetTurDis[u] = 1;
   // Se o curr�culo N�O foi "cortado" define o subproblema em que ele vai aparecer = subproblema de uma disciplina dele
   if (vetTurDis[u] == 0)
       vetAux[u] = mr__.vetSpDis_[vetTurmas__[u].vetDis_[0]];
   else{
	   memset(vetAuxSps,0,sizeof(vetAuxSps));
	   aux = 0;
	   for(int i = 0; i < vetTurmas__[u].numDis_; i++){
		   vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]]++;
		   if (vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]] > aux){
			   aux = vetAuxSps[mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]]];
			   sp_u = mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]];  
	       }  
       }
	   vetAux[u] = sp_u;
    }    
  }
    
   // ------------------ 
   memset(vetDisSps,-1,sizeof(vetDisSps));
   memset(vetVar,0,sizeof(vetVar));
   // Determina se a disciplina est� ou n�o (-1) no subproblema sp... e se � original (1) ou c�pia (2)
   for(int i = 0; i < numDis__; i++)
     if (mr__.vetSpDis_[i] == sp)
	   vetDisSps[i] = 1;  
   for(int u = 0; u < numTur__; u++)
	 if (vetAux[u] == sp)
	   for(int i = 0; i < vetTurmas__[u].numDis_; i++)
		   if (mr__.vetSpDis_[vetTurmas__[u].vetDis_[i]] != sp)
		       vetDisSps[vetTurmas__[u].vetDis_[i]] = 2;

   // ------------------   
   sprintf(arq,"sp%d.lp",sp+1);
   f = fopen(arq,"w");
   // ------------------ FO
   fprintf(f,"MIN\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1){
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p]){
           fprintf(f,"+ 0 x_%d_%d ",c,p);
           cont++;
         }
       fprintf(f,"\n"); 
     }
   fprintf(f,"\n\\Capacidade das salas\n");
   for(int p = 0; p < numPerTot__; p++){
     for(int s = 0; s < (numSalDif__-1); s++){
       for(int c = 0; c < numDis__; c++)
         if(vetDisSps[c] == 1){
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
             fprintf(f,"+ %d y_%d_%d_%d ",MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
             cont++;
           }
         }else if(vetDisSps[c] == 2){
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
             fprintf(f,"+ 0 y_%d_%d_%d ",s,c,p);
             cont++;
           }
         }
     }
     fprintf(f,"\n"); 
   }
   fprintf(f,"\n\\Dias minimos\n");
   for(int c = 0; c < numDis__; c++){
     if(vetDisSps[c] == 1){ 
       fprintf(f,"+ 5 w_%d ",c);
       cont++;
     }else if(vetDisSps[c] == 2){ 
       fprintf(f,"+ 0 w_%d ",c);
       cont++;
     }
   }
   fprintf(f,"\n\n\\Aulas isoladas\n");
   for(int u = 0; u < numTur__; u++){
     if(vetAux[u] == sp){
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"+ 2 v_%d_%d ",u,p); 
       fprintf(f,"\n"); 
     }
   }
   // ------------------ restri��es
   fprintf(f,"\nST\n");
   fprintf(f,"\n\\R1 - Numero de aulas\n");
   for(int c = 0; c < numDis__; c++){
	 imp = 0;
     if(vetDisSps[c] != -1){
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p]){
           fprintf(f,"+ x_%d_%d ",c,p);
           imp = 1;
         }
       if (imp) 
          fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
     } 
   }
   fprintf(f,"\n\\R2 - Numero de salas\n");
   for(int p = 0; p < numPerTot__; p++){
	 imp = 0;
     for(int c = 0; c < numDis__; c++){
       if(vetDisSps[c] != -1)         
         if(!matResDisPer__[c][p]){
           fprintf(f,"+ x_%d_%d ",c,p);
           imp = 1;
         } 
     }
     if (imp)
       fprintf(f,"<= %d\n",numSal__); 
   }
   fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
   for(int s = 0; s < (numSalDif__-1); s++){
     for(int c = 0; c < numDis__; c++){
       if(vetDisSps[c] != -1)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
     }
     fprintf(f,"\n"); 
   }
   fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
   for(int s = 0; s < (numSalDif__-1); s++){
     aux = 0;
     for(int i = 0; i < numSal__; i++)
       if(vetSalas__[i].capacidade_ > vetTamSal__[s])
         aux++;
     for(int p = 0; p < numPerTot__; p++) {
	   imp = 0;
       for(int c = 0; c < numDis__; c++)
         if(vetDisSps[c] != -1)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s])){
             fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
             imp = 1;
           }
       if (imp)
         fprintf(f,"<= %d\n",aux); 
     } 
     fprintf(f,"\n"); 
   }
   fprintf(f,"\n\\R5 - Dias minimos 1\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int d = 0; d < numDia__; d++){
         for(int q = 0; q < numPerDia__; q++)
           if(!matResDisPer__[c][(d*numPerDia__)+q])
             fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
         fprintf(f,"- z_%d_%d >= 0\n",c,d);
       }
   fprintf(f,"\n\\R6 - Dias minimos 2\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1){
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"+ z_%d_%d ",c,d);
       fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
     }
   fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp){
       for(int p = 0; p < numPerTot__; p++){
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
             fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
         fprintf(f,"- r_%d_%d = 0\n",u,p);
       }
       fprintf(f,"\n"); 
     }
   fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp){
       for(int d = 0; d < numDia__; d++){ 
         fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
         for(int q = 1; q < numPerDia__-1; q++)
           fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
         fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
       }
       fprintf(f,"\n"); 
     }
   fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
   for(int t = 0; t < numPro__; t++){
     for(int p = 0; p < numPerTot__; p++){
	   imp = 0;
       for(int c = 0; c < numDis__; c++)
         if(vetDisSps[c] != -1)
           if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t)){
             fprintf(f,"+ x_%d_%d ",c,p);
             imp = 1;
           }
       if (imp)
		fprintf(f,"<= 1\n");
     }
     fprintf(f,"\n");
   }
   fprintf(f,"\nBOUNDS\n");
   fprintf(f,"\n\\Variaveis x\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
   fprintf(f,"\n\\Variaveis y\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetDisSps[c] != -1)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
   fprintf(f,"\n\\Variaveis w\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
   fprintf(f,"\n\\Variaveis v\n");
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
   fprintf(f,"\n\\Variaveis z\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
   fprintf(f,"\n\\Variaveis r\n");
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
     
   fprintf(f,"\nBINARIES\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"x_%d_%d\n",c,p);
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetDisSps[c] != -1)
         if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
           for(int p = 0; p < numPerTot__; p++)
             if(!matResDisPer__[c][p])
               fprintf(f,"y_%d_%d_%d\n",s,c,p);
   fprintf(f,"\n");   
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       for(int d = 0; d < numDia__; d++)
         fprintf(f,"z_%d_%d\n",c,d);
   for(int u = 0; u < numTur__; u++)
     if(vetAux[u] == sp)
       for(int p = 0; p < numPerTot__; p++)
         fprintf(f,"r_%d_%d\n",u,p);
   fprintf(f,"\n");
         
   fprintf(f,"\n\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     if(vetDisSps[c] != -1)
       fprintf(f,"w_%d\n",c);
   fprintf(f,"\nEND");
   fclose(f);
   
 hf = omp_get_wtime();
 mr__.temSub_ += (hf-hi);
}

//------------------------------------------------------------------------------
void parGraDis(const int &sps,const int &fat,const int &pes)
{
 double hi, hf;
 int aux,flag;
 int areCor,pesAre;
 int matAdj[MAX_DIS][MAX_DIS];
 hi = omp_get_wtime();
 mr__.numSps_ = MIN(sps,numDis__);
 mr__.fatDes_ = fat;
 memset(mr__.vetSpDis_,-1,sizeof(mr__.vetSpDis_));
 memset(matAdj,0,sizeof(matAdj));
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numDis__; j++)
     if(j != i)
       for(int k = 0; k < numTur__; k++)
         if(matDisTur__[i][k] && matDisTur__[j][k])
           matAdj[i][j]++;
 //-------------------------
 // todas as disciplinas em um �nico subproblema
 if(mr__.numSps_ <= 1)
   memset(mr__.vetSpDis_,0,sizeof(mr__.vetSpDis_));
 //-------------------------
 // cada disciplina em um subproblema
 else if(mr__.numSps_ == numDis__){
   for(int i = 0; i < numDis__; i++)
     mr__.vetSpDis_[i] = i;
 }
 //-------------------------
 // particionamento usando a METIS
 else{
   int pos,are;
   idx_t *vetPrt;    // vetor de identifica��o das parti��es
   idx_t *vetNumAdj; // vetor com o n�mero de adjac�ncias de cada disciplina
   idx_t *vetDisAdj; // vetor com as disciplinas adjacentes de cada disciplina
   idx_t *vetPesAre; // vetor com o peso das arestas
   int wgtFlag = 0;  // flag para indicar a utilizacao ou nao de pesos (0: sem pesos)
   int numFlag = 0;  // flag para indicar o estilo dos contadores (comeca em 0: 0)
   idx_t *opts = new idx_t[METIS_NOPTIONS];
   METIS_SetDefaultOptions(opts);
   opts[METIS_OPTION_UFACTOR] = mr__.fatDes_;
   idx_t *nCon = new idx_t[1];
   nCon[0] = 1;
   vetPrt = new idx_t[numDis__];
   vetNumAdj = new idx_t[numDis__+1];
   vetDisAdj = new idx_t[numDis__*numDis__];
   vetPesAre = new idx_t[numDis__*numDis__];
   pos = 0;
   vetNumAdj[0] = 0;
   for(int i = 0; i < numDis__; i++){
     are = 0;
     for(int j = 0; j < numDis__; j++){
       if(j != i){
         if(matAdj[i][j] > 0){
           vetDisAdj[pos] = j;
           vetPesAre[pos] = matAdj[i][j];
           pos++;
           are++;
         }
       }
     }
     vetNumAdj[i+1] = vetNumAdj[i] + are;
   } 
   idx_t aux1 = numDis__;
   idx_t aux2 = mr__.numSps_;
   idx_t aux3; 
   if(pes)
      METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetDisAdj,NULL,NULL,vetPesAre,&aux2,NULL,NULL,opts,&aux3,vetPrt);   
   else
      METIS_PartGraphRecursive(&aux1,nCon,vetNumAdj,vetDisAdj,NULL,NULL,NULL,&aux2,NULL,NULL,opts,&aux3,vetPrt);
   mr__.numCtsM_ = aux3;
   for(int i = 0; i < numDis__; i++)
     mr__.vetSpDis_[i] = vetPrt[i];
   delete[] vetPrt;
   delete[] vetNumAdj;
   delete[] vetDisAdj;
   delete[] vetPesAre;
   delete[] opts;
   delete[] nCon;
  }
 mr__.numCts_ = mr__.pesCts_ = 0;
 if(mr__.numSps_ > 1)
   for(int i = 0; i < (numDis__-1); i++)
     for(int j = i+1; j < numDis__; j++)
       if(mr__.vetSpDis_[j] != mr__.vetSpDis_[i]){
         mr__.numCts_ += MIN(1,matAdj[i][j]);
         mr__.pesCts_ += matAdj[i][j];
       } 
 hf = omp_get_wtime();
 mr__.temPrt_ = hf-hi;
}
//------------------------------------------------------------------------------

//==============================================================================


//=================================== CPLEX ====================================

//------------------------------------------------------------------------------
void exeCpx(Solucao &s,char *arq,const int &rl)
{
 int sts;
 double hi, hf;
 #ifdef DBG
   CPXsetintparam(env__,CPX_PARAM_SCRIND,CPX_ON);
 #endif
 vetCpx__[0].lp_ = CPXcreateprob(env__,&sts,"");
 CPXreadcopyprob(env__,vetCpx__[0].lp_,arq,NULL);
 vetCpx__[0].numVar_ = CPXgetnumcols(env__,vetCpx__[0].lp_);
 vetCpx__[0].numRes_ = CPXgetnumrows(env__,vetCpx__[0].lp_);
 hi = omp_get_wtime();
 if(rl)
   CPXprimopt(env__,vetCpx__[0].lp_);
 else
   CPXmipopt(env__,vetCpx__[0].lp_);
 hf = omp_get_wtime();
 vetCpx__[0].tempo_ = hf-hi;
 if(rl)
   CPXgetobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limInf_);
 else
  {
   CPXgetmipobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limSup_);
   CPXgetbestobjval(env__,vetCpx__[0].lp_,&vetCpx__[0].limInf_);
   vetCpx__[0].gap_ = INT_MAX;
   if(vetCpx__[0].limInf_ != 0)
     vetCpx__[0].gap_ = ((vetCpx__[0].limSup_ - vetCpx__[0].limInf_)/vetCpx__[0].limSup_)*100;
   #ifdef MOD_BUR
     CPXgetmipx(env__,vetCpx__[0].lp_,s.vetSol_,0,(numPerTot__*numSal__*numDis__-1));
   #else
     CPXgetmipx(env__,vetCpx__[0].lp_,s.vetSol_,0,(numPerTot__*numDis__-1));
   #endif
  }
 CPXfreeprob(env__,&vetCpx__[0].lp_);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void escResCpx(FILE *f,const int &rl)
{
 if(f == NULL)
   f = stdout;
 fprintf(f,"\n< ----------------------------------- CPLEX ---------------------------------- >\n");
 #ifdef MOD_BUR
   fprintf(f,"Modelo utilizado...............................................: Burke et al. (2010)\n");
 #else
   fprintf(f,"Modelo utilizado...............................................: Lach e Lubbecke (2012)\n");
 #endif
 fprintf(f,"Numero de variaveis............................................: %d\n",vetCpx__[0].numVar_);
 fprintf(f,"Numero de restricoes...........................................: %d\n",vetCpx__[0].numRes_);
 if(rl)
   fprintf(f,"Valor do melhor no (limitante inferior)........................: %.2f\n",vetCpx__[0].limInf_);
 else
  {
   fprintf(f,"Valor da solucao (limitante superior)..........................: %.2f\n",vetCpx__[0].limSup_);
   fprintf(f,"Valor do melhor no (limitante inferior)........................: %.2f\n",vetCpx__[0].limInf_);
   fprintf(f,"GAP............................................................: %.2f\n",vetCpx__[0].gap_);
  }
 fprintf(f,"Tempo (segundos)...............................................: %.2f\n",vetCpx__[0].tempo_);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monModBurEal10(char *arq,const int &rl)
{
 int val;
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 fprintf(f,"\n\\Capacidade das salas\n");
 for(int r = 0; r < numSal__; r++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
      if(vetDisciplinas__[c].numAlu_ > vetSalas__[r].capacidade_)
        fprintf(f,"+ %d x_%d_%d_%d ",PESOS[0]*(vetDisciplinas__[c].numAlu_ - vetSalas__[r].capacidade_),p,r,c);
      else
        fprintf(f,"+ 0 x_%d_%d_%d ",p,r,c);
     fprintf(f,"\n");
    }
  }
 fprintf(f,"\n\n\\Janelas de horarios\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int s = 0; s < numPerDia__; s++)
        fprintf(f,"+ %d z_%d_%d_%d ",PESOS[1],u,d,s);
     fprintf(f,"\n");
    }
  }
 fprintf(f,"\n\\Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"+ %d q_%d ",PESOS[2],c);
 fprintf(f,"\n\n\\Salas diferentes\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int r = 0; r < numSal__; r++)
     fprintf(f,"+ %d y_%d_%d ",PESOS[3],r,c);
   fprintf(f,"\n");
  }
 val = PESOS[3] * numDis__;
 fprintf(f,"- val\n");
 // ------------------ restri��es
 fprintf(f,"\nST\n");
 fprintf(f,"\n\\Valor constante\n");
 fprintf(f,"val = %d\n",val);
 fprintf(f,"\n\\ ------------------------------------ HARD------------------------------------\n");
 fprintf(f,"\n\\R1 - Numero de aulas\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     for(int r = 0; r < numSal__; r++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
    fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_);
  }
 fprintf(f,"\n\\R2 - Aulas na mesma sala no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int r = 0; r < numSal__; r++)
    {
     for(int c = 0; c < numDis__; c++)
      fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R3 - Aulas de uma disciplina no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int c = 0; c < numDis__; c++)
    {
     for(int r = 0; r < numSal__; r++)
      fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R4 - Aulas de um professor no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int t = 0; t < numPro__; t++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
         if(vetDisciplinas__[c].professor_ == t)
           fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R5 - Aulas de uma turma no mesmo periodo\n");
 for(int p = 0; p < numPerTot__; p++)
   for(int u = 0; u < numTur__; u++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int c = 0; c < numDis__; c++)
        {
         for(int k = 0; k < vetTurmas__[u].numDis_; k++)
           if(vetTurmas__[u].vetDis_[k] == c)
            {
             fprintf(f,"+ x_%d_%d_%d ",p,r,c);
             break;
            }
        }
     fprintf(f,"<= 1\n");
    }
 fprintf(f,"\n\\R6 - Restricoes de oferta (alocacao)\n");
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(matResDisPer__[c][p])
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
       fprintf(f,"= 0\n");
      }
 fprintf(f,"\n\\ ------------------------------------ SOFT------------------------------------\n");
 fprintf(f,"\n\\R7 - Numero de salas usadas por disciplina\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
      {
       for(int r = 0; r < numSal__; r++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
       fprintf(f,"- v_%d_%d <= 0\n",d,c);
      }
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R8 - Numero de salas usadas por disciplina\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int r = 0; r < numSal__; r++)
       for(int p = (d*numPerDia__); p < (d*numPerDia__)+numPerDia__; p++)
         fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"- v_%d_%d >= 0\n",d,c);
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R9 - Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"+ v_%d_%d ",d,c);
   fprintf(f,"+ q_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
  }
 fprintf(f,"\n\\R10 a R13+#PER_DIA - Janelas no horario das turmas\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int c = 0; c < numDis__; c++)
       if(matDisTur__[c][u] == 1)
        {
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__),r,c,(d*numPerDia__)+1,r,c);
        }
     fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,0);
    }
  }
 fprintf(f,"\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int c = 0; c < numDis__; c++)
       if(matDisTur__[c][u] == 1)
        {
         for(int r = 0; r < numSal__; r++)
           fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+numPerDia__-1,r,c,(d*numPerDia__)+numPerDia__-2,r,c);
        }
     fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,1);
    }
  }
 fprintf(f,"\n");
 for(int s = 2; s < numPerDia__; s++)
  {
   for(int u = 0; u < numTur__; u++)
    {
     for(int d = 0; d < numDia__; d++)
      {
       for(int c = 0; c < numDis__; c++)
         if(matDisTur__[c][u] == 1)
          {
           for(int r = 0; r < numSal__; r++)
             fprintf(f,"+ x_%d_%d_%d - x_%d_%d_%d - x_%d_%d_%d ",(d*numPerDia__)+s-1,r,c,(d*numPerDia__)+s-2,r,c,(d*numPerDia__)+s,r,c);
          }
       fprintf(f,"- z_%d_%d_%d <= 0\n",u,d,s);
      }
    }
  fprintf(f,"\n");
 }
 fprintf(f,"\n\\R14 - Salas utilizadas por disciplina\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int r = 0; r < numSal__; r++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"x_%d_%d_%d - y_%d_%d <= 0\n",p,r,c,r,c);
   fprintf(f,"\n");
  }
 fprintf(f,"\n\\R15 - Salas utilizadas por disciplina\n");
 for(int r = 0; r < numSal__; r++)
  {
   for(int c = 0; c < numDis__; c++)
    {
     for(int p = 0; p < numPerTot__; p++)
       fprintf(f,"+ x_%d_%d_%d ",p,r,c);
     fprintf(f,"- y_%d_%d >= 0\n",r,c);
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Variaveis x\n");
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerTot__; p++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"0 <= x_%d_%d_%d <= 1\n",p,r,c);
 fprintf(f,"\n\\Variaveis v\n");
 for(int d = 0; d < numDia__; d++)
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"0 <= v_%d_%d <= 1\n",d,c);
 fprintf(f,"\n\\Variaveis q\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"0 <= q_%d <= %d\n",c,numDia__);
 fprintf(f,"\n\\Variaveis z\n");
 for(int u = 0; u < numTur__; u++)
   for(int d = 0; d < numDia__; d++)
     for(int s = 0; s < numPerDia__; s++)
        fprintf(f,"0 <= z_%d_%d_%d <= 1\n",u,d,s);
 fprintf(f,"\n\\Variaveis y\n");
 for(int r = 0; r < numSal__; r++)
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"0 <= y_%d_%d <= 1\n",r,c);
 if(!rl)
  {
   fprintf(f,"\nGENERALS\n");
   fprintf(f,"\n\\Variaveis q\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"q_%d\n",c);
   fprintf(f,"\nBINARIES\n");
   for(int r = 0; r < numSal__; r++)
     for(int p = 0; p < numPerTot__; p++)
       for(int c = 0; c < numDis__; c++)
         fprintf(f,"x_%d_%d_%d\n",p,r,c);
   fprintf(f,"\n\\Variaveis v\n");
   for(int d = 0; d < numDia__; d++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"v_%d_%d\n",d,c);
   fprintf(f,"\n\\Variaveis z\n");
   for(int u = 0; u < numTur__; u++)
     for(int d = 0; d < numDia__; d++)
       for(int s = 0; s < numPerDia__; s++)
          fprintf(f,"z_%d_%d_%d\n",u,d,s);
   fprintf(f,"\n\\Variaveis y\n");
   for(int r = 0; r < numSal__; r++)
     for(int c = 0; c < numDis__; c++)
       fprintf(f,"y_%d_%d\n",r,c);
  }
 fprintf(f,"\nEND");
 fclose(f);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void monModLacLub12(char *arq,const int &rl)
{
 int val;
 FILE *f = fopen(arq,"w");
 // ------------------ FO
 fprintf(f,"MIN\n");
 fprintf(f,"\n\\Variaveis x\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ 0 x_%d_%d ",c,p);
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\Capacidade das salas\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int s = 0; s < (numSalDif__-1); s++)
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
         fprintf(f,"+ %d y_%d_%d_%d ",MIN(vetDisciplinas__[c].numAlu_ - vetTamSal__[s],vetTamSal__[s+1]-vetTamSal__[s]),s,c,p);
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\Dias minimos\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"+ 5 w_%d ",c); 
 fprintf(f,"\n\n\\Aulas isoladas\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"+ 2 v_%d_%d ",u,p); 
   fprintf(f,"\n"); 
  }
 // ------------------ restri��es
 fprintf(f,"\nST\n");
 fprintf(f,"\n\\R1 - Numero de aulas\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ x_%d_%d ",c,p); 
   fprintf(f,"= %d\n",vetDisciplinas__[c].numPer_); 
  }
 fprintf(f,"\n\\R2 - Numero de salas\n");
 for(int p = 0; p < numPerTot__; p++)
  {
   for(int c = 0; c < numDis__; c++)
     if(!matResDisPer__[c][p])
       fprintf(f,"+ x_%d_%d ",c,p); 
   fprintf(f,"<= %d\n",numSal__); 
  }
 fprintf(f,"\n\\R3 - Capacidade das salas 1\n");
 for(int s = 0; s < (numSalDif__-1); s++)
  {
   for(int c = 0; c < numDis__; c++)
     if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"x_%d_%d - y_%d_%d_%d >= 0\n",c,p,s,c,p);
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R4 - Capacidade das salas 2\n");
 for(int s = 0; s < (numSalDif__-1); s++)
  {
   val = 0;
   for(int i = 0; i < numSal__; i++)
     if(vetSalas__[i].capacidade_ > vetTamSal__[s])
       val++;
   for(int p = 0; p < numPerTot__; p++) 
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].numAlu_ > vetTamSal__[s]))
         fprintf(f,"+ x_%d_%d - y_%d_%d_%d ",c,p,s,c,p);
     fprintf(f,"<= %d\n",val); 
    } 
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R5 - Dias minimos 1\n");
 for(int c = 0; c < numDis__; c++)
   for(int d = 0; d < numDia__; d++)
    {
     for(int q = 0; q < numPerDia__; q++)
       if(!matResDisPer__[c][(d*numPerDia__)+q])
         fprintf(f,"+ x_%d_%d ",c,(d*numPerDia__)+q);
     fprintf(f,"- z_%d_%d >= 0\n",c,d);
    }
 
 fprintf(f,"\n\\R6 - Dias minimos 2\n");
 for(int c = 0; c < numDis__; c++)
  {
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"+ z_%d_%d ",c,d);
   fprintf(f,"+ w_%d >= %d\n",c,vetDisciplinas__[c].diaMin_);
  }
 fprintf(f,"\n\\R7 - Aulas isoladas 1\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int k = 0; k < vetTurmas__[u].numDis_; k++)
       if(!matResDisPer__[vetTurmas__[u].vetDis_[k]][p])
         fprintf(f,"+ x_%d_%d ",vetTurmas__[u].vetDis_[k],p);
     fprintf(f,"- r_%d_%d = 0\n",u,p);
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R8 - Aulas isoladas 2\n");
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     fprintf(f,"+ r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__),u,(d*numPerDia__)+1,u,(d*numPerDia__));
     for(int q = 1; q < numPerDia__-1; q++)
       fprintf(f,"- r_%d_%d + r_%d_%d - r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+q-1,u,(d*numPerDia__)+q,u,(d*numPerDia__)+q+1,u,(d*numPerDia__)+q);
     fprintf(f,"- r_%d_%d + r_%d_%d - v_%d_%d <= 0\n",u,(d*numPerDia__)+numPerDia__-2,u,(d*numPerDia__)+numPerDia__-1,u,(d*numPerDia__)+numPerDia__-1);
    }
   fprintf(f,"\n"); 
  }
 fprintf(f,"\n\\R9 - Aulas de um professor no mesmo periodo\n");
 for(int t = 0; t < numPro__; t++)
  {
   for(int p = 0; p < numPerTot__; p++)
    {
     for(int c = 0; c < numDis__; c++)
       if((!matResDisPer__[c][p]) && (vetDisciplinas__[c].professor_ == t))
         fprintf(f,"+ x_%d_%d ",c,p);
     fprintf(f,"<= 1\n");
    }
   fprintf(f,"\n");
  }
 fprintf(f,"\nBOUNDS\n");
 fprintf(f,"\n\\Variaveis w\n");
 for(int c = 0; c < numDis__; c++)
   fprintf(f,"0 <= w_%d <= %d\n",c,vetDisciplinas__[c].diaMin_);
 fprintf(f,"\n\\Variaveis r\n");
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"0 <= r_%d_%d <= 1\n",u,p);
 fprintf(f,"\n\\Variaveis v\n");
 for(int u = 0; u < numTur__; u++)
   for(int p = 0; p < numPerTot__; p++)
     fprintf(f,"0 <= v_%d_%d <= 1\n",u,p);
 fprintf(f,"\n\\Variaveis x\n");
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
       fprintf(f,"0 <= x_%d_%d <= 1\n",c,p);
 fprintf(f,"\n\\Variaveis y\n");
 for(int s = 0; s < (numSalDif__-1); s++)
   for(int c = 0; c < numDis__; c++)
     if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
       for(int p = 0; p < numPerTot__; p++)
         if(!matResDisPer__[c][p])
           fprintf(f,"0 <= y_%d_%d_%d <= 1\n",s,c,p);
 fprintf(f,"\n\\Variaveis z\n");
 for(int c = 0; c < numDis__; c++)
   for(int d = 0; d < numDia__; d++)
     fprintf(f,"0 <= z_%d_%d <= 1\n",c,d);
 if(!rl)
 {
   fprintf(f,"\n\nGENERALS\n");
   for(int c = 0; c < numDis__; c++)
     fprintf(f,"w_%d\n",c);
   fprintf(f,"\nBINARIES\n");
   for(int u = 0; u < numTur__; u++)
     for(int p = 0; p < numPerTot__; p++)
       fprintf(f,"r_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int u = 0; u < numTur__; u++)
     for(int p = 0; p < numPerTot__; p++)
       fprintf(f,"v_%d_%d\n",u,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     for(int p = 0; p < numPerTot__; p++)
       if(!matResDisPer__[c][p])
         fprintf(f,"x_%d_%d\n",c,p);
   fprintf(f,"\n");
   for(int s = 0; s < (numSalDif__-1); s++)
     for(int c = 0; c < numDis__; c++)
       if(vetDisciplinas__[c].numAlu_ > vetTamSal__[s])
         for(int p = 0; p < numPerTot__; p++)
           if(!matResDisPer__[c][p])
             fprintf(f,"y_%d_%d_%d\n",s,c,p);
   fprintf(f,"\n");
   for(int c = 0; c < numDis__; c++)
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"z_%d_%d\n",c,d);
 }
 fprintf(f,"\nEND");
 fclose(f);
}
//------------------------------------------------------------------------------

//==============================================================================


//================================== SOLU��O ===================================

//------------------------------------------------------------------------------
void escSolBurEal10(Solucao &s,FILE *f)
{
 int pos,dia,per,aux;
 if(f == NULL)
   f = stdout;
 fprintf(f,"\n< ---------------------------------- SOLUCAO --------------------------------- >\n");
 fprintf(f,"Instancia......................................................: %s\n\n",nomInst__);
 //------------------------------------------------------------- 
 // montar a solucao
 memset(s.matSolSal_,-1,sizeof(s.matSolSal_));
 memset(s.matSolTur_,-1,sizeof(s.matSolTur_));
 pos = 0;
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerTot__; p++)
     for(int c = 0; c < numDis__; c++)
       {
        if(s.vetSol_[pos] > 0)
         {
          dia = p / numPerDia__;
          per = p % numPerDia__;
          fprintf(f,"x_%d_%d_%d = %.2f\n",p,r,c,s.vetSol_[pos]);
          if(s.matSolSal_[per][dia][r] != -1)
           {
            printf("\n\nERRO - SALA %d- p%d d%d c%d!\n\n",r,per,dia,c);
           }
          else
            s.matSolSal_[per][dia][r] = c;
          for(int u = 0; u < numTur__; u++)
           {
            if(matDisTur__[c][u] == 1)
             {
              if(s.matSolTur_[per][dia][u] != -1)
               {
                printf("\n\nERRO - TURMA %d - p%d d%d c%d!\n\n",u,per,dia,c);
               }
              else
                s.matSolTur_[per][dia][u] = c;
             }
           }
         }
        pos++;
       } 
 fprintf(f,"\n");
 for(int r = 0; r < numSal__; r++)
  {
   fprintf(f,"SALA %d\n",r);
   for(int p = 0; p < numPerDia__; p++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"%d  ",s.matSolSal_[p][d][r]);
     fprintf(f,"\n");
    }
   fprintf(f,"\n");
  } 
 fprintf(f,"\n");
 for(int u = 0; u < numTur__; u++)
  {
   fprintf(f,"TURMA %d\n",u);
   for(int p = 0; p < numPerDia__; p++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"%d  ",s.matSolTur_[p][d][u]);
     fprintf(f,"\n");
    }
   fprintf(f,"\n");
  } 
 //------------------------------------------------------------- 
 // verificar a solucao
 s.vioNumAul_ = 0;
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int r = 0; r < numSal__; r++)
    {
     for(int p = 0; p < numPerDia__; p++)
       for(int d = 0; d < numDia__; d++)
         if(s.matSolSal_[p][d][r] == c)
           aux++;
    }    
   s.vioNumAul_ += abs(vetDisciplinas__[c].numPer_ - aux);
  }
 s.vioAulSim_ = 0;
 s.vioDisSim_ = 0;
 s.vioProSim_ = 0;
 s.vioTurSim_ = 0;
 for(int r = 0; r < numSal__; r++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     for(int p = 0; p < numPerDia__; p++)
      {
       if(s.matSolSal_[p][d][r] != -1)
         for(int r2 = r+1; r2 < numSal__; r2++)
          {
           if(s.matSolSal_[p][d][r2] != -1)
            {
             if(s.matSolSal_[p][d][r2] == s.matSolSal_[p][d][r])
               s.vioDisSim_++;
             if(vetDisciplinas__[s.matSolSal_[p][d][r2]].professor_ == vetDisciplinas__[s.matSolSal_[p][d][r]].professor_)
               s.vioProSim_++;
             for(int u = 0; u < numTur__; u++)
               if((matDisTur__[s.matSolSal_[p][d][r2]][u] == 1) && (matDisTur__[s.matSolSal_[p][d][r]][u] == 1))
                 s.vioTurSim_++;
            }
          }
      } 
    }
  }
 s.capSal_ = 0;
 for(int r = 0; r < numSal__; r++)
   for(int p = 0; p < numPerDia__; p++)
     for(int d = 0; d < numDia__; d++)
       {
        if(s.matSolSal_[p][d][r] != -1)
          if(vetDisciplinas__[s.matSolSal_[p][d][r]].numAlu_ > vetSalas__[r].capacidade_)
            s.capSal_ += (vetDisciplinas__[s.matSolSal_[p][d][r]].numAlu_ - vetSalas__[r].capacidade_);
       } 
 s.janHor_ = 0;
 for(int u = 0; u < numTur__; u++)
  {
   for(int d = 0; d < numDia__; d++)
    {
     if((s.matSolTur_[0][d][u] != -1) && (s.matSolTur_[1][d][u] == -1))
       s.janHor_++;
     if((s.matSolTur_[numPerDia__-1][d][u] != -1) && (s.matSolTur_[numPerDia__-2][d][u] == -1))
       s.janHor_++;
     for(int p = 2; p < numPerDia__; p++)
       if((s.matSolTur_[p-1][d][u] != -1) && (s.matSolTur_[p-2][d][u] == -1) && (s.matSolTur_[p][d][u] == -1))
         s.janHor_++;
    }
  }       
 s.diaMin_ = 0;
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int d = 0; d < numDia__; d++)
    {
     pos = 0;
     for(int p = 0; p < numPerDia__; p++)
      {
       for(int r = 0; r < numSal__; r++)
        {
         if(s.matSolSal_[p][d][r] == c)
          {
           pos = 1;
           break;
          }
        } 
       if(pos == 1)
         break;
      } 
     aux += pos;
    }
   if(aux < vetDisciplinas__[c].diaMin_)
     s.diaMin_++;
  }
 s.salDif_ = 0;
 for(int c = 0; c < numDis__; c++)
  {
   aux = 0;
   for(int r = 0; r < numSal__; r++)
    {
     pos = 0;
     for(int d = 0; d < numDia__; d++)
      {
       for(int p = 0; p < numPerDia__; p++)
        {
         if(s.matSolSal_[p][d][r] == c)
          {
           pos = 1;
           break;
          }
        } 
       if(pos == 1)
         break;
      } 
     aux += pos;
    }
   s.salDif_ += aux - 1;
  }
 s.funObj_ = PESOS[0] * s.capSal_ + PESOS[1] * s.janHor_ + PESOS[2] * s.diaMin_ + PESOS[3] * s.salDif_;   
 fprintf(f,"\n\n>>> RESULTADOS CALCULADOS\n\n");
 fprintf(f,"Func. obj.....: %d\n",s.funObj_);
 fprintf(f,"\n\nHARD----------------------------------\n\n");
 fprintf(f,"Num. aulas....: %d\n",s.vioNumAul_);
 fprintf(f,"Aulas simul...: %d\n",s.vioAulSim_);
 fprintf(f,"Disc. simul...: %d\n",s.vioDisSim_);
 fprintf(f,"Prof. simul...: %d\n",s.vioProSim_);
 fprintf(f,"Turm. simul...: %d\n",s.vioTurSim_);
 fprintf(f,"\n\nSOFT------------------------------------\n\n");
 fprintf(f,"Cap. salas....: %d\n",s.capSal_);
 fprintf(f,"Jan. turmas...: %d\n",s.janHor_);
 fprintf(f,"Dias minimo...: %d\n",s.diaMin_);
 fprintf(f,"Salas difer...: %d\n",s.salDif_);
 fclose(f);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void escSolLacLub12(Solucao &s,FILE *f)
{
 int pos,dia,per,aux;
 if(f == NULL)
   f = stdout;
 fprintf(f,"\n< ---------------------------------- SOLUCAO --------------------------------- >\n");
 fprintf(f,"Instancia......................................................: %s\n\n",nomInst__);
 //------------------------------------------------------------- 
 // montar a solucao
 memset(s.matSolTur_,-1,sizeof(s.matSolTur_));
 pos = 0;
 for(int c = 0; c < numDis__; c++)
   for(int p = 0; p < numPerTot__; p++)
     if(!matResDisPer__[c][p])
      {
       if(s.vetSol_[pos] > 0)
        {
         dia = p / numPerDia__;
         per = p % numPerDia__;
         fprintf(f,"x_%d_%d = %.2f\n",c,p,s.vetSol_[pos]);
         for(int u = 0; u < numTur__; u++)
          {
           if(matDisTur__[c][u] == 1)
            {
             if(s.matSolTur_[per][dia][u] != -1)
              {
               printf("\n\nERRO - TURMA %d - p%d d%d c%d!\n\n",u,per,dia,c);
              }
             else
               s.matSolTur_[per][dia][u] = c;
            }
          }
        } 
       pos++;
      }
 fprintf(f,"\n");
 for(int u = 0; u < numTur__; u++)
  {
   fprintf(f,"TURMA %d\n",u);
   for(int p = 0; p < numPerDia__; p++)
    {
     for(int d = 0; d < numDia__; d++)
       fprintf(f,"%d  ",s.matSolTur_[p][d][u]);
     fprintf(f,"\n");
    }
   fprintf(f,"\n");
  } 
 //------------------------------------------------------------- 
 // verificar a solucao
 s.vioNumAul_ = 0;
 s.diaMin_ = 0;
 for(int c = 0; c < numDis__; c++)
  {
   for(int u = 0; u < numTur__; u++)
     if(matDisTur__[c][u])
      {
       aux = 0;
       for(int p = 0; p < numPerDia__; p++)
         for(int d = 0; d < numDia__; d++)
           if(s.matSolTur_[p][d][u] == c)
             aux++;
       s.vioNumAul_ += abs(vetDisciplinas__[c].numPer_ - aux);    

       aux = 0;
       for(int d = 0; d < numDia__; d++)
        {
         for(int p = 0; p < numPerDia__; p++)
           if(s.matSolTur_[p][d][u] == c)
            {
             aux++;
             break;
            }
        } 
       s.diaMin_ += MAX(0,vetDisciplinas__[c].diaMin_-aux);
       break;
      }
  }
 s.vioProSim_ = 0;
 s.janHor_ = 0;
 for(int u = 0; u < numTur__; u++)
   for(int d = 0; d < numDia__; d++)
    {
     for(int p = 0; p < numPerDia__; p++)
       if(s.matSolTur_[p][d][u] != -1)
         for(int u2 = u+1; u2 < numTur__; u2++)
           if(s.matSolTur_[p][d][u2] != -1)
             if((s.matSolTur_[p][d][u2] != s.matSolTur_[p][d][u]) && (vetDisciplinas__[s.matSolTur_[p][d][u2]].professor_ == vetDisciplinas__[s.matSolTur_[p][d][u]].professor_))
               s.vioProSim_++;
  
     if((s.matSolTur_[0][d][u] != -1) && (s.matSolTur_[1][d][u] == -1))
       s.janHor_++;
     if((s.matSolTur_[numPerDia__-1][d][u] != -1) && (s.matSolTur_[numPerDia__-2][d][u] == -1))
       s.janHor_++;
     for(int p = 2; p < numPerDia__; p++)
       if((s.matSolTur_[p-1][d][u] != -1) && (s.matSolTur_[p-2][d][u] == -1) && (s.matSolTur_[p][d][u] == -1))
         s.janHor_++;
    } 
 s.capSal_ = 0;
 s.salDif_ = 0;
 s.funObj_ = PESOS[0] * s.capSal_ + PESOS[1] * s.janHor_ + PESOS[2] * s.diaMin_ + PESOS[3] * s.salDif_;   
 fprintf(f,"\n\n>>> RESULTADOS CALCULADOS\n\n");
 fprintf(f,"Func. obj.....: %d\n",s.funObj_);
 fprintf(f,"\n\nHARD----------------------------------\n\n");
 fprintf(f,"Num. aulas....: %d\n",s.vioNumAul_);
 fprintf(f,"Aulas simul...: -\n");
 fprintf(f,"Disc. simul...: -\n");
 fprintf(f,"Prof. simul...: %d\n",s.vioProSim_);
 fprintf(f,"Turm. simul...: -\n");
 fprintf(f,"\n\nSOFT------------------------------------\n\n");
 fprintf(f,"Cap. salas....: -\n");
 fprintf(f,"Jan. turmas...: %d\n",s.janHor_);
 fprintf(f,"Dias minimo...: %d\n",s.diaMin_);
 fprintf(f,"Salas difer...: -\n");
 fclose(f);
}
//------------------------------------------------------------------------------

//==============================================================================


//================================= AUXILIARES =================================

//------------------------------------------------------------------------------
void lerInstancia(char *arq)
{
 int pos,per;
 char aux[50];
 FILE *f = fopen(arq,"r");
 fscanf(f,"Name: %s\n",&nomInst__);
 fscanf(f,"Courses: %d\n",&numDis__);
 fscanf(f,"Rooms: %d\n",&numSal__);
 fscanf(f,"Days: %d\n",&numDia__);
 fscanf(f,"Periods_per_day: %d\n",&numPerDia__);
 fscanf(f,"Curricula: %d\n",&numTur__);
 fscanf(f,"Constraints: %d\n",&numRes__);
 fscanf(f,"\nCOURSES:\n");
 numPerTot__ = numDia__ * numPerDia__;
 numPro__ = 0;
 for(int i = 0; i < numDis__; i++)
  {
   fscanf(f,"%s %s %d %d %d\n",&vetDisciplinas__[i].nome_,&aux,
          &vetDisciplinas__[i].numPer_,&vetDisciplinas__[i].diaMin_,&vetDisciplinas__[i].numAlu_);
   pos = -1;
   for(int p = 0; p < numPro__; p++)
     if(strcmp(aux,vetProfessores__[p].nome_) == 0)
      {
       vetDisciplinas__[i].professor_ = p;
       pos = p;
       break;
      }
   if(pos == -1)
    {
     vetDisciplinas__[i].professor_ = numPro__;
     strcpy(vetProfessores__[numPro__].nome_,aux);
     numPro__++;
    }
  }
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numTur__; j++)
     matDisTur__[i][j] = 0;
 fscanf(f,"\nROOMS:\n");
 for(int i = 0; i < numSal__; i++)
   fscanf(f,"%s %d\n",&vetSalas__[i].nome_,&vetSalas__[i].capacidade_);
 fscanf(f,"\nCURRICULA:\n");
 for(int i = 0; i < numTur__; i++)
  {
   fscanf(f,"%s %d",&vetTurmas__[i].nome_,&vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
    {
     fscanf(f, "%s ",&aux);
     vetTurmas__[i].vetDis_[j] = -1;
     for(int k = 0; k < numDis__; k++)
       if(strcmp(aux,vetDisciplinas__[k].nome_) == 0)
        {
         vetTurmas__[i].vetDis_[j] = k;
         matDisTur__[k][i] = 1;
         break;
        }
    }
  }
 fscanf(f,"\nUNAVAILABILITY_CONSTRAINTS:\n");
 memset(matResDisPer__,0,sizeof(matResDisPer__));
 for(int i = 0; i < numRes__; i++) 
  {
   fscanf(f,"%s %d %d\n",&aux,&pos,&per);
   for(int j = 0; j < numDis__; j++)
     if(strcmp(aux,vetDisciplinas__[j].nome_) == 0)
      {
       matResDisPer__[j][(pos * numPerDia__) + per] = 1;
       break;
      }
  }
 fclose(f);
 //---------------
 // preeencher e ordenar o vetor com as diferentes capacidades de sala
 ordVetCapSal();
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void testarEntrada()
{
 int aux;

 printf("Name: %s\n",nomInst__);
 printf("Courses: %d\n",numDis__);
 printf("Rooms: %d\n",numSal__);
 printf("Days: %d\n",numDia__);
 printf("Periods_per_day: %d\n",numPerDia__);
 printf("Curricula: %d\n",numTur__);
 printf("Constraints: %d\n",numRes__);

 printf("\nDISCIPLINAS:\n");
 for(int i = 0; i < numDis__; i++)
   printf("%d  %s  %d  %d  %d  %d\n",i,vetDisciplinas__[i].nome_,vetDisciplinas__[i].professor_,vetDisciplinas__[i].numPer_,vetDisciplinas__[i].diaMin_,vetDisciplinas__[i].numAlu_);

 printf("\nTURMAS:\n");
 for(int i = 0; i < numTur__; i++)
  {
   printf("%d  %s  %d --> ",i,vetTurmas__[i].nome_,vetTurmas__[i].numDis_);
   for(int j = 0; j < vetTurmas__[i].numDis_; j++)
     printf("%d  ",vetTurmas__[i].vetDis_[j]);
   printf("\n");
  } 

 printf("\nPROFESSORES:\n");
 for(int i = 0; i < numPro__; i++)
   printf("%d  %s\n",i,vetProfessores__[i].nome_);

 printf("\nSALAS:\n");
 for(int i = 0; i < numSal__; i++)
   printf("%d  %s  %d\n",i,vetSalas__[i].nome_,vetSalas__[i].capacidade_);

 aux = 0;
 printf("\nRESTRICOES:\n");
 for(int i = 0; i < numDis__; i++)
   for(int j = 0; j < numPerTot__; j++)
     if(matResDisPer__[i][j])
      {
       printf("%d  %d  %d\n",aux,i,j);
       aux++;
      } 

 printf("\nDISC x TURMA:\n");
 for(int i = 0; i < numDis__; i++)
  {
   for(int j = 0; j < numTur__; j++)
     printf("%d  ",matDisTur__[i][j]);
   printf("\n");
  }
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void ordVetCapSal()
{
 int aux,flag;
 numSalDif__ = 0;
 memset(vetTamSal__,0,sizeof(vetTamSal__));
 for(int i = 0; i < numSal__; i++)
  {
   flag = 0;
   for(int j = 0; j < numSalDif__; j++)
     if(vetSalas__[i].capacidade_ == vetTamSal__[j])
      {
       flag = 1; 
       break;
      }
   if(!flag)
    {
     vetTamSal__[numSalDif__] = vetSalas__[i].capacidade_;
     numSalDif__++; 
    }
  }
 flag = 1;
 while(flag)
  {
   flag = 0;
   for(int i = 0; i < numSalDif__-1; i++)
     if(vetTamSal__[i] > vetTamSal__[i+1])
      {
       aux = vetTamSal__[i];
       vetTamSal__[i] = vetTamSal__[i+1];
       vetTamSal__[i+1] = aux;
       flag = 1;
      }
  }
}
//------------------------------------------------------------------------------
void configuraParametros(int piNParam, char **piParams){
	
	for (int i = 2; i < piNParam; i++){
		if (strncmp("facDes", piParams[i], 6) == 0){
			facDes = atoi(piParams[i] + 7);
		} else if (strncmp("numSubProb", piParams[i], 10) == 0){
			numSubProb = atoi(piParams[i] + 11);
		} else if (strncmp("comPeso", piParams[i], 7) == 0){
			comPeso = atoi(piParams[i] + 8);
		} else if (strncmp("tGrafo", piParams[i], 6) == 0){
			tGrafo = atoi(piParams[i] + 7);
		} else if (strncmp("tempLim", piParams[i], 7) == 0){
			tempLim = atoi(piParams[i] + 8);
		} else if (strncmp("tMetodo", piParams[i], 7) == 0){
			tMetodo = atoi(piParams[i] + 8);
		}		
	}
}
//------------------------------------------------------------------------------

//==============================================================================
