#!/bin/bash
reset

#Informações colhidas nos testes:
#Seed
#fo da construção
#tempo da construção
#fo da busca local
#tempo da busca local
#número de iteracoes da busca local
#número de movimentos realizados
#número de movimentos tentados
#número de movimentos tentados de cada tipo
#número total de iterações GRASP
#tempo total


cd src
mkdir -p bin
make
cd ../src

mkdir -p ../results
mkdir -p ../tests
mkdir -p ../tests/summaryperinstance

seeds=( 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71)
#seeds=( 31 37 41 43 47 )
instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )
#instances=( DDS1 DDS2 DDS3 DDS4 DDS5 DDS6 DDS7 EA01 EA02 EA03 EA04 EA05 EA06 EA07 EA08 EA09 EA10 EA11 EA12 )
#instances=( Udine1 Udine2 Udine3 Udine4 Udine5 Udine6 Udine7 Udine8 Udine9 UUMCAS_A131 )
#instances=( comp01 comp02 comp05 comp07 comp11 comp12 comp14 comp17 comp20 )
testes=( __1 )

title="$instance"
for teste in ${testes[@]} ; do
	echo "testing $teste..."
	#Cria o diretório onde vão ficar os resultados
	mkdir -p ../tests/$teste

	#Executa as instâncias para todas as seeds
	for instance in ${instances[@]} ; do
		  echo "Instancia $instance"
			instancia=../instances/$instance.ctt

			#for seed in ${seeds[@]} ; do
			for ((seed=1;seed<=1;seed++));do
				#echo "Seed $seed"
				resultado=../results/$teste.$instance.$seed.res
				saida=../tests/$teste/$instance-$seed.dat

				#Testing Contruction Algoritms
				if [ $teste = __1 ]; then
					./grasp $instancia $resultado bl=sa viz=10 timeout=3600 const=0 t0=17.3 tFinal=0.003 beta=0.9999 gc=1 > $saida
				elif [ $teste = __2 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=440 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __3 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=880 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __4 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=1320 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __5 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=1760 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __6 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=2200 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __7 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=13.7 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __8 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=440 const=0 grafico=1 t0=13.7 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __9 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=880 const=0 grafico=1 t0=13.7 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __10 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=1320 const=0 grafico=1 t0=13.7 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __11 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=1760 const=0 grafico=1 t0=13.7 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = __12 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=2200 const=0 grafico=1 t0=13.7 tFinal=0.003 beta=0.9999 > $saida
				else
				  echo "Invalid test ($teste)"
				fi

			done
			cat ../tests/$teste/$instance-* > ../tests/summaryperinstance/$teste-$instance.dat
	done
done

echo "done"
